package pe.bbvacontinental.fu.dao;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbCanal;

public interface CanalDAO extends IOperations<TbCanal> {
	public TbCanal findOne(byte idCanal);
}
