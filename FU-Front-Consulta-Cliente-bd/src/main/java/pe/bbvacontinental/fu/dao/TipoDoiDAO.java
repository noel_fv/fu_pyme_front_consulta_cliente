package pe.bbvacontinental.fu.dao;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoDoi;

public interface TipoDoiDAO extends IOperations<TbTipoDoi> {
	public List<TbTipoDoi> listaTipoDocumentos();

	public TbTipoDoi buscarPorValor(String valor);
}
