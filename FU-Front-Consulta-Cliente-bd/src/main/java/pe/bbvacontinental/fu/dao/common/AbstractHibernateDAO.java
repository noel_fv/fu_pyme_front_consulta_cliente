package pe.bbvacontinental.fu.dao.common;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

@Transactional(value = "transactionManager")
public abstract class AbstractHibernateDAO<T extends Serializable> implements IOperations<T>  {

    private Class<T> clazz;

    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    private static final String FIELD_FECHA_MODIFICACION = "fechaModificacion";
    private static final String FIELD_FECHA_CREACION = "fechaCreacion";
    private static final String FROM="from ";
    // API

    protected final void setClazz(final Class<T> clazzToSet) {
        clazz = Preconditions.checkNotNull(clazzToSet);
    }

    @Override
    public final T findOne(final long id) {
        return (T) getCurrentSession().get(clazz, id);
    }

    @Override
    public final List<T> findAll() {
        return getCurrentSession().createQuery(FROM + clazz.getName()).list();
    }
    
    @Override
    public final List<T> findAllActive() {
    	
        return getCurrentSession().createQuery(FROM + clazz.getName()+" where estado=1").list();
    }
    
    @Override
    public final List<T> findAllActivePNatural() {
    	
        return getCurrentSession().createQuery(FROM + clazz.getName()+" where visibilidad=1").list();
    }

    @Override
    @Transactional
    public final void create(final T entity) {
        Preconditions.checkNotNull(entity);
        getCurrentSession().saveOrUpdate(setAttributeEntity(entity, AbstractHibernateDAO.FIELD_FECHA_CREACION, new Date()));
    }
    
    
    @Override
    @Transactional
    public final void insertAll(final List<T> list) {
        Preconditions.checkNotNull(list);

        for(T entity:list){
        	getCurrentSession().saveOrUpdate(setAttributeEntity(entity, AbstractHibernateDAO.FIELD_FECHA_CREACION, new Date()));
        	getCurrentSession().flush();
        	getCurrentSession().clear();
        }
        
        
    }

    @Override
    @Transactional
    public final T update(final T entity) {
        Preconditions.checkNotNull(entity);
        return (T) getCurrentSession().merge(setAttributeEntity(entity, AbstractHibernateDAO.FIELD_FECHA_MODIFICACION, new Date()));
    }

    @Override
    public final void delete(final T entity) {
        Preconditions.checkNotNull(entity);
        getCurrentSession().delete(entity);
    }

    @Override
    public final void deleteById(final long entityId) {
        final T entity = findOne(entityId);
        Preconditions.checkState(entity != null);
        delete(entity);
    }

    protected final Session getCurrentSession() {
 
        return sessionFactory.getCurrentSession();
    }

    
	private T setAttributeEntity(T entity, String nombreAtributo, Object valorAtributo){
    	try {
        	
			Field fechaModificacion = clazz.getDeclaredField(nombreAtributo);
			boolean accessible = fechaModificacion.isAccessible();
			Preconditions.checkNotNull(fechaModificacion);
			fechaModificacion.setAccessible(true);
			fechaModificacion.set(entity, valorAtributo);
			fechaModificacion.setAccessible(accessible);
			
		} catch (Exception e) {
			/* */
		}
		
    	
    	return entity;
		
    }
}
