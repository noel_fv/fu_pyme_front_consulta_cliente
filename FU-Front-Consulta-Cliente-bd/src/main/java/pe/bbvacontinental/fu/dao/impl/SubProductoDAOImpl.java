package pe.bbvacontinental.fu.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.SubProductoDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbSubProducto;


@Repository
public class SubProductoDAOImpl extends AbstractHibernateDAO<TbSubProducto> implements SubProductoDAO {

	public static final String COD_PRODUCTO_WP="codProductoWP";
	
	public SubProductoDAOImpl() {
		super();
		setClazz(TbSubProducto.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<TbSubProducto> findSubProductoById(Long codProducto){
		Query query = getCurrentSession().createQuery("from TbSubProducto prod where prod.codProducto = :codProducto ");
		query.setParameter("codProducto", codProducto);
			
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public TbSubProducto buscarPorValor(String valor) {
		Query query = getCurrentSession().createQuery("from TbSubProducto subProd where subProd.valor = :valor ");
		query.setParameter("valor", valor);
		List<TbSubProducto> list = query.list();
		return (list.isEmpty() ? null : list.get(0));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TbSubProducto> findByCodigoProductoWPFlujoRegular(String codProductoWP) {

		String sql = "from TbSubProducto prod where prod.codigoProductoWP = :codProductoWP and prod.estadoFlujoRegular = 1 and prod.estado = 1";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(COD_PRODUCTO_WP, codProductoWP);
			
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TbSubProducto> findByCodigoProductoWPEstado(String codProductoWP) {

		String sql = "from TbSubProducto prod where prod.codigoProductoWP = :codProductoWP and prod.estado = 1";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(COD_PRODUCTO_WP, codProductoWP);

		return query.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<TbSubProducto> getSubProductoByCodes(List<String> codigos){
		
		Query query = getCurrentSession().createQuery("from TbSubProducto tbSubProducto where tbSubProducto.codigoProductoWP in (:codigos) and estado=1 ");
		query.setParameterList("codigos", codigos);

		return query.list();

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampCalc(
			String codProductoWP) {
		String sql = "from TbSubProducto prod where prod.codigoProductoWP = :codProductoWP and prod.estadoFlujoCampanhaCalculadora = 1 and prod.estado = 1";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(COD_PRODUCTO_WP, codProductoWP);
			
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampAprob(
			String codProductoWP) {
		String sql = "from TbSubProducto prod where prod.codigoProductoWP = :codProductoWP and prod.estadoFlujoCampanhaAprobada = 1 and prod.estado = 1";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(COD_PRODUCTO_WP, codProductoWP);

		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public TbSubProducto findByValorWP(String valorWP) {
		Query query = getCurrentSession().createQuery("from TbSubProducto subProd where subProd.valorWp = :valorWP ");
		query.setParameter("valorWP", valorWP);
		List<TbSubProducto> list = query.list();
		return (list.isEmpty() ? null : list.get(0));
	}
}
