package pe.bbvacontinental.fu.dao.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.SolicitudDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbSolicitud;


@Repository
public class SolicitudDAOImpl  extends AbstractHibernateDAO<TbSolicitud>  implements SolicitudDAO{

	public SolicitudDAOImpl(){
		super();
		setClazz(TbSolicitud.class);
	}
	
	public TbSolicitud findSolicitudByCode(String codSolicitud){
		Query query = getCurrentSession().createQuery("from TbSolicitud sol where sol.codSolicitud = :codSolicitud");
		query.setParameter("codSolicitud", codSolicitud);
			
		return (TbSolicitud) query.uniqueResult();
	}

}
