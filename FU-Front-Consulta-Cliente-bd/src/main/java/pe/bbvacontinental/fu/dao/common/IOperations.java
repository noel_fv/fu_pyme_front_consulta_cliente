package pe.bbvacontinental.fu.dao.common;

import java.io.Serializable;
import java.util.List;

public interface IOperations<T extends Serializable> {

    T findOne(final long id);

    List<T> findAll();
    
    List<T> findAllActive();

    void create(final T entity);

    T update(final T entity);

    void delete(final T entity);

    void deleteById(final long entityId);

	void insertAll(List<T> list);
	
	 List<T> findAllActivePNatural();
	
}
