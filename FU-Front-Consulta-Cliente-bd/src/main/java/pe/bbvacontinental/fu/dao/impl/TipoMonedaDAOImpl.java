package pe.bbvacontinental.fu.dao.impl;

import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.TipoMonedaDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbTipoMoneda;

@Repository
public class TipoMonedaDAOImpl extends AbstractHibernateDAO<TbTipoMoneda> implements TipoMonedaDAO{

	public TipoMonedaDAOImpl() {
		super();
		setClazz(TbTipoMoneda.class);
	}
	


}
