package pe.bbvacontinental.fu.dao;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbGiroNegocio;

public interface GiroNegocioDAO extends IOperations<TbGiroNegocio> {

	public TbGiroNegocio buscarPorCodigo(String codigoGiro);
}
