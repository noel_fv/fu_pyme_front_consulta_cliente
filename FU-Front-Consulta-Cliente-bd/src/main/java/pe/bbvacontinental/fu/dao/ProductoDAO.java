package pe.bbvacontinental.fu.dao;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbProducto;

public interface ProductoDAO extends IOperations<TbProducto> {

	public TbProducto getProducto(String codigo);
	
	public List<TbProducto> getProductoByCodes(List<String> codigos);
	
	public List<Object[]> findByEstadoFlujoRegular(String esquema);
	
	public List<Object[]> findByEstadoFlujoCampCalc(String esquema);
	
	public List<Object[]> findByEstadoFlujoCampAprob(String esquema);
	
	public List<Object[]> findByEstado(String esquema);

}
