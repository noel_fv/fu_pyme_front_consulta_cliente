package pe.bbvacontinental.fu.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.TipoDoiDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbTipoDoi;

@Repository
public class TipoDoiDAOImpl extends AbstractHibernateDAO<TbTipoDoi> implements TipoDoiDAO {

	public TipoDoiDAOImpl() {
		super();
		setClazz(TbTipoDoi.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TbTipoDoi> listaTipoDocumentos() {
		Query query = getCurrentSession().createQuery("from TbTipoDoi tipoDocumento where tipoDocumento.estado = 1 order by decode(tipoDocumento.valor,'R',1,'L',2,'E',3,'P',4) asc, tipoDocumento.nombre asc");
		return query.list();
	}

	@Override
	public TbTipoDoi buscarPorValor(String valor) {
		Query query = getCurrentSession().createQuery("from TbTipoDoi tipoDocumento where tipoDocumento.estado = 1 and tipoDocumento.valor = :valor");
		query.setParameter("valor", valor);
		return (TbTipoDoi) query.uniqueResult();
	}

}
