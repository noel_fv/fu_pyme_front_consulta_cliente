package pe.bbvacontinental.fu.dao.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.TipoPersonaDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbTipoPersona;

@Repository
public class TipoPersonaDAOImpl extends AbstractHibernateDAO<TbTipoPersona> implements TipoPersonaDAO {
	public TipoPersonaDAOImpl() {
		super();
		setClazz(TbTipoPersona.class);
	}

	@Override
	public TbTipoPersona findByCodigo(String codigo) {
		
		String sql = "from TbTipoPersona tipoPersona where tipoPersona.estado = 1 and tipoPersona.codigo = :codigo";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter("codigo", codigo);
		
		return (TbTipoPersona) query.uniqueResult();
	}
}
