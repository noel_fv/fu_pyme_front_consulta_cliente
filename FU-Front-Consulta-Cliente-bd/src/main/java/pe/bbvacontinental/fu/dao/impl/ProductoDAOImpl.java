package pe.bbvacontinental.fu.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;
import pe.bbvacontinental.fu.dao.ProductoDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbProducto;

@Repository
public class ProductoDAOImpl extends AbstractHibernateDAO<TbProducto> implements ProductoDAO{

	public ProductoDAOImpl() {
		super();
		setClazz(TbProducto.class);
	}

	@SuppressWarnings("unchecked")
	public TbProducto getProducto(String codigo){
		
		Query query = getCurrentSession().createQuery("from TbProducto tbProducto where tbProducto.valor = :valor and estado=1 ");
		query.setParameter("valor", codigo);
		List<TbProducto> list = query.list();
		
		return (list.isEmpty() ? null : list.get(0));

	}  
	
	@SuppressWarnings("unchecked")
	public List<TbProducto> getProductoByCodes(List<String> codigos){
		
		Query query = getCurrentSession().createQuery("from TbProducto tbProducto where tbProducto.valor in (:codigos) and estado=1 ");
		query.setParameterList("codigos", codigos);
		return query.list();

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> findByEstadoFlujoRegular(String esquema) {
		
		/* JRomero - 03/05/2018 */
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT DISTINCT SUBPRODUCTO.COD_PRODUCTO_WP, SUBPRODUCTO.DESCRIPCION_PRODUCTO_WP FROM ");
		sql.append(esquema);
		sql.append(".MAE_PJ_SUB_PRODUCTO SUBPRODUCTO WHERE SUBPRODUCTO.ESTADO_FLUJO_REGULAR = 1  AND SUBPRODUCTO.ESTADO = 1");
		
		Query query = getCurrentSession().createSQLQuery(sql.toString());		
		
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> findByEstado(String esquema) {
		/* JRomero - 03/05/2018 */
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT DISTINCT SUBPRODUCTO.COD_PRODUCTO_WP, SUBPRODUCTO.DESCRIPCION_PRODUCTO_WP FROM ");
		sql.append(esquema);
		sql.append(".MAE_PJ_SUB_PRODUCTO SUBPRODUCTO WHERE SUBPRODUCTO.ESTADO = 1");
		
		Query query = getCurrentSession().createSQLQuery(sql.toString());		
		
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> findByEstadoFlujoCampCalc(String esquema){
		/* JRomero - 03/05/2018 */
		try{
			StringBuilder sql=new StringBuilder();
			sql.append("SELECT DISTINCT SUBPRODUCTO.COD_PRODUCTO_WP, SUBPRODUCTO.DESCRIPCION_PRODUCTO_WP FROM");
			sql.append(esquema);
			sql.append(".MAE_PJ_SUB_PRODUCTO SUBPRODUCTO WHERE SUBPRODUCTO.ESTADO_FLUJO_CAMP_CALCU = 1  AND SUBPRODUCTO.ESTADO = 1");
			
			Query query = getCurrentSession().createSQLQuery(sql.toString());		
		
			return query.list();
		}catch(Exception e){
			throw new ExcepcionBBVA(e, "ProductoDAOImpl.findByEstadoFlujoCampCalc");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> findByEstadoFlujoCampAprob(String esquema) {
		/* JRomero - 03/05/2018 */
		try{
			StringBuilder sql=new StringBuilder();
			sql.append("SELECT DISTINCT SUBPRODUCTO.COD_PRODUCTO_WP, SUBPRODUCTO.DESCRIPCION_PRODUCTO_WP FROM");
			sql.append(esquema);
			sql.append(".MAE_PJ_SUB_PRODUCTO SUBPRODUCTO WHERE SUBPRODUCTO.ESTADO_FLUJO_CAMP_APROB= 1  AND SUBPRODUCTO.ESTADO = 1");
			
			Query query = getCurrentSession().createSQLQuery(sql.toString());		
			
			return query.list();
		}catch(Exception e){
			throw new ExcepcionBBVA(e, "ProductoDAOImpl.findByEstadoFlujoCampAprob");
		}
	} 

}
