package pe.bbvacontinental.fu.dao;

import java.util.List;

import java.util.concurrent.ConcurrentMap;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;

public interface ParametroAplicativoDAO extends IOperations<TbParametrosAplicativo> {

	public TbParametrosAplicativo getParameterByCode(String codigo);

	public List<TbParametrosAplicativo> getListParameterByType(String tipo);

	public TbParametrosAplicativo getParameterByCodeAndType(String codigo, String tipo);

	public void actualizarCacheLocal() ;

	public void infoCacheLocal();

	public ConcurrentMap<Long, TbParametrosAplicativo> getCache();

}
