package pe.bbvacontinental.fu.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.GiroNegocioDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbGiroNegocio;

@Repository
public class GiroNegocioDAOImpl extends AbstractHibernateDAO<TbGiroNegocio> implements GiroNegocioDAO {

	public GiroNegocioDAOImpl() {
		super();
		setClazz(TbGiroNegocio.class);
	}
	
	@Override
	public TbGiroNegocio buscarPorCodigo(String codigoGiro){
		Query query = getCurrentSession().createQuery("from TbGiroNegocio giroNegocio where giroNegocio.codigoGiroNegFu = :codigoGiro and giroNegocio.estado = 1");
		query.setParameter("codigoGiro", codigoGiro);
		List<TbGiroNegocio> list = query.list();
		return (list.isEmpty() ? null : list.get(0));
	}
}
