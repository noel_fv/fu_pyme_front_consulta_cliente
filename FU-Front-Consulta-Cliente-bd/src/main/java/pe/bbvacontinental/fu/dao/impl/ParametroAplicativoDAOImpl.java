package pe.bbvacontinental.fu.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;

@Repository
public class ParametroAplicativoDAOImpl extends AbstractHibernateDAO<TbParametrosAplicativo> implements ParametroAplicativoDAO {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	protected static final ConcurrentHashMap<Long, TbParametrosAplicativo> cacheLocal = new ConcurrentHashMap<>();

	public ParametroAplicativoDAOImpl() {
		super();
		setClazz(TbParametrosAplicativo.class);
	}

	public TbParametrosAplicativo getParameterByCode(String codigo) {

		TbParametrosAplicativo tmp = null;
		for (Map.Entry<Long, TbParametrosAplicativo> entry : getCache().entrySet()) {
			if (entry.getValue().getCodigo() != null && entry.getValue().getCodigo().equalsIgnoreCase(codigo)) {
				tmp = entry.getValue();
				break;
			}
		}
		return tmp;
	}

	public List<TbParametrosAplicativo> getListParameterByType(String tipo) {

		List<TbParametrosAplicativo> tmp = new ArrayList<>();
		for (Map.Entry<Long, TbParametrosAplicativo> entry : getCache().entrySet()) {
			if (entry.getValue().getTipoParametro() != null && entry.getValue().getTipoParametro().equalsIgnoreCase(tipo)) {
				tmp.add(entry.getValue());
			}
		}
		return tmp;
	}

	@Override
	public TbParametrosAplicativo getParameterByCodeAndType(String codigo, String tipo) {

		TbParametrosAplicativo tmp = null;
		for (Map.Entry<Long, TbParametrosAplicativo> entry : getCache().entrySet()) {
			if (entry.getValue().getCodigo() != null && entry.getValue().getTipoParametro() != null && entry.getValue().getCodigo().equalsIgnoreCase(codigo)
					&& entry.getValue().getTipoParametro().equalsIgnoreCase(tipo) && entry.getValue().getEstado()) {
				tmp = entry.getValue();
				break;
			}
		}
		return tmp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actualizarCacheLocal() {
		Query query = getCurrentSession().createQuery("select t from TbParametrosAplicativo t");
		List<TbParametrosAplicativo> listParametros = (List<TbParametrosAplicativo>) query.list();
		for (TbParametrosAplicativo parametro : listParametros) {
			cacheLocal.put(parametro.getIdParametrosAplicativo(), parametro);
		}

	}

	@Override
	public void infoCacheLocal() {
		StringBuilder cad=null;
		for (Map.Entry<Long, TbParametrosAplicativo> entry : cacheLocal.entrySet()) {
			cad=new StringBuilder();
			cad.append("KEY: ");
			cad.append(entry.getKey().toString());
			cad.append(" | VALUE: ");
			cad.append(entry.getValue().toString());
			
			logger.info("message {}",cad);
		}
	}

	@Override
	public ConcurrentMap<Long, TbParametrosAplicativo> getCache() {
		return cacheLocal;
	}
}
