package pe.bbvacontinental.fu.dao.impl;

import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.RegimentTributarioDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbRegimenTributario;


@Repository
public class RegimentTributarioDAOImpl extends AbstractHibernateDAO<TbRegimenTributario> implements RegimentTributarioDAO{

	public RegimentTributarioDAOImpl() {
		super();
		setClazz(TbRegimenTributario.class);
	}
	
	

}
