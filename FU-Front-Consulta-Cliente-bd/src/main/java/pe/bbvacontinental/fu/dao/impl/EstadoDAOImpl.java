package pe.bbvacontinental.fu.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.EstadoDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbEstado;

@Repository
public class EstadoDAOImpl extends AbstractHibernateDAO<TbEstado> implements EstadoDAO {

	public EstadoDAOImpl() {
		super();
		setClazz(TbEstado.class);
	}

	@SuppressWarnings("unchecked")
	public TbEstado buscarPorValor(String valor) {
		Query query = getCurrentSession().createQuery("from TbEstado tbEstado where tbEstado.valor = :valor and tbEstado.estado = 1");
		query.setParameter("valor", valor);
		List<TbEstado> list = query.list();
		return (list.isEmpty() ? null : list.get(0));
	}
}
