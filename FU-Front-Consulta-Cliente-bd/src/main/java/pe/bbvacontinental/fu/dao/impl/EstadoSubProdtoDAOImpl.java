package pe.bbvacontinental.fu.dao.impl;

import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.EstadoSubProdtoDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbEstadoSubProdto;

@Repository
public class EstadoSubProdtoDAOImpl extends AbstractHibernateDAO<TbEstadoSubProdto> implements EstadoSubProdtoDAO{

	
	public EstadoSubProdtoDAOImpl() {
		super();
		setClazz(TbEstadoSubProdto.class);
	}
	

	
}
