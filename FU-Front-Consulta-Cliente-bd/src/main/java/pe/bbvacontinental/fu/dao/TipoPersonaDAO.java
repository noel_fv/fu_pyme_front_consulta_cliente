package pe.bbvacontinental.fu.dao;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoPersona;

public interface TipoPersonaDAO extends IOperations<TbTipoPersona> {
	
	public TbTipoPersona findByCodigo(String codigo);
	
}
