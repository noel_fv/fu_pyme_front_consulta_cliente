package pe.bbvacontinental.fu.dao;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSolicitdSubProduct;

public interface SolicitudSubProductDAO extends IOperations<TbSolicitdSubProduct> {

	public List<TbSolicitdSubProduct> listarSubProductPorIdSolicitudProPuestaOficina(Long idSolicitud);
	
	public List<TbSolicitdSubProduct> listarSubProductPorIdSolicitudProPuestaRiesgos(Long idSolicitud);
	
	public List<TbSolicitdSubProduct> listarSolicitudSubProductoByIds(List<Long> listIdSolicitudSubProducto);
	
	public void eliminarPorSolicitud(long idSolicitud, String esquema);

	public List<TbSolicitdSubProduct> listaSubProductosPorIdSolicitud(Long idSolicitud);
}
