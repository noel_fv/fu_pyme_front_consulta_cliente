package pe.bbvacontinental.fu.dao;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbEstado;

public interface EstadoDAO extends IOperations<TbEstado> {
	public TbEstado buscarPorValor(String valor);
}
