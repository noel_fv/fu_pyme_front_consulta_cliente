package pe.bbvacontinental.fu.dao;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSubProducto;

public interface SubProductoDAO extends IOperations<TbSubProducto>{
	
	public List<TbSubProducto> findSubProductoById(Long idProducto);
	
	public TbSubProducto buscarPorValor(String valor);
	
	public List<TbSubProducto> findByCodigoProductoWPFlujoRegular(String codProductoWP);
	
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampCalc(String codProductoWP);
	
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampAprob(String codProductoWP);
	
	public List<TbSubProducto> findByCodigoProductoWPEstado(String codProductoWP);
	
	public List<TbSubProducto> getSubProductoByCodes(List<String> codigos);
	
	public TbSubProducto findByValorWP(String valorWP);

}
