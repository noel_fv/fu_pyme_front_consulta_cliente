package pe.bbvacontinental.fu.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.SolicitudSubProductDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbSolicitdSubProduct;

@Repository
public class SolicitudSubProductDAOImpl extends AbstractHibernateDAO<TbSolicitdSubProduct> implements SolicitudSubProductDAO {
	
	private static final String ID_SOL="idSolicitud";
	
	public SolicitudSubProductDAOImpl() {
		super();
		setClazz(TbSolicitdSubProduct.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TbSolicitdSubProduct> listarSubProductPorIdSolicitudProPuestaOficina(Long idSolicitud) {
		Query query = getCurrentSession().createQuery("from TbSolicitdSubProduct solicitudSubProd where solicitudSubProd.tbSolicitud.idSolicitud = :idSolicitud and esPropuestaOficina=1");
		query.setParameter(ID_SOL, idSolicitud);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<TbSolicitdSubProduct> listarSubProductPorIdSolicitudProPuestaRiesgos(Long idSolicitud) {
		Query query = getCurrentSession().createQuery("from TbSolicitdSubProduct solicitudSubProd where solicitudSubProd.tbSolicitud.idSolicitud = :idSolicitud and esPropuestaRiesgos=1");
		query.setParameter(ID_SOL, idSolicitud);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<TbSolicitdSubProduct> listarSolicitudSubProductoByIds(List<Long> listIdSolicitudSubProducto){

		Query query = getCurrentSession().createQuery("from TbSolicitdSubProduct tb where tb.idSolcitudSubPro in (:idSolcitudSubPro)");
		query.setParameterList("idSolcitudSubPro", listIdSolicitudSubProducto);
		return query.list();
		
		
	}
	
	public void eliminarPorSolicitud(long idSolicitud, String esquema){

		StringBuilder sql=new StringBuilder();
		sql.append("delete from ");
		sql.append(esquema);
		sql.append(".det_pj_soli_sub_prto solisubprod where solisubprod.ID_DET_PJ_SOLICITUD = ");
		sql.append(idSolicitud);
		
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.executeUpdate();	
	}
	
	@SuppressWarnings("unchecked")
	public List<TbSolicitdSubProduct> listaSubProductosPorIdSolicitud(Long idSolicitud) {
		Query query = getCurrentSession().createQuery("from TbSolicitdSubProduct tbSolicitdSubProd "
														+ "where tbSolicitdSubProd.tbSolicitud.idSolicitud = :idSolicitud "
														+ "and tbSolicitdSubProd.estado = 1");
		query.setParameter(ID_SOL, idSolicitud);

		return query.list();
	}	
}
