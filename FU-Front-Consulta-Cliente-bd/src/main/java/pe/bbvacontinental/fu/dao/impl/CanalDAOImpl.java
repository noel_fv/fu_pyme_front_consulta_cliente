package pe.bbvacontinental.fu.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.bbvacontinental.fu.dao.CanalDAO;
import pe.bbvacontinental.fu.dao.common.AbstractHibernateDAO;
import pe.bbvacontinental.fu.model.TbCanal;

@Repository
public class CanalDAOImpl extends AbstractHibernateDAO<TbCanal> implements CanalDAO {

	public CanalDAOImpl() {
		super();
		setClazz(TbCanal.class);
	}

	@SuppressWarnings("unchecked")
	public TbCanal findOne(byte idCanal) {
		Query query = getCurrentSession().createQuery("from TbCanal tbCanal where tbCanal.idCanal = :idCanal");
		query.setParameter("idCanal", idCanal);
		List<TbCanal> list = query.list();
		return (list.isEmpty() ? null : list.get(0));
	}
}
