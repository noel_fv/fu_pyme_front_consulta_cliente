package pe.bbvacontinental.fu.dao;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSolicitud;

public interface SolicitudDAO extends IOperations<TbSolicitud> {

	public TbSolicitud findSolicitudByCode(String numeroSolicitud);
	
}
