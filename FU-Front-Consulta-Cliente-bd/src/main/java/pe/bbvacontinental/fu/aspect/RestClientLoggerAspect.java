package pe.bbvacontinental.fu.aspect;


import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RestClientLoggerAspect {
	protected final Logger logger = LogManager.getLogger(getClass());
	public static final Level LOGGER_SMC = Level.forName("LOGGER_SMC", 350);

	protected static String prefijoCodigoErrorServicioRest = "mensaje.servicio.rest.error.";
	
	@Pointcut("execution(* pe.grupobbva.rest.client.*.impl.*(..))")
	protected void obtenerLlamada() {
		logger.log(LOGGER_SMC, "RestClientLoggerAspect.obtenrLlamada");
	}

	@Before(value = "obtenerLlamada() && args(parametro)")
	public void obtenerLlamadaBefore(JoinPoint joinPoint, Map<Object, Object> parametro) {
		
		logger.log(LOGGER_SMC, joinPoint.getSignature().getDeclaringType().getSimpleName() +
				"." + joinPoint.getSignature().getName() + " (envio): "
				 + "");
	}

	@AfterReturning(pointcut = "obtenerLlamada()", returning = "resultado")
	public void obtenerLlamadaAfterReturning(JoinPoint joinPoint, Object resultado) {
		logger.log(LOGGER_SMC, joinPoint.getSignature().getDeclaringType().getSimpleName() + "." + joinPoint.getSignature().getName()
				 + " (retorno): ");
	}

	@AfterThrowing(pointcut = "obtenerLlamada()", throwing = "e")
	public void obtenerLlamadaAfterThrowing(JoinPoint joinPoint, Throwable e) {
		logger.log(LOGGER_SMC, "RestClientLoggerAspect.obtenerLlamadaAfterThrowing");
	}
}
