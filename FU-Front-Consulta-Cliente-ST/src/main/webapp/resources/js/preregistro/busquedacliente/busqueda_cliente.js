var contextPath = $("#contextPath").val();

$(document).ready(function() {
	
	comboTipoDocumentoIdentidad();
	comboTipoPersona();

	$('#codigoCentral').keyup(function(e){
		if (/\D/g.test(this.value)){
			// Filter non-digits from input value.
			this.value = this.value.replace(/\D/g, '');
		}
	});
	
	$("#btnBuscarCliente").click(function(){
		if(validarBuscar()){
			buscarCliente();
		}
	});
	
	$("#btnSiguiente").attr("disabled", true);
	
    $("#codigoCentral").on("input",function(e){
        if($(this).value !=""){
           limpiarDatosCliente();
           $('#datosCliente').hide();
           $('#numeroDocumento').val("");
           $('#tipoDocumentoIdentidad').val(""); 
           $("#tipoDocumentoIdentidad").select2({
               placeholder: "Seleccione",
               allowClear: true
           });
        }
    });
	
	$('#tipoDocumentoIdentidad').change(function(){
		var tipoDocumento = $(this).val();
		$("#formBusquedaClienteId")[0].reset();
		$(this).val(tipoDocumento);
		limpiarDatosCliente();
		$('#datosCliente').hide();
	});
	
	$("#numeroDocumento").on("input",function(e){
        if($(this).value !=""){
           limpiarDatosCliente();
           $('#datosCliente').hide();
           $('#codigoCentral').val("");  
        }
       });
	
	$('.close').click(function(){
		$(this).parent().hide();
	});
	
	inicializarBotonSiguiente();
        $("#tipoDocumentoIdentidad").select2({
            placeholder: "Seleccione",
            allowClear: true
        });
        
		$('#tipoPersonaFU').attr("disabled", true);
//		$('#nombreCompleto').attr("disabled", true);
//		if($("#nombreTerritorio")) $("#nombreTerritorio").attr("disabled", true);
//		if($("#nombreOficina")) $("#nombreOficina").attr("disabled", true);
});

function validarBuscar() {
	
	var codigoCentral = $("#codigoCentral").val();
	var tipoDocumentoIdentidad = $("#tipoDocumentoIdentidad").val();
	var numeroDocumento = $("#numeroDocumento").val();
	
	if (codigoCentral == null || codigoCentral == "") {
		
		$("#seccionMensajeId").html("Debe ingresar los campos obligatorios: C&oacute;digo Central o Tipo y N&uacute;mero de Documento");
		if (tipoDocumentoIdentidad == null || tipoDocumentoIdentidad == "") {
			mostrarError();
			return false;
		} else {
			if (numeroDocumento == null || numeroDocumento == "") {
				mostrarError();
				return false;
			}
		}
		
	} else if ((tipoDocumentoIdentidad != null && tipoDocumentoIdentidad != "") || (numeroDocumento != null && numeroDocumento != "")){
		
		$("#seccionMensajeId").html("Solo debe ingresar C&oacute;digo Central o Tipo y N&uacute;mero de Documento");
		mostrarError();
		return false;
		
	}
	return true;
}

function buscarCliente(){
	
	var metodoEnv = "POST";
	var direccionUrl = contextPath + "/fu/preregistro/busquedacliente/busquedacliente/";
	//var request = JSON.stringify($('#formBusquedaClienteId').serializeObject());
	var jsonRequest=$('#formBusquedaClienteId').serializeObject();
	jsonRequest.localCache=getLocalObject('locaObjectFUCC');	
	var request = JSON.stringify(jsonRequest);
	
	bloquearPantalla();
	consultarAjax(metodoEnv, direccionUrl, request, contextPath, function(respuesta) {
		
		desBloquearPantalla();
		
		if (respuesta.head && respuesta.head.estado == "OK" && respuesta.body != null) {
			
			if(respuesta.body.mensajeServicio != null && respuesta.body.mensajeServicio != "" && (respuesta.body.respuesta1 == null || respuesta.body.respuesta1 == "")){
				$("#btnSiguiente").attr("disabled", true);
				$('#datosCliente').hide();				
				$("#seccionMensajeId").html(respuesta.body.mensajeServicio);				
				mostrarError();	
			}else if (respuesta.body.tipoPersonaFU == null && respuesta.body.nombreCompleto == null){
				$("#btnSiguiente").attr("disabled", true);
				$('#datosCliente').hide();				
				$("#seccionMensajeId").html("Cliente no Registrado");
				mostrarError();			
			} else {
				
				$.each(respuesta.body, function(i, item) {
					if (i == "tipoPersonaFU") {
					
						if (item == "F01" || item == "F02") {
							
							$('#tipoPersonaFU option[value="3_PJ"]').remove();
                            $('#tipoPersonaFU').prop('selectedIndex', 1);//mostramos PNN 
							$('#tipoPersonaFU').attr("disabled", false);
							
						} else {
							
							if (item == "F00")
								$("#tipoPersonaFU").val("1_PN");
							//else if (item == "C"){
							else if (item!=null && item.indexOf("M")!= "-1"){
								
								if ($("#tipoPersonaFU" + " option[value='3_PJ']").length == 0){
									var newOption = '<option value="3_PJ">PERSONA JURIDICA</option>';
									$("#tipoPersonaFU").append(newOption);
								}
								$("#tipoPersonaFU").val("3_PJ");
							}
							$('#tipoPersonaFU').attr("disabled", true);
						}
					} else {
						
						if(item==null){
							$("#" + i).val("");
						}else{
							$("#" + i).val("" + item);	
						}
	
					}
					
					//Datos que se reenviaran al server
					if(i == "territorio") $("#codigoTerritorioId").val(item);
					if(i == "codigoSegmento")  $("#codigoSegmentoId").val(item);
					if(i == "descripcionSegmento")  $("#descripcionSegmentoId").val(item);
					if(i == "codigoCentralSesion")  {
						$("#codigoCentralSesion").val(item);
						$("#codigoCentral").val(item);
					}
					if(i == "tipoDocumentoIdentidadSesion")  {
						$("#tipoDocumentoIdentidadSesion").val(item);
						$("#tipoDocumentoIdentidad").val(item);
					}
					if(i == "numeroDocumentoSesion")  {
						$("#numeroDocumentoSesion").val(item);
						$("#numeroDocumento").val(item);
					}
					
					if (i == "tipoPersona")  $("#tipoPersona").val(item);
					if(i=="oficina") $("#idOficina").val(item);
					if(i=="nombreTerritorio") $("#nombreTerritorio").val(item);
					if(i=="nombreOficina") $("#nombreOficina").val(item);
					

				});
				$('#datosCliente').show();
				$("#btnSiguiente").attr("disabled", false);
				$("#alertaError").hide();
				
				if(respuesta.body.respuesta1 != null && respuesta.body.respuesta1 != ""){
					$("#seccionMensajeId").html(respuesta.body.respuesta1);
					$("#alertaError").removeClass("alert-danger");
					$("#alertaError").addClass("alert-warning");				
					$("#alertaError").show();
				}
			}
			
		} else {
			$("#btnSiguiente").attr("disabled", true);
			$('#datosCliente').hide();
			$("#seccionMensajeId").html(respuesta.mensajeRespuesta!=null && respuesta.mensajeRespuesta!=""?respuesta.mensajeRespuesta: respuesta.head.mensaje);
			mostrarError();	
		}
		
	});
}

function comboTipoDocumentoIdentidad() {
	
	var metodoEnv = "GET";
	var direccionUrl = contextPath + "/fu/preregistro/busquedacliente/tipo-documento/";
	consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
		if (respuesta != null) {
			var $selectElement = $("#tipoDocumentoIdentidad");
			$(respuesta).each(function() {
				var newOption = '<option value="' + this.valor + '">' + this.nombre + '</option>';
				$selectElement.append(newOption);
			});
		}
	});
}

function comboTipoPersona() {
	
	var metodoEnv = "GET";
	var direccionUrl = contextPath + "/fu/preregistro/busquedacliente/tipo-persona/";
	consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
		if (respuesta != null) {
			var $selectElement = $("#tipoPersonaFU");
			$(respuesta).each(function() {
				var newOption = '<option value="' + this.idTipoPersona+'_'+this.codigo + '">' + this.descripcion + '</option>';
				$selectElement.append(newOption);
			});
		}
	});
}

function limpiarDatosCliente(){
	
	$("#tipoPersonaFU").val("");
	$("#nombreCompleto").val("");
	$("#nombreOficina").val("");
    $("#nombreTerritorio").val("");
	$("#btnSiguiente").attr("disabled", true);
}

function mostrarError(){
	
	$("#alertaError").removeClass("alert-warning");
	$("#alertaError").addClass("alert-danger");
	$("#alertaError").show();
}

function mostrarMensaje(mensaje, tipo){
	
	if(mensaje==""){
		mensaje="Debe ingresar los campos obligatorios: C&oacute;digo Central o Tipo y N&uacute;mero de Documento";
	}
	$( "#alertaError #seccionMensajeId" ).html(mensaje);
	$("#alertaError").show();
}
	
function fnObtenerTipoPersona(){
		
		var contextPath = $("#contextPath").val();
		var direccionUrl = contextPath + "/fu/preregistro/busquedacliente/tipo-persona/";
		consultarAjax("GET", direccionUrl, "" , contextPath, function(respuesta){

				construirSelect("selTipoPersona", respuesta);
				
		});
}

function fnObtenerTipoDocumento(){
		
	var contextPath = $("#contextPath").val();
	var direccionUrl = contextPath + "/fu/preregistro/busquedacliente/tipo-documento/";
	consultarAjax("GET", direccionUrl, "" , contextPath, function(respuesta){

				construirSelect("seltipoDoi", respuesta);
				
	});
}
	
function fnIniciarRegistroSolicitud(){
		
	var metodoEnv = "POST";
	var direccionUrl = contextPath + "/fu/preregistro/busquedacliente/operacionSiguiente/";

	if($("#codigoCentralId").val()!="" && $("#codigoCentralId").val()!=null){
		$("#codigoCentralId").val("");
		$("#codigoCentral").val("");	
	}
		

	$('#cookieSSO').val(readCookie(idCookieSsoFu));
	$('#tipoPersonaFU').attr("disabled", false);
	$('#nombreCompleto').attr("disabled", false);
	
	$("#nombreTerritorio").attr("disabled", false);
	$("#nombreOficina").attr("disabled", false);
	
	var jsonRequest=$('#formBusquedaClienteId').serializeObject();
	jsonRequest.localCache=getLocalObject('locaObjectFUCC');	
	var request = JSON.stringify(jsonRequest);
	
	 bloquearPantalla();	
	consultarAjax(metodoEnv, direccionUrl, request, contextPath, function(respuesta){
					
			if(respuesta.indexOf("URL_APPS_PJ") != "-1"){
				
				var arrayUrlPJ = respuesta.split("|");		
				$( "#formBusquedaClienteId" ).attr("action",arrayUrlPJ[1]);
				$( "#formBusquedaClienteId" ).submit();
				
			}else{
				if(respuesta.indexOf("URL_APPS_PN") != "-1"){
					
					var arrayUrlPN = respuesta.split("|");
					window.location.replace(arrayUrlPN[1],"_self");
					
				}
				else{

					$('#tipoPersonaFU').attr("disabled", true);
					$('#nombreCompleto').attr("disabled", true);
					$("#nombreTerritorio").attr("disabled", true);
					$("#nombreOficina").attr("disabled", true);
					desBloquearPantalla();
					mostrarMensaje(respuesta,0); //mensaje de validacion
				}
			}	 
	});
}

function inicializarBotonSiguiente(){
		
		$("#btnSiguiente").click(function(){
			fnIniciarRegistroSolicitud();
		});
}
	
