/**
 * 
 */


	var contextPath = $("#contextPath").val();
	var dataSetProducto = [];
	var dataEdit=null;
	var dataTableRow=null;
	var accion=null;
	var indexRow=null;
	var tipoBtn = 'G';//G= boton guardar, S= boton siguiente
	
	//PARA MODO EDICION, SE HACE GLOBAL LA VARIABLE QUE REPRESENTA AL CARRITO 
	var dataTableProducto;

	$(document).ready(function() {//********************************************* INI READY
		
//		************************ INI - CARGA INICIAL ************************
		validarTipoPersona();
		combMoneda();
	  	combCampanha();
		comboTipoFlujo();
		comboGiroNegocio();
		comboRegimenTributario();
		changeValueComboTipoFlujo();
		changeValueComboTipoCampania();
		changeValueComboOperacionCDR();
		
		$('#registrarSolicitudForm').bootstrapValidator({
			framework: 'bootstrap',	
			excluded: ':hidden',
			ignore: ":hidden",
			debug: true,
	        fields: {
	            regimenTributario: {
	                selector: '#regimenTributario',
	                validators: {
						callback: {
							message: 'Este campo es requerido',
							 callback: function(value, validator, $field) {
								 var regimenTributario = $('#regimenTributario').val();
								 if(tipoBtn == 'S'){
						    		 if(regimenTributario ==''){
										 return false;
									 }					    							    		
						    	 }
						    	 return true;
							 }
						}
	                }

	            },
	            tipoCampania: {
	                selector: '#tipoCampania',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                }
	            },
	            tipoOperacionCDR: {
	                selector: '#tipoOperacionCDR',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                }
	            },
	            tipoOperacion: {
	                selector: '#tipoOperacion',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                }
	            },
	            radClienteNuevo: {
	                validators: {                
		                callback: {
		                	message: 'Este campo es requerido',
							 callback: function(value, validator, $field) {
								 var idClienteRecurrente = $("#idClienteRecurrente").is(':checked');
								 var idClienteNuevo = $("#idClienteNuevo").is(':checked');
								 if(tipoBtn == 'S'){
						    		 if(idClienteRecurrente == false && idClienteNuevo == false){
										 return false;
									 }					    							    		
						    	 }
						    	 return true;
							 }
						}
	                }
	            }
	        }
	    });
						
//		************************ FIN - CARGA INICIAL ************************
		
				
//		************************ INI - MODAL PRODUCTO  ************************		
		$("#datetimepicker").datetimepicker({
	          format: "dd/MM/yyyy",             
	          startDate: new Date()          
	       }); 
		
		$('#addNewProductForm').bootstrapValidator({
			framework: 'bootstrap', 
            excluded: [':hidden', ':disabled'],
            ignore:[':hidden', ':disabled'],
	        /**feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },**/
	        fields: {
	        	producto: {
	                selector: '#producto',
	                validators: {
	                    notEmpty: {
	                        message: 'Debe seleccionar un producto'
	                    }
	                }
	            },
	            subProducto: {
	                selector: '#subProducto',
	                validators: {
	                    notEmpty: {
	                        message: 'Debe seleccionar un sub producto'
	                    }
	                }
	            },
	            monto: {
                    selector: '#monto',
                    validators: {
                        notEmpty: {
                            message: 'Debe ingresar el monto solicitado'
                        },
                        numeric: {
                            message: 'El valor no es un n&uacute;mero',
                            // The default separators
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
                subProductoMoneda: {
                    selector: '#subProductoMoneda',
                    validators: {
                        notEmpty: {
                            message: 'Debe seleccionar la moneda'
                        }
                    }
                },                
                txtFechaVencimiento: {
                    selector: '#txtFechaVencimiento',
                    validators: {
                        notEmpty: {
                            message: 'Debe ingresar una fecha de vencimiento'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            message: 'Fecha no v&aacute;lida',                        
                        }
                    }
                },
                plazoVencimientoDias: {
                    selector: '#txtPlazoVencimientoDias',
                    validators: {
                        notEmpty: {
                            message: 'Debe ingresar el plazo de vencimiento'
                        },
                        integer: {
                            message: 'El valor no es un n&uacute;mero'
                            // The default separators
//                          thousandsSeparator: '',
//                          decimalSeparator: '.'
                        }
                    }
                },
                plazoReembolsoCuota: {
                    selector: '#txtPlazoReembolsoCuota',
                    validators: {
                        notEmpty: {
                            message: 'Debe ingresar el plazo de reembolso'
                        },
                        integer: {
                            message: 'El valor no es un n&uacute;mero'
                            // The default separators
//                          thousandsSeparator: '',
//                          decimalSeparator: '.'
                        },
                        greaterThan: {
                            value: 1,
                            message: 'El plazo de reembolso de cuotas debe ser mayor a cero.'
                        }
                    }
                },
                rdDerivado: {
                    validators: {                
                        callback: {
                            message: 'Debe indicar si el producto es mejorado o no',
                             callback: function(value, validator, $field) {
                                 var rdDerivado = $("#rdDerivado").is(':checked');
                                
                                
                                 if(rdDerivado == false && rdCancelableNo == false){
                                     return false;
                                 }                                                              
                                 
                                 return true;
                             }
                        }
                    }
                },   
                rdCancelable: {
                    validators: {                
                        callback: {
                            message: 'Debe indicar si el pr&eacute;stamo es cancelable',
                             callback: function(value, validator, $field) {
                                 var rdCancelable = $("#rdCancelable").is(':checked');
                                 var rdCancelableNo = $("#rdCancelableNo").is(':checked');
                                
                                 if(rdCancelable == false && rdCancelableNo == false){
                                     return false;
                                 }                                                                                               
                                 return true;
                             }
                        }
                    }
                },
                cmbCampana: {
                    selector: '#cmbCampana',
                    validators: {
                        notEmpty: {
                            message: 'Debe ingresar la campa&ntilde;a'
                        }
                    }
                } 
	        }
	    });
		
		
	  $("#tasaAnual").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });
		
	  $("#txtPlazoVencimientoDias").keypress(function (e) {
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                     return false;
          }
         });
      
      $("#txtPlazoReembolsoCuota").keypress(function (e) {
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                     return false;
          }
         });
      
      $("#monto").focusout(function() {
    	  if($("#monto").val() != ""){
              var montoFormateado = formatMontoAll($("#monto").val());
              $("#monto").val(montoFormateado);  
          } 
        });
		           
      $("#rdCancelable").click(function(){
          document.getElementById("plazoReembolso").style.display = "none";
          document.getElementById("plazoVencimiento").style.display = "block";
       });
          
      $("#rdCancelableNo").click(function(){
          document.getElementById("plazoReembolso").style.display = "block";
          document.getElementById("plazoVencimiento").style.display = "none";
       });
      
      $("#subProductoMoneda").change(function(){
          $('.form-group').removeClass('has-error');    
        });
      
      $("#monto").on("input",function(e){
         if($(this).value !=""){
            $('.form-group').removeClass('has-error');  
         }
        });
      
      $("#tasaAnual").on("input",function(e){
         if($(this).value !=""){
            $('.form-group').removeClass('has-error');  
         }
        });
      
      $("#txtFechaVencimiento").on("input",function(e){
         if($(this).value !=""){
            $('.form-group').removeClass('has-error');  
         }
        });
      
      $("#txtPlazoReembolsoCuota").on("input",function(e){
             if($(this).value !=""){
                $('.form-group').removeClass('has-error');  
             }
            });
      
      $("#txtPlazoVencimientoDias").on("input",function(e){
         if($(this).value !=""){
            $('.form-group').removeClass('has-error');  
         }
        });
      
      $("#producto").change(function(){
    	  $('.form-group').removeClass('has-error');
		  hidePrestamoCancelable();
          hideFechaVencimientoLinea();
          hidePlazos();
          document.getElementById("alertaMensajeErrorProducto").style.display = "none";            
			$("#subProducto").val("");
			combSubProducto($("#producto").val());
		});
               
      $('#addProduct').on( 'click', function () {
			accion="add";			
			$("#idTitleProduct").text("Agregar Producto");			
			document.getElementById("alertaMensajeErrorProducto").style.display = "none";            
			$('#datosProducto').modal('show'); 	
		});
      
      $('#datosProducto').on('shown.bs.modal', function() {
    	  $('#addNewProductForm').bootstrapValidator('resetForm', true);
    	  cleanValues();
    	  hidePrestamoCancelable();
          hideFechaVencimientoLinea();
          hidePlazos();
    	  });
      
      $("#subProducto").change(function(){
    	  $('.form-group').removeClass('has-error');
    	  hidePrestamoCancelable();
          hideFechaVencimientoLinea();
          hidePlazos();
          
		       var selected = $(this).find("option:selected");
		       var varLineal = selected.data("lineal"); 
		       var varDerivado =  selected.data("derivado");
		       
		       if(varLineal){
		    	   $("#lineal").html("L&iacute;nea").text();
		    	   $("#idlineal").val("1");
//		    	   showDateOpOnline();
		       }else{
		    	   $("#lineal").html("Puntual").text();
		    	   $("#idlineal").val("0");
//		    	   hideDateOpOnline();
		       }
		       
		       if(varDerivado){
		    	   $("#rdDerivado").attr("disabled", false);
		    	   $("#rdDerivado").attr('checked',false);
		    	   $("#rdDerivadoNo").attr("disabled", false);
		    	   $("#rdDerivadoNo").attr('checked',false);		    	   
		       }else{
		    	   $("#rdDerivado").attr("disabled", true);
		    	   $("#rdDerivado").attr('checked',false);
		    	   $("#rdDerivadoNo").attr("disabled", true);
		    	   $("#rdDerivadoNo").attr('checked',false);
		       }
		       
		       validarCamposLinealPuntual($("#subProducto").val());		      
		});
      
      $("#btnAceptar").click(function(){
			
			var validator = $('#addNewProductForm').data('bootstrapValidator');
			$(".help-block").css({"color": "#c4136c"});          
	        validator.validate();
	        $('.form-group').removeClass('has-error');
	        
	        if (validator.isValid()) {
              var validacionMontos = {};
              validacionMontos.subProducto = $('#subProducto').val();
              validacionMontos.montoSolicitado = fnBorrarFormatoMonto($('#monto').val());
              validacionMontos.monedaSolicitada = $('#subProductoMoneda').val();
              var dataJson=JSON.stringify(validacionMontos);
              var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/validarCamposSubProductoMonto/"; 
              
              consultarAjax("POST", direccionUrl, dataJson, contextPath, function(respuesta) {
                  
                  if(respuesta!=null && respuesta != ""){
                      
                      mostrarMensajeClasico('#alertaMensajeErrorProducto', respuesta,2000);
                      
                  }else{
                      if(accion=="add"){
                          agregarProducto();
                      }else if(accion=="edit"){
                          editarProducto();
                      }
                       $('#datosProducto').modal('toggle');                           
                  }
              }); 
          }
		
	});	
      
      
//		************************ FIN - MODAL PRODUCTO  ************************		
		
//		************************ INI - TABLA PRODUCTOS  ************************
      	var btnEditar=  '<li><button class="btn-link" type="button" data-toggle="modal" data-target="#editarDatosProducto" id="edit"><img src="'+contextST+'/images/iconoEditar.png"></button></li>';
		var btnEliminar='<li><button class="btn-link" type="button" id="delete"><IMG src="'+contextST+'/images/iconoEliminar.png"></button></li>';
 
      //var dataTableProducto=$('#tableProducto').DataTable( {
		  dataTableProducto=$('#tableProducto').DataTable( {
	    		//data: dataSetProducto,
	        	"ajax": function (data, callback, settings) {
	                 callback ( { data: dataSetProducto } );
	             },
				"ordering": false,
				"info"    : false,
				"bLengthChange" : false,
				"bFilter": false,
				"iDisplayLength" : 5,
		        "columns": [
		            { "data": "producto" },
		            { "data": "subProducto" },
		            { "data": "moneda" },
		            { "data": "montoSolicitadoString" },
		            { "data": null }
		        ],
		         "columnDefs": [{
		         "targets": -1,
		         "data": null,
		         "defaultContent": '<ul class="list-actions">'+btnEditar+btnEliminar+'</ul>'
	
		        }]
		});
      
		$('#tableProducto tbody').on( 'click', '#delete', function () {
			var indice = $(this).parents('tr').index();
			$('#eliminarDatosProducto').attr('data-attr-index',indice);
			$('#eliminarDatosProducto').modal('show'); 

		} );
		
		$('#eliminarDatosProducto').on('click', '.btn-delete', function(){
			deleteProducto();
		}
		);
		
		$('#tableProducto tbody').on( 'click', '#edit', function () {
			
			accion="edit";
			
			$("#idTitleProduct").text("Editar Producto");
		
			indexRow=$(this).parents('tr').index();
			
					
			var productoModel =dataSetProducto[indexRow];
			
			$('#datosProducto').modal('show'); 
			
			setTimeout(function(){				 
                $("#producto").val(productoModel.idProducto);
                $("#producto").trigger("change");               
                $("#monto").val(productoModel.montoSolicitadoString);
                $("#tasaAnual").val(productoModel.tasaAnual);
                $("#txtFechaVencimiento").val(productoModel.fechaVencimiento);
                $("#txtPlazoVencimientoDias").val(productoModel.plazoVencimientoDias);
                $("#txtPlazoReembolsoCuota").val(productoModel.plazoReembolsoCuotas);          
                $("#subProductoMoneda").val(productoModel.idMoneda);
                $("#cmbCampana").val(productoModel.idCampanha);
            }, 500);
            
            setTimeout(function(){
                $("#subProducto").val(productoModel.idSubProducto);
                $("#subProducto").trigger("change");                                        
            }, 600);
            
            setTimeout(function(){
                if($("#txtPlazoReembolsoCuota").val()!= ""){
                    $("#rdCancelableNo").attr('checked', 'checked');
                    $("#rdCancelableNo").trigger("click");                      
                }else if($("#txtPlazoVencimientoDias").val()!= ""){
                    $("#rdCancelable").attr('checked', 'checked');
                    $("#rdCancelable").trigger("click");                
                }                                   
            }, 700);

		} );
		
        
//		************************ FIN - TABLA PRODUCTOS  ************************		
		
//		************************ INI - GUARDAR  ************************
		$("#botonGuadarId").click(function(event) {
			
			var registroSolicitudModel={};
			var datosTitularModel={};
			
			registroSolicitudModel.tipoOperacion= $("#tipoOperacion").val();
			registroSolicitudModel.nombreTipoOperacion=$("#tipoOperacion option:selected").text();
			registroSolicitudModel.listaCarritoProducto=dataSetProducto;
			registroSolicitudModel.tipoCampania= $("#tipoCampania").val();
			registroSolicitudModel.tipoOperacionCDR=$("#tipoOperacionCDR").val();
			
			if($("#idClienteNuevo").is(':checked')){
				datosTitularModel.clienteNuevo= $("#idClienteNuevo").val();				
			}else if($("#idClienteRecurrente").is(':checked')){
				datosTitularModel.clienteNuevo= $("#idClienteRecurrente").val();				
			}
			
			if($("#idCategoria1").is(':checked')){
				datosTitularModel.categoria1= $("#idCategoria1").is(':checked');			
			}
			if($("#idCategoria3").is(':checked')){
				datosTitularModel.categoria3= $("#idCategoria3").is(':checked');				
			}	
			
			datosTitularModel.codigoRegimenTributario= $("#regimenTributario").val();	
			datosTitularModel.codigoGiroNegocio= $("#giroNegocio").val();
			registroSolicitudModel.datosTitularModel = datosTitularModel;
			
			registroSolicitudModel.cookieSSO = readCookie(idCookieSsoFu);
			
			var dataJson=JSON.stringify(registroSolicitudModel);
			
			var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/evt-grabar-solicitud/";

			tipoBtn = 'G';
			var validator = $('#registrarSolicitudForm').data('bootstrapValidator');
			$(".help-block").css({"color": "#c4136c"});
	        validator.validate();
	        $('.form-group').removeClass('has-error');
	        if(validator.isValid() && dataSetProducto.length>0){
				consultarAjax("POST", direccionUrl, dataJson, contextPath, function(respuesta) {
					if(respuesta.codigoRespuesta == "99"){
						mostrarMensajeFlotante(respuesta.mensajeRespuesta, "danger");
					}else if(respuesta.mensajeServicio != null && respuesta.mensajeServicio != ""){
						mostrarMensajeFlotante(respuesta.mensajeServicio, "danger");						
					}else{
						mostrarMensajeFlotante("La solicitud fue guardada satisfactoriamente.", "success");		
						document.getElementById("idHeaderNumeroSolicitud").innerHTML= "&nbsp;"+respuesta.respuesta2;		
						$('#botonAnularId').attr('disabled', false);
					}					
				});				
			}else if(dataSetProducto.length==0){
				fnMostrarError('msjErrorSubProductos','Se requiere registrar al menos un producto');
	        }	
		});
		
//		************************ FIN - GUARDAR  ************************		


//		************************ INI - BTN SIGUIENTE ********************
		$("#idSiguienteConDel").click(function(event) {
			
			var codigoRegimenTributario= {};
			codigoRegimenTributario = $("#regimenTributario").val();
			var dataJson=JSON.stringify(codigoRegimenTributario);
			var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/validacion-ventas-anuales/";
			
			document.getElementById("msjErrorSubProductos").style.display = "none";
			tipoBtn = 'S';
			var validator = $('#registrarSolicitudForm').data('bootstrapValidator');	
			$(".help-block").css({"color": "#c4136c"});			
		    validator.validate();
		    $('.form-group').removeClass('has-error');
			if(validator.isValid() && dataSetProducto.length>0){
				consultarAjax("POST", direccionUrl, dataJson, contextPath, function(respuesta) {
					if(respuesta.codigoServicio=="OK"){
						siguiente();
					}else if(respuesta.codigoServicio=="ERR_VM"){				
						tipoModal(1, respuesta.respuesta1);						
		    			return false;
					}else if(respuesta.codigoServicio=="OK_VM"){
		//				tipoModal(2, respuesta.respuesta1);						
		//				return false;
						siguiente();
					}
					});				
			}else if(dataSetProducto.length==0){
				document.getElementById("msjErrorSubProductos").style.display = "block";
				fnMostrarError('msjErrorSubProductos','Se requiere registrar al menos un producto');
		    }
		});
		
		
		function fnMostrarError($id, $mensaje) {
			var div = '<div class="alert alert-danger margin-bottom-x15"><img src="'+contextST+'/images/iconoAlerta.png"> <span">'+$mensaje+'</span></div>';
			$('#'+$id).html(div);
		}
		
		//tipo 1:con boton aceptar, 2: sin boton aceptar
		function tipoModal(tipo, mensaje){			
			
			if(tipo == 1){
				$("#mensajeSistemamensajeSistema").text("");
				$("#mensajeSistemamensajeSistema").html(mensaje);
				$('#btnMensajeSistemaSinVentas').modal('show'); 
			}
			if(tipo == 2){
				$("#mensajeSistema").text("");
				$("#mensajeSistema").html(mensaje);
				$('#btnMensajeSistema').modal('show'); 
			}			
		}
		
		$("#btnConfirmarRechazo").click(function(event) {
			$('#btnMensajeSistema').modal('hide'); 
			siguiente();
		});
		
		
		function siguiente(){		
		$(".preloader").fadeIn();
			var registroSolicitudModel={};
			var datosTitularModel={};
			registroSolicitudModel.tipoOperacion= $("#tipoOperacion").val();
			registroSolicitudModel.nombreTipoOperacion=$("#tipoOperacion option:selected").text();
			registroSolicitudModel.tipoCampania= $("#tipoCampania").val();
			registroSolicitudModel.tipoOperacionCDR= $("#tipoOperacionCDR").val();
			
			if($("#idClienteNuevo").is(':checked')){
				datosTitularModel.clienteNuevo= $("#idClienteNuevo").val();				
			}else if($("#idClienteRecurrente").is(':checked')){
				datosTitularModel.clienteNuevo= $("#idClienteRecurrente").val();				
			}
			
			if($("#idCategoria1").is(':checked')){
				datosTitularModel.categoria1= $("#idCategoria1").is(':checked');			
			}
			if($("#idCategoria3").is(':checked')){
				datosTitularModel.categoria3= $("#idCategoria3").is(':checked');				
			}	
			datosTitularModel.codigoGiroNegocio= $("#giroNegocio").val();
			datosTitularModel.codigoRegimenTributario= $("#regimenTributario").val();			
			registroSolicitudModel.datosTitularModel = datosTitularModel;
			
			registroSolicitudModel.cookieSSO = readCookie(idCookieSsoFu);
			
			registroSolicitudModel.listaCarritoProducto=dataSetProducto;
			
			var dataJson=JSON.stringify(registroSolicitudModel);
			var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/grabar-siguiente/";
			
			consultarAjax("POST", direccionUrl, dataJson, contextPath, function(respuesta) {
				if(respuesta.estadoServicio=="OK"){
					
					//location.href = respuesta.respuesta2+'?faseActual=5&faseDestino=6'; //LOCAL
					$("#idFrmllamarWebPymes").attr("action",respuesta.respuesta2+'?faseActual=5&faseDestino=6');
					$("#idFrmllamarWebPymes").submit();	  		
					
				}else{
					
					$(".preloader").fadeOut();
					var mensaje = respuesta.mensajeServicio!=null?respuesta.mensajeServicio:respuesta.respuesta1;    				
					mostrarMensajeClasico('#alertaMensajeError', mensaje,2000);					
					return false;
				}
			});
		
		
		
		}

//		************************ FIN - BTN SIGUIENTE ********************
         		
//		 **************************** INI - ANULAR SOLICITUD ****************************
		$("#btnConfirmarAnularSolicitud").click(
		function() {
			anularSolicitud();

		});	
//		 **************************** FIN - ANULAR SOLICITUD ****************************
		
		var winPadre = "";
		$(".modalComentarioAnulacion").click(function(){
			$("#modalAnulacion").modal("hide");
			$("#modalAnulacion").on('hidden.bs.modal', function (e) {
				$('#modalComentarioAnulacion').modal("show");
				winPadre = "add";
			});
		});
		
		$(".cerrarModal").click(function(){
			$("#modalAnulacion").modal("hide");
			$("#modalAnulacion").off('hidden.bs.modal');
		});
		
		$(".regresarModal").click(function(){
			$("#modalComentarioAnulacion").modal("hide");
			$("#modalComentarioAnulacion").on('hidden.bs.modal', function (e) {
				if(winPadre == "add"){
					$('#modalAnulacion').modal("show");
				}else{
					$('#modalOtros').modal("show");
				}					
			});
		});
		
		$(".cerrarSubModal").click(function(){
			$("#modalComentarioAnulacion").modal("hide");
			$("#modalComentarioAnulacion").off('hidden.bs.modal');
		});
		
		//MODO ENDICION SELECCIONAR VALOR EN COMBOS
		mostrarCarritoSubProducto(datosSolModoEdit.modoEdicion);
		
	});//************************************************** FIN READY
	
	function validarCamposLinealPuntual(idSubProducto){
    	 
        var idProducto = $("#producto").val();
        var metodoEnv = "POST";
        var parametros = {};
        parametros.idProducto = idProducto;
        parametros.idSubProducto = idSubProducto;
        var request = JSON.stringify(parametros);
        var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/validarCamposLinealPuntual/";
        consultarAjax(metodoEnv, direccionUrl, request, contextPath, function(respuesta) {
            if(respuesta){
               showPrestamoCancelable();
               var selected = $("#subProducto").find("option:selected");
               var varLineal = selected.data("lineal"); 
                              
               if(varLineal){
                   showFechaVencimientoLinea();
               }                       
            }
        }); 
    }
	
	function validarTipoPersona(){
		document.getElementById("lblCategoria1").style.display = "block";
		var idTipoPersona = $("#idTipoPersona").val();
		if(idTipoPersona=='PJ'){				
			document.getElementById("lblCategoria1").style.display = "none";
		}
	}
	
	function combMoneda() {
		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/tipo-moneda/";
		consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
			if (respuesta != null) {
				var $selectElement = $("#subProductoMoneda");
				$(respuesta).each(function() {
					var newOption = '<option value="' + this.idTipoMoneda + '">' + this.valor + '</option>';
					$selectElement.append(newOption);
				});
			}
		});
	}
	
	function combCampanha() {
		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/obtener-campanha/";
		consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
			if (respuesta != null) {
				var $selectElement = $("#cmbCampana");
				$(respuesta).each(function() {
					var newOption = '<option value="' + this.codigo + '">' + this.descripcion + '</option>';
					$selectElement.append(newOption);
				});
			}
		});
	}

	function comboTipoFlujo() {
		
		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/tipo-flujo/";
		consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
			
			if (respuesta != null) {
				
				var $selectElement = $("#tipoOperacion");
				var newOption = '<option value="">Seleccione</option>';
				$selectElement.append(newOption);
				$(respuesta).each(function() {
					 newOption = '<option value="' + this.valor + '">' + this.descripcion + '</option>';
					$selectElement.append(newOption);
				});
				
				seleccionarValCombos('TF');
			}
		});
	}
	
	function comboGiroNegocio(){
		
		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/giro-negocio/";
		consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
			if (respuesta != null) {
				var $selectElement = $("#giroNegocio");
				var newOption = '<option value="">Seleccione</option>';
				$selectElement.append(newOption);
				$(respuesta).each(function() {
					 newOption = '<option value="' + this.idGiroNegocio+'_'+this.codigoGiroNegFu + '">' + this.descripcionGiroNegFu + '</option>';
					$selectElement.append(newOption);
				});
				
				seleccionarValCombos('GN');
			}
		});
		
	}

	function comboRegimenTributario(){
		
		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/regimen-tributario/";
		consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
			if (respuesta != null) {
				var $selectElement = $("#regimenTributario");
				var newOption = '<option value="">Seleccione</option>';
				$selectElement.append(newOption);
				$(respuesta).each(function() {
					 newOption = '<option value="' + this.idRegimenTributario + '">' + this.descripcion + '</option>';
					$selectElement.append(newOption);
				});
				
				seleccionarValCombos('RT');
			}
		});
		
	}
	
	function changeValueComboTipoFlujo(){
		  
		  $( "#tipoOperacion" ).change(function () {
		    var valor = "";
		    cleanCbm("#tipoCampania");
			cleanCbm("#tipoOperacionCDR");
			nullCombo();
		    $( "#tipoOperacion option:selected" ).each(function() {
		    	
		    	valor = $( this ).val();
		    
		    	cleanCbm("#producto");		    	
		    	$("#addProduct").attr("disabled", true);
		    	if(valor!="" && valor=="2"){//Operacion Regular
		    		combProducto("FR"+valor); 
		    		$("#addProduct").attr("disabled", false);
		    	}else if(valor!="" && valor=="1"){//Operacion Puntual									
					comboOperacionCDR(valor);
					showOpCDR();
				}else if(valor!="" && valor=="4"){//Campania					
					comboTipoCampania(valor);
					showTipoCampana();
					showCampana();
				}else{
					nullCombo();
				}
		    	
		    });
		    
		  });	
	}
	
	function changeValueComboTipoCampania(){
		  
		  $("#tipoCampania").change(function () {	
		    var valor = "";
		    $("#addProduct").attr("disabled", false);
		    $( "#tipoCampania option:selected" ).each(function() {		    	
		    	valor = $( this ).val();
		    	combProducto("FC"+valor);   		    	
		    });
		    
		  });	
	}
	
	function changeValueComboOperacionCDR(){
		  
		  $("#tipoOperacionCDR").change(function () {	
		    var valor = "";
		    $("#addProduct").attr("disabled", false);
		    $( "#tipoOperacionCDR option:selected" ).each(function() {		    	
		    	valor = $( this ).val();
		    	combProducto("FO"+valor);   		    	
		    });
		    
		  });	
	}
	
	function combProducto(tipoFlujo) {
		 
		 var metodoEnv = "GET";
		 cleanCbm("#producto");
			var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/producto/"+tipoFlujo;
			consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
				if (respuesta != null) {
					var $selectElement = $("#producto");
					$(respuesta).each(function() {
						var newOption = '<option value="' + this[0] + '">' + this[1]+ '</option>';
						$selectElement.append(newOption);
					});
				}
			});			 
	}
	
	function comboOperacionCDR(valorTipoOperacion){

		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/oper-cdr/"+valorTipoOperacion;
		var paramsJSON={}
		
		consultarAjax(metodoEnv, direccionUrl, paramsJSON, contextPath, function(respuesta) {
			if (respuesta != null) {
				var $selectElement = $("#tipoOperacionCDR");
				var newOption = '';
				$selectElement.append(newOption);
				$(respuesta).each(function() {
					 newOption = '<option value="' + this.valor + '">' + this.nombre + '</option>';
					$selectElement.append(newOption);
				});
			}
		});
	}



	function comboTipoCampania(valorTipoOperacion){
		
		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/tipo-campania/"+valorTipoOperacion;
		
		consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
	    	
			if (respuesta != null) {
				var $selectElement = $("#tipoCampania");
				var newOption = '';
				$selectElement.append(newOption);
				$(respuesta).each(function() {
					 newOption = '<option value="' + this.valor + '">' + this.nombre + '</option>';
					$selectElement.append(newOption);
				});
			}
		});
		
	}
	
	function combSubProducto(idProducto) {
		var tipoFlujo = $("#tipoOperacion").val();			
		var tipoOperacion = "";
		if(tipoFlujo=="2"){//Operacion Regular
			tipoOperacion = "FR"+tipoFlujo;
    	}else if(tipoFlujo=="1"){//Operacion Puntual	
    		var tipoPuntual = $("#tipoOperacionCDR").val();	
			tipoOperacion = "FO"+ tipoPuntual;
		}else if(tipoFlujo=="4"){//Campania
			var tipoCampanha = $("#tipoCampania").val();
			tipoOperacion = "FC"+ tipoCampanha;
		}
		
		var metodoEnv = "GET";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/sub-producto/"+idProducto+"/"+tipoOperacion;
		document.getElementById("alertaMensajeErrorProducto").style.display = "none";            
		consultarAjax(metodoEnv, direccionUrl, null, contextPath, function(respuesta) {
			if (respuesta != null) {
				var $selectElement = $("#subProducto");
				cleanCbm("#subProducto");
				
				$(respuesta).each(function() {
					var newOption = '<option value="'+this.idSubProductos+'_'+this.valorWp+'" data-lineal="'+this.esLinealPuntual+'" data-derivado="'+this.aplicaDerivado+'">' + this.nombreWp + '</option>';
					$selectElement.append(newOption);
				});
			}
		});
	}

	function deleteProducto(){
//		var index=dataTableProducto.row(event.data.row).index();
		dataSetProducto.splice($('#eliminarDatosProducto').attr('data-attr-index'),1);
		dataTableProducto.ajax.reload();

	}
	
	function agregarProducto(){
		
		var productoModel ={};
		
		productoModel.idProducto=$("#producto").val();
		productoModel.producto=$("#producto option:selected").text();			
		productoModel.idSubProducto=$("#subProducto").val();
		productoModel.subProducto=$("#subProducto option:selected").text();
//		productoModel.plazosMeses=$("#plazo").val();
		productoModel.montoSolicitado=fnBorrarFormatoMonto($('#monto').val());
        productoModel.montoSolicitadoString=$('#monto').val();
        productoModel.tasaAnual=$("#tasaAnual").val();
	   if($("#rdDerivado").is(':checked')){
		   productoModel.derivado = "true";	
		}else if($("#rdDerivadoNo").is(':checked')){
			productoModel.derivado = "false";		
		}
		productoModel.fechaVencimiento=$("#txtFechaVencimiento").val();
		productoModel.plazoVencimientoDias=$("#txtPlazoVencimientoDias").val();
		productoModel.plazoReembolsoCuotas=$("#txtPlazoReembolsoCuota").val();
		productoModel.idMoneda=$("#subProductoMoneda").val();
		productoModel.moneda=$("#subProductoMoneda option:selected").text();
		productoModel.idCampanha=$("#cmbCampana").val();
		if($("#cmbCampana").val()!=""){
			productoModel.campanha=$("#cmbCampana option:selected").text();
		}			
		productoModel.flagLinea=$("#idlineal").val();

		
		dataSetProducto.push(productoModel);					
	
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/agregar-producto/";
		
		var request = JSON.stringify(dataSetProducto);
		consultarAjax("POST", direccionUrl, request, contextPath, function(respuesta) {
			if (respuesta != null) {
				 dataSetProducto=respuesta;
				 dataTableProducto.ajax.reload();
				 cleanValues();
			}
	
		});
	}
	
	function editarProducto(){
		
		$(".help-block").css({"color": "#c4136c"});
		var productoModel ={};
		
		productoModel.idProducto=$("#producto").val();
		productoModel.producto=$("#producto option:selected").text();
		productoModel.idSubProducto=$("#subProducto").val();
		productoModel.subProducto=$("#subProducto option:selected").text();
//		productoModel.plazosMeses=$("#plazo").val();			
		productoModel.montoSolicitado=fnBorrarFormatoMonto($('#monto').val());
        productoModel.montoSolicitadoString=$('#monto').val();
		productoModel.tasaAnual=$("#tasaAnual").val();
		if($("#rdDerivado").is(':checked')){
		   productoModel.derivado = "true";	
		}else if($("#rdDerivadoNo").is(':checked')){
			productoModel.derivado = "false";		
		}
		productoModel.fechaVencimiento=$("#txtFechaVencimiento").val();
		productoModel.plazoVencimientoDias=$("#txtPlazoVencimientoDias").val();
		productoModel.plazoReembolsoCuotas=$("#txtPlazoReembolsoCuota").val();
		productoModel.idMoneda=$("#subProductoMoneda").val();
		productoModel.moneda=$("#subProductoMoneda option:selected").text();
		productoModel.flagLinea=$("#idlineal").val();
		
		dataSetProducto[indexRow]=productoModel;
		
		dataTableProducto.ajax.reload();
	}
	
//	INI - ANULAR SOLICITUD
	function anularSolicitud(){

		document.getElementById("alertaMensajeAnularSolicitud").style.display = "none";
		var tbTrazabilidad={};
		
		tbTrazabilidad.comentario= $("#input0").val();
		
		var dataJson=JSON.stringify(tbTrazabilidad);
		
		var metodoEnv = "POST";
		var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/anular-solicitud/";
		
		var direccionUrlBandeja = contextPath + "/fu/bandeja/mispendientes/init";

		consultarAjax(metodoEnv, direccionUrl, dataJson, contextPath, function(respuesta) {
			
			if (respuesta == "" ) {
//				location.href = direccionUrlBandeja;// TEST
				location.href = direccionUrlBandeja+"?username=P020093";// LOCAL
			}else{
				mostrarMensajeClasico('#alertaMensajeAnularSolicitud', respuesta,2000);
			}
		});
	}
//	FIN - ANULAR SOLICITUD	
	
	function showPrestamoCancelable(){
        document.getElementById("productoCancelable").style.display = "block";
    }
    function hidePrestamoCancelable(){
        document.getElementById("productoCancelable").style.display = "none";
    }
    function showFechaVencimientoLinea(){
        document.getElementById("fechaVencimiento").style.display = "block";
    }
    function hideFechaVencimientoLinea(){
        document.getElementById("fechaVencimiento").style.display = "none";
    }
    function hidePlazos(){
        document.getElementById("plazoReembolso").style.display = "none";
        document.getElementById("plazoVencimiento").style.display = "none";
    }
	
	function showDateOpOnline(){
		document.getElementById("fechaVencimiento").style.display = "block";
		document.getElementById("plazoVencimiento").style.display = "block";
		document.getElementById("plazoReembolso").style.display = "block";
	}
	function hideDateOpOnline(){
		document.getElementById("fechaVencimiento").style.display = "none";
		document.getElementById("plazoVencimiento").style.display = "none";
		document.getElementById("plazoReembolso").style.display = "none";
	}
	function showTipoCampana(){
		document.getElementById("tipoCampanaDiv").style.display = "block";
		document.getElementById("campana").style.display = "none";
		document.getElementById("opCDR").style.display = "none";
	}
	function showCampana(){
		document.getElementById("campana").style.display = "block";
		document.getElementById("opCDR").style.display = "none";
	}
	function hideCampana(){
		document.getElementById("campana").style.display = "none";
		document.getElementById("opCDR").style.display = "none";
	}
	function showOpCDR(){
		document.getElementById("opCDR").style.display = "block";
		document.getElementById("tipoCampanaDiv").style.display = "none";
		document.getElementById("campana").style.display = "none";
	}
	function nullCombo(){
		document.getElementById("tipoCampanaDiv").style.display = "none";
		document.getElementById("campana").style.display = "none";
		document.getElementById("opCDR").style.display = "none";
	}

	function cleanCbm(idCombo){
		$(idCombo+" option").remove();
		var selectOption = '<option value="">Seleccionar</option>';
		$(idCombo).append(selectOption);
	}
	
	function cleanValues(){
		
		$("#producto").val("");
		$("#subProducto").val("");
//		$("#plazo").val("");
		$("#monto").val("");
		$("#tasaAnual").val("");
		cleanCbm("#subProducto");
		$('#rdDerivado').prop('checked', false);
		$('#rdDerivadoNo').prop('checked', false);
		
		$("#txtFechaVencimiento").val("");
		$("#txtPlazoReembolsoCuota").val("");
		$("#txtFechaVencimiento").val("");
		$("#cmbCampana").val("");
		
	}

	function hiddenInformation004(){		
		document.getElementById("containerInformation004").style.display = "none";
		return true;
	}
	
	function hiddenInformation005(){		
		document.getElementById("containerInformation004").style.display = "none";
		return true;
	}
	
	function delegacion(){
		tipoModal(1, "Tu solicitud acaba de ser ingresada a Admisi&oacute;n de Riesgos");
	}
	
	// FUNCION PARA MOSTRAR LA PANTALLA EN MODO EDICIO
	function seleccionarValCombos(nombCombo){

		if(nombCombo=="TF"){
			$("#tipoOperacion").val(datosSolModoEdit.codigoTipoFlujo);
			
		}
		if(nombCombo=="RT"){
			
			$("#regimenTributario").val(datosSolModoEdit.codRegTributario);
			
		}
		if(nombCombo=="GN"){
			$("#giroNegocio").val(datosSolModoEdit.codigoGiroNegocio);
		}

		
		
	}
	
	function mostrarCarritoSubProducto(modoEdicion){	
		
		if(modoEdicion=="E"){

			var direccionUrl = contextPath + "/fu/preregistro/registrosolicitud/listar-subproducto-carrito/";
			var request =null;
			consultarAjax("POST", direccionUrl, request, contextPath, function(respuesta) {
				if (respuesta != null) {
					 dataSetProducto=respuesta;
					 dataTableProducto.ajax.reload();
					 cleanValues();
					 
					 
			    	 $("#addProduct").attr("disabled", false);
					 
					 //Cargamos combos del carrito
			    		cleanCbm("#producto");
			    		combProducto(datosSolModoEdit.codigoTipoFlujo); 

				}
			});
			
		}
	}
