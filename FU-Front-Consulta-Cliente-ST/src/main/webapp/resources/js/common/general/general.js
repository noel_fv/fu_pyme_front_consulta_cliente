var valorContextoApp="FU-Front-Consulta-Cliente-web"; //"FU-Front-web/indexIframe.jsp"; // "PortalFileUnico0";

$(document).ready(function() {
	
	//Para ambiente Portal.
	$("body,html").bind("touchstart touchmove scroll mousedown mousemove DOMMouseScroll mousewheel keyup click", function(e){

		/*Funcion expuesta por Portal,
		 actualiza el timer visualmente y renueva la sesiÃ³n con webseal, debe ser invocado por aplicaciones que carguen en el iframe.
		 El tiempo de ejecuciÃ³n automÃ¡tica es cada 10 segundos para el Portal de File Ãšnico solamente
		*/
		    if(window.parent.location.href.indexOf(valorContextoApp) == -1) {
		    	
				top.actualizarTimerSesion();
		    }
		});
	//Controlando sesion interna del aplicativo (1min)
    setInterval(function() {
    	consultarAjax("POST", $("#contextPath").val()+"/fu/check/status", "", $("#contextPath").val(), function(respuesta){});
    }, 2 * 60 * 1000);
});

function saveLocalObject(idObj, valueObj){
	window.sessionStorage.setItem(idObj,valueObj);
}

function getLocalObject(idObj){
	return window.sessionStorage.getItem(idObj);
}
function removeLocalObject(idObj){
	window.sessionStorage.removeItem(idObj);
}

function bloquearPantalla(){

  //Solo para trabajar en la Version Preliminar del portal
  if(window.parent.location.href.indexOf(valorContextoApp) > -1) {
	$(".preloader").fadeIn();
  }else{
	  top.showPreloader();
	  }
}

function desBloquearPantalla(){

  //Solo para trabajar en la Version Preliminar del portal
	if(window.parent.location.href.indexOf(valorContextoApp) > -1) {
	    	
	    $(".preloader").fadeOut();
	    	
	}else{
		top.hidePreloader();
	}
		
}