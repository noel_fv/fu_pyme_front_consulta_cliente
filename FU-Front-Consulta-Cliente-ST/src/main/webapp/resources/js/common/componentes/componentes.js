/**
 * @fileoverview Libreria de componentes Javascript.
 * 
 * @autor miguel.bezada@hundred.com.pe
 * @autor cesar.santos@hundred.com.pe 
 * @version 1.0
 */

/** @constant {number} */
var MOSTRAR = 1;

/** @constant {number} */
var OCULTAR = 0;

/** @constant {number} */
var ERROR = 0;

/** @constant {number} */
var INFO = 1;

/** @constant {number} */
var ENABLED = 1;

/** @constant {number} */
var DISABLED = 0;

/** @constant {number} */
var TIEMPO_VALI = 5000;

/** @constant {number} */
var TIEMPO_INFO = 15000;

/** @constant {number} */
var TIEMPO_INFO_02 = 1000;

/** @constant {string} */
var ESTADO_PROV_ENVIADO_BANCO = "01103";

/** @constant {string} */
var ESTADO_BANCO_PRESELECCIONADO = "0";// "01202";

/** @constant {number} */
var TOTAL_ROWS_POR_PAGINA = 10;

/** @constant {string} */
var PER_NATURAL = "02101";

/** @constant {string} */
var PER_JURIDICA = "02102";

/** @constant {string} */
var USD = "01401";

/** @constant {string} */
var PEN = "01402";

/** Variables Globales */
var paginaActualMisReferidos = 0;
var paginaActualReferidosPorVendedor = 0;

/** Variables Conversion UMT - Coordenadas Geográficas **/
var pi = 3.14159265358979;
var sm_a = 6378137.0;
var sm_b = 6356752.314;
var sm_EccSquared = 6.69437999013e-03;
var UTMScaleFactor = 0.9996;

/**
 * Componente que permite mostrar mensajes del sistema, de tipo informativo o de
 * error.
 * 
 * @param {string}
 *            mensaje - Descripcion del mensaje del sistema.
 * @param {number}
 *            tipoMensaje - Mensaje informativo: 1(Cons INFO), Mensaje de error:
 *            0(Cons ERROR).
 * @param {number}
 *            tiempo - Tiempo en milisegundos en que el mensaje sera visible.
 * @param {requestCallback}
 *            callback - Nombre de la funcion que se ejecutara al finalizar el
 *            proceso. De indicarse null, se omite su uso.
 */
function mostrarMensaje(mensaje, tipoMensaje, tiempo, callback) {
	var clase = null;

	if (tipoMensaje === INFO) {
		clase = 'messageboxok';
	} else {
		clase = 'messageboxerror';
	}

	$(".btn_cerrar").stop(true);

	$('.btn_cerrar').css("display", "none");

	$("#msgbox").removeClass("messageboxok");
	$("#msgbox").removeClass("messageboxerror");
	$("#cerrar").removeAttr('style');

	$("#msgbox").addClass(clase);

	$("#msgbox").html(mensaje);

	$('.btn_cerrar').css("display", "block");
	$('.btn_cerrar').css("opacity", 0);
	$(".btn_cerrar").animate({
		opacity : 1
	}, "fast", function() {
		$(".btn_cerrar").animate({
			opacity : 1
		}, tiempo, function() {
			$(".btn_cerrar").animate({
				opacity : 0
			}, "fast", function() {
				if (typeof callback === 'function') {
					callback();
				}
			});
		});
	});
}

/**
 * Componente que permite mostrar u ocultar la animacion(Gif) de precarga,
 * mientras se procesa una peticion en el servidor.
 * 
 * @param {number}
 *            tipoOper - Mostrar animacion de precarga: 1(Cons MOSTRAR), Ocultar
 *            animacion de precarga: 0(Cons OCULTAR).
 * @param {requestCallback}
 *            callback - Nombre de la funcion que se ejecutara al finalizar el
 *            proceso. De indicarse null, se omite su uso.
 */
function animarCargador(tipoOper, callback) {
	var retardo = 500;

	if (tipoOper === MOSTRAR) {
		$(".preloader").fadeIn(retardo, function() {
			if (callback === null) {
				$(".btn_cerrar").stop(true);
				$(".btn_cerrar").hide();
			}
		});
	}

	if (tipoOper === OCULTAR) {
		$(".preloader").fadeOut(retardo, function() {
			if (typeof callback === 'function') {
				$(".btn_cerrar").stop(true).fadeOut();
				$(".preloader").stop(true).fadeOut();
				callback.call();
			}
		});
	}

	if (callback === null) {
		$("#msgbox").html("").removeClass('messageboxerror');
		$("#cerrar").removeClass('btnCerrar');
		$(".btn_cerrar").hide();
		$(".btn_cerrar").stop(true);
	}
}

function cerrarAlert() {
	$(".btn_cerrar").hide();
	$(".btn_cerrar").stop(true);
}

/**
 * Componente que permite realizar peticiones de tipo Ajax al servidor.
 * 
 * @param {string}
 *            metodoEnv - Metodo de envio. Puede ser POST o GET.
 * @param {string}
 *            direccionUrl - Url del controlador que gestionara la peticion.
 * @param {string}
 *            jsonString - Objeto en formato Json que contiene los datos de la
 *            peticion.
 * @param {string}
 *            idBoton - Identificador del boton que dispara la peticion. Si no
 *            es disparado por un boton, sera null.
 * @param {requestCallback}
 *            callback - Nombre de la funcion que se ejecutara al finalizar el
 *            proceso. De indicarse null, se omite su uso.
 */
function consultarAjax(metodoEnv, direccionUrl, jsonString, contextPath, callback) {
	
	$.ajax({
		type : metodoEnv,
		data : jsonString,
		async : true,
		url : direccionUrl,
		contentType : "application/json",
		success : function(respuesta) {
			callback(respuesta);
		},
		error : function(respuesta) {
			var resp = null;

			if (respuesta.status == "404") {
				resp = {
					"mensajeRespuesta" : "El recurso solicitado no existe (HTTP: 404).",
					"codigoRespuesta" : "99"
				};
			} else {
				resp = {
					"mensajeRespuesta" : "Error no identificado.",
					"codigoRespuesta" : "99"
				};
			}
			callback(resp);
		}
	});
}

/**
 * Componente que permite abrir una ventana de dialogo.
 * 
 * @param {string}
 *            idDialog - Identificador de la ventana de dialogo (Por ejm.
 *            #MyModal).
 */
function abrirDialogo(idDialog, callback) {
	$("#msgbox").html("").removeClass('messageboxerror');
	$("#cerrar").removeAttr('style');
	$(".btn_cerrar").hide();

	$(idDialog).slideDown();

	$("#backDialog").fadeIn("fast", function() {
		if (typeof callback === 'function') {
			callback();
		}
	});
}

/**
 * Componente que permite cerrar una ventana de dialogo.
 * 
 * @param {string}
 *            idDialog - Identificador de la ventana de dialogo (Por ejm.
 *            #MyModal).
 */
function cerrarDialogo(idDialog, callback) {
	$(idDialog).hide();

	$("#backDialog").fadeOut("fast", function() {
		if (typeof callback === 'function') {
			callback();
		}
	});
}

/**
 * Componente que permite desactivar un boton de tipo submit.
 * 
 * @param {string}
 *            idBoton - Identificador del boton de tipo submit (Por ejm.
 *            #btnAceptar).
 * @param {number}
 *            accion - Activar boton: 1(Cons ENABLED), Desactivar boton: 0(Cons
 *            DISABLED).
 */
function cambiarEstadoBoton(idBoton, accion) {
	if (idBoton !== null) {
		if (accion === ENABLED) {
			$(idBoton).prop("disabled", null);
		} else {
			$(idBoton).prop("disabled", "disabled");
		}
	}
}

/**
 * Componente que permite construir un menu de opciones y subopciones.
 * 
 */
function menu() {
	var url = "generarMenu";


	$.getJSON(url, function(datos) {
		$.each(datos.menuPadre, function(key, valorP) {
			var img = 'img/' + valorP.icono;

			var menuPadre = "<li>" + "<a href='#'>" + "<img alt='' src=" + img
					+ " />" + valorP.descripcionP + "</a>" + "</li>";

			$(".nav").append(menuPadre);

			$.each(datos.menuHijo, function(key, valorH) {
				if (valorP.idMenuP === valorH.idPadre) {
					var menuHijo = "<ul class='dropdown-menu'>" + "<li>"
							+ "<a href=" + valorH.ruta + ">"
							+ valorH.descripcionH + "</a>" + "</li>" + "</ul>";
					$(".dropdown-menu").append(menuHijo);
				}
			});
		});
	});
}

/**
 * Funcion que permite obtener el valor de un elemento.
 * 
 * @param {string}
 *            selector - Selector del elemento a evaluar.
 * @returns {string}
 */
function obtenerValorElemento(selector) {
	var value = $(selector).val();
	return verificarValorElemento(value);
}

/**
 * Funcion que permite verificar el valor de un elemento.
 * 
 * @param {string}
 *            value - Valor del elemento.
 * @returns {string}
 */
function verificarValorElemento(value) {

	if (typeof value === "undefined") {
		value = null;
	}

	if ((value != null) && ($.trim(value) == "")) {
		value = null;
	}

	if (value != null) {
		value = $.trim(value);
	}

	return value;

}

/**
 * Funcion que permite verificar el valor numerico de un elemento.
 * 
 * @param {string}
 *            value - Valor numerico del elemento.
 * @returns {string}
 */
function verificarValorNumericoElemento(value) {

	if (typeof value === "undefined") {
		value = null;
	}

	if ((value != null) && ($.trim(value) == "")) {
		value = null;
	}

	if (value != null) {
		value = $.trim(value);
	}

	// numeros enteros y decimales
	if (!/^[0-9]+(\.[0-9]+)?$/.test(value)) {
		value = null;
	}

	return value;

}

/**
 * Funcion que permite validar una cadena frente a un conjunto de caracteres
 * permitidos.
 * 
 * @param {string}
 *            valor - Cadena de caracteres a evaluar.
 * @param {string}
 *            valores - Cadena de caracteres permitidos.
 * @returns {boolean}
 */
function validarValorAlfanumerico(valor, valores) {

	if (verificarValorElemento(valor) == null) {
		return false;
	}

	for (var i = 0; i < valor.length; i++) {
		if (valores.indexOf(valor.charAt(i)) == -1) {
			return false;
		}
	}

	return true;

}

/**
 * Funcion que permite validar una cadena, verificando que no tenga solo valores
 * numericos.
 * 
 * @param {string}
 *            valor - Cadena de caracteres a evaluar.
 * @returns {boolean}
 */
function validarValorNoSoloNumerico(valor) {

	var ind = false;

	if (verificarValorElemento(valor) == null) {
		return ind;
	}

	if (valor.length == 0) {
		return ind;
	}

	var valores = "0123456789";
	for (var i = 0; i < valor.length; i++) {
		if (valores.indexOf(valor.charAt(i)) == -1) {
			ind = true;
		}
	}

	return ind;

}

/**
 * Funcion que permite formatear un numero decimal que ha sido representado con
 * coma, y no con punto.
 * 
 */
function formatearNumeroDecimal(valor) {
	if (valor.indexOf(',') != -1) {
		valor = valor.replace(',', '.');
	}
	return valor;
}

/**
 * Funcion que permite mostrar un numero con comas en miles.
 * 
 * @param n
 * @returns
 */
function numeroConComas(n) {
	var parts = n.toString().split(".");
	return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			+ (parts[1] ? "." + parts[1] : "");
}

/**
 * Funcion que llena de datos un campo seleccionable
 * 
 * @param nombreControl
 * @param nuevaOpciones
 */
function construirSelect(nombreControl , nuevaOpciones){
	
	if(nuevaOpciones.length > 0 ){
		var select = $('select[name="'+ nombreControl +'"]');	
		$('option', select).remove();		
		$.each(nuevaOpciones, function(key, obj) {
			select.append('<option value="'+obj.codigo+'">'+obj.descripcion+'</option>');
		});
		select.val("");
	}
}

/**
 * Funcion que limpia todos los campos de un formulario
 * @param form
 */
function limpiarFormulario(form) {

	//-- Itera todos los elementos inputs del formulario que se mapean	
	$(':input', form).each(function() {

		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		
	    //-- Limpiar: text inputs, password inputs y textareas
	    if (type == 'text' || type == 'password' || tag == 'textarea'){
	        this.value = "";    	
	    }
    
	    //-- Limpiar: checkboxes y radios se necesita tener su check status limpio
	    else if (type == 'checkbox' || type == 'radio'){
	        this.checked = false;    	
	    }
    
	    //-- Limpiar: select (trabaj con select simples y multiples)
	    else if (tag == 'select'){
	      this.selectedIndex = '';
	    }
	    
  });
	//form.bootstrapValidator('resetForm', true);  
  
};

/**
 * Funcion que codifica una cadena en texto html por ejemplo: "pájaro" se convertira en "p&acute;jaro"
 * @returns {String}
 */
function htmlEncode(value){
    return $('<div/>').text(value).html();
}
/**
 * Funcion que decodifica un texto html en cadena por ejemplo: "p&acute;jaro"  se convertira en "pájaro"
 * @returns {String}
 */
function htmlDecode(value){
    return $('<div/>').html(value).text();
}

/**
 * Obtener todos los valores de los campos del formulario como objeto JSON
 * @returns {String}
 */
$.fn.serializeObject = function(){
    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push":     /^$/,
            "fixed":    /^\d+$/,
            "named":    /^[a-zA-Z0-9_]+$/
        };
    this.build = function(base, key, value){
        base[key] = value;
        return base;
    };
    this.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };
    $.each($(this).serializeArray(), function(){
        if(!patterns.validate.test(this.name)){
            return;
        }
        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;
        while((k = keys.pop()) !== undefined){
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }
        json = $.extend(true, json, merge);
    });
    return json;
};

/**
 * Obtener el dia actual en formato dd/mm/yyyy
 * @returns {String}
 */
function obtenerDiaActual(){
	var fullDate = new Date();
	var twoDigitDate = (fullDate.getDate().length === 1)? fullDate.getDate() : '0' + fullDate.getDate();
	var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
	return twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
}

/**
 * Obtener la resta de dias de una fecha en formato dd/mm/yyyy
 * @param fecha
 * @param dias
 * @returns {String}
 */
function restarDias(fecha, dias){
	 var partes = fecha.split("/");
	 
	 var dateOffset = (24*60*60*1000) * dias;
	 var diaNuevo = new Date(partes[2], partes[1] - 1, partes[0]);
	 diaNuevo.setTime(diaNuevo.getTime() - dateOffset);
	 
     var twoDigitDate = (diaNuevo.getDate().length === 1)? diaNuevo.getDate() : '0' + diaNuevo.getDate();
	 var twoDigitMonth = ((diaNuevo.getMonth().length+1) === 1)? (diaNuevo.getMonth()+1) : '0' + (diaNuevo.getMonth()+1);

	 return twoDigitDate + "/" + twoDigitMonth + "/" + diaNuevo.getFullYear(); 
}

/**
 * @param div Div a Mostrar / Ocultar
 * @param valorSeleccionado Valor que se quiere mostrar / ocultar
 * @param posicion Posicion que se selecciona en el combo (this.value)
 * @param opcion Mostrar / Ocultar
 * @param Funcion de validacion
 */
function mostrarCboDependiente(div,valorSeleccionado,posicion,opcion,callBack){
	var valida = false;
	var tDisplay;
	var tDisplayE;
	if(opcion == 'mostrar'){
		tDisplay = 'block';
		tDisplayE = 'none';
		
	}else if (opcion == 'ocultar'){
		tDisplay = 'none';
		tDisplayE = 'block';
		if(valorSeleccionado == '' ){
			valorSeleccionado = posicion;
		}
	}
	
	if (posicion == valorSeleccionado){
		if ( div !== null ) {
			div.style.display = tDisplay;
			valida = true;
		}
	}else{
		if ( div !== null ) {
			div.style.display = tDisplayE;
			valida = false;
		}
	}
	
	callBack(valida);
}

/**
 * Mover a una seccion de la pagina con animacion
 * @param id
 */
function scrollTo(id){
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top
    }, 'slow');
}

/**
 * Muestra el mensaje estilo Bootstrap Alert. El tipo de mensaje viene desde la inicializacion del componente donde puede ser "alert-success" "alert-danger"
 * mas informacion mirar el api de Bootstrap http://getbootstrap.com/components/#alerts
 * 
 * Finalidad del metodo, es mostrar el mensaje por cierta cantidad de segundos y ocultarlo.
 * Ejemplo:
 *	- Inicializa <div id="alertaMensajeSuccess" class="alert alert-success" role="alert" style="margin-top: 5px;display:none;"></div>
 *	- Se llama a este metodo para visualizarlo
 *
 * @param idMensaje
 * @param textoMensaje
 * @param tiempoMensaje
 */
function mostrarMensajeClasico(idMensaje, textoMensaje, tiempoMensaje){
	$(idMensaje).html(textoMensaje);
	$(idMensaje).show();
//	$(idMensaje).fadeTo(tiempoMensaje,500).slideUp(500, function(){
//		$(idMensaje).hide();	
//	});
}

/**
 * Muestra mensaje en una notificacion basado en Boostrap Alert. Finalidad del metodo, es mostrar notificaciones en la parte superior
 * derecha que esten por un tiempo minimo indicando una accion del sistema. Lo unico que se hace es colocar el metodo presente y listo se visualizara
 * la notificacion cada vez que se llame al metodo
 *  
 * @param textMensaje
 * @param tipoMensaje
 */
function mostrarMensajeFlotante(textMensaje, tipoMensaje){
	$.notify({
		message: textMensaje
	},{
		type: tipoMensaje
	});			
}

function alertaGenerica(op, msg, time) {
	if (time == undefined)
		time = 900;
	
}

/**
 * Convierte Deg a Radianes
 * @param deg
 * @returns {Number}
 */
function DegToRad(deg){
	return (deg / 180.0 * pi)
}

/**
 * Convierte Radianes a Deg
 * @param rad
 * @returns {Number}
 */
function RadToDeg(rad){
	return (rad / pi * 180.0)
}

/**
 * @param phi
 * @returns {Number}
 */
function ArcLengthOfMeridian(phi){
	var alpha, beta, gamma, delta, epsilon, n;
	var result;
	n = (sm_a - sm_b) / (sm_a + sm_b);
	alpha = ((sm_a + sm_b) / 2.0)
	* (1.0 + (Math.pow (n, 2.0) / 4.0) + (Math.pow (n, 4.0) / 64.0));
	beta = (-3.0 * n / 2.0) + (9.0 * Math.pow (n, 3.0) / 16.0)
	+ (-3.0 * Math.pow (n, 5.0) / 32.0);
	gamma = (15.0 * Math.pow (n, 2.0) / 16.0)
	+ (-15.0 * Math.pow (n, 4.0) / 32.0);
	delta = (-35.0 * Math.pow (n, 3.0) / 48.0)
	+ (105.0 * Math.pow (n, 5.0) / 256.0);
	epsilon = (315.0 * Math.pow (n, 4.0) / 512.0);
	result = alpha
	* (phi + (beta * Math.sin (2.0 * phi))
	+ (gamma * Math.sin (4.0 * phi))
	+ (delta * Math.sin (6.0 * phi))
	+ (epsilon * Math.sin (8.0 * phi)));
	return result;
}

/**
 * @param zone
 * @returns {Number}
 */
function UTMCentralMeridian(zone){
	var cmeridian;
	cmeridian = DegToRad (-183.0 + (zone * 6.0));
	return cmeridian;
}

/**
 * @param y
 * @returns {Number}
 */
function FootPointLatitude(y){
	var y_, alpha_, beta_, gamma_, delta_, epsilon_, n;
	var result;
	n = (sm_a - sm_b) / (sm_a + sm_b);
	alpha_ = ((sm_a + sm_b) / 2.0)
	* (1 + (Math.pow (n, 2.0) / 4) + (Math.pow (n, 4.0) / 64));
	y_ = y / alpha_;
	beta_ = (3.0 * n / 2.0) + (-27.0 * Math.pow (n, 3.0) / 32.0)
	+ (269.0 * Math.pow (n, 5.0) / 512.0);
	gamma_ = (21.0 * Math.pow (n, 2.0) / 16.0)
	+ (-55.0 * Math.pow (n, 4.0) / 32.0);
	delta_ = (151.0 * Math.pow (n, 3.0) / 96.0)
	+ (-417.0 * Math.pow (n, 5.0) / 128.0);
	epsilon_ = (1097.0 * Math.pow (n, 4.0) / 512.0);
	result = y_ + (beta_ * Math.sin (2.0 * y_))
	+ (gamma_ * Math.sin (4.0 * y_))
	+ (delta_ * Math.sin (6.0 * y_))
	+ (epsilon_ * Math.sin (8.0 * y_));
	return result;
}

/**
 * @param phi
 * @param lambda
 * @param lambda0
 * @param xy
 */
function MapLatLonToXY (phi, lambda, lambda0, xy){
	var N, nu2, ep2, t, t2, l;
	var l3coef, l4coef, l5coef, l6coef, l7coef, l8coef;
	ep2 = (Math.pow (sm_a, 2.0) - Math.pow (sm_b, 2.0)) / Math.pow (sm_b, 2.0);
	nu2 = ep2 * Math.pow (Math.cos (phi), 2.0);
	N = Math.pow (sm_a, 2.0) / (sm_b * Math.sqrt (1 + nu2));
	t = Math.tan (phi);
	t2 = t * t;
	l = lambda - lambda0;
	l3coef = 1.0 - t2 + nu2;
	l4coef = 5.0 - t2 + 9 * nu2 + 4.0 * (nu2 * nu2);
	l5coef = 5.0 - 18.0 * t2 + (t2 * t2) + 14.0 * nu2
	- 58.0 * t2 * nu2;
	l6coef = 61.0 - 58.0 * t2 + (t2 * t2) + 270.0 * nu2
	- 330.0 * t2 * nu2;
	l7coef = 61.0 - 479.0 * t2 + 179.0 * (t2 * t2) - (t2 * t2 * t2);
	l8coef = 1385.0 - 3111.0 * t2 + 543.0 * (t2 * t2) - (t2 * t2 * t2);
	xy[0] = N * Math.cos (phi) * l
	+ (N / 6.0 * Math.pow (Math.cos (phi), 3.0) * l3coef * Math.pow (l, 3.0))
	+ (N / 120.0 * Math.pow (Math.cos (phi), 5.0) * l5coef * Math.pow (l, 5.0))
	+ (N / 5040.0 * Math.pow (Math.cos (phi), 7.0) * l7coef * Math.pow (l, 7.0));
	xy[1] = ArcLengthOfMeridian (phi)
	+ (t / 2.0 * N * Math.pow (Math.cos (phi), 2.0) * Math.pow (l, 2.0))
	+ (t / 24.0 * N * Math.pow (Math.cos (phi), 4.0) * l4coef * Math.pow (l, 4.0))
	+ (t / 720.0 * N * Math.pow (Math.cos (phi), 6.0) * l6coef * Math.pow (l, 6.0))
	+ (t / 40320.0 * N * Math.pow (Math.cos (phi), 8.0) * l8coef * Math.pow (l, 8.0));
	return;
}

function MapXYToLatLon(x, y, lambda0, philambda){
	var phif, Nf, Nfpow, nuf2, ep2, tf, tf2, tf4, cf;
	var x1frac, x2frac, x3frac, x4frac, x5frac, x6frac, x7frac, x8frac;
	var x2poly, x3poly, x4poly, x5poly, x6poly, x7poly, x8poly;
	phif = FootPointLatitude (y);
	ep2 = (Math.pow (sm_a, 2.0) - Math.pow (sm_b, 2.0))
	/ Math.pow (sm_b, 2.0);
	cf = Math.cos (phif);
	nuf2 = ep2 * Math.pow (cf, 2.0);
	Nf = Math.pow (sm_a, 2.0) / (sm_b * Math.sqrt (1 + nuf2));
	Nfpow = Nf;
	tf = Math.tan (phif);
	tf2 = tf * tf;
	tf4 = tf2 * tf2;
	x1frac = 1.0 / (Nfpow * cf);
	Nfpow *= Nf;
	x2frac = tf / (2.0 * Nfpow);
	Nfpow *= Nf;
	x3frac = 1.0 / (6.0 * Nfpow * cf);
	Nfpow *= Nf;
	x4frac = tf / (24.0 * Nfpow);
	Nfpow *= Nf;
	x5frac = 1.0 / (120.0 * Nfpow * cf);
	Nfpow *= Nf;
	x6frac = tf / (720.0 * Nfpow);
	Nfpow *= Nf;
	x7frac = 1.0 / (5040.0 * Nfpow * cf);
	Nfpow *= Nf;
	x8frac = tf / (40320.0 * Nfpow);
	x2poly = -1.0 - nuf2;
	x3poly = -1.0 - 2 * tf2 - nuf2;
	x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2
	- 3.0 * (nuf2 *nuf2) - 9.0 * tf2 * (nuf2 * nuf2);
	x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;
	x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2
	+ 162.0 * tf2 * nuf2;
	x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * (tf4 * tf2);
	x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575 * (tf4 * tf2);
	philambda[0] = phif + x2frac * x2poly * (x * x)
	+ x4frac * x4poly * Math.pow (x, 4.0)
	+ x6frac * x6poly * Math.pow (x, 6.0)
	+ x8frac * x8poly * Math.pow (x, 8.0);
	philambda[1] = lambda0 + x1frac * x
	+ x3frac * x3poly * Math.pow (x, 3.0)
	+ x5frac * x5poly * Math.pow (x, 5.0)
	+ x7frac * x7poly * Math.pow (x, 7.0);
	return;
}

/**
 * @author arturo.oyola
 * @param lat
 * @param lon
 * @param zone
 * @param xy
 * @returns
 */
function LatLonToUTMXY(lat, lon, zone, xy){
	MapLatLonToXY (lat, lon, UTMCentralMeridian (zone), xy);
	xy[0] = xy[0] * UTMScaleFactor + 500000.0;
	xy[1] = xy[1] * UTMScaleFactor;
	if (xy[1] < 0.0)
	xy[1] = xy[1] + 10000000.0;
	return zone;
}
/**
 * @author arturo.oyola
 * @param x	
 * @param y
 * @param zone
 * @param southhemi
 * @param latlon
 * @returns
 */
function UTMXYToLatLon (x, y, zone, southhemi, latlon){
	var cmeridian;
	x -= 500000.0;
	x /= UTMScaleFactor;
	if (southhemi)
	y -= 10000000.0;
	y /= UTMScaleFactor;
	cmeridian = UTMCentralMeridian (zone);
	MapXYToLatLon (x, y, cmeridian, latlon);
	return;
}
/**
 * @author arturo.oyola
 * @param latitud
 * @param longitud
 * @returns
 */
function ValidarPuntosGeograficos(latitud,longitud){
	var regExpLat = /^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,25})?))$/;
	var regExpLng = /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,25})?))$/;
	if(!(regExpLat.test(latitud))){
		return false;
	}
	if(!(regExpLng.test(longitud))){
		return false;
	}
	return true;
}

function distribuidoTipoRegimen(value){
	var tipoRegimen;
	if(value == 1){
		tipoRegimen = "R\xe9gimen General";
	}else if(value == 2){
		tipoRegimen = "R\xe9gimen RUS";
	}else if(value == 3){
		tipoRegimen = "R\xe9gimen Especial";
	}
	return tipoRegimen;
}
/**
 * @author arturo.oyola
 * @param r
 * @param n
 * @param p
 * @returns
 */
function PMT(r, n, p) {
	var pmt = 0;
    var r1 = parseFloat(r) + parseFloat(1);
    pmt = ( p * Math.pow(parseFloat(r1), n) ) * r / ((1) * (1 - Math.pow(parseFloat(r1), n)));    
	return parseFloat(pmt).toFixed(2);
}

function cleanCbm(idCombo){
	$(idCombo+" option").remove();
	var selectOption = '<option value="">Seleccionar</option>';
	$(idCombo).append(selectOption);
}

function fnValidarFormatoWebPymesWithOutComma(variable){
	var f14d2	= new RegExp(/^(-?[0-9]{1,14})(\.[0-9]{1,2})?$/);

	if(f14d2.test(variable)){
	return true;
	}else{
	return false;
	}
}

function formatMontoAll(monto){
	if(monto != null){
		var textMonto = monto.toString();
		monto = textMonto.trim().replace(/\,/g,'');//Suprimimos las comas antes de parsear el monto
		monto = parseFloat(monto).toFixed(2).toString();
		if(monto.trim() == ""){
			monto ="0.00";
		}else{
			var text = monto.trim();
			if(fnValidarFormatoWebPymesWithOutComma(text)){
				var num = monto.trim().replace(/\,/g,'');
				if(!isNaN(num)){
					num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					num = num.split('').reverse().join('').replace(/^[\,]/,'');
					if(num.indexOf('.') == -1 && num.length<=12){
						num = num.concat('.00');
					}else{
						if(num.toString().split('.')[1] != undefined && num.toString().split('.')[1].length == 1){
							num = num.concat('0');
						}
					}
					monto = num;
				}
			}
		}
	}
	return monto
}

function fnBorrarFormatoMonto(monto){
	if(monto!=null && monto != undefined){
		monto = monto.toString();
		monto = monto.replace(/\,/g,'');
	}
	return monto;
}
