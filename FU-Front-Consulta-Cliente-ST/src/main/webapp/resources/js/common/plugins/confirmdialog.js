/**
 * Module for displaying "Waiting for..." dialog using Bootstrap
 *
 * @author Eugene Maslovich <ehpc@em42.ru>
 */

var confirmDialog = confirmDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $confirmDialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +

		      '<div class="warning alert-warning" style="border-radius: 6px; padding: 5px;">' +
		      		'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
		      		'<strong><span class="glyphicon glyphicon-alert"></span>&nbsp;&nbsp;Info</strong>'+
		      '</div>' +
		      
		      '<div class="modal-body">' +
		      		'<h4 style="margin:0; padding:0"></h4>' +
		      '</div>' +
		      '<div class="modal-footer" >'+
				'<button type="button" class="btn btn-primary" id="btn_confirm_procesar">Aceptar</button>' +
				'<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>' +
			  '</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Error';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null, // This callback runs after the dialog was hidden
				onProcesar: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$confirmDialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);


			$confirmDialog.find('h4').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$confirmDialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($confirmDialog);
				});
			}
			
			if (typeof settings.onProcesar === 'function') {
				
				$confirmDialog.unbind('click');
				$confirmDialog.on('click','#btn_confirm_procesar', function(){
					settings.onProcesar.call($confirmDialog);
				});
			}
			
			// Opening dialog
			$confirmDialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$confirmDialog.modal('hide');
		},
		

	};

})(jQuery);