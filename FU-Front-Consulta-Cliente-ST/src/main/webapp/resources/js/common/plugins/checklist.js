function disableDoiCentral() {
	
	if($('#radioTipoDoi').prop('checked')){
		$('#codigoCentral').val("");
		$('#codigoCentral').attr('disabled',  true);
		$('#tipoDoi').attr('disabled', false);
		$('#doi').attr('disabled', false);
        
		$('#codigoCentral').closest('.input-group')
	        .removeClass('has-error')
	        .removeClass('has-success');
		
		$('#codigoCentral1')
	    	.removeClass('glyphicon-remove')
	    	.removeClass('glyphicon-ok'); 
        
	}else{
		$('#tipoDoi').attr('disabled', true);
		$('#doi').val("");
		$('#doi').attr('disabled', true);
		$('#codigoCentral').attr('disabled',  false);
		
		$('#doi').closest('.input-group')
	        .removeClass('has-error')
	        .removeClass('has-success');
		
		$('#doi1')
	    	.removeClass('glyphicon-remove')
	    	.removeClass('glyphicon-ok'); 
	}
}	
		
function validaCampo(element) {
	
	if (!$(element).is(':visible')){ //Si el campo no se muestra no lo validamos
        var id_attr = element + "1";
        $(element).closest('.input-group')
	        .removeClass('has-error')
	        .removeClass('has-success');
        $(id_attr)
        	.removeClass('glyphicon-remove')
        	.removeClass('glyphicon-ok'); 
        return true;
	}else if($.trim($(element).val()).length) { //Si el elemento esta ok
        var id_attr_1 = element + "1";
        $(element).closest('.input-group')
	        .removeClass('has-error')
	        .addClass('has-success');
        $(id_attr_1)
        	.removeClass('glyphicon-remove')
        	.addClass('glyphicon-ok'); 
        return true;
	}else{ //Si el elemento tiene error
        var id_attr_2 = element + "1";
        $(element).closest('.input-group')
	        .removeClass('has-success')
	        .addClass('has-error');
        $(element).focus();
        
        $(id_attr_2)
        	.removeClass('glyphicon-ok')
        	.addClass('glyphicon-remove');
        return false;
	}
}
		