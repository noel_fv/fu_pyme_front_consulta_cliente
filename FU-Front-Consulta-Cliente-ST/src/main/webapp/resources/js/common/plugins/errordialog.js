/**
 * Module for displaying "Waiting for..." dialog using Bootstrap
 *
 * @author Eugene Maslovich <ehpc@em42.ru>
 */

var errorDialog = errorDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialogError = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +

		      '<div class="alert alert-danger fade in">' +
		      		'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
		      		'<strong><span class="glyphicon glyphicon-alert"></span>&nbsp;&nbsp;Error</strong>'+
		      '</div>' +
		      
		      '<div class="modal-body">' +
		      		'<h4 style="margin:0; padding:0"></h4>' +
		      '</div>' +
		      '<div class="modal-footer" >'+
				'<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>' +
			  '</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Error';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialogError.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);


			$dialogError.find('h4').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialogError.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialogError);
				});
			}
			// Opening dialog
			$dialogError.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialogError.modal('hide');
		}
	};

})(jQuery);