package pe.bbvacontinental.fu.util;

import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase que obtiene el codigo de usuario enviado por webseal
 * 
 * @author huber.quinto
 *
 */
@SuppressWarnings("rawtypes")
public class ServiciosSeguridadBbva {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private static final String NO_AUTENTICADO="No Autenticado";
	
	HttpServletRequest req;
	String usuario;
	String password;
	String fuenteAutenticacion;
	List<String> vectorGrupos;

	public ServiciosSeguridadBbva(HttpServletRequest req) {
		this.req = req;
	}

	public String getFuenteAutenticacion() {
		return this.fuenteAutenticacion;
	}

	public String getPassword() {
		return this.password;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public List<String> getVectorGrupos() {
		return this.vectorGrupos;
	}

	@SuppressWarnings("unchecked")
	public void obtenerId() {
		this.usuario = NO_AUTENTICADO;
		this.fuenteAutenticacion = "Ninguna";
		boolean autenticacionOK = false;
		String policyUser ="";
		String policyGroup ="";
		StringTokenizer tokenizerGrupos = null;
		String aux = null;
		String cabeceraIv ="";
		cabeceraIv = this.req.getHeader("iv-user");
		if (cabeceraIv != null) {
			policyUser = cabeceraIv;
			cabeceraIv = this.req.getHeader("iv-groups");
			if (cabeceraIv != null) {
				policyGroup = cabeceraIv;
				this.vectorGrupos = new Vector();
				tokenizerGrupos = new StringTokenizer(policyGroup, ",");
				while (tokenizerGrupos.hasMoreTokens()) {
					aux = tokenizerGrupos.nextToken();
					String a=aux.substring(1, aux.length() - 1);
					logger.info("info: {}",a);
					this.vectorGrupos.add(aux.substring(1, aux.length() - 1));
				}
			}
			this.usuario = policyUser;
			this.fuenteAutenticacion = "Policy";
			autenticacionOK = true;
		}
		String cabeceraSciv ="";
		cabeceraSciv = this.req.getHeader("sciv-user");
		if (cabeceraSciv != null) {
			policyUser = cabeceraSciv;
			cabeceraSciv = this.req.getHeader("sciv-groups");
			if (cabeceraSciv != null) {
				policyGroup = cabeceraSciv;
				this.vectorGrupos = new Vector();
				tokenizerGrupos = new StringTokenizer(policyGroup, ",");
				while (tokenizerGrupos.hasMoreTokens()) {
					aux = tokenizerGrupos.nextToken();
					String a=aux.substring(1, aux.length() - 1);
					logger.info(a);
					this.vectorGrupos.add(aux.substring(1, aux.length() - 1));
				}
			}
			this.usuario = policyUser;
			this.fuenteAutenticacion = "Policy";
			autenticacionOK = true;
		}
		if (!autenticacionOK) {
			this.fuenteAutenticacion = "Aplicacion";
			String usuarioAplicacion = "";
			String passAplicacion = "";
			usuarioAplicacion = this.req.getParameter("username") == null ? "":this.req.getParameter("username");
			passAplicacion = this.req.getParameter("password");
			this.usuario = usuarioAplicacion.toUpperCase();
			this.password = passAplicacion;
		}
	}
}