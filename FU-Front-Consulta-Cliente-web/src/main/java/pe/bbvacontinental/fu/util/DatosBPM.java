package pe.bbvacontinental.fu.util;

import java.io.Serializable;
import java.util.Map;

public class DatosBPM implements Serializable{

	private static final long serialVersionUID = 1L;
	private Map<String, Object> datosParamBPM;

	public Map<String, Object> getDatosParamBPM() {
		return datosParamBPM;
	}

	public void setDatosParamBPM(Map<String, Object> datosParamBPM) {
		this.datosParamBPM = datosParamBPM;
	}
}
