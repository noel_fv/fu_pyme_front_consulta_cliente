package pe.bbvacontinental.fu.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.validation.FieldError;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;
import pe.bbvacontinental.fu.model.excepcion.MensajeErrorCollection;
import pe.bbvacontinental.fu.model.excepcion.MensajeErrorModel;

public abstract class ManejadorExcepcionBase {
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Value("${header.codigo.estado.recargar.frontend}")
	protected int codigoErrorParaRecargarFrontEnd;

	@Value("${header.codigo.error}")
	protected String codigoErrorHeader;

	@Value("${mensaje.error.general}")
	protected String mensajeErrorGenereral;

	@Autowired
	private MessageSource messageSource;

	protected MensajeErrorCollection obtenerErrores(ExcepcionBBVA excepcionBBVA) {
		logger.info("obtenerErrores: {}","inicio");
		ArrayList<MensajeErrorModel> listaDeErrores = new ArrayList<>();
		if (excepcionBBVA.getErrores() != null) {
			List<FieldError> fieldErrors = excepcionBBVA.getErrores().getFieldErrors();
			for (FieldError fieldError : fieldErrors) {
				MensajeErrorModel errorElementoDeFormulario = new MensajeErrorModel();
				String errorDeProperty = obtenerErrorDeProperty(fieldError.getCode() + "." + fieldError.getObjectName() + "." + fieldError.getField());
				errorElementoDeFormulario.setCodigoError(fieldError.getField());
				errorElementoDeFormulario.setMensajeError(errorDeProperty);
				listaDeErrores.add(errorElementoDeFormulario);
			}
		}

		MensajeErrorCollection errorFormulario = new MensajeErrorCollection();
		if (excepcionBBVA.getMessage() != null && excepcionBBVA.getMessage() != "") {
			MensajeErrorModel errorElementoDeFormulario = new MensajeErrorModel();
			errorElementoDeFormulario.setCodigoError(excepcionBBVA.getMessage());
			errorElementoDeFormulario.setMensajeError(obtenerErrorDeProperty(excepcionBBVA.getMessage()));
			listaDeErrores.add(errorElementoDeFormulario);
		}

		errorFormulario.setErrores(listaDeErrores);
		logger.info("obtenerErrores: {}","fin");
		return errorFormulario;
	}

	//@Cacheable(value="errores")
	public String obtenerErrorDeProperty(String codigoMensajeDeError) {
		logger.info("obtenerErrorDeProperty: {}","inicio");
		Locale idioma = new Locale("es_PE");
		String mensajeError = messageSource.getMessage(codigoMensajeDeError, null, mensajeErrorGenereral, idioma);
		logger.info("obtenerErrorDeProperty: {}","fin");
		return mensajeError;
	}

}
