package pe.bbvacontinental.fu.web.controller;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.validation.Validator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import pe.bbvacontinental.fu.common.enumeration.AtributosSesionEnum;
import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.util.StateBean;
import pe.bbvacontinental.fu.model.util.StateBean.StateType;
import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.model.view.LocalCacheModel;
import pe.bbvacontinental.fu.model.view.UsuarioPortal;
import pe.bbvacontinental.fu.service.BuscadorClienteService;
import pe.bbvacontinental.fu.service.ConsultaIDMService;
import pe.bbvacontinental.fu.service.DatosNegocioService;
import pe.bbvacontinental.fu.service.EstadoService;
import pe.bbvacontinental.fu.service.GiroNegocioService;
import pe.bbvacontinental.fu.service.InvocarPersonaJuridicaService;
import pe.bbvacontinental.fu.service.InvocarPersonaNaturalService;
import pe.bbvacontinental.fu.service.ParametroAplicativoService;
import pe.bbvacontinental.fu.service.SolicitudService;
import pe.bbvacontinental.fu.service.TerritorioService;
import pe.bbvacontinental.fu.service.TipoDocumentoService;
import pe.bbvacontinental.fu.service.TipoMonedaService;
import pe.bbvacontinental.fu.service.TipoPersonaService;
import pe.bbvacontinental.fu.util.DatosBPM;
import pe.bbvacontinental.fu.util.ServiciosSeguridadBbva;

public abstract class AbstractBase {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	protected DatosNegocioService datosNegocioService;

	@Autowired
	protected TipoMonedaService tipoMonedaService;

	@Autowired
	protected GiroNegocioService giroNegocioService;

	@Autowired
	protected EstadoService estadoService;

	@Autowired
	protected TerritorioService territorioService;

	@Autowired
	protected ConsultaIDMService consultaIDMService;

	@Autowired
	protected SolicitudService solicitudService;

	@Autowired
	protected ParametroAplicativoService parametroAplicativoService;

	@Autowired
	protected Environment environment;

	@Autowired
	protected TipoPersonaService tipoPersonaService;

	@Autowired
	protected TipoDocumentoService tipoDocumentoService;

	@Autowired
	protected BuscadorClienteService buscadorClienteService;

	@Autowired
	@Qualifier("buscarClienteValidator")
	protected Validator buscarClienteValidator;

	@Autowired
	protected InvocarPersonaNaturalService invocarPersonaNaturalService;

	@Autowired
	protected InvocarPersonaJuridicaService invocarPersonaJuridicaService;

	@Autowired
	protected ObjectMapper objectMapper;
	
	private Gson gSon= new Gson();

	@Value("${error.excepcion.generico.fu}")
	protected String errorExcepcion;

	@Value("${tipo.persona.requerido}")
	protected String tipoPersonaRequerido;

	@Value("${sin.acceso.flujo.persona.natural}")
	protected String sinAccesoPN;

	@Value("${pertenece.circuito.bec}")
	protected String perteneceBEC;

	@Value("${fielunico.cc.modo.dummy}")
	protected String esModoDummy;

	protected void guardarUsuarioEnSession(HttpServletRequest request){
		/*Remove this call to "equals"; comparisons between unrelated types always return false
		 * correccion: add getTypeState */
		if (guadarDatosUsuarioIDM(request).getTypeState().equals(StateType.ERROR)) {
			throw new ExcepcionBBVA("[guardarUsuarioEnSession] Error al guardar el usuario en la sesion");
		}
	}

	/**
	 * Registra los datos del usuario IDM en la session Obtiene el codigo de
	 * usaurio del
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public StateBean guadarDatosUsuarioIDM(HttpServletRequest request) {

		StateBean rpta = new StateBean();
		rpta.setTypeState(StateType.ERROR);
		long tiempoINI = System.currentTimeMillis();
		logger.info("INI {}","FUController.guadarDatosUsuarioIDM ");
		boolean listo = false;
		UsuarioPortal usuarioPortal = null;
		HttpSession session = request.getSession();
		try {
			ServiciosSeguridadBbva serviciosSeguridadBbva = new ServiciosSeguridadBbva(request);
			serviciosSeguridadBbva.obtenerId();
			logger.info("serviciosSeguridadBbva.getUsuario(): {}",serviciosSeguridadBbva.getUsuario());

			if (session.getAttribute(AtributosSesionEnum.USUARIO_FU.getCodigo()) != null) {
				usuarioPortal = (UsuarioPortal) session.getAttribute(AtributosSesionEnum.USUARIO_FU.getCodigo());

				if (serviciosSeguridadBbva.getUsuario() != null && serviciosSeguridadBbva.getUsuario().toUpperCase().startsWith("P")) {
					listo = true;
					rpta.setTypeState(StateType.OK);
					if (!serviciosSeguridadBbva.getUsuario().equalsIgnoreCase(usuarioPortal.getCodigoUsuario())) {
						usuarioPortal = getUsuarioIDM(request, serviciosSeguridadBbva.getUsuario());
						session.setAttribute(AtributosSesionEnum.USUARIO_FU.getCodigo(), usuarioPortal);
					}
				}
			}

			if (!listo) {

				if (!"No Autenticado".equals(serviciosSeguridadBbva.getUsuario())) {
					usuarioPortal = getUsuarioIDM(request, serviciosSeguridadBbva.getUsuario());
					session.setAttribute(AtributosSesionEnum.USUARIO_FU.getCodigo(), usuarioPortal);
					logger.info("FUController.guadarDatosUsuarioIDM {}","usuarioPortal Guardado en session ");
					rpta.setTypeState(StateType.OK);
				} else {
					// NO hay autenticacion
					logger.error("FUController.guadarDatosUsuarioIDM serviciosSeguridadBbva Usuario no existe. {}",serviciosSeguridadBbva.getUsuario());
				}
			}
		} catch (Exception e) {
			logger.error("FU Error FUBFUControlleraseController.guadarDatosUsuarioIDM - Exception: ", e);
		} finally {
			logger.info("FIN FUController.guadarDatosUsuarioIDM [ {} ms]", (System.currentTimeMillis() - tiempoINI));
		}
		
		if(rpta.getTypeState().equals(StateType.OK) && usuarioPortal != null && StringUtils.isEmpty(usuarioPortal.getIP())){
			usuarioPortal.setIP(devolverRemoteAddr(request));
			session.setAttribute(AtributosSesionEnum.USUARIO_FU.getCodigo(), usuarioPortal);
		}
		return rpta;
	}
	
	private String devolverRemoteAddr(HttpServletRequest request){
		String remoteAddr = request.getHeader("X-FORWARDED-FOR");
		if (StringUtils.isEmpty(remoteAddr)) {
			remoteAddr = request.getRemoteAddr();
		}
		return remoteAddr;
	}

	private UsuarioPortal getUsuarioIDM(HttpServletRequest request, String registroUsuario)  throws MalformedURLException,JsonProcessingException {
		String remoteAddr = "";

		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}

		GenericModel genericModel = new GenericModel();
		genericModel.setIpUsuario(remoteAddr);
		genericModel.setNombreAplicacion("FILE-UNICO-NEGOCIOS");
		genericModel.setIdUsuario(registroUsuario);

		UsuarioPortal usuarioPortal = consultaIDMService.obtenerUsuarioPortal(genericModel);
		usuarioPortal.setIP(remoteAddr);
		return usuarioPortal;
	}

	/**
	 * Las invocaciones por Rest Api al BPM se hacen mediante estos parametros
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> obtenerParamAccesoBPMPorRestAPI(HttpServletRequest request, String cookieSSO) {

		Map<String, Object> datosParamBPM = (HashMap<String, Object>) request.getSession().getAttribute("datosConexionBPMSSO");
		try {
			if (datosParamBPM == null) {

				GenericModel genericModel = parametroAplicativoService.getParametroAplicativo(ParametrosEnum.SERVICIO_ACCESO_BPM.getCodigo());

				datosParamBPM = new HashMap<>();
				datosParamBPM.put("IP_BPM", genericModel.getRespuesta1());
				datosParamBPM.put("PORT_BPM", new Integer(genericModel.getRespuesta2()));
				datosParamBPM.put("SSL_VAL", new Boolean(genericModel.getRespuesta3()));
				
				DatosBPM objDatosBPM = new DatosBPM();
				objDatosBPM.setDatosParamBPM(datosParamBPM);

				request.getSession().setAttribute("datosConexionBPMSSO", objDatosBPM);
			}

			logger.info("  - obtenerParamAccesoBPMPorRestAPI - ingresaActualizarSolicitud request.getParameter(cookiesso): {}", request.getParameter("cookiesso"));
			logger.info("  - obtenerParamAccesoBPMPorRestAPI - ingresaActualizarSolicitud cookieSSO: {}",cookieSSO);

			datosParamBPM.put("COOKIE_BPM", cookieSSO == null ? request.getParameter("cookiesso") : cookieSSO);

		} catch (Exception e) {
			logger.error("FU Error FUBaseController.obtenerParamAccesoBPMPorRestAPI", e);
			throw e;
		}

		return datosParamBPM;
	}

	/**
	 * Para Balanceador BPM, las invocacicones a servicios a medida hechos en
	 * BPM se invocan por este medio
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map obtenerParamAccesoBalanceador(HttpServletRequest request) {

		// Obtenemos parametros para llamar a BPM
		Map<String, Object> datosParamBPM = (HashMap<String, Object>) request.getSession().getAttribute("datosConexionBalancer");

		if (datosParamBPM == null) {

			TbParametrosAplicativo registroIPPuertoBalancer = parametroAplicativoService.getParameterByCode(ParametrosEnum.SERV_BALANCER_BPM.getCodigo());
			datosParamBPM = new HashMap<>();
			datosParamBPM.put("IP_BALANCEADOR", registroIPPuertoBalancer.getValor1());
			datosParamBPM.put("PORT_IP_BALANCEADOR", new Integer(registroIPPuertoBalancer.getValor2()));

			DatosBPM objDatosBPM = new DatosBPM();
			objDatosBPM.setDatosParamBPM(datosParamBPM);
			
			request.getSession().setAttribute("datosConexionBalancer", objDatosBPM);
		}

		return datosParamBPM;
	}

	/**
	 * Metodo que retorna el objeto String en formato JSON de los datos que se
	 * guardaran en LocalStrage. Reemplaza al HttpSession
	 * 
	 * @param usuarioPortal,
	 *            datos del usuario logueado
	 * @param clienteModel,
	 *            datos del Cliente
	 * @param params,
	 *            datos adicionales que se desea grabar en localStorage.
	 * @return cadena JSON con datos a guardar en LocalStorage
	 */
	public String obtenerLocalStorage(UsuarioPortal usuarioPortal, ClienteModel clienteModel, Map<String, String> params, HttpServletRequest request) {

		LocalCacheModel localCacheModel = new LocalCacheModel();
		localCacheModel.setUsuarioPortal(usuarioPortal);
		localCacheModel.setClienteModel(clienteModel);
		localCacheModel.setMapDatos(params);

		request.getSession().setAttribute(AtributosSesionEnum.LOCALCACHEMODEL.getCodigo(), localCacheModel);
		String cadena = "";
		try {
			cadena = gSon.toJson(localCacheModel);
		} catch (Exception e) {
			/* */
		}
		return cadena;
	}

	protected UsuarioPortal obtenerUsuarioPortal(HttpServletRequest request){
		guardarUsuarioEnSession(request);
		return (UsuarioPortal) request.getSession().getAttribute(AtributosSesionEnum.USUARIO_FU.getCodigo());
	}

	/**
	 * Metodo que retorna el objeto que guarda datos de session.
	 * 
	 * @param cadenaJSON,
	 *            mensaje JSON
	 * @param clase,
	 *            clase a la
	 * @return
	 */
	public LocalCacheModel obtenerLocalCache(String cadenaJSON, HttpSession session) {

		LocalCacheModel cache = null;

		if (cadenaJSON != null && !cadenaJSON.isEmpty()) {
			try {
				cache = gSon.fromJson(cadenaJSON, LocalCacheModel.class);
			} catch (Exception e) {
				cache = null;
			}
		}

		if (cache == null && session.getAttribute(AtributosSesionEnum.LOCALCACHEMODEL.getCodigo()) != null) {
			cache = (LocalCacheModel) session.getAttribute(AtributosSesionEnum.LOCALCACHEMODEL.getCodigo());
		}
		if (cache == null) {
			logger.error(" [obtenerLocalCache] no se obtuvo cache model ni por string ni por {}","httpsession");
		}
		return cache;
	}
}