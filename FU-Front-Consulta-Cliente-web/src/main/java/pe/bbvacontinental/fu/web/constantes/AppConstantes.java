package pe.bbvacontinental.fu.web.constantes;

public class AppConstantes {

	private AppConstantes(){}
	
	public static final String COD_GENERAL_MONTO_ARRIENDO = String.valueOf("0");
	public static final String RESOURCES_LOCATION = "/resources/";
	public static final String RESOURCE_HANDLER = "/**";
	public static final String WEB_INF_TILES_XML = "/WEB-INF/**/tiles.xml";
	public static final String JSP_EXTENSION = ".jsp";
	public static final String WEB_INF_JSP = "/WEB-INF/jsp/";
	public static final String RESOURCE_MENSAJES = "properties/mensajes";
	public static final String RESOURCE_MENSAJES_PROPERTIES = RESOURCE_MENSAJES + ".properties";
	public static final String PROPERTIES_LOCATION = "/WEB-INF/properties/";
	public static final String PROPERTIES_EXTENSION = ".configuracion.properties";
	public static final String ENCODING = "UTF-8";

	
	public static final String NOMBRE_ARCHIVO_XML_DIGNITALIZACION="datosSolicitud.xml";
	public static final String NOMBRE_ARCHIVO_TEMPLATE_DIGNITALIZACION="template_digitalizacion_v1.xml";
	
	
	

}
