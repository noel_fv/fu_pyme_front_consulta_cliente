package pe.bbvacontinental.fu.web.controller.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pe.bbvacontinental.fu.model.view.ClienteModel;

@Component
@Scope("prototype")
public class BuscarClienteValidator implements Validator{

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		ClienteModel buscarClienteModel = (ClienteModel) target;
		
		if(StringUtils.isEmpty(buscarClienteModel.getTerritorio())){
			errors.reject("PROBANDO");
		}
		
	}

}
