package pe.bbvacontinental.fu.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.model.view.RespuestaCabeceraModel;
import pe.bbvacontinental.fu.model.view.RespuestaGeneralModel;

@RestController
@RequestMapping(value = { "/fu/cache" })
public class CacheController {

	@Autowired
	private ParametroAplicativoDAO parametroAplicativoDAO;

	@RequestMapping(value = "/limpiar", method = RequestMethod.GET)
	public RespuestaGeneralModel limpiarCache() {
		RespuestaGeneralModel respuestaGeneralModel = new RespuestaGeneralModel();
		RespuestaCabeceraModel respuestaCabeceraModel = new RespuestaCabeceraModel("ERROR", "500", "Error al limpliar la cache");
		try{
			parametroAplicativoDAO.actualizarCacheLocal();
			respuestaCabeceraModel = new RespuestaCabeceraModel("OK", "200", "Se ha limpliado la cache | cantidad de datos: "+parametroAplicativoDAO.getCache().size());
		}catch(Exception e){
			/* */
		}
		respuestaGeneralModel.setHead(respuestaCabeceraModel);
		return respuestaGeneralModel;
	}
	
	@RequestMapping(value = "/size", method = RequestMethod.GET)
	public String sizeCache() {
		return parametroAplicativoDAO.getCache().size()+"";
	}
}
