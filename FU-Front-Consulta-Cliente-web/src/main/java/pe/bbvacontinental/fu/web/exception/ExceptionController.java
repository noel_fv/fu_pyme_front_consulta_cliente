package pe.bbvacontinental.fu.web.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;
import pe.bbvacontinental.fu.model.view.RespuestaCabeceraModel;
import pe.bbvacontinental.fu.model.view.RespuestaGeneralModel;

@ControllerAdvice
public class ExceptionController {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@ExceptionHandler(ExcepcionBBVA.class)
	@ResponseBody
	public Object handleCustomException(ExcepcionBBVA ex) {
		RespuestaCabeceraModel head = new RespuestaCabeceraModel("99", ex.getCodError(), ex.toString());
		
		logger.info("ERROR ExcepcionBBVA {} {}", ex.getCodError(), ex);
		return new RespuestaGeneralModel(head, null);
	}
	
	@ExceptionHandler(BBVADBException.class)
	@ResponseBody
	public Object handleCustomException(BBVADBException ex) {
		RespuestaCabeceraModel head = new RespuestaCabeceraModel("99", ex.getCodError(), ex.toString());
		
		logger.info("ERROR BBVADBException {} {}", ex.getCodError(), ex);
		return new RespuestaGeneralModel(head, null);
	}
}
