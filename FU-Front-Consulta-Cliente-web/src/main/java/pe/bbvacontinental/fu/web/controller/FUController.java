package pe.bbvacontinental.fu.web.controller;

import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.bbvacontinental.fu.dto.OficinaDTO;
import pe.bbvacontinental.fu.dto.TerritorioDTO;
import pe.bbvacontinental.fu.model.TbGiroNegocio;

@SuppressWarnings("unchecked")
@Controller
public class FUController extends AbstractBase {
	
	protected final Logger log = LoggerFactory.getLogger(getClass());
	private static final String TERRITORIO="listaTerriorios";
	
	@RequestMapping(value = "/lista-giro-negocio/", method = RequestMethod.GET)
	@ResponseBody
	public List<TbGiroNegocio> obtenerListaGiroNegocio() {
		return giroNegocioService.findAllActive();
	}

	@RequestMapping(value = "/lista-territorio/", method = RequestMethod.GET)
	@ResponseBody
	public List<TerritorioDTO> obtenerListaTerritorio(HttpServletRequest request) throws MalformedURLException {

		log.info(" obtenerListaTerritorio: {}",".");

		String codArea =null; //(String) request.getSession().getAttribute("codArea")
		codArea = "2664";

		HttpSession session = request.getSession();
		List<TerritorioDTO> listaTerritorio = null;

		if (session.getAttribute(TERRITORIO) == null) {

			listaTerritorio = territorioService.listarTerritorio(codArea);
			session.setAttribute(TERRITORIO, listaTerritorio);

		} else {

			listaTerritorio = (List<TerritorioDTO>) session.getAttribute(TERRITORIO);
		}

		return listaTerritorio;

	}

	@RequestMapping(value = "/lista-oficina_territorio/{codTerritorio}", method = RequestMethod.GET)
	@ResponseBody
	public List<OficinaDTO> obtenerListaOficina(@PathVariable(value = "codTerritorio") String codTerritorio, HttpServletRequest request) {

		HttpSession session = request.getSession();

		List<TerritorioDTO> listaTerritorio = (List<TerritorioDTO>) session.getAttribute(TERRITORIO);

		// Buscamos el territorio
		/* add new TerritorioDTO - JRomero - 20/05/2018 */
		TerritorioDTO territorioDTOSel = new TerritorioDTO();
		for (TerritorioDTO territorioDTO : listaTerritorio) {
			if (territorioDTO.getId().equals(codTerritorio)) {
				territorioDTOSel = territorioDTO;
				break;
			}
		}

		
		return territorioDTOSel.getListaOficinas();
	}
	
	@RequestMapping(value = "/cerrar-session", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String cerrarSession(HttpSession session) {

		String mensaje = "error";
		try{
			Enumeration<String> atributosSession = session.getAttributeNames();
			removerAtributoSession(atributosSession, session);
			session.invalidate();
			mensaje = "session cerrada";
		}catch(Exception e){
			mensaje = e.getMessage();
		}
		return mensaje;
	}
	
	private void removerAtributoSession(Enumeration<String> atributosSession, HttpSession session) {
		try{
			while(atributosSession.hasMoreElements()){
				session.removeAttribute(atributosSession.nextElement());
			}
		}catch(Exception ex){ /* */ }
	}
}
