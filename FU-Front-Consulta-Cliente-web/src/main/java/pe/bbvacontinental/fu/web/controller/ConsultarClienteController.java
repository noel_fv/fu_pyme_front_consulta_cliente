package pe.bbvacontinental.fu.web.controller;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.bbvacontinental.fu.back.util.Util;
import pe.bbvacontinental.fu.common.enumeration.AtributosSesionEnum;
import pe.bbvacontinental.fu.common.enumeration.EstadoServiciosEnum;
import pe.bbvacontinental.fu.common.enumeration.MensajesGeneralesEnum;
import pe.bbvacontinental.fu.common.enumeration.TipoOperacionServiciosEnum;
import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.TbTipoDoi;
import pe.bbvacontinental.fu.model.TbTipoPersona;
import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.model.view.LocalCacheModel;
import pe.bbvacontinental.fu.model.view.RespuestaCabeceraModel;
import pe.bbvacontinental.fu.model.view.RespuestaGeneralModel;
import pe.bbvacontinental.fu.model.view.UsuarioPortal;
import pe.bbvacontinental.fu.wp.model.ConsultaDatosNegocioModel;


@Controller
@RequestMapping(value = { "/fu/preregistro/busquedacliente" })
@Scope("request")
public class ConsultarClienteController extends FUController{
	
	protected final Logger logg = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value = "/init", method = RequestMethod.GET)
	public String ingresarBusquedaCliente(Model model, HttpServletRequest request){
		
		UsuarioPortal usuarioPortal = obtenerUsuarioPortal(request);
		String objetoJsonLocalCacheModel=obtenerLocalStorage(usuarioPortal,null,null, request);
		model.addAttribute("localCacheModelFUCC",objetoJsonLocalCacheModel);
		
		TbParametrosAplicativo paramAplicativo = parametroAplicativoService.getParameterByCode(AtributosSesionEnum.VERSION_ST.getCodigo());
		model.addAttribute("versionST", paramAplicativo == null ? "" : paramAplicativo.getValor1());
		
		return "layout.preregistro-busqueda-cliente";
	}
	
	@RequestMapping(value = "/busquedacliente/", method = RequestMethod.POST)
	@ResponseBody
	public Object buscarCliente(@Valid @RequestBody ClienteModel clienteModel, BindingResult result,HttpServletRequest request) {
		logg.info("INI - {}","ConsultarClienteController.buscarCliente");
		
		ClienteModel datosCliente = new ClienteModel();
		RespuestaGeneralModel respuestaGeneralModel = new RespuestaGeneralModel();
		RespuestaCabeceraModel respuestaCabeceraModel = null;
		try {
			
			if (result.hasErrors()) {
				throw new ExcepcionBBVA(result);
			}
			
			//LocalCacheModel localCacheModel =obtenerLocalCache(clienteModel.getLocalCache())
			//WJMA ya no se usa localstorage
			
			UsuarioPortal usuarioPortal = obtenerUsuarioPortal(request);
			
			datosCliente = buscadorClienteService.buscarDatosCliente(clienteModel,usuarioPortal);
			
			datosCliente.setTipoPersonaFU(datosCliente.getTipoPersona());
			
			logg.info(" datosCliente getTipoPersona: {}",datosCliente.getTipoPersona());
			logg.info(" datosCliente getCodigoSegmento: {}", datosCliente.getCodigoSegmento());
			logg.info(" datosCliente getCodigoCentralSesion(): {}", datosCliente.getCodigoCentralSesion());
			logg.info(" datosCliente getTipoDocumentoIdentidadSesion(): {}", datosCliente.getTipoDocumentoIdentidadSesion());
			logg.info(" datosCliente getNumeroDocumentoSesion(): {}", datosCliente.getNumeroDocumentoSesion());
			
			respuestaCabeceraModel = new RespuestaCabeceraModel("OK", "200", "Servicio ejecutado correctamente");
			respuestaGeneralModel.setHead(respuestaCabeceraModel);
			
		} catch (ExcepcionBBVA e) {
			
			respuestaCabeceraModel = new RespuestaCabeceraModel("ERROR", MensajesGeneralesEnum.MENSAJE_103.getCodigo(),
					environment.getProperty(MensajesGeneralesEnum.MENSAJE_103.getCodigo()));
			
			datosCliente.setMensajeServicio(errorExcepcion +" "+e.getErrores());
			respuestaGeneralModel.setHead(respuestaCabeceraModel);			
			logg.error("FU Error ConsultarClienteController.buscarCliente : ",e);
			
		} catch (Exception e) {
			
			respuestaCabeceraModel = new RespuestaCabeceraModel("ERROR", MensajesGeneralesEnum.MENSAJE_103.getCodigo(),
					environment.getProperty(MensajesGeneralesEnum.MENSAJE_103.getCodigo()));
			
			datosCliente.setMensajeServicio(environment.getProperty(MensajesGeneralesEnum.MENSAJE_103.getCodigo()));
			respuestaGeneralModel.setHead(respuestaCabeceraModel);		
			logg.error("FU Error ConsultarClienteController.buscarCliente : ", e);
		}
		
		respuestaGeneralModel.setBody(datosCliente);
		logg.info("FIN - {}","ConsultarClienteController.buscarCliente");
		return respuestaGeneralModel;
	}
	
	@RequestMapping(value = "/operacionSiguiente/", method = RequestMethod.POST)
	@ResponseBody
	public String validarAccionSiguiente(@Valid @RequestBody ClienteModel clienteModel,HttpServletRequest request, BindingResult result){
		
		logg.info("INI - {}","ConsultarClienteController.validarAccionSiguiente");
		String respuesta="";
		try{
			
			LocalCacheModel localCacheModel =obtenerLocalCache(clienteModel.getLocalCache(), request.getSession());
			UsuarioPortal usuarioPortal = localCacheModel.getUsuarioPortal();
			//WJMA Ya no se usa localstorage
			//UsuarioPortal usuarioPortal = obtenerUsuarioPortal(request)
			
			logg.info(" validarAccionSiguienteclienteModel: {},",clienteModel);

			//clienteModel.setTipoPersonaFU(clienteModel.getTipoPersona())
			

			String [] codigoTipoPersona =clienteModel.getTipoPersonaFU().split("_"); 
			String tipoPersonaFU= codigoTipoPersona[1];
			
			logg.info("  validarAccionSiguiente tipoPersonaFU: {}",tipoPersonaFU);


			clienteModel.setTipoPersonaFU(tipoPersonaFU);
			clienteModel.setIdSesion(request.getSession().getId());
			clienteModel.setIdUsuario(usuarioPortal.getCodigoUsuario());
			clienteModel.setFechaHoraEnvio(Util.formatDate(new Date(), "yy-MM-dd HH:mm:ss.SSS"));
			clienteModel.setCodigoCentral(clienteModel.getCodigoCentralSesion());
			clienteModel.setNumeroDocumento(clienteModel.getNumeroDocumentoSesion());
			clienteModel.setTipoDocumentoIdentidad(clienteModel.getTipoDocumentoIdentidadSesion());

			if(StringUtils.isEmpty(tipoPersonaFU)){			
				
				respuesta= environment.getProperty(MensajesGeneralesEnum.MENSAJE_105.getCodigo());
				logg.error("FU .validarAccionSiguiente: {}",respuesta);
				
			}else if(tipoPersonaFU.equals("PN")){
				respuesta = validarPersonaPN(clienteModel,request);				
			}else{
				
				clienteModel.setIdOperacion(TipoOperacionServiciosEnum.SERV_CONSULTA_DATOS_NEGOCIO_WP.getDescripcion());					
				// INI - VERIFICAR PERTENECE CIRCUITO BEC
				ConsultaDatosNegocioModel consultaDatosNegocioModel = datosNegocioService.consultarDatosNegocio(clienteModel);
				
				logg.info(" consultaDatosNegocioModel MensajeServicio: {}",consultaDatosNegocioModel.getEstadoServicio()+":"+consultaDatosNegocioModel.getMensajeServicio());

				
				if(consultaDatosNegocioModel.getEstadoServicio()!=null && consultaDatosNegocioModel.getEstadoServicio().equals("ERR")){					
					respuesta = environment.getProperty(MensajesGeneralesEnum.MENSAJE_104.getCodigo())+" ["+consultaDatosNegocioModel.getMensajeServicio()+"]";
					logg.error("FU Error ConsultarClienteController.validarAccionSiguiente: {}",consultaDatosNegocioModel.getMensajeServicio());
					return respuesta;
					
				}else{

					logg.info("getVentasAnualesMN(): {}",consultaDatosNegocioModel.getVentasAnualesMN());					
					boolean flujoPjPnn=solicitudService.esFlujoPjPnn(tipoPersonaFU, consultaDatosNegocioModel.getVentasAnualesMN());
					logg.info(" flujoPJ_PNN: {}",flujoPjPnn);
					
					if(!flujoPjPnn){
						respuesta= environment.getProperty(MensajesGeneralesEnum.MENSAJE_108.getCodigo());
						logg.error("FU Error.validarAccionSiguiente: {}",respuesta);
						return respuesta;
					}

				}
				// FIN - VERIFICAR PERTENECE CIRCUITO BEC
				
//				INI - INVOCAR PJ
				clienteModel.setIdOperacion(TipoOperacionServiciosEnum.SERV_PANTALLA_PERSONA_JURIDICA.getDescripcion());
				
				GenericModel genericModel = invocarPersonaJuridicaService.invocarPantallaPersonaJuridica(clienteModel);
				
				if(clienteModel.getNumeroDocumento()!= "" && clienteModel.getNumeroDocumento()!= null){
					clienteModel.setCodigoCentral("");
				}
				respuesta = validarPersonaPJ(genericModel);
//				FIN - INVOCAR PJ
			}
					
			//SOLO PARA PRUEBA
			if(!esModoDummy.equalsIgnoreCase("0")){
				respuesta = "URL_APPS_PJ|http://localhost:9070/FU-Front-web/fu/preregistro/registrosolicitud/init?username="+usuarioPortal.getCodigoUsuario();
			}
//			respuesta="URL_APPS_PJ|http://localhost:9070/FU-Front-web/fu/preregistro/registrosolicitud/init?username=P020683"
		}catch(Exception e){
			
			respuesta = environment.getProperty(MensajesGeneralesEnum.MENSAJE_104.getCodigo());
			logg.error("FU Error ConsultarClienteController.validarAccionSiguiente : ",e);            
		}

		logg.info("FIN - {}","ConsultarClienteController.validarAccionSiguiente");
	
		
		return respuesta;
	}
	
	@RequestMapping(value = "/tipo-persona/", method = RequestMethod.GET)
	@ResponseBody
	public List<TbTipoPersona> obtenerListaTipoPersona() {
		logg.info("Entro -{}"," ConsultarClienteController.obtenerListaTipoPersona");

		List<TbTipoPersona>  lista = tipoPersonaService.listaTipoPersona();
		
		logg.info("Salio - {}","ConsultarClienteController.obtenerListaTipoPersona");
		return lista;
	}
	
	@RequestMapping(value = "/tipo-documento/", method = RequestMethod.GET)
	@ResponseBody
	public List<TbTipoDoi> obtenerListaTipoDocumento(){
		logg.info("Entro - {}","ConsultarClienteController.obtenerListaTipoDocumento");

		List<TbTipoDoi> lista = tipoDocumentoService.listarTipoDocumento();

		logg.info("Salio - {}","ConsultarClienteController.obtenerListaTipoDocumento");
		return lista;
	}
	
	private String validarPersonaPN(ClienteModel clienteModel, HttpServletRequest request){
		clienteModel.setNumeroDocumento(clienteModel.getNumeroDocumentoSesion());
		clienteModel.setTipoDocumentoIdentidad(clienteModel.getTipoDocumentoIdentidadSesion());
//		INI - ENVIO COOKIE PN
		Map<String,Object> datosParamBPM=obtenerParamAccesoBPMPorRestAPI(request,clienteModel.getCookieSSO());
		String cookie ="";
		if(datosParamBPM.get("COOKIE_BPM") != null){
			cookie = (String)datosParamBPM.get("COOKIE_BPM");
		}
		logg.info("COOKIE_BPM-cookie from portal busqueda cliente: {}",cookie);				
		cookie = clienteModel.getCookieSSO();
		logg.info("clienteModel-cookie from portal busqueda cliente: {}",cookie);
//		FIN - ENVIO COOKIE PN				
		clienteModel.setIdOperacion(TipoOperacionServiciosEnum.SERV_PANTALLA_PERSONA_NATURAL.getDescripcion());		
		
		GenericModel genericModel = invocarPersonaNaturalService.invocarPantallaPersonaNatural(clienteModel, cookie);
		
		String respuesta;
		if(StringUtils.equals(genericModel.getCodigoServicio(), EstadoServiciosEnum.RESULTADO_OK_GENERAL.getCodigo())){
			respuesta = "URL_APPS_PN|"+genericModel.getRespuesta1();
		}else{
			respuesta = environment.getProperty(MensajesGeneralesEnum.MENSAJE_106.getCodigo());
			logg.error("FU Error .validarAccionSiguiente: {}",respuesta);
		}
		return respuesta;
	}
	
	private String validarPersonaPJ(GenericModel genericModel){
		String respuesta;
		if(StringUtils.equals(genericModel.getCodigoServicio(), EstadoServiciosEnum.RESULTADO_OK_GENERAL.getCodigo())){
			respuesta = "URL_APPS_PJ|"+genericModel.getRespuesta1();
		}else{
			respuesta = environment.getProperty(MensajesGeneralesEnum.MENSAJE_107.getCodigo());
		}
		return respuesta;
	}

}
