package pe.bbvacontinental.fu.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;
import pe.bbvacontinental.fu.common.excepcion.advice.ResultadoJsonExcepcionBBVA;
import pe.bbvacontinental.fu.common.excepcion.advice.ResultadoPaginaEnBlancoExcepcionBBVA;
import pe.bbvacontinental.fu.common.excepcion.advice.ResultadoPaginaExcepcionBBVA;
import pe.bbvacontinental.fu.common.excepcion.advice.ResultadoPaginaLightboxExcepcionBBVA;
import pe.bbvacontinental.fu.model.excepcion.MensajeErrorCollection;
import pe.bbvacontinental.fu.utils.shared.UtilJson;

@ControllerAdvice
public class ManejadorExcepcionController extends ManejadorExcepcionBase {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@ExceptionHandler({ ResultadoPaginaExcepcionBBVA.class })
	public ModelAndView resultadoPagina(ResultadoPaginaExcepcionBBVA excepcionBBVA, WebRequest request, HttpServletResponse response) {
		log.info("resultadoPagina: inicio");
		MensajeErrorCollection errorProceso = this.obtenerErrores(excepcionBBVA);
		ModelAndView paginaErrorProceso = new ModelAndView("shared/error");
		paginaErrorProceso.addObject("errorProceso", errorProceso);
		log.info("resultadoPagina: fin");
		return paginaErrorProceso;
	}

	@ExceptionHandler({ ResultadoPaginaLightboxExcepcionBBVA.class })
	public ModelAndView resultadoPaginaLightbox(ResultadoPaginaLightboxExcepcionBBVA excepcionBBVA, WebRequest request, HttpServletResponse response) {
		log.info("resultadoPagina: inicio");
		MensajeErrorCollection errorProceso = this.obtenerErrores(excepcionBBVA);
		ModelAndView paginaErrorProceso = new ModelAndView("shared/error-lightbox");
		paginaErrorProceso.addObject("errorProceso", errorProceso);
		log.info("resultadoPagina: fin");
		return paginaErrorProceso;
	}
	
	@ExceptionHandler({ ResultadoPaginaEnBlancoExcepcionBBVA.class })
	public ModelAndView resultadoPaginaEnBlanco(ResultadoPaginaEnBlancoExcepcionBBVA excepcionBBVA, WebRequest request, HttpServletResponse response) {
		log.info("resultadoPaginaEnBlanco: inicio");
		log.info("resultadoPaginaEnBlanco: fin");
		return null;
	}

	@ExceptionHandler({ ResultadoJsonExcepcionBBVA.class })
	@ResponseBody
	public String resultadoJson(ResultadoJsonExcepcionBBVA excepcionBBVA, WebRequest request, HttpServletResponse response) {
		log.info("resultadoJson: inicio");
		MensajeErrorCollection errorFormulario = obtenerErrores(excepcionBBVA);
		response.setHeader(codigoErrorHeader, codigoErrorHeader);
		log.info("resultadoJson: fin");
		return UtilJson.convertirObjectoACadena(errorFormulario);
	}

	@ExceptionHandler({ BindException.class })
	@ResponseBody
	public String resultadoJson(BindException excepcionValid, HttpServletResponse response) {
		log.info("resultadoJson: inicio");
		MensajeErrorCollection errorFormulario = obtenerErrores(new ExcepcionBBVA(excepcionValid.getBindingResult()));
		response.setHeader(codigoErrorHeader, codigoErrorHeader);
		log.info("resultadoJson: fin");
		return UtilJson.convertirObjectoACadena(errorFormulario);
	}

}
