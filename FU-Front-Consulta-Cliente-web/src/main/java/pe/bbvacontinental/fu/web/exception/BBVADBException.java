package pe.bbvacontinental.fu.web.exception;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;

public class BBVADBException extends ExcepcionBBVA {

	private static final long serialVersionUID = -4938556712560828963L;

	// Constructores ...
	public BBVADBException(Exception objException) {
		super(objException);
	}

	public BBVADBException(String msjError) {
		super(msjError);
	}

	public BBVADBException(String codError, String msjError, Exception objException) {
		super(codError, msjError, objException);
	}

	public BBVADBException(String codError, String msjError, String nombreSP, String nombreBD, Exception objException) {
		super(codError, msjError + "**" + nombreSP + "**" + nombreBD, objException);
	}

}
