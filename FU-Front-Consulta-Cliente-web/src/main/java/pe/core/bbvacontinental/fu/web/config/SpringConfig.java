package pe.core.bbvacontinental.fu.web.config;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import pe.bbvacontinental.fu.web.constantes.AppConstantes;

@Configuration
@EnableWebMvc
@PropertySource(value = { AppConstantes.PROPERTIES_LOCATION + "iniciar.fu_cc" + AppConstantes.PROPERTIES_EXTENSION })
@PropertySources(value = {
		@PropertySource("${path.file.configuracion}" + "principal.fu_cc" + AppConstantes.PROPERTIES_EXTENSION),
		@PropertySource("${path.file.configuracion}" + "mensajes.fu_cc" + AppConstantes.PROPERTIES_EXTENSION) })
@Import({ AppConfigFU.class })
public class SpringConfig extends WebMvcConfigurerAdapter {

	@Value("${fileunico.junction}")
	private String fileUnicoJuntion;
	@Value("${codigo.cookie.bpm}")
	private String codigoCookieWebseal;

	@Autowired
	private ServletContext servletContext;

	@Value("${fileunico.protocol.staticos}")
	private String fileunicoProtocolST;

	@Value("${fileunico.ip.staticos}")
	private String ipST;

	@Value("${fileunico.root.staticos}")
	private String fileunicoRootST;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		// Ponemos en contexto de aplicaciones variables que se utilizan en
		// todos los moudulos de la aplicacion
		servletContext.setAttribute("fileunicoJunction", fileUnicoJuntion);
		servletContext.setAttribute("codigoCookieWebseal", codigoCookieWebseal);
		servletContext.setAttribute("CONTEXT_ST", fileunicoProtocolST + ipST + fileunicoRootST);

		registry.addResourceHandler(AppConstantes.RESOURCE_HANDLER)
				.addResourceLocations(AppConstantes.RESOURCES_LOCATION);
	}

	@Bean
	public UrlBasedViewResolver getViewResolver() {

		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(TilesView.class);
		viewResolver.setOrder(1);

		return viewResolver;
	}

	@Bean
	public TilesConfigurer getTilesConfigurer() {

		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(AppConstantes.WEB_INF_TILES_XML);
		tilesConfigurer.setCheckRefresh(true);

		return tilesConfigurer;
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {

		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(900000);
		return commonsMultipartResolver;

	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}