package pe.core.bbvacontinental.fu.web.config;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class WSConfig {

	@Autowired
	private Environment environment;

	@Bean(name = "restTemplatePesado")
	public RestTemplate restTemplatePesado() {

		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();

		clientHttpRequestFactory.setReadTimeout(
				Integer.valueOf(environment.getProperty("servicio.rest.tiempo.espera.lectura.milisegundos.pesado")));
		clientHttpRequestFactory.setConnectTimeout(
				Integer.valueOf(environment.getProperty("servicio.rest.tiempo.espera.conexion.milisegundos.pesado")));

		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("iso-8859-1")));

		return restTemplate;
	}

	@Bean(name = "restTemplateLigero")
	public RestTemplate restTemplateLigero() {

		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setReadTimeout(
				Integer.valueOf(environment.getProperty("servicio.rest.tiempo.espera.lectura.milisegundos.ligero")));
		clientHttpRequestFactory.setConnectTimeout(
				Integer.valueOf(environment.getProperty("servicio.rest.tiempo.espera.conexion.milisegundos.ligero")));

		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("iso-8859-1")));
		/*
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>()
		interceptors.add(new LoggingRestTemplate())
		restTemplate.setInterceptors(interceptors)
		*/
		return restTemplate;
	}
}
