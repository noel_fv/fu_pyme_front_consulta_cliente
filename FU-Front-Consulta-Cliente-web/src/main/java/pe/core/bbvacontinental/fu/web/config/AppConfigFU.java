package pe.core.bbvacontinental.fu.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import pe.bbvacontinental.fu.web.constantes.AppConstantes;

@Configuration
@ComponentScan(basePackages = { "pe.bbvacontinental.*" })
//@EnableAsync
@PropertySource(value = {AppConstantes.PROPERTIES_LOCATION + "iniciar.fu_cc" + AppConstantes.PROPERTIES_EXTENSION})
@PropertySources(value = {
					  		@PropertySource("${path.file.configuracion}" + "principal.fu_cc" + AppConstantes.PROPERTIES_EXTENSION), 
						    @PropertySource("${path.file.configuracion}" + "mensajes.fu_cc"+ AppConstantes.PROPERTIES_EXTENSION )
						  })
@Import({ WSConfig.class, BDConfig.class })
public class AppConfigFU {
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
		    return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);

        return mapper;
    }

	@Bean
    public SpringConfigurableMapper springConfigurableMapper() {
		return new SpringConfigurableMapper();
	}
}
