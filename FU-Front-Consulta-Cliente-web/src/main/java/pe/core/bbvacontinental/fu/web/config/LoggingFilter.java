package pe.core.bbvacontinental.fu.web.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.MDC;

public class LoggingFilter implements Filter {
	@Override
	public void destroy() {
		/* */
	}

	@Override
	public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain filterChain) throws IOException, ServletException {
		
		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpSession session = request.getSession(false);

		if (session != null) {
			MDC.put("sessionId", session.getId());
		} else {
			MDC.put("sessionId", "without_session");
		}
		
		final HttpServletResponse response = (HttpServletResponse) servletResponse;

		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-cache");
		
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		/* */
	}
}