package pe.core.bbvacontinental.fu.web.config;

import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class BDConfig {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private Environment env;

	@Bean
	public DataSource getDataSource() {

		DataSource dataSource = null;
		try {

			final JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
			jndiObjectFactoryBean.setLookupOnStartup(false);
			jndiObjectFactoryBean.setResourceRef(true);
			dataSource = (DataSource) jndiObjectFactoryBean.getJndiTemplate()
					.lookup(env.getProperty("jndi.dataSource"));

		} catch (NamingException e) {
			logger.error(" FUCC-PJ BDConfig getDataSource", e);
		}
		return dataSource;
	}

	@Bean(name = "sessionFactory")
	public LocalSessionFactoryBean sessionFactory() {

		final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(getDataSource());
		//sessionFactory.setPackagesToScan(new String[] { "pe.bbvacontinental.fu.model" })
		sessionFactory.setPackagesToScan("pe.bbvacontinental.fu.model");
		sessionFactory.setHibernateProperties(getAdditionalProperties());

		return sessionFactory;
	}

	@Bean(name = "transactionManager")
	public HibernateTransactionManager transactionManager(final SessionFactory sessionFactory) {

		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);

		return txManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@SuppressWarnings("serial")
	Properties getAdditionalProperties() {

		Properties prop=new Properties();
		prop.setProperty("hibernate.dialect", env.getProperty("fu.hibernate.dialect"));
		prop.setProperty("hibernate.show_sql", env.getProperty("fu.hibernate.show_sql"));
		prop.setProperty("hibernate.format_sql", env.getProperty("fu.hibernate.format_sql"));
		prop.setProperty("hibernate.use_sql_comments", env.getProperty("fu.hibernate.use_sql_comments"));
		prop.setProperty("hibernate.default_schema", env.getProperty("fu.hibernate.default_schema"));
		 
		return prop;
		
		
	}

}
