package pe.core.bbvacontinental.fu.web.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class LoggingRestTemplate implements ClientHttpRequestInterceptor {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingRestTemplate.class);

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		traceRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		return traceResponse(response);
	}

	private void traceRequest(HttpRequest request, byte[] body) throws IOException {
		if (!LOGGER.isDebugEnabled()) {
			return;
		}
		String a=new String(body, "UTF-8");
		LOGGER.debug("==========================request begin============================================={}","=");
		LOGGER.debug("URI                 : {}", request.getURI());
		LOGGER.debug("Method            : {}", request.getMethod());
		LOGGER.debug("Headers         : {}", request.getHeaders());
		LOGGER.debug("Request body: {}", a);
		LOGGER.debug("==========================request end==============================================={}","=");
	}

	private ClientHttpResponse traceResponse(ClientHttpResponse responseWrapper) throws IOException {
		if (!LOGGER.isDebugEnabled()) {
			return responseWrapper;
		}
		//final ClientHttpResponse responseWrapper = new BufferingClientHttpResponseWrapper(response)
		StringBuilder inputStringBuilder = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(responseWrapper.getBody(), "UTF-8"));
		String line = bufferedReader.readLine();
		while (line != null) {
			inputStringBuilder.append(line);
			inputStringBuilder.append('\n');
			line = bufferedReader.readLine();
		}
		String a=inputStringBuilder.toString();
		LOGGER.debug("==========================response begin============================================{}","=");
		LOGGER.debug("Status code    : {}", responseWrapper.getStatusCode());
		LOGGER.debug("Status text    : {}", responseWrapper.getStatusText());
		LOGGER.debug("Headers            : {}", responseWrapper.getHeaders());
		LOGGER.debug("Response body: {}",a );
		LOGGER.debug("==========================response end=============================================={}","=");
		return responseWrapper;
	}

}
