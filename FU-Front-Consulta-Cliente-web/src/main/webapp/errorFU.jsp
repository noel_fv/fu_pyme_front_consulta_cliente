<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page isErrorPage="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mensaje File Unico</title>
<link rel="shortcut icon" href="${CONTEXT_ST}/images/favicon.ico" />
<link rel="stylesheet" href="${CONTEXT_ST}/css/general-bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="${CONTEXT_ST}/css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="${CONTEXT_ST}/css/fonts.css" type="text/css"/>
<link rel="stylesheet" href="${CONTEXT_ST}/css/bootstrap-datetimepicker.min.css" type="text/css"/>
<link rel="stylesheet" href="${CONTEXT_ST}/css/jquery-ui.min.css" type="text/css">
<link rel="stylesheet" href="${CONTEXT_ST}/css/bootstrap-datetimepicker.min.css" type="text/css">

<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/bootstrap.min.js"></script>
</head>
<body>

        <br>
		<div class="container">

				<div class="panel-group">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" href="#collapse1">
					         Se produjo un error en File Unico Consulta de Cliente. Por favor sirvase consultar con el administrador del sistema
					        </a>
					      </h4>
					    </div>
					    <div id="collapse1" class="panel-collapse collapse">
					      <div class="panel-body">
								     <ul> 
									    <li>Exception: <c:out value="${requestScope['javax.servlet.error.exception']}" /></li>
									    <li>Exception type: <c:out value="${requestScope['javax.servlet.error.exception_type']}" /></li>
									    <li>Exception message: <c:out value="${requestScope['javax.servlet.error.message']}" /></li>
									    <li>Request URI: <c:out value="${requestScope['javax.servlet.error.request_uri']}" /></li>
									    <li>Servlet name: <c:out value="${requestScope['javax.servlet.error.servlet_name']}" /></li>
									    <li>Status code: <c:out value="${requestScope['javax.servlet.error.status_code']}" /></li>
									</ul>
					      </div>
					      <div class="panel-footer"></div>
					    </div>
					  </div>
				</div>
	</div>
</body>
</html>