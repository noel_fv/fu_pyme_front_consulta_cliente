<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<section class="container-fluid container-section-primary margin-top-x20">
	
	<div class="row">
		<div class="col-md-8 col-sm-8">
			<h2 class="title-general pull-left">Consultar Clientes</h2>
		</div>
	</div>

	<div class="tab-content clearfix">
		<div class="panel-body panel-body-tabs panel-heading-white">	

			<form:form id="formBusquedaClienteId" class="form-cliente form-horizontal" method="POST" 
								  modelAttribute="clienteModel">
				<div class="row">
					<div class="col-md-12">
						<div id="alertaError" class="alert alert-danger alert-dismissible" style="display: none;">
							<img src="${CONTEXT_ST}/images/iconInformacion.png">
							<button type="button" class="close" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<div id="seccionMensajeId">
								Debe ingresar los campos obligatorios: C&oacute;digo Central o Tipo y N&uacute;mero de Documento
							</div>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-3">
						<div class="form-group" style="margin-right: 0px; margin-left: 0px;">
							<label class="control-label">C&oacute;digo Central:</label>
							<input name="codigoCentral" class="form-control" id="codigoCentral" type="text" placeholder="Código Central" maxlength="8">
						</div>
					</div>
					<div class="col-md-3 col-sm-3">	
                                            <div class="form-group" style="margin-right: 0px; margin-left: 0px;">
							<label class="control-label">Tipo de Documento de Identidad:</label>
                                                        <select name="tipoDocumentoIdentidad" id="tipoDocumentoIdentidad" style="width: 236px;">
                                                            <option></option>
							</select>
                                                </div>
					</div>
					
					<div class="col-md-3 col-sm-3">
						<div class="form-group" style="margin-right: 0px; margin-left: 0px;">
							<label class="control-label">N&uacute;mero de Documento:</label>
							<input name="numeroDocumento" class="form-control" id="numeroDocumento" type="text" placeholder="N&uacute;mero Documento" maxlength="11">
						</div>
					</div>
					<div class="col-md-3 col-sm-3 margin-top-x25">
						<div class="form-group" style="margin-right: 0px; margin-left: 0px;">
							<button id="btnBuscarCliente" class="btn btn-default pull-left" data-toggle="modal" data-target="#" type="button">
								Buscar<img src="${CONTEXT_ST}/images/botonBuscar.png">
							</button>
						</div>
					</div>
				</div>
				
				<div class="panel-body-general background-primary margin-top-x20" style="display:none;" id="datosCliente">
					<div class="form-group">
						<div class="col-md-3 col-sm-3">
							<label class="control-label">Tipo de Persona:</label>
							<select name="tipoPersonaFU" class="form-control" id="tipoPersonaFU" disabled></select>
						</div>
						<div class="col-md-3 col-sm-3">
							<label class="control-label">Nombre Completo:</label>
							<input name="nombreCompleto" class="form-control" id="nombreCompleto" type="text" readonly="">
						</div>
						<div class="col-md-3 col-sm-3">
							<label class="control-label">Territorio:</label>
							<input name="nombreTerritorio" class="form-control" id="nombreTerritorio" type="text" readonly="">
						</div>
						<div class="col-md-3 col-sm-3">
							<label class="control-label">Oficina:</label>
							<input name="nombreOficina" class="form-control" id="nombreOficina" type="text" readonly="">
						</div>
					</div>
				</div>
				
			<input type="hidden" name="territorio" 	id="codigoTerritorioId"/>
			<input type="hidden" name="codigoSegmento"  	id="codigoSegmentoId"/>	
			<input type="hidden" name="descripcionSegmento" id="descripcionSegmentoId"/>
			<input type="hidden" name="codigoCentralSesion" id="codigoCentralSesion"/>
			<input type="hidden" name="tipoDocumentoIdentidadSesion" id="tipoDocumentoIdentidadSesion"/>
			<input type="hidden" name="numeroDocumentoSesion" id="numeroDocumentoSesion"/>
			<input type="hidden" name="cookieSSO" id="cookieSSO"/>
			<input type="hidden" name="oficina" 	id="idOficina"/>
			
			<input type="hidden" name="tipoPersona" 	id="tipoPersona"/>
			
			</form:form> 
		</div>
		<div class="row">
			<div class="col-md-12 margin-height-x15">
				<button id="btnSiguiente" class="btn btn-default pull-right" type="button">
				Siguiente<img alt="" src="${CONTEXT_ST}/images/botonAceptar.png"></button>
			</div>
		</div>
	</div>
</section>

<!-- POPUP - MENSAJES -->
<div id="alertMessages" class="modal fade" role="dialog">
	<div class="modal-dialog modal-imd modal-height-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header modal-header-warning">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="kindMessage">Mensaje del Sistema</h4>
			</div>
			<div class="modal-body">
				<h6 id="message"></h6>
			</div>
			<div class="modal-footer">
				<button id="btnConfirmarRechazo" type="button" class="btn btn-default btn-delete" data-dismiss="modal">
					Aceptar 
				<img alt="" src="${CONTEXT_ST}/images/botonAceptar.png">
				</button>

			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="${CONTEXT_ST}/js/preregistro/busquedacliente/busqueda_cliente.js?version=${versionST}"></script>

