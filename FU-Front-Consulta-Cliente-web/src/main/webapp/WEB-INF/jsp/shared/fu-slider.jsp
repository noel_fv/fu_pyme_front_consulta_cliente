
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
	<!-- INICIO SLIDER -->
	<div class="row margin-top-x20">
		<div class="col-md-12">
			<div class="container-tabs-bullet">
				<div class="line-tabs-bullets">
				</div>
				
				<core:choose>
				  <core:when  test="${requestScope.estadoSolicitud eq '21'}">
				  
				  		<ul class="tabs-bullets">
								<li><a href="Solicitud_Oficina.html" class="hover"><span class="iconoSolicitud"></span></a>
			                        <div class="text-bullet">
			                            Pre Registro
			                        </div>
								</li>
								<li><a href="#" class="hover"><span  class="iconoPreEvaluacion"></span></a>
			                        <div class="text-bullet">
			                            Informaci&oacute;n Global
			                        </div>
								</li>
								<li><a href="#" class="hover"><span class="iconoEvaluacion"></span></a>
			                        <div class="text-bullet">
			                            Solicitud
			                        </div>
								</li>
								<li><a href="#" class="active"><span class="iconoDictamen"></span></a>
			                        <div class="text-bullet">
			                            Checklist
			                        </div>
								</li>
							</ul>
				  
				 </core:when>
				  <core:otherwise>
				  
						<ul class="tabs-bullets">		
							<li><a href="Solicitud_Oficina.html" class="active"><span class="iconoSolicitud"></span></a>
		                        <div class="text-bullet">
		                            Pre Registro
		                        </div>
							</li>
							<li><a href="#" class="inactive"><span  class="iconoPreEvaluacion"></span></a>
		                        <div class="text-bullet">
		                            Informaci&oacute;n Global
		                        </div>
							</li>
							<li><a href="#" class="inactive"><span class="iconoEvaluacion"></span></a>
		                        <div class="text-bullet">
		                            Solicitud
		                        </div>
							</li>
							<li><a href="#" class="inactive"><span class="iconoDictamen"></span></a>
		                        <div class="text-bullet">
		                            Checklist
		                        </div>
							</li>
						</ul>
				  </core:otherwise>
				
				</core:choose> 


			</div>
		</div>
	</div>
<!-- Formulario para invocar a webpymes por get -->
<form class="form-horizontal" id="idFrmllamarWebPymes">
	   <input type="hidden" id="idCodigoSolicitud" name="codigoSolicitud" />
</form>	