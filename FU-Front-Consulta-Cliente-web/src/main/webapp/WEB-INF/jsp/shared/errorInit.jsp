<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<section class="container-fluid margin-top-x20">
	<div class="panel-heading-white margin-top-x10 relative">
		<div class="form-group">
			<div class="col-md-12">															
				<div id="alertaMensajeError" class="alert alert-danger margin-bottom-x15" role="alert">
					<img src="${CONTEXT_ST}/images/iconoAlerta.png"><span>${mensajeError}</span>				
				</div>														
			</div>
		</div>
	</div>
</section>