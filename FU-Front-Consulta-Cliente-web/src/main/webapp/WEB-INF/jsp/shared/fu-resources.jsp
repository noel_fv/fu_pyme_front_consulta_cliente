<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<!-- CSS de Bootstrap -->
<link rel="shortcut icon" href="${CONTEXT_ST}/images/favicon.ico" />
<link rel="stylesheet" href="${CONTEXT_ST}/css/general-bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="${CONTEXT_ST}/css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="${CONTEXT_ST}/css/fonts.css" type="text/css"/>
<link rel="stylesheet" href="${CONTEXT_ST}/css/wiziwyg.css" type="text/css">
<link rel="stylesheet" href="${CONTEXT_ST}/css/jquery-ui.min.css" type="text/css">
<link rel="stylesheet" href="${CONTEXT_ST}/css/bootstrap-datetimepicker.min.css" type="text/css">
<link rel="stylesheet" href="${CONTEXT_ST}/css/select2.css" type="text/css">

<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/iframeResizer.contentWindow.min.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/general/general.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/componentes/componentes.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/bootstrapNotify/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/select2.min.js"></script>
<script type="text/javascript" src="${CONTEXT_ST}/js/common/plugins/i18n/es.js"></script>

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Corrextor anchos Windows Phone 8 y Internet Explorer 10 -->
<script>
	/*@-webkit-viewport   { width: device-width; }
	@-moz-viewport      { width: device-width; }
	@-ms-viewport       { width: device-width; }
	@-o-viewport        { width: device-width; }
	@viewport           { width: device-width; }*/
	if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
		var msViewportStyle = document.createElement("style");
		msViewportStyle.appendChild(
			document.createTextNode(
				"@-ms-viewport{width:auto!important}"
			)
		);
		document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
	}
	//Corrector <select> Navegador Android
	var nua = navigator.userAgent;
	var isAndroid = (nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1 && nua.indexOf('Chrome') === -1);
	if (isAndroid) {
		$('select.form-control').removeClass('form-control').css('width', '100%');
	}

	/**
	 *   Idenfiticador de cookie para sso del bpm
	 */
	var idCookieSsoFu="${applicationScope['codigoCookieWebseal']}"
	
	/**
	* Funcion que retorna la cookie del webseal para que la utnciacacion del BPM sea mediante  SSO
	*/
	function readCookie(name) {
		return document.cookie;
		
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for (var i = 0; i < ca.length; i++) {
	          var c = ca[i];
	          while (c.charAt(0) == ' ')
	                 c = c.substring(1, c.length);
	          if (c.indexOf(nameEQ) == 0) {
		          var respuesta=decodeURIComponent(c.substring(nameEQ.length, c.length));
	                 return respuesta;
	          }
	    }
	    return null;
	}


	//Ini GRABAMOS EN SESSION LOCAL LOS DATOS IMPORTANTE DE USUARIO
	if('${localCacheModelFUCC}'!=''){

		var localCaachefu = '${localCacheModelFUCC}';
		//Grabamos 
		saveLocalObject("locaObjectFUCC", localCaachefu);
		//Fin GRABAMOS EN SESSION LOCAL LOS DATOS IMPORTANTE DE USUARIO	
	}	
	
</script>

	<style>
		.tabs-bullets li a.active,
		.tabs-bullets li a:hover,
		.tabs-bullets li a:focus {
			background: transparent linear-gradient(to bottom, #07acd7 0%, #05a7d5 100%) repeat scroll 0% 0%;
			box-shadow: inset 0 0 10px rgba(0,0,0,0.1);
			}
		.tabs-bullets li a.no-hover:hover, .tabs-bullets li a.no-hover:focus {background: transparent linear-gradient(to bottom, #0065c2 0%, #004c92 100%) repeat scroll 0% 0% !important;} 	
		.tabs-bullets li a.inactive {
			background: transparent linear-gradient(to bottom, #666 0%, #666 100%) repeat scroll 0% 0%;
			cursor:default;
			}
	</style>