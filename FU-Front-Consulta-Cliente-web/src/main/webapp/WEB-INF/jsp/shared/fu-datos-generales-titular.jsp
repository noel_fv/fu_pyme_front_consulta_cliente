<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div class="panel panel-default border-bottom-acordeonv2">
	<div class="panel-heading">
		<h3 class="panel-title">Datos de la Solicitud</h3>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<div class="col-md-3 col-sm-3">
				<label class="control-label">Operación:</label>
				<select id="" class="form-control" disabled>
					<option value="${tipoFlujo.valor}">${tipoFlujo.descripcion}</option>
				</select>
			</div>
			<c:choose>
				<c:when test="${tipoFlujo.valor == 1}">
					<div class="col-md-3 col-sm-3">
				    	<label class="control-label">Operación CDR:</label>
				    	<select id="operacionCDR" class="form-control" disabled>
				    		<option value="${operacionCdr.valor}">${operacionCdr.nombre}</option>
				    	</select>
				    </div>
				</c:when>
				<c:when test="${tipoFlujo.valor == 4}">
					<div class="col-md-3 col-sm-3">
				    	<label class="control-label">Tipo Campaña:</label>
				    	<select id="tipoCampana" class="form-control" disabled>
				    		<option value="${tipoCampanha.valor}">${tipoCampanha.nombre}</option>
				    	</select>
					</div>
				</c:when>
			</c:choose>
			<div class="col-md-3 col-sm-3">
				<label class="control-label">Territorio:</label>
				<input id="territorio" type="text" class="form-control" disabled value="${solicitud.nombreTerritorio}">
			</div>
			<div class="col-md-3 col-sm-3">
				<label class="control-label">Oficina:</label>
				<input id="oficina" type="text" class="form-control" disabled value="${solicitud.nombreOficina}">
			</div>
			<div class="col-md-3 col-sm-3">
				<label class="control-label">Canal:</label>
				<input id="canal" type="text" class="form-control" value="${solicitud.nombreCanal}" disabled>
			</div>
			<div class="col-md-3 col-sm-3">
				<label class="control-label">Fecha Registro:</label>
				<input id="fechaRegistro" type="text" class="form-control"  value="<fmt:formatDate value='${solicitud.fechaRegistro}' pattern='dd/MM/yyyy HH:mm'/>" disabled>
			</div>
			<div class="col-md-3 col-sm-3">
				<label class="control-label">EBN Registrante:</label>
				<input id="EBNRegistrante" type="text" class="form-control" disabled>
			</div>
			<div class="col-md-3 col-sm-3">
				<label class="control-label">EBN Asignado:</label>
				<input id="EBNAsignado" type="text" class="form-control" disabled>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default border-bottom-acordeonv2 margin-top-x20">
	<div class="panel-heading">
		<h3 class="panel-title">Datos del Titular</h3>
	</div>
	<div class="panel-body">
		<div class="panel panel-collapse margin-bottom-x5">
			<div class="form-group">
				<div class="col-md-3 col-sm-3">
					<label class="control-label"> Segmento:</label>
					<input id="segmento" class="form-control" type="text" disabled value="${solicitud.datosTitularModel.segmento}">
				</div>
				<div class="col-md-3 col-sm-3">
					<label class="control-label">C&oacute;digo Central:</label>
					<input id="codigoCentral" class="form-control" type="text" disabled value="${solicitud.datosTitularModel.codigoCentral}">
				</div>
				<div class="col-md-3 col-sm-3">
					<label class="control-label">Tipo Documento:</label>
					<input id="tipoDocumento" class="form-control" type="text" disabled value="${solicitud.datosTitularModel.tipoDocumento}">
				</div>
				<div class="col-md-3 col-sm-3">
					<label class="control-label">N&uacute;mero Documento:</label>
					<input id="" class="form-control" type="text" disabled value="${solicitud.datosTitularModel.numerDocumento}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3 col-sm-3 has-feedback">
					<label class="control-label"> Tipo Persona:</label>
					<input id="tipoPersona" class="form-control" type="text" disabled value="${solicitud.datosTitularModel.descripcionTipoPersona}">
				</div>
				<div class="col-md-3 col-sm-3">
					<label class="control-label"> Nombre del Cliente:</label>
					<input id="nombreCliente" class="form-control" type="text" disabled value="${solicitud.datosTitularModel.nombreCliente}">
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="row">
						<label class="control-label col-md-12">Cliente Nuevo:</label>
						<div class="col-md-12">
							<c:choose>
								<c:when test="${solicitud.datosTitularModel.esClienteNuevo eq 'true'}">
								<label class="radio-inline"><input name="radClienteNuevo" type="radio" checked disabled>Nuevo</label>
									<label class="radio-inline"><input name="radClienteNuevo" type="radio" disabled>Recurrente</label>
						    		</c:when>
						    	<c:when test="${solicitud.datosTitularModel.esClienteNuevo eq 'false'}">
						    		<label class="radio-inline"><input name="radClienteNuevo" type="radio" disabled>Nuevo</label>
									<label class="radio-inline"><input name="radClienteNuevo" type="radio" checked disabled>Recurrente</label>
						    	</c:when>
						    </c:choose>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3 col-sm-3">
					<div class="row">
						<label class="control-label col-md-12">Tipo de Renta:</label>
						<div class="col-md-12">
							<c:if test="${solicitud.datosTitularModel.categoria3 eq true}">
								<label class="checkbox-inline"><input type="checkbox" checked disabled>Renta 3ra Categ</label><br>
							</c:if>
							<c:if test="${solicitud.datosTitularModel.categoria1 eq true}">
								<label class="checkbox-inline"><input type="checkbox" checked disabled>Elegir si también Renta 1ra Categ</label>
							</c:if>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<label class="control-label">Regimen Tributario:</label>
					<select id="regimenTributario" class="form-control" disabled>
				    	<option>${regimenTributario.descripcion}</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-3">
					<label class="control-label">Giro de Negocio:</label>
					<select id="giroNegocio" class="form-control" disabled>
				    	<option>${giroNegocio.descripcionGiroNegFu}</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
