<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<p class="block-error iconed-24">
	<span class="icon-24 red m01-alerta"></span>
	<c:if
		test="${not empty errorProceso.errores and fn:length(errorProceso.errores) > 0}">
		<c:forEach items="${errorProceso.errores}" var="e">
			<tr>
				<td colspan="2"><span class="icon-24 red m01-alerta"></span><strong>${e.mensajeError}</strong>
				</td>
			</tr>
		</c:forEach>
	</c:if>
</p>
