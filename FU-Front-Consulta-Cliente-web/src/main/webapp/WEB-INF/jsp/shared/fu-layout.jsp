<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html lang="es-PE">

	<head>
		<script type="text/javascript">
			var contextST = '${CONTEXT_ST}';
		</script>
	    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Sistema File Unico Consulta de Clientes</title>
	    <tiles:insertAttribute name="resources"/>
	</head>
	
	<body>
	    
	    <div id="backDialog" class="backOpacity" style="display:none;"></div>
		<div id="preloader" class="preloader" style="display:none;"></div>
	   	<div id="status" class="preloader" style="display:none">&nbsp;</div>	    
	    <span class="btn_cerrar" id="msgbox" style="display:none"></span>
	    <button type="button" id="cerrar" class="btn_cerrar close" data-dismiss="alert" style="display:none" onclick="cerrarAlert()">&times;</button>
<%-- 	    <input id="contextPath" type="hidden" value="${pageContext.request.contextPath}" />  --%>
<%--     <input id="contextPath" type="hidden" value="/FUtest${pageContext.servletContext.contextPath}"/>  --%>
    	<input id="contextPath" type="hidden" value="${applicationScope['fileunicoJunction']}${pageContext.servletContext.contextPath}" />    
		<div class="container">
		
			<tiles:insertAttribute name="slider" />
		
			<tiles:insertAttribute name="body" />
			
			<tiles:insertAttribute name="footer"/>
			
		</div>
		
		<tiles:insertAttribute name="modal" />
		<tiles:insertAttribute name="modal2" />

	</body>

</html>