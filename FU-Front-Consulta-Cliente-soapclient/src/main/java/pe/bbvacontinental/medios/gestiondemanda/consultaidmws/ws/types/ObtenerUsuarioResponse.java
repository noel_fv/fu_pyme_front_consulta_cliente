
package pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.base.AuditResponse;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.base.Parametros;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="auditResponse" type="{http://bbvacontinental.pe/medios/gestiondemanda/consultaidmws/ws/types/base}AuditResponse"/>
 *         &lt;element name="registro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombres" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apellidos" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descripcion_puesto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="puesto_corporativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="puesto_funcional_local" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oficina" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="listaParametrosResponse" type="{http://bbvacontinental.pe/medios/gestiondemanda/consultaidmws/ws/types/base}Parametros"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "auditResponse",
    "registro",
    "nombres",
    "apellidos",
    "descripcionPuesto",
    "puestoCorporativo",
    "puestoFuncionalLocal",
    "oficina",
    "listaParametrosResponse"
})
@XmlRootElement(name = "obtenerUsuarioResponse")
public class ObtenerUsuarioResponse {

    @XmlElement(required = true)
    protected AuditResponse auditResponse;
    @XmlElement(required = true)
    protected String registro;
    @XmlElement(required = true)
    protected String nombres;
    @XmlElement(required = true)
    protected String apellidos;
    @XmlElement(name = "descripcion_puesto", required = true)
    protected String descripcionPuesto;
    @XmlElement(name = "puesto_corporativo", required = true)
    protected String puestoCorporativo;
    @XmlElement(name = "puesto_funcional_local", required = true)
    protected String puestoFuncionalLocal;
    @XmlElement(required = true)
    protected String oficina;
    @XmlElement(required = true)
    protected Parametros listaParametrosResponse;

    /**
     * Obtiene el valor de la propiedad auditResponse.
     * 
     * @return
     *     possible object is
     *     {@link AuditResponse }
     *     
     */
    public AuditResponse getAuditResponse() {
        return auditResponse;
    }

    /**
     * Define el valor de la propiedad auditResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditResponse }
     *     
     */
    public void setAuditResponse(AuditResponse value) {
        this.auditResponse = value;
    }

    /**
     * Obtiene el valor de la propiedad registro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistro() {
        return registro;
    }

    /**
     * Define el valor de la propiedad registro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistro(String value) {
        this.registro = value;
    }

    /**
     * Obtiene el valor de la propiedad nombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * Define el valor de la propiedad nombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombres(String value) {
        this.nombres = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Define el valor de la propiedad apellidos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidos(String value) {
        this.apellidos = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionPuesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionPuesto() {
        return descripcionPuesto;
    }

    /**
     * Define el valor de la propiedad descripcionPuesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionPuesto(String value) {
        this.descripcionPuesto = value;
    }

    /**
     * Obtiene el valor de la propiedad puestoCorporativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuestoCorporativo() {
        return puestoCorporativo;
    }

    /**
     * Define el valor de la propiedad puestoCorporativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuestoCorporativo(String value) {
        this.puestoCorporativo = value;
    }

    /**
     * Obtiene el valor de la propiedad puestoFuncionalLocal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuestoFuncionalLocal() {
        return puestoFuncionalLocal;
    }

    /**
     * Define el valor de la propiedad puestoFuncionalLocal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuestoFuncionalLocal(String value) {
        this.puestoFuncionalLocal = value;
    }

    /**
     * Obtiene el valor de la propiedad oficina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * Define el valor de la propiedad oficina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficina(String value) {
        this.oficina = value;
    }

    /**
     * Obtiene el valor de la propiedad listaParametrosResponse.
     * 
     * @return
     *     possible object is
     *     {@link Parametros }
     *     
     */
    public Parametros getListaParametrosResponse() {
        return listaParametrosResponse;
    }

    /**
     * Define el valor de la propiedad listaParametrosResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link Parametros }
     *     
     */
    public void setListaParametrosResponse(Parametros value) {
        this.listaParametrosResponse = value;
    }

}
