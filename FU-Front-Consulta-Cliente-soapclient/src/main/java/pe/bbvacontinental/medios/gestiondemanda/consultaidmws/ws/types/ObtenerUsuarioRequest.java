
package pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.base.AuditRequest;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.base.Parametros;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="auditRequest" type="{http://bbvacontinental.pe/medios/gestiondemanda/consultaidmws/ws/types/base}AuditRequest"/>
 *         &lt;element name="registro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="listaParametrosRequest" type="{http://bbvacontinental.pe/medios/gestiondemanda/consultaidmws/ws/types/base}Parametros"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "auditRequest",
    "registro",
    "listaParametrosRequest"
})
@XmlRootElement(name = "obtenerUsuarioRequest")
public class ObtenerUsuarioRequest {

    @XmlElement(required = true)
    protected AuditRequest auditRequest;
    @XmlElement(required = true)
    protected String registro;
    @XmlElement(required = true)
    protected Parametros listaParametrosRequest;

    /**
     * Obtiene el valor de la propiedad auditRequest.
     * 
     * @return
     *     possible object is
     *     {@link AuditRequest }
     *     
     */
    public AuditRequest getAuditRequest() {
        return auditRequest;
    }

    /**
     * Define el valor de la propiedad auditRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditRequest }
     *     
     */
    public void setAuditRequest(AuditRequest value) {
        this.auditRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad registro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistro() {
        return registro;
    }

    /**
     * Define el valor de la propiedad registro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistro(String value) {
        this.registro = value;
    }

    /**
     * Obtiene el valor de la propiedad listaParametrosRequest.
     * 
     * @return
     *     possible object is
     *     {@link Parametros }
     *     
     */
    public Parametros getListaParametrosRequest() {
        return listaParametrosRequest;
    }

    /**
     * Define el valor de la propiedad listaParametrosRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link Parametros }
     *     
     */
    public void setListaParametrosRequest(Parametros value) {
        this.listaParametrosRequest = value;
    }

}
