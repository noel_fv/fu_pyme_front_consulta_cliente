
package pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AuditRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AuditRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTransaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ipAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usuarioAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuditRequest", propOrder = {
    "idTransaccion",
    "ipAplicacion",
    "nombreAplicacion",
    "usuarioAplicacion"
})
public class AuditRequest {

    @XmlElement(required = true)
    protected String idTransaccion;
    @XmlElement(required = true)
    protected String nombreAplicacion;
    @XmlElement(required = true)
    protected String ipAplicacion;
    @XmlElement(required = true)
    protected String usuarioAplicacion;

    /**
     * Obtiene el valor de la propiedad idTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * Define el valor de la propiedad idTransaccion.
     * 
     * @param valor
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTransaccion(String valor) {
        this.idTransaccion = valor;
    }

    /**
     * Obtiene el valor de la propiedad ipAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpAplicacion() {
        return ipAplicacion;
    }

    /**
     * Define el valor de la propiedad ipAplicacion.
     * 
     * @param valor
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpAplicacion(String valor) {
        this.ipAplicacion = valor;
    }

    /**
     * Obtiene el valor de la propiedad nombreAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAplicacion() {
        return nombreAplicacion;
    }

    /**
     * Define el valor de la propiedad nombreAplicacion.
     * 
     * @param valor
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAplicacion(String valor) {
        this.nombreAplicacion = valor;
    }

    /**
     * Obtiene el valor de la propiedad usuarioAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioAplicacion() {
        return usuarioAplicacion;
    }

    /**
     * Define el valor de la propiedad usuarioAplicacion.
     * 
     * @param valor
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioAplicacion(String valor) {
        this.usuarioAplicacion = valor;
    }

}
