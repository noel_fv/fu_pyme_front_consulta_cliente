
package pe.com.bbva.hi.fileunico.cabecera;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestHeader complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoTerminalEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoAplicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaHoraEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idSesion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPeticionEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPeticionBanco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idInterconexion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestHeader", propOrder = {
    "codigoEmpresa",
    "codigoTerminalEmpresa",
    "canal",
    "codigoAplicacion",
    "usuario",
    "fechaHoraEnvio",
    "idSesion",
    "idPeticionEmpresa",
    "idPeticionBanco",
    "idOperacion",
    "idServicio",
    "idInterconexion"
})
public class RequestHeader {

    protected String codigoEmpresa;
    protected String codigoTerminalEmpresa;
    protected String canal;
    protected String codigoAplicacion;
    protected String usuario;
    protected String fechaHoraEnvio;
    protected String idSesion;
    protected String idPeticionEmpresa;
    protected String idPeticionBanco;
    protected String idOperacion;
    protected String idServicio;
    protected String idInterconexion;

    /**
     * Obtiene el valor de la propiedad codigoEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    /**
     * Define el valor de la propiedad codigoEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoEmpresa(String value) {
        this.codigoEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoTerminalEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTerminalEmpresa() {
        return codigoTerminalEmpresa;
    }

    /**
     * Define el valor de la propiedad codigoTerminalEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTerminalEmpresa(String value) {
        this.codigoTerminalEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad canal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanal() {
        return canal;
    }

    /**
     * Define el valor de la propiedad canal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanal(String value) {
        this.canal = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAplicacion() {
        return codigoAplicacion;
    }

    /**
     * Define el valor de la propiedad codigoAplicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAplicacion(String value) {
        this.codigoAplicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define el valor de la propiedad usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaHoraEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaHoraEnvio() {
        return fechaHoraEnvio;
    }

    /**
     * Define el valor de la propiedad fechaHoraEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaHoraEnvio(String value) {
        this.fechaHoraEnvio = value;
    }

    /**
     * Obtiene el valor de la propiedad idSesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSesion() {
        return idSesion;
    }

    /**
     * Define el valor de la propiedad idSesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSesion(String value) {
        this.idSesion = value;
    }

    /**
     * Obtiene el valor de la propiedad idPeticionEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPeticionEmpresa() {
        return idPeticionEmpresa;
    }

    /**
     * Define el valor de la propiedad idPeticionEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPeticionEmpresa(String value) {
        this.idPeticionEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad idPeticionBanco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPeticionBanco() {
        return idPeticionBanco;
    }

    /**
     * Define el valor de la propiedad idPeticionBanco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPeticionBanco(String value) {
        this.idPeticionBanco = value;
    }

    /**
     * Obtiene el valor de la propiedad idOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOperacion() {
        return idOperacion;
    }

    /**
     * Define el valor de la propiedad idOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOperacion(String value) {
        this.idOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdServicio() {
        return idServicio;
    }

    /**
     * Define el valor de la propiedad idServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdServicio(String value) {
        this.idServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad idInterconexion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInterconexion() {
        return idInterconexion;
    }

    /**
     * Define el valor de la propiedad idInterconexion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInterconexion(String value) {
        this.idInterconexion = value;
    }

}
