
package pe.com.bbva.hi.fileunico.ccli;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.com.bbva.hi.fileunico.ccli package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private static final  QName _IntegrationRequest_QNAME = new QName("http://fileunico.hi.bbva.com.pe/ccli/", "integrationRequest");
	private static final  QName _IntegrationResponse_QNAME = new QName("http://fileunico.hi.bbva.com.pe/ccli/", "integrationResponse");
    
    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.com.bbva.hi.fileunico.ccli
     * 
     */
    public ObjectFactory() {
    	super();
    }

    /**
     * Create an instance of {@link IntegrationResponse }
     * 
     */
    public IntegrationResponse createIntegrationResponse() {
        return new IntegrationResponse();
    }

    /**
     * Create an instance of {@link IntegrationRequest }
     * 
     */
    public IntegrationRequest createIntegrationRequest() {
        return new IntegrationRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntegrationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fileunico.hi.bbva.com.pe/ccli/", name = "integrationRequest")
    public JAXBElement<IntegrationRequest> createIntegrationRequest(IntegrationRequest value) {
        return new JAXBElement<>(_IntegrationRequest_QNAME, IntegrationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntegrationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fileunico.hi.bbva.com.pe/ccli/", name = "integrationResponse")
    public JAXBElement<IntegrationResponse> createIntegrationResponse(IntegrationResponse value) {
        return new JAXBElement<>(_IntegrationResponse_QNAME, IntegrationResponse.class, null, value);
    }

}
