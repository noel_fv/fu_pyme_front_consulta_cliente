
package pe.com.bbva.hi.fileunico.cconcli;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import pe.com.bbva.hi.fileunico.cabecera.ResponseHeader;


/**
 * <p>Clase Java para integrationResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="integrationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseHeader" type="{http://fileunico.hi.bbva.com.pe/cabecera/}ResponseHeader" minOccurs="0"/>
 *         &lt;element name="codigoCentral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroEntidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroOficina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codProductoAltamira" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSubproductoAltamira" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaFormalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaCancelacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divisaMoneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="garantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuarioFormaliza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oficinaGestora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indicadorContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral02" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv02" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv02" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral03" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv03" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv03" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral05" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv05" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv05" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral06" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv06" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv06" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral07" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv07" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv07" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral08" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv08" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv08" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral09" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv09" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv09" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCentral10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoInterv10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secuenciaInterv10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scoring" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "integrationResponse", propOrder = {
    "responseHeader",
    "codigoCentral",
    "tipoDocumento",
    "numeroDocumento",
    "nombreCliente",
    "numeroEntidad",
    "numeroOficina",
    "numeroProducto",
    "numeroCuenta",
    "codProductoAltamira",
    "codSubproductoAltamira",
    "fechaAlta",
    "fechaFormalizacion",
    "fechaCancelacion",
    "divisaMoneda",
    "garantia",
    "usuarioFormaliza",
    "oficinaGestora",
    "indicadorContrato",
    "codigoCentral01",
    "tipoInterv01",
    "secuenciaInterv01",
    "codigoCentral02",
    "tipoInterv02",
    "secuenciaInterv02",
    "codigoCentral03",
    "tipoInterv03",
    "secuenciaInterv03",
    "codigoCentral04",
    "tipoInterv04",
    "secuenciaInterv04",
    "codigoCentral05",
    "tipoInterv05",
    "secuenciaInterv05",
    "codigoCentral06",
    "tipoInterv06",
    "secuenciaInterv06",
    "codigoCentral07",
    "tipoInterv07",
    "secuenciaInterv07",
    "codigoCentral08",
    "tipoInterv08",
    "secuenciaInterv08",
    "codigoCentral09",
    "tipoInterv09",
    "secuenciaInterv09",
    "codigoCentral10",
    "tipoInterv10",
    "secuenciaInterv10",
    "scoring"
})
public class IntegrationResponse {

    protected ResponseHeader responseHeader;
    protected String codigoCentral;
    protected String tipoDocumento;
    protected String numeroDocumento;
    protected String nombreCliente;
    protected String numeroEntidad;
    protected String numeroOficina;
    protected String numeroProducto;
    protected String numeroCuenta;
    protected String codProductoAltamira;
    protected String codSubproductoAltamira;
    protected String fechaAlta;
    protected String fechaFormalizacion;
    protected String fechaCancelacion;
    protected String divisaMoneda;
    protected String garantia;
    protected String usuarioFormaliza;
    protected String oficinaGestora;
    protected String indicadorContrato;
    protected String codigoCentral01;
    protected String tipoInterv01;
    protected String secuenciaInterv01;
    protected String codigoCentral02;
    protected String tipoInterv02;
    protected String secuenciaInterv02;
    protected String codigoCentral03;
    protected String tipoInterv03;
    protected String secuenciaInterv03;
    protected String codigoCentral04;
    protected String tipoInterv04;
    protected String secuenciaInterv04;
    protected String codigoCentral05;
    protected String tipoInterv05;
    protected String secuenciaInterv05;
    protected String codigoCentral06;
    protected String tipoInterv06;
    protected String secuenciaInterv06;
    protected String codigoCentral07;
    protected String tipoInterv07;
    protected String secuenciaInterv07;
    protected String codigoCentral08;
    protected String tipoInterv08;
    protected String secuenciaInterv08;
    protected String codigoCentral09;
    protected String tipoInterv09;
    protected String secuenciaInterv09;
    protected String codigoCentral10;
    protected String tipoInterv10;
    protected String secuenciaInterv10;
    protected String scoring;

    /**
     * Obtiene el valor de la propiedad responseHeader.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Define el valor de la propiedad responseHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral() {
        return codigoCentral;
    }

    /**
     * Define el valor de la propiedad codigoCentral.
     * 
     * @param val
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral(String val) {
        this.codigoCentral = val;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param val
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String val) {
        this.tipoDocumento = val;
    }

    /**
     * Obtiene el valor de la propiedad numeroDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * Define el valor de la propiedad numeroDocumento.
     * 
     * @param val
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDocumento(String val) {
        this.numeroDocumento = val;
    }

    /**
     * Obtiene el valor de la propiedad nombreCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Define el valor de la propiedad nombreCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCliente(String value) {
        this.nombreCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroEntidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroEntidad() {
        return numeroEntidad;
    }

    /**
     * Define el valor de la propiedad numeroEntidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroEntidad(String value) {
        this.numeroEntidad = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOficina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroOficina() {
        return numeroOficina;
    }

    /**
     * Define el valor de la propiedad numeroOficina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroOficina(String value) {
        this.numeroOficina = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProducto() {
        return numeroProducto;
    }

    /**
     * Define el valor de la propiedad numeroProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProducto(String value) {
        this.numeroProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Define el valor de la propiedad numeroCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCuenta(String value) {
        this.numeroCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad codProductoAltamira.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProductoAltamira() {
        return codProductoAltamira;
    }

    /**
     * Define el valor de la propiedad codProductoAltamira.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProductoAltamira(String value) {
        this.codProductoAltamira = value;
    }

    /**
     * Obtiene el valor de la propiedad codSubproductoAltamira.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSubproductoAltamira() {
        return codSubproductoAltamira;
    }

    /**
     * Define el valor de la propiedad codSubproductoAltamira.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSubproductoAltamira(String value) {
        this.codSubproductoAltamira = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAlta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAlta() {
        return fechaAlta;
    }

    /**
     * Define el valor de la propiedad fechaAlta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAlta(String value) {
        this.fechaAlta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFormalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFormalizacion() {
        return fechaFormalizacion;
    }

    /**
     * Define el valor de la propiedad fechaFormalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFormalizacion(String value) {
        this.fechaFormalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCancelacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCancelacion() {
        return fechaCancelacion;
    }

    /**
     * Define el valor de la propiedad fechaCancelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCancelacion(String value) {
        this.fechaCancelacion = value;
    }

    /**
     * Obtiene el valor de la propiedad divisaMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivisaMoneda() {
        return divisaMoneda;
    }

    /**
     * Define el valor de la propiedad divisaMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivisaMoneda(String value) {
        this.divisaMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad garantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * Define el valor de la propiedad garantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGarantia(String value) {
        this.garantia = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioFormaliza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioFormaliza() {
        return usuarioFormaliza;
    }

    /**
     * Define el valor de la propiedad usuarioFormaliza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioFormaliza(String value) {
        this.usuarioFormaliza = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaGestora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficinaGestora() {
        return oficinaGestora;
    }

    /**
     * Define el valor de la propiedad oficinaGestora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficinaGestora(String value) {
        this.oficinaGestora = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorContrato() {
        return indicadorContrato;
    }

    /**
     * Define el valor de la propiedad indicadorContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorContrato(String value) {
        this.indicadorContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral01.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral01() {
        return codigoCentral01;
    }

    /**
     * Define el valor de la propiedad codigoCentral01.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral01(String value) {
        this.codigoCentral01 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv01.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv01() {
        return tipoInterv01;
    }

    /**
     * Define el valor de la propiedad tipoInterv01.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv01(String value) {
        this.tipoInterv01 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv01.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv01() {
        return secuenciaInterv01;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv01.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv01(String value) {
        this.secuenciaInterv01 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral02.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral02() {
        return codigoCentral02;
    }

    /**
     * Define el valor de la propiedad codigoCentral02.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral02(String value) {
        this.codigoCentral02 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv02.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv02() {
        return tipoInterv02;
    }

    /**
     * Define el valor de la propiedad tipoInterv02.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv02(String value) {
        this.tipoInterv02 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv02.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv02() {
        return secuenciaInterv02;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv02.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv02(String value) {
        this.secuenciaInterv02 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral03.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral03() {
        return codigoCentral03;
    }

    /**
     * Define el valor de la propiedad codigoCentral03.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral03(String value) {
        this.codigoCentral03 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv03.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv03() {
        return tipoInterv03;
    }

    /**
     * Define el valor de la propiedad tipoInterv03.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv03(String value) {
        this.tipoInterv03 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv03.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv03() {
        return secuenciaInterv03;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv03.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv03(String value) {
        this.secuenciaInterv03 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral04.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral04() {
        return codigoCentral04;
    }

    /**
     * Define el valor de la propiedad codigoCentral04.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral04(String value) {
        this.codigoCentral04 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv04.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv04() {
        return tipoInterv04;
    }

    /**
     * Define el valor de la propiedad tipoInterv04.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv04(String value) {
        this.tipoInterv04 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv04.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv04() {
        return secuenciaInterv04;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv04.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv04(String value) {
        this.secuenciaInterv04 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral05.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral05() {
        return codigoCentral05;
    }

    /**
     * Define el valor de la propiedad codigoCentral05.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral05(String value) {
        this.codigoCentral05 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv05.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv05() {
        return tipoInterv05;
    }

    /**
     * Define el valor de la propiedad tipoInterv05.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv05(String value) {
        this.tipoInterv05 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv05.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv05() {
        return secuenciaInterv05;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv05.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv05(String value) {
        this.secuenciaInterv05 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral06.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral06() {
        return codigoCentral06;
    }

    /**
     * Define el valor de la propiedad codigoCentral06.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral06(String value) {
        this.codigoCentral06 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv06.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv06() {
        return tipoInterv06;
    }

    /**
     * Define el valor de la propiedad tipoInterv06.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv06(String value) {
        this.tipoInterv06 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv06.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv06() {
        return secuenciaInterv06;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv06.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv06(String value) {
        this.secuenciaInterv06 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral07.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral07() {
        return codigoCentral07;
    }

    /**
     * Define el valor de la propiedad codigoCentral07.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral07(String value) {
        this.codigoCentral07 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv07.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv07() {
        return tipoInterv07;
    }

    /**
     * Define el valor de la propiedad tipoInterv07.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv07(String value) {
        this.tipoInterv07 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv07.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv07() {
        return secuenciaInterv07;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv07.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv07(String value) {
        this.secuenciaInterv07 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral08.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral08() {
        return codigoCentral08;
    }

    /**
     * Define el valor de la propiedad codigoCentral08.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral08(String value) {
        this.codigoCentral08 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv08.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv08() {
        return tipoInterv08;
    }

    /**
     * Define el valor de la propiedad tipoInterv08.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv08(String value) {
        this.tipoInterv08 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv08.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv08() {
        return secuenciaInterv08;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv08.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv08(String value) {
        this.secuenciaInterv08 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral09.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral09() {
        return codigoCentral09;
    }

    /**
     * Define el valor de la propiedad codigoCentral09.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral09(String value) {
        this.codigoCentral09 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv09.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv09() {
        return tipoInterv09;
    }

    /**
     * Define el valor de la propiedad tipoInterv09.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv09(String value) {
        this.tipoInterv09 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv09.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv09() {
        return secuenciaInterv09;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv09.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv09(String value) {
        this.secuenciaInterv09 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral10() {
        return codigoCentral10;
    }

    /**
     * Define el valor de la propiedad codigoCentral10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral10(String value) {
        this.codigoCentral10 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoInterv10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoInterv10() {
        return tipoInterv10;
    }

    /**
     * Define el valor de la propiedad tipoInterv10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoInterv10(String value) {
        this.tipoInterv10 = value;
    }

    /**
     * Obtiene el valor de la propiedad secuenciaInterv10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuenciaInterv10() {
        return secuenciaInterv10;
    }

    /**
     * Define el valor de la propiedad secuenciaInterv10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuenciaInterv10(String value) {
        this.secuenciaInterv10 = value;
    }

    /**
     * Obtiene el valor de la propiedad scoring.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScoring() {
        return scoring;
    }

    /**
     * Define el valor de la propiedad scoring.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScoring(String value) {
        this.scoring = value;
    }

}
