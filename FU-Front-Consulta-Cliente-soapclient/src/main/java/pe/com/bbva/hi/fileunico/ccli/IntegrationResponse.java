
package pe.com.bbva.hi.fileunico.ccli;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import pe.com.bbva.hi.fileunico.cabecera.ResponseHeader;


/**
 * <p>Clase Java para integrationResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="integrationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseHeader" type="{http://fileunico.hi.bbva.com.pe/cabecera/}ResponseHeader" minOccurs="0"/>
 *         &lt;element name="codigoCentral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoPersona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteFatca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientePep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoSegmento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcionSegmento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oficinaGestora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoGestor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codGiroEconomico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desGiroEconomico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deuda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nuevoCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="haberes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "integrationResponse", propOrder = {
    "responseHeader",
    "codigoCentral",
    "nombres",
    "tipoDocumento",
    "numeroDocumento",
    "tipoPersona",
    "clienteFatca",
    "clientePep",
    "codigoSegmento",
    "descripcionSegmento",
    "oficinaGestora",
    "codigoGestor",
    "codGiroEconomico",
    "desGiroEconomico",
    "fechaNacimiento",
    "codEstadoCivil",
    "desEstadoCivil",
    "deuda",
    "moneda",
    "nuevoCliente",
    "buro",
    "haberes"
})
public class IntegrationResponse {

    protected ResponseHeader responseHeader;
    protected String codigoCentral;
    protected String nombres;
    protected String tipoDocumento;
    protected String numeroDocumento;
    protected String tipoPersona;
    protected String clienteFatca;
    protected String clientePep;
    protected String codigoSegmento;
    protected String descripcionSegmento;
    protected String oficinaGestora;
    protected String codigoGestor;
    protected String codGiroEconomico;
    protected String desGiroEconomico;
    protected String fechaNacimiento;
    protected String codEstadoCivil;
    protected String desEstadoCivil;
    protected String deuda;
    protected String moneda;
    protected String nuevoCliente;
    protected String buro;
    protected String haberes;

    /**
     * Obtiene el valor de la propiedad responseHeader.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Define el valor de la propiedad responseHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral() {
        return codigoCentral;
    }

    /**
     * Define el valor de la propiedad codigoCentral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral(String value) {
        this.codigoCentral = value;
    }

    /**
     * Obtiene el valor de la propiedad nombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * Define el valor de la propiedad nombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombres(String value) {
        this.nombres = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * Define el valor de la propiedad numeroDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDocumento(String value) {
        this.numeroDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPersona.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Define el valor de la propiedad tipoPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPersona(String value) {
        this.tipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad clienteFatca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteFatca() {
        return clienteFatca;
    }

    /**
     * Define el valor de la propiedad clienteFatca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteFatca(String value) {
        this.clienteFatca = value;
    }

    /**
     * Obtiene el valor de la propiedad clientePep.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientePep() {
        return clientePep;
    }

    /**
     * Define el valor de la propiedad clientePep.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientePep(String value) {
        this.clientePep = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoSegmento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoSegmento() {
        return codigoSegmento;
    }

    /**
     * Define el valor de la propiedad codigoSegmento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoSegmento(String value) {
        this.codigoSegmento = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionSegmento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionSegmento() {
        return descripcionSegmento;
    }

    /**
     * Define el valor de la propiedad descripcionSegmento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionSegmento(String value) {
        this.descripcionSegmento = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaGestora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficinaGestora() {
        return oficinaGestora;
    }

    /**
     * Define el valor de la propiedad oficinaGestora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficinaGestora(String value) {
        this.oficinaGestora = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoGestor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoGestor() {
        return codigoGestor;
    }

    /**
     * Define el valor de la propiedad codigoGestor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoGestor(String value) {
        this.codigoGestor = value;
    }

    /**
     * Obtiene el valor de la propiedad codGiroEconomico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodGiroEconomico() {
        return codGiroEconomico;
    }

    /**
     * Define el valor de la propiedad codGiroEconomico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodGiroEconomico(String value) {
        this.codGiroEconomico = value;
    }

    /**
     * Obtiene el valor de la propiedad desGiroEconomico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesGiroEconomico() {
        return desGiroEconomico;
    }

    /**
     * Define el valor de la propiedad desGiroEconomico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesGiroEconomico(String value) {
        this.desGiroEconomico = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Define el valor de la propiedad fechaNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaNacimiento(String value) {
        this.fechaNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoCivil() {
        return codEstadoCivil;
    }

    /**
     * Define el valor de la propiedad codEstadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoCivil(String value) {
        this.codEstadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad desEstadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesEstadoCivil() {
        return desEstadoCivil;
    }

    /**
     * Define el valor de la propiedad desEstadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesEstadoCivil(String value) {
        this.desEstadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad deuda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeuda() {
        return deuda;
    }

    /**
     * Define el valor de la propiedad deuda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeuda(String value) {
        this.deuda = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad nuevoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNuevoCliente() {
        return nuevoCliente;
    }

    /**
     * Define el valor de la propiedad nuevoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNuevoCliente(String value) {
        this.nuevoCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad buro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuro() {
        return buro;
    }

    /**
     * Define el valor de la propiedad buro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuro(String value) {
        this.buro = value;
    }

    /**
     * Obtiene el valor de la propiedad haberes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHaberes() {
        return haberes;
    }

    /**
     * Define el valor de la propiedad haberes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHaberes(String value) {
        this.haberes = value;
    }

}
