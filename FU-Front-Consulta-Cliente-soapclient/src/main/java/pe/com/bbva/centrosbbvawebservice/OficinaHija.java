
package pe.com.bbva.centrosbbvawebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para OficinaHija complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OficinaHija">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreOficina" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oficinaMadre" type="{http://www.bbva.com.pe/CentrosBBVAWebService/}OficinaMadre"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OficinaHija", propOrder = {
    "id",
    "nombreOficina",
    "oficinaMadre"
})
public class OficinaHija {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String nombreOficina;
    @XmlElement(required = true)
    protected OficinaMadre oficinaMadre;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOficina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOficina() {
        return nombreOficina;
    }

    /**
     * Define el valor de la propiedad nombreOficina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOficina(String value) {
        this.nombreOficina = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaMadre.
     * 
     * @return
     *     possible object is
     *     {@link OficinaMadre }
     *     
     */
    public OficinaMadre getOficinaMadre() {
        return oficinaMadre;
    }

    /**
     * Define el valor de la propiedad oficinaMadre.
     * 
     * @param value
     *     allowed object is
     *     {@link OficinaMadre }
     *     
     */
    public void setOficinaMadre(OficinaMadre value) {
        this.oficinaMadre = value;
    }

}
