
package pe.com.bbva.centrosbbvawebservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para OficinaTerritorio complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OficinaTerritorio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreOficina" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oficinaMadre" type="{http://www.bbva.com.pe/CentrosBBVAWebService/}OficinaMadre"/>
 *         &lt;element name="idTerritorio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oficinasHijas" type="{http://www.bbva.com.pe/CentrosBBVAWebService/}OficinaHija" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OficinaTerritorio", propOrder = {
    "id",
    "nombreOficina",
    "oficinaMadre",
    "idTerritorio",
    "oficinasHijas"
})
public class OficinaTerritorio {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String nombreOficina;
    @XmlElement(required = true)
    protected OficinaMadre oficinaMadre;
    @XmlElement(required = true)
    protected String idTerritorio;
    @XmlElement(nillable = true)
    protected List<OficinaHija> oficinasHijas;    

    /**
     * Obtiene el valor de la propiedad nombreOficina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOficina() {
        return nombreOficina;
    }

    /**
     * Define el valor de la propiedad nombreOficina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOficina(String value) {
        this.nombreOficina = value;
    }/**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad idTerritorio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTerritorio() {
        return idTerritorio;
    }

    /**
     * Define el valor de la propiedad idTerritorio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTerritorio(String value) {
        this.idTerritorio = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaMadre.
     * 
     * @return
     *     possible object is
     *     {@link OficinaMadre }
     *     
     */
    public OficinaMadre getOficinaMadre() {
        return oficinaMadre;
    }

    /**
     * Define el valor de la propiedad oficinaMadre.
     * 
     * @param value
     *     allowed object is
     *     {@link OficinaMadre }
     *     
     */
    public void setOficinaMadre(OficinaMadre value) {
        this.oficinaMadre = value;
    }

    /**
     * Gets the value of the oficinasHijas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the oficinasHijas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOficinasHijas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OficinaHija }
     * 
     * 
     */
    public List<OficinaHija> getOficinasHijas() {
        if (oficinasHijas == null) {
            oficinasHijas = new ArrayList<>();
        }
        return this.oficinasHijas;
    }

}
