
package pe.com.bbva.centrosbbvawebservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Territorio complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Territorio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreTerritorio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="suprarea" type="{http://www.bbva.com.pe/CentrosBBVAWebService/}Suprarea"/>
 *         &lt;element name="oficinas" type="{http://www.bbva.com.pe/CentrosBBVAWebService/}OficinaTerritorio" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Territorio", propOrder = {
    "id",
    "nombreTerritorio",
    "suprarea",
    "oficinas"
})
public class Territorio {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String nombreTerritorio;
    @XmlElement(required = true)
    protected Suprarea suprarea;
    @XmlElement(nillable = true)
    protected List<OficinaTerritorio> oficinas;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreTerritorio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreTerritorio() {
        return nombreTerritorio;
    }

    /**
     * Define el valor de la propiedad nombreTerritorio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreTerritorio(String value) {
        this.nombreTerritorio = value;
    }

    /**
     * Obtiene el valor de la propiedad suprarea.
     * 
     * @return
     *     possible object is
     *     {@link Suprarea }
     *     
     */
    public Suprarea getSuprarea() {
        return suprarea;
    }

    /**
     * Define el valor de la propiedad suprarea.
     * 
     * @param value
     *     allowed object is
     *     {@link Suprarea }
     *     
     */
    public void setSuprarea(Suprarea value) {
        this.suprarea = value;
    }

    /**
     * Gets the value of the oficinas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the oficinas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOficinas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OficinaTerritorio }
     * 
     * 
     */
    public List<OficinaTerritorio> getOficinas() {
        if (oficinas == null) {
            oficinas = new ArrayList<>();
        }
        return this.oficinas;
    }

}
