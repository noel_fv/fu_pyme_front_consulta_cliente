
package pe.com.bbva.centrosbbvawebservice;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.com.bbva.centrosbbvawebservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.com.bbva.centrosbbvawebservice
     * 
     */
    public ObjectFactory() {
    	super();
    }

    /**
     * Create an instance of {@link ObtenerTerritorioRequest }
     * 
     */
    public ObtenerTerritorioRequest createObtenerTerritorioRequest() {
        return new ObtenerTerritorioRequest();
    }

    /**
     * Create an instance of {@link ListarOficinaRequest }
     * 
     */
    public ListarOficinaRequest createListarOficinaRequest() {
        return new ListarOficinaRequest();
    }

    /**
     * Create an instance of {@link ListarOficinaTerritorioRequest }
     * 
     */
    public ListarOficinaTerritorioRequest createListarOficinaTerritorioRequest() {
        return new ListarOficinaTerritorioRequest();
    }

    /**
     * Create an instance of {@link ObtenerOficinaResponse }
     * 
     */
    public ObtenerOficinaResponse createObtenerOficinaResponse() {
        return new ObtenerOficinaResponse();
    }

    /**
     * Create an instance of {@link Oficina }
     * 
     */
    public Oficina createOficina() {
        return new Oficina();
    }

    /**
     * Create an instance of {@link ListarAreaResponse }
     * 
     */
    public ListarAreaResponse createListarAreaResponse() {
        return new ListarAreaResponse();
    }

    /**
     * Create an instance of {@link Suprarea }
     * 
     */
    public Suprarea createSuprarea() {
        return new Suprarea();
    }

    /**
     * Create an instance of {@link ListarTerritorioRequest }
     * 
     */
    public ListarTerritorioRequest createListarTerritorioRequest() {
        return new ListarTerritorioRequest();
    }

    /**
     * Create an instance of {@link ListarTerritorioResponse }
     * 
     */
    public ListarTerritorioResponse createListarTerritorioResponse() {
        return new ListarTerritorioResponse();
    }

    /**
     * Create an instance of {@link Territorio }
     * 
     */
    public Territorio createTerritorio() {
        return new Territorio();
    }

    /**
     * Create an instance of {@link ListarOficinaTerritorioSuprareaRequest }
     * 
     */
    public ListarOficinaTerritorioSuprareaRequest createListarOficinaTerritorioSuprareaRequest() {
        return new ListarOficinaTerritorioSuprareaRequest();
    }

    /**
     * Create an instance of {@link ListarAreaRequest }
     * 
     */
    public ListarAreaRequest createListarAreaRequest() {
        return new ListarAreaRequest();
    }

    /**
     * Create an instance of {@link ObtenerTerritorioResponse }
     * 
     */
    public ObtenerTerritorioResponse createObtenerTerritorioResponse() {
        return new ObtenerTerritorioResponse();
    }

    /**
     * Create an instance of {@link ListarOficinaResponse }
     * 
     */
    public ListarOficinaResponse createListarOficinaResponse() {
        return new ListarOficinaResponse();
    }

    /**
     * Create an instance of {@link ObtenerOficinaRequest }
     * 
     */
    public ObtenerOficinaRequest createObtenerOficinaRequest() {
        return new ObtenerOficinaRequest();
    }

    /**
     * Create an instance of {@link ListarOficinaTerritorioSuprareaResponse }
     * 
     */
    public ListarOficinaTerritorioSuprareaResponse createListarOficinaTerritorioSuprareaResponse() {
        return new ListarOficinaTerritorioSuprareaResponse();
    }

    /**
     * Create an instance of {@link ListarOficinaTerritorioResponse }
     * 
     */
    public ListarOficinaTerritorioResponse createListarOficinaTerritorioResponse() {
        return new ListarOficinaTerritorioResponse();
    }

    /**
     * Create an instance of {@link OficinaHija }
     * 
     */
    public OficinaHija createOficinaHija() {
        return new OficinaHija();
    }

    /**
     * Create an instance of {@link Banca }
     * 
     */
    public Banca createBanca() {
        return new Banca();
    }

    /**
     * Create an instance of {@link OficinaMadre }
     * 
     */
    public OficinaMadre createOficinaMadre() {
        return new OficinaMadre();
    }

    /**
     * Create an instance of {@link OficinaTerritorio }
     * 
     */
    public OficinaTerritorio createOficinaTerritorio() {
        return new OficinaTerritorio();
    }

}
