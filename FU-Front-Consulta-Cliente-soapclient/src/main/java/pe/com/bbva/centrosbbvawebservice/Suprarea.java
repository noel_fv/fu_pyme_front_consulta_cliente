
package pe.com.bbva.centrosbbvawebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Suprarea complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Suprarea">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreSuprarea" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="suprarea" type="{http://www.bbva.com.pe/CentrosBBVAWebService/}Banca"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Suprarea", propOrder = {
    "id",
    "nombreSuprarea",
    "suprarea"
})
public class Suprarea {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String nombreSuprarea;
    @XmlElement(required = true)
    protected Banca suprarea;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreSuprarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreSuprarea() {
        return nombreSuprarea;
    }

    /**
     * Define el valor de la propiedad nombreSuprarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreSuprarea(String value) {
        this.nombreSuprarea = value;
    }

    /**
     * Obtiene el valor de la propiedad suprarea.
     * 
     * @return
     *     possible object is
     *     {@link Banca }
     *     
     */
    public Banca getSuprarea() {
        return suprarea;
    }

    /**
     * Define el valor de la propiedad suprarea.
     * 
     * @param value
     *     allowed object is
     *     {@link Banca }
     *     
     */
    public void setSuprarea(Banca value) {
        this.suprarea = value;
    }

}
