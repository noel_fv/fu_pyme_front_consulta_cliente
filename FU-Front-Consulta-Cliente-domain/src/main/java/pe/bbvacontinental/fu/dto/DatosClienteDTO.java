package pe.bbvacontinental.fu.dto;

import java.io.Serializable;

public class DatosClienteDTO  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5788460851014070303L;
	
	private String codigoCentral;
	private String tipoDocumentoIdentidad;
	private String numeroDocumento;
	private String tipoPersona;
	private String nombreCompleto;
	private String codigoTerritorio;
	private String nombreTerritorio;
	private String codigoOficina;
	private String nombreOficina;
	private String codigoSegmento;
	private String descripcionSegmento;
	
	public String getTipoDocumentoIdentidad() {
		return tipoDocumentoIdentidad;
	}

	public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
		this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getCodigoCentral() {
		return codigoCentral;
	}

	public void setCodigoCentral(String codigoCentral) {
		this.codigoCentral = codigoCentral;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
	public String getCodigoOficina() {
		return codigoOficina;
	}

	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}

	public String getCodigoTerritorio() {
		return codigoTerritorio;
	}

	public void setCodigoTerritorio(String codigoTerritorio) {
		this.codigoTerritorio = codigoTerritorio;
	}

	public String getNombreTerritorio() {
		return nombreTerritorio;
	}

	public void setNombreTerritorio(String nombreTerritorio) {
		this.nombreTerritorio = nombreTerritorio;
	}

	public String getCodigoSegmento() {
		return codigoSegmento;
	}

	public void setCodigoSegmento(String codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}

	public String getDescripcionSegmento() {
		return descripcionSegmento;
	}

	public void setDescripcionSegmento(String descripcionSegmento) {
		this.descripcionSegmento = descripcionSegmento;
	}

	public String getNombreOficina() {
		return nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

}
