package pe.bbvacontinental.fu.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TerritorioDTO implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 7612694882159198930L;
	protected String id;
    protected String nombreTerritorio;
    
	private List<OficinaDTO> listaOficinas= new ArrayList<>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombreTerritorio() {
		return nombreTerritorio;
	}
	public void setNombreTerritorio(String nombreTerritorio) {
		this.nombreTerritorio = nombreTerritorio;
	}
	public List<OficinaDTO> getListaOficinas() {
		return listaOficinas;
	}
	public void setListaOficinas(List<OficinaDTO> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}

}
