package pe.bbvacontinental.fu.dto;

import java.io.Serializable;

public class OficinaDTO implements Serializable{
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 5824758161622791050L;
	
	
	private String codigoOficina;
	private String nombreOficina;
	private String codigoTerritorio;
	private String nombreTerritorio;
	private String tipoResultado;
	private String mensaje;
	
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nomOficina) {
		this.nombreOficina = nomOficina;
	}
	public String getCodigoTerritorio() {
		return codigoTerritorio;
	}
	public void setCodigoTerritorio(String codTerritorio) {
		this.codigoTerritorio = codTerritorio;
	}
	public String getNombreTerritorio() {
		return nombreTerritorio;
	}
	public void setNombreTerritorio(String nomTerritorio) {
		this.nombreTerritorio = nomTerritorio;
	}
	public String getCodigoOficina() {
		return codigoOficina;
	}
	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}
	public String getTipoResultado() {
		return tipoResultado;
	}
	public void setTipoResultado(String tipoResultado) {
		this.tipoResultado = tipoResultado;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
