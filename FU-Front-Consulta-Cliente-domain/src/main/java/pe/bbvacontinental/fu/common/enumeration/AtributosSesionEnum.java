package pe.bbvacontinental.fu.common.enumeration;

public enum AtributosSesionEnum {

	USUARIO_FU("codigo_usuario_fucc"),
	DATOS_CLIENTE("datosCliente_fcc"),
	CODIGO_SOLICITUD("codigoSolicitud"),
	ID_INSTANCIA_BPM("numero_intancia_bpm"),
	LOCALCACHEMODEL("loca_cache_model"),
	VERSION_ST("VERSION_ST");

	private String codigo;

	private AtributosSesionEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
}

