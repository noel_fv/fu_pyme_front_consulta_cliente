package pe.bbvacontinental.fu.common.enumeration;

public enum TipoMonedaEnum {
	
	SOLES("1","PEN", "SOLES", "S/."),
	DOLARES("2","USD", "DOLARES", "$");
	
	private String id;
	private String valor;
	private String descripcion;
	private String simbolo;
	
	private TipoMonedaEnum(String id, String valor, String descripcion, String simbolo){
		
		this.id = id;
		this.valor = valor;
		this.descripcion = descripcion;
		this.simbolo = simbolo;
		
	}
	
	public String getId() {
		return id;
	}

	protected void setId(String id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	protected void setValor(String valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	protected void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSimbolo() {
		return simbolo;
	}

	protected void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
}
