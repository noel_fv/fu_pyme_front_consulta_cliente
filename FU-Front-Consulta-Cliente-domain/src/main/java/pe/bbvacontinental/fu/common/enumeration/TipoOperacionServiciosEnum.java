package pe.bbvacontinental.fu.common.enumeration;

public enum TipoOperacionServiciosEnum {
	
	SERV_ACTUALIZACION_ESTADO("1","ACTEST"),
	SERV_REGISTRO_DICTAMEN("2","REGDICWP"),
	SERV_COMUNICACION_ERRORES("3","COMERR"),
	SERV_ALERTA_REGISTRO_COMPLETO_WP("4","REGCOMWP"),
	SERV_GUARDA_CHECKLIST("5","GUARCHECK"),
	SERV_PANTALLA_PERSONA_NATURAL("6","CONCLI"),
	SERV_PANTALLA_PERSONA_JURIDICA("7","CONCLIPJ"),
	SERV_CONSULTA_DATOS_NEGOCIO_WP("8","CONEGWP");
	
	private String codigo;
	
	private String descripcion;
	
	
	private TipoOperacionServiciosEnum(String codigo, String descripcion){
		this.setCodigo(codigo);
		this.setDescripcion(descripcion);	
	}

	public String getCodigo() {
		return codigo;
	}

	protected void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	protected void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
