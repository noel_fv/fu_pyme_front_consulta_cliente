package pe.bbvacontinental.fu.common.excepcion;

import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.validation.Errors;

/**
 * The Class ExcepcionBBVA.
 */
public class ExcepcionBBVA extends RuntimeException implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String CLASES_FU = "pe.bbvacontinental";
	private final String codError;
	private final Exception excepcion;
	private final Errors errores;
	private final String mensajePersonalizado;

	public ExcepcionBBVA(String codigoError) {
		super(codigoError);
		this.codError = codigoError;
		this.errores = null;
		this.excepcion = null;
		this.mensajePersonalizado = null;
	}
	
	public ExcepcionBBVA(Errors errores) {
		super();
		this.codError = null;
		this.errores = errores;
		this.excepcion = null;
		this.mensajePersonalizado = null;
	}
	
	public ExcepcionBBVA(Exception ex) {
		super();
		this.codError = null;
		this.errores = null;
		this.excepcion = ex;
		this.mensajePersonalizado = null;
	}
	
	public ExcepcionBBVA(String codigoError,String mensaje , Exception ex) {
		super();
		this.codError = codigoError;
		this.errores = null;
		this.excepcion = ex;
		this.mensajePersonalizado = mensaje;
	}
	
	public ExcepcionBBVA(Exception ex,String mensaje) {
		super();
		this.codError = null;
		this.errores = null;
		this.excepcion = ex;
		this.mensajePersonalizado = mensaje;
	}

	public String getCodError() {
		return codError;
	}
	
	public Exception getExcepcion() {
		return excepcion;
	}

	public Errors getErrores() {
		return errores;
	}

	public String getMensajePersonalizado() {
		return mensajePersonalizado;
	}
	
	@Override
	public String toString() {
		if(null!=this.getMessage() && !this.getMessage().isEmpty()) {
			return this.getMessage();
		}else {
			return obtenerMensajeError();
		}
	}

	private String obtenerMensajeError() {
		String idError = obtenerCodigoError();
		StringBuilder mensaje = new StringBuilder();

		mensaje.append(MessageFormat.format("Mensaje Personalizado: {0} \n", 
				(mensajePersonalizado != null) && (!mensajePersonalizado.isEmpty()) ? mensajePersonalizado : ""));
		StackTraceElement[] elem = excepcion.getStackTrace();
		mensaje.append(MessageFormat.format("Mensaje Excepcion: {0} \n", excepcion));

		int j = elem.length;
		for (int i = 0; i < j; i++) {
			StackTraceElement ex = elem[i];

			if (ex.getClassName().contains(CLASES_FU)) {
				if (i == j - 1) {
					mensaje.append(MessageFormat.format("Clase ''{0}'.'{2}'' linea: ''{1}''", 
							ex.getClassName(), Integer.valueOf(ex.getLineNumber()), ex.getMethodName() ));
				} else {
					mensaje.append(MessageFormat.format("Clase ''{0}'.'{2}'' linea: ''{1}'' \n", 
							ex.getClassName(), Integer.valueOf(ex.getLineNumber()), ex.getMethodName() ));
				}
			}

		}
		return "[" + idError + "] " + mensaje.toString();
	}

	private static String obtenerCodigoError() {
		return new SimpleDateFormat("ddMMyyyyhhmmss").format(new Date());
	}    
}
