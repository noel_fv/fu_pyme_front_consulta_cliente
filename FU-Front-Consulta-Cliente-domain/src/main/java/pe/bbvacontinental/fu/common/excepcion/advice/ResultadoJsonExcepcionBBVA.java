package pe.bbvacontinental.fu.common.excepcion.advice;

import java.io.Serializable;

import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;

public class ResultadoJsonExcepcionBBVA extends ExcepcionBBVA implements Serializable {

	private static final long serialVersionUID = 1L;

	public ResultadoJsonExcepcionBBVA(String codigoError) {
		super(codigoError);
	}

	public ResultadoJsonExcepcionBBVA(BindingResult errores) {
		super(errores);
	}

	public ResultadoJsonExcepcionBBVA(Errors errores) {
	   super(errores);
    }

}
