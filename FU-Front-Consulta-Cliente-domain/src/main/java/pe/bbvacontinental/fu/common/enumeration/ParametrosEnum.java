package pe.bbvacontinental.fu.common.enumeration;

public enum ParametrosEnum {
	
	SERVICIO_DATOS_CLIENTE("SERV_DATOS_CLIENTE",""),
	SERVICIO_CONTRATO_CLIENTE("SERV_CONTRATO_CLIENTE",""),
	SERVICIO_TERRITORIO_OFICINA("SERV_TERRITORIO_OFICINA",""),
	SERVICIO_DATOS_NEGOCIO_WP("SERV_DATOS_NEGOCIO",""),
	SERVICIO_TIPO_CAMBIO("SERV_TIPO_CAMBIO_BPM",""),
	SERVICIO_GENERAR_SOLICITUD("SERV_GENERAR_SOLICITUD",""),
	SERVICIO_CONSULTAR_IDM("SERV_CONSULTAR_IDM",""),
	PANTALLA_PERSONA_NATURAL("PANT_PERSONA_NATURAL",""),
	SERVICIO_ACCESO_BPM("SERV_ACCESO_BPM_SSO","Las invocaciones por Rest Api al BPM se hacen mediante estos parametros"),
	SERV_BALANCER_BPM("SERV_BALANCER_BPM","Para Balanceador BPM, las invocacicones a servicios a medida hechos en BPM se invocan por este medio"),
	SERVICIO_CONSULT_CLIENT_PJ("SERV_CONSULT_CLIENT_PJ",""),
	URL_FILE_UNICO_PJ("URL_FILE_UNICO_PJ",""),
	VAL_VENTAS_ANUALES("VAL_VENTAS_ANUALES","");
	
	private String codigo;
	private String contexto;

	ParametrosEnum(String codigo,String contexto) {
		this.codigo=codigo;
		this.contexto=contexto;
	}

	public String getCodigo() {
		return codigo;
	}

	protected void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getContexto() {
		return contexto;
	}

	protected void setContexto(String contexto) {
		this.contexto = contexto;
	}

	
	

}
