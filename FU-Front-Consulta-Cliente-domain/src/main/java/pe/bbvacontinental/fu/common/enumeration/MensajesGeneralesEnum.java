package pe.bbvacontinental.fu.common.enumeration;

public enum MensajesGeneralesEnum {

//	TIPO: INDICA FLUJO AL QUE PERTENECE
//	1: CONSULTA CLIENTE
//	9: OTROS
	
	MENSAJE_100("M100", "1"),
	MENSAJE_101("M101", "1"),
	MENSAJE_102("M102", "1"),
	MENSAJE_103("M103", "1"),
	MENSAJE_104("M104", "1"),
	MENSAJE_105("M105", "1"),
	MENSAJE_106("M106", "1"),
	MENSAJE_107("M107", "1"),
	MENSAJE_108("M108", "1"),
	MENSAJE_109("M109", "1");

	private String codigo;
	
	private String tipo;
	
	private MensajesGeneralesEnum(String codigo, String tipo){
		
		this.codigo = codigo;
		this.tipo = tipo;		
	}

	public String getCodigo() {
		return codigo;
	}

	protected void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	protected void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
