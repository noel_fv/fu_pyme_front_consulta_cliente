package pe.bbvacontinental.fu.common.enumeration;

public enum EstadoServiciosEnum {
	
	RESULTADO_OK("1","OK","01"),
	RESULTADO_ERROR("0","ERROR","00"),
	RESULTADO_ERROR_BPM("99","ERROR","9999"),
	RESULTADO_OK_BPM("00","OK","00"),
	RESULTADO_EXITO_CONSULTAR_CLIENTE("0000000","EXITO","00"),
	RESULTADO_OK_GENERAL("00","OK","00");
	
	private String codigo;
	
	private String descripcion;
	
	private String valorOpcional;
	
	private EstadoServiciosEnum(String codigo, String descripcion, String valorOpcional){
		this.setCodigo(codigo);
		this.setDescripcion(descripcion);	
		this.setValorOpcional(valorOpcional);
	}

	public String getCodigo() {
		return codigo;
	}

	protected void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	protected void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getValorOpcional() {
		return valorOpcional;
	}

	protected void setValorOpcional(String valorOpcional) {
		this.valorOpcional = valorOpcional;
	}
}
