package pe.bbvacontinental.fu.common.enumeration;

public enum TipoAplicativoEnum {
	
	TIPO_WP("1", "WEB_PYMES"),
	TIPO_FU("2", "FILE_UNICO"),
	TIPO_BPM_FU("3", "BPM_FILE_UNICO"),
	APP_FU("FU","FILE UNICO");
	private String codigo;
	
	private String descripcion;
	
	private TipoAplicativoEnum(String codigo, String descripcion){
		
		this.codigo = codigo;
		this.descripcion = descripcion;	
	}

	public String getCodigo() {
		return codigo;
	}

	protected void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	protected void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
