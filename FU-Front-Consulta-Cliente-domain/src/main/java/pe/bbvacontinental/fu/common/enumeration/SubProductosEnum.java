package pe.bbvacontinental.fu.common.enumeration;

public enum SubProductosEnum {

//		TARJETA_CAPITAL_TRABAJO("0201","Tarjeta Capital de Trabajo", 30000, 10000, "La línea mínima es S/. 30,000.00", "La línea mínima es $ 10,000.00"),
//		TARJETA_EMPRESARIAL_ORO("0202","Tarjeta Empresarial Oro", 5000, 1500, "La línea mínima es S/. 5,000.00", "La línea mínima es $ 1,500.00"),
	TARJETA_CAPITAL_TRABAJO("0201","Tarjeta Capital de Trabajo"),
	TARJETA_EMPRESARIAL_ORO("0202","Tarjeta Empresarial Oro"),
		ADQUISICION_BIENES_INMUEBLES("0104", "Adquisición de Bienes Inmuebles"),
		ADQUISICION_BIENES_MUEBLES("0102", "Adquisición de Bienes Muebles"),
		PRESTAMO_CAPITAL_TRABAJO("0106", "Préstamo Capital de Trabajo"),
		LINEA_PRESTAMO_CAPITAL_TRABAJO("0105", "Línea de Préstamo Capital de Trabajo"),
		SUBROGACIONES_CAPITAL_TRABAJO("0108", "Subrogaciones de Capital de Trabajo"),
		PRORROGA_PRESTAMO_CAPITAL_TRABAJO("0116", "Prorroga de Prestamo Capital de trabajo"),
		SUBROGACION_ADQUISICION_BIENES_INMUEBLES("0110", "Subrogación de adquisición de bienes inmuebles"),
		SUBROGACION_ADQUISICION_BIENES_MUEBLES("0109", "Subrogación de adquisición de bienes muebles");
		
		private String codigoWP;
		private String descripcionWP;

		
		private SubProductosEnum(String codigoWP, String descripcionWP){
			this.codigoWP = codigoWP;
			this.descripcionWP = descripcionWP;
		}

		public String getCodigoWP() {
			return codigoWP;
		}

		protected void setCodigoWP(String codigoWP) {
			this.codigoWP = codigoWP;
		}

		public String getDescripcionWP() {
			return descripcionWP;
		}

		protected void setDescripcionWP(String descripcionWP) {
			this.descripcionWP = descripcionWP;
		}


}
