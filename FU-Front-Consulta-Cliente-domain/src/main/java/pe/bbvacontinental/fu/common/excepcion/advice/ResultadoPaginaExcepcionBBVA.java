package pe.bbvacontinental.fu.common.excepcion.advice;

import java.io.Serializable;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;

public class ResultadoPaginaExcepcionBBVA extends ExcepcionBBVA implements Serializable{

	private static final long serialVersionUID = 1L;
	public ResultadoPaginaExcepcionBBVA(String codigoError) {
		super(codigoError);
	}
 
}
