package pe.bbvacontinental.fu.common.enumeration;

public enum TipoTablaEnum {

	
	TABLA_URL_APPS("URL_APPS","FU"),
	TABLA_SERV_WEB("SERV","SOAP Y REST");
	
	private String codigo;
	
	private String descripcion;
	
	private TipoTablaEnum(String codigo, String descripcion){
		
		this.codigo = codigo;
		this.descripcion = descripcion;	
	}

	public String  getCodigo() {
		return codigo;
	}

	protected void  setCodigo(String cod) {
		this.codigo = cod;
	}

	public String  getDescripcion() {
		return descripcion;
	}

	protected void  setDescripcion(String desc) {
		this.descripcion = desc;
	}
	
}
