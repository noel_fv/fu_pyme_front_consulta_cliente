package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class DatosTitularModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8603926144290800179L;
	private String segmento;
	private String codigoCentral;
	private String tipoDocumento;
	private String numerDocumento;
	
	private String idTipoPersona;
	
	private String clienteNuevo;
	private String esClienteNuevo;
	private Boolean categoria1;
	private Boolean categoria3;

	private String codigoRegimenTributario;
	private String codigoGiroNegocio;
	private String codOficina;
	private String nombreCliente;
	private String codigoGiroNegocioWP;
	private String idGiroNegocio;
	private String descripcionTipoPersona;
	
	private String tipoPersonaFU;
	
	public String getSegmento() {
		return segmento;
	}
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	public String getCodigoCentral() {
		return codigoCentral;
	}
	public void setCodigoCentral(String codigoCentral) {
		this.codigoCentral = codigoCentral;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumerDocumento() {
		return numerDocumento;
	}
	public void setNumerDocumento(String numerDocumento) {
		this.numerDocumento = numerDocumento;
	}
	public String getIdTipoPersona() {
		return idTipoPersona;
	}
	public void setIdTipoPersona(String idTipoPersona) {
		this.idTipoPersona = idTipoPersona;
	}
	
	
	public String getEsClienteNuevo() {
		return esClienteNuevo;
	}
	public void setEsClienteNuevo(String esClienteNuevo) {
		this.esClienteNuevo = esClienteNuevo;
	}
	public String getCodigoRegimenTributario() {
		return codigoRegimenTributario;
	}
	public void setCodigoRegimenTributario(String codigoRegimenTributario) {
		this.codigoRegimenTributario = codigoRegimenTributario;
	}
	public String getCodOficina() {
		return codOficina;
	}
	public void setCodOficina(String codOficina) {
		this.codOficina = codOficina;
	}
	public String getClienteNuevo() {
		return clienteNuevo;
	}
	public void setClienteNuevo(String clienteNuevo) {
		this.clienteNuevo = clienteNuevo;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public Boolean getCategoria1() {
		return categoria1;
	}
	public void setCategoria1(Boolean categoria1) {
		this.categoria1 = categoria1;
	}
	public Boolean getCategoria3() {
		return categoria3;
	}
	public void setCategoria3(Boolean categoria3) {
		this.categoria3 = categoria3;
	}
	public String getCodigoGiroNegocio() {
		return codigoGiroNegocio;
	}
	public void setCodigoGiroNegocio(String codigoGiroNegocio) {
		this.codigoGiroNegocio = codigoGiroNegocio;
	}
	public String getCodigoGiroNegocioWP() {
		
		String [] codigosFUWP= getCodigoGiroNegocio().split("_");
		codigoGiroNegocioWP = (codigosFUWP.length > 1) ? codigosFUWP[1] : "";
		
		return codigoGiroNegocioWP;
	}
	public void setCodigoGiroNegocioWP(String codigoGiroNegocioWP) {
		this.codigoGiroNegocioWP = codigoGiroNegocioWP;
	}
	public String getIdGiroNegocio() {
		return idGiroNegocio;
	}
	public void setIdGiroNegocio(String idGiroNegocio) {
		this.idGiroNegocio = idGiroNegocio;
	}
	public String getDescripcionTipoPersona() {
		return descripcionTipoPersona;
	}
	public void setDescripcionTipoPersona(String descripcionTipoPersona) {
		this.descripcionTipoPersona = descripcionTipoPersona;
	}
	public String getTipoPersonaFU() {
		return tipoPersonaFU;
	}
	public void setTipoPersonaFU(String tipoPersonaFU) {
		this.tipoPersonaFU = tipoPersonaFU;
	}

}
