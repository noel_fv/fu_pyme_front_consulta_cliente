package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class SolicitudConsultaModel extends DataTablesRequestModel  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5200301295777134315L;
	
	private String numeroSolicitud;
	private String codigoCentral;
	private String territorio;
	private String oficina;
	private String giroNegocio;
	private String tipoFlujo;
	private String fechaRegistroDesde;
	private String fechaRegistroHasta;
	private String producto;
	private String subProducto;

	private String estado;
	private String usuarioAsignado;
	
	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}
	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}
	public String getCodigoCentral() {
		return codigoCentral;
	}
	public void setCodigoCentral(String codigoCentral) {
		this.codigoCentral = codigoCentral;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	public String getOficina() {
		return oficina;
	}
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	public String getGiroNegocio() {
		return giroNegocio;
	}
	public void setGiroNegocio(String giroNegocio) {
		this.giroNegocio = giroNegocio;
	}
	public String getTipoFlujo() {
		return tipoFlujo;
	}
	public void setTipoFlujo(String tipoFlujo) {
		this.tipoFlujo = tipoFlujo;
	}	
	public String getFechaRegistroDesde() {
		return fechaRegistroDesde;
	}
	public void setFechaRegistroDesde(String fechaRegistroDesde) {
		this.fechaRegistroDesde = fechaRegistroDesde;
	}
	public String getFechaRegistroHasta() {
		return fechaRegistroHasta;
	}
	public void setFechaRegistroHasta(String fechaRegistroHasta) {
		this.fechaRegistroHasta = fechaRegistroHasta;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getSubProducto() {
		return subProducto;
	}
	public void setSubProducto(String subProducto) {
		this.subProducto = subProducto;
	}

	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioAsignado() {
		return usuarioAsignado;
	}
	public void setUsuarioAsignado(String usuarioAsignado) {
		this.usuarioAsignado = usuarioAsignado;
	}
}
