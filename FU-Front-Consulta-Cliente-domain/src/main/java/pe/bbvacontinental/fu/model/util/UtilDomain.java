package pe.bbvacontinental.fu.model.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtilDomain {
	
	public static final String FORMATO_DIA_DDMMYYYY = "dd/MM/yyyy";
		
	protected static final Logger logger = LoggerFactory.getLogger(UtilDomain.class);
	
	
	
	private UtilDomain() {
		super();
	}

	public static Timestamp getTimestampFromUtilDate(Date myDate){						
		return new Timestamp(myDate.getTime());
	}
	
	public static Date getDateFromTimestamp(Timestamp timestamp){
		return new Date(timestamp.getTime());
	}
	
	public static long getNumberOfDays(Timestamp tsIni, Timestamp tsEnd){
		return getNumberOfDays(getDateFromTimestamp(tsIni), getDateFromTimestamp(tsEnd));
	}
	
	public static long getNumberOfDays(Date dateIni, Date dateEnd){
		Calendar calIni = new GregorianCalendar();
		calIni.setTime(dateIni);
		Calendar calFin = new GregorianCalendar();
		calFin.setTime(dateEnd);
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.set(calIni.get(Calendar.YEAR),calIni.get(Calendar.MONTH), calIni.get(Calendar.DATE));
		cal2.set(calFin.get(Calendar.YEAR),calFin.get(Calendar.MONTH), calFin.get(Calendar.DATE));
		long milis1 = cal1.getTimeInMillis();
		long milis2 = cal2.getTimeInMillis();
		long diff = milis2 - milis1;		
		return  diff / (24 * 60 * 60 * 1000);
	}
	
	public static String dateToString(Date date,String format)  {
		SimpleDateFormat sdf=new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	public static String formatDate(Date date,String format){
		String fecha="";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat(format);
			fecha=sdf.format(date);
		}catch(Exception e){
			logger.error("UtilDomain:",e);
		}

		return fecha;
	}
	
	public static String dateToddMMyyyy(Date date){
		return dateToString(date, FORMATO_DIA_DDMMYYYY);
	}
	
	
	public static Date ddMMyyyyToDate(String date) {
		return stringToDate(date, FORMATO_DIA_DDMMYYYY);
	}
	

	
	public static Date stringToDate(String date, String format ){
		if (date == null) {
			return null;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(format); 
	    Date parsedDate=null;
		try {
			parsedDate = dateFormat.parse(date);
		} catch (ParseException e) {
			logger.error("UtilDomain:",e);
		} 	
	    return parsedDate;
	
	}
	
	public static String formatNumber(BigDecimal number){
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
		return (new DecimalFormat("#,###,###.00", otherSymbols)).format(number);
	}
}
