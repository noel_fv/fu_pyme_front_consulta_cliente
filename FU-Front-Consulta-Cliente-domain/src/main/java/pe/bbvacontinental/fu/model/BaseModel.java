/**
 * 
 */
package pe.bbvacontinental.fu.model;

import java.io.Serializable;

/**
 * @author carlos.diaz
 *
 */
public class BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 502893777390130093L;
	
	private String mensajeRespuesta;
	private String codigoRespuesta;
	
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

}
