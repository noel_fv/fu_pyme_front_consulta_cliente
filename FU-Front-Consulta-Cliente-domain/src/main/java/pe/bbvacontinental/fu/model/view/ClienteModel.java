package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;
import javax.validation.constraints.AssertTrue;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClienteModel extends GenericModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idSolicitud;
	private String codigoCentral;
	private String codigoCentralSesion;
	private String tipoDocumentoIdentidad;
	private String tipoDocumentoIdentidadSesion;
	private String numeroDocumento;
	private String numeroDocumentoSesion;
	private String tipoPersona;
	private String tipoPersonaFU;
	private String nombreCompleto="";
	private String territorio="";
	private String oficina="";
	private String codigoSegmento;
	private String descripcionSegmento;
	private String nombreTerritorio="";
	private String nombreOficina="";
	
	private String clienteNuevo;
	private String categoria1;
	private String categoria3;
	private String codigoGiroNegocio;
	private String codigoRegimenTributario;

	private String ventasAnualesMN;
	private String deudaTotalMN;
	private String rating;
	private String antiguedadEmpresa;
	private String codClasificacionBanco;
	private String codClasificacionSSFF;
	
	@AssertTrue(message = "Debe ingresar los campos obligatorios: Código Central o Tipo y Número de Documento")
	private boolean isValidaEntrada() {
		boolean ind = true;
		if(codigoCentral == null || codigoCentral.equals("")){
			if(tipoDocumentoIdentidad == null || tipoDocumentoIdentidad.equals(""))
				ind = false;
			if(!"".equals(tipoDocumentoIdentidad) && (numeroDocumento == null || numeroDocumento.equals("")))
						ind = false;
		}
		return ind;
	}
	
	@JsonIgnore
	public String getCodigoCentral() {
		return codigoCentral;
	}

	@JsonProperty
	public void setCodigoCentral(String codigoCentral) {
		this.codigoCentral = codigoCentral;
	}

	@JsonIgnore
	public String getTipoDocumentoIdentidad() {
		return tipoDocumentoIdentidad;
	}

	@JsonProperty
	public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
		this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
	}

	@JsonIgnore
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	@JsonProperty
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getTerritorio() {
		return territorio;
	}

	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getCodigoSegmento() {
		return codigoSegmento;
	}

	public void setCodigoSegmento(String codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}

	public String getDescripcionSegmento() {
		return descripcionSegmento;
	}

	public void setDescripcionSegmento(String descripcionSegmento) {
		this.descripcionSegmento = descripcionSegmento;
	}

	@JsonIgnore
	public Long getIdSolicitud() {
		return idSolicitud;
	}

	@JsonIgnore
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getNombreTerritorio() {
		return nombreTerritorio;
	}

	public void setNombreTerritorio(String nombreTerritorio) {
		this.nombreTerritorio = nombreTerritorio;
	}

	public String getNombreOficina() {
		return nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	public String getClienteNuevo() {
		return clienteNuevo;
	}

	public void setClienteNuevo(String clienteNuevo) {
		this.clienteNuevo = clienteNuevo;
	}

	

	public String getCategoria1() {
		return categoria1;
	}

	public void setCategoria1(String categoria1) {
		this.categoria1 = categoria1;
	}

	public String getCategoria3() {
		return categoria3;
	}

	public void setCategoria3(String categoria3) {
		this.categoria3 = categoria3;
	}


	public String getCodigoRegimenTributario() {
		return codigoRegimenTributario;
	}

	public void setCodigoRegimenTributario(String codigoRegimenTributario) {
		this.codigoRegimenTributario = codigoRegimenTributario;
	}

	public String getTipoPersonaFU() {
		return tipoPersonaFU;
	}

	public void setTipoPersonaFU(String tipoPersonaFU) {
		this.tipoPersonaFU = tipoPersonaFU;
	}

	@JsonIgnore
	public String getVentasAnualesMN() {
		return ventasAnualesMN;
	}

	public void setVentasAnualesMN(String ventasAnualesMN) {
		this.ventasAnualesMN = ventasAnualesMN;
	}
	@JsonIgnore
	public String getDeudaTotalMN() {
		return deudaTotalMN;
	}

	public void setDeudaTotalMN(String deudaTotalMN) {
		this.deudaTotalMN = deudaTotalMN;
	}
	@JsonIgnore
	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
	@JsonIgnore
	public String getAntiguedadEmpresa() {
		return antiguedadEmpresa;
	}

	public void setAntiguedadEmpresa(String antiguedadEmpresa) {
		this.antiguedadEmpresa = antiguedadEmpresa;
	}
	@JsonIgnore
	public String getCodClasificacionBanco() {
		return codClasificacionBanco;
	}

	public void setCodClasificacionBanco(String codClasificacionBanco) {
		this.codClasificacionBanco = codClasificacionBanco;
	}
	@JsonIgnore
	public String getCodClasificacionSSFF() {
		return codClasificacionSSFF;
	}

	public void setCodClasificacionSSFF(String codClasificacionSSFF) {
		this.codClasificacionSSFF = codClasificacionSSFF;
	}

	public String getCodigoGiroNegocio() {
		return codigoGiroNegocio;
	}

	public void setCodigoGiroNegocio(String codigoGiroNegocio) {
		this.codigoGiroNegocio = codigoGiroNegocio;
	}

	public String getCodigoCentralSesion() {
		return codigoCentralSesion;
	}

	public void setCodigoCentralSesion(String codigoCentralSesion) {
		this.codigoCentralSesion = codigoCentralSesion;
	}

	public String getTipoDocumentoIdentidadSesion() {
		return tipoDocumentoIdentidadSesion;
	}

	public void setTipoDocumentoIdentidadSesion(String tipoDocumentoIdentidadSesion) {
		this.tipoDocumentoIdentidadSesion = tipoDocumentoIdentidadSesion;
	}

	public String getNumeroDocumentoSesion() {
		return numeroDocumentoSesion;
	}

	public void setNumeroDocumentoSesion(String numeroDocumentoSesion) {
		this.numeroDocumentoSesion = numeroDocumentoSesion;
	}
}