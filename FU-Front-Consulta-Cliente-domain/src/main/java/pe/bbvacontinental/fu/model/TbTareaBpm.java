package pe.bbvacontinental.fu.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MAE_PJ_TAREA_BPM")
public class TbTareaBpm implements java.io.Serializable {

	private static final long serialVersionUID = 1L; 
	
	private byte idTareaBpm;
	private String valor;
	private String descripcion;
	private Boolean estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;


	public TbTareaBpm() {
		super();
	}




	@Id
	@Column(name = "ID_MAE_PJ_TAREA_BPM", unique = true, nullable = false, precision = 2, scale = 0)
	public byte getIdTareaBpm() {
		return this.idTareaBpm;
	}

	public void setIdTareaBpm(byte idTareaBpm) {
		this.idTareaBpm = idTareaBpm;
	}

	@Column(name = "VALOR", length = 2)
	public String getValor() {
		return this.valor;
	}

	public void setValor(String val) {
		this.valor = val;
	}

	@Column(name = "DESCRIPCION", length = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String desc) {
		this.descripcion = desc;
	}

	@Column(name = "ESTADO", precision = 1, scale = 0)
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fecModific) {
		this.fechaModificacion = fecModific;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fecCreac) {
		this.fechaCreacion = fecCreac;
	}

	@Column(name = "USUARIO_MODIFICACION", length = 14)
	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usrModific) {
		this.usuarioModificacion = usrModific;
	}

	@Column(name = "USUARIO_CREACION", length = 14)
	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usrCreac) {
		this.usuarioCreacion = usrCreac;
	}

}
