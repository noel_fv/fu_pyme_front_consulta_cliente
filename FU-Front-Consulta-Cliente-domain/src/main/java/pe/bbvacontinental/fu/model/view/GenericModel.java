package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class GenericModel  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idUsuario;
	
	private String oficinaUsuario;
	
	private String territorioUsuario;
	
	private String fechaHoraEnvio;
	
	private String idSesion;
	
	private String idOperacion;
	
	private String ipUsuario;
	
	private String codigoServicio;
	
	private String mensajeServicio;
	
	private String estadoServicio;
	
	private String nombreAplicacion;
	
	private String respuesta1;
	
	private String respuesta2;
	
	private String respuesta3;
	
	private String cookieSSO;
	
	/*Para enviar al cliente el usuario del sistema*/
	private String localCache;

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getFechaHoraEnvio() {
		return fechaHoraEnvio;
	}

	public void setFechaHoraEnvio(String fecHoraEnvio) {
		this.fechaHoraEnvio = fecHoraEnvio;
	}

	public String getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(String idSession) {
		this.idSesion = idSession;
	}

	public String getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(String idOper) {
		this.idOperacion = idOper;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public String getMensajeServicio() {
		return mensajeServicio;
	}

	public void setMensajeServicio(String mensajeServicio) {
		this.mensajeServicio = mensajeServicio;
	}

	public String getEstadoServicio() {
		return estadoServicio;
	}

	public void setEstadoServicio(String estadoServicio) {
		this.estadoServicio = estadoServicio;
	}

	public String getRespuesta1() {
		return respuesta1;
	}

	public void setRespuesta1(String respuesta1) {
		this.respuesta1 = respuesta1;
	}

	public String getRespuesta2() {
		return respuesta2;
	}

	public void setRespuesta2(String respuesta2) {
		this.respuesta2 = respuesta2;
	}

	public String getIpUsuario() {
		return ipUsuario;
	}

	public void setIpUsuario(String ipUsuario) {
		this.ipUsuario = ipUsuario;
	}

	public String getOficinaUsuario() {
		return oficinaUsuario;
	}

	public void setOficinaUsuario(String oficinaUsuario) {
		this.oficinaUsuario = oficinaUsuario;
	}

	public String getTerritorioUsuario() {
		return territorioUsuario;
	}

	public void setTerritorioUsuario(String territorioUsuario) {
		this.territorioUsuario = territorioUsuario;
	}

	public String getNombreAplicacion() {
		return nombreAplicacion;
	}

	public void setNombreAplicacion(String nombreAplicacion) {
		this.nombreAplicacion = nombreAplicacion;
	}

	public String getRespuesta3() {
		return respuesta3;
	}

	public void setRespuesta3(String respuesta3) {
		this.respuesta3 = respuesta3;
	}

	public String getCookieSSO() {
		return cookieSSO;
	}

	public void setCookieSSO(String cookieSSO) {
		this.cookieSSO = cookieSSO;
	}

	public String getLocalCache() {
		return localCache;
	}

	public void setLocalCache(String localCache) {
		this.localCache = localCache;
	}

}
