package pe.bbvacontinental.fu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MAE_PJ_MOTIVO_SOLICITUD")
public class TbMotivoSolicitud implements Serializable {

	private static final long serialVersionUID = 4169314187047136826L;
	
	private long idMotivoSolicitud;
	private String valor;
	private String descripcion;
	private Boolean estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;

	@Id
	@Column(name = "ID_MAE_PJ_MOTIVO_SOLICITUD", unique = true)
	public long getIdMotivoSolicitud() {
		return idMotivoSolicitud;
	}

	public void setIdMotivoSolicitud(long idMotivoSolicitud) {
		this.idMotivoSolicitud = idMotivoSolicitud;
	}

	@Column(name="VALOR")
	public String getValor() {
		return valor;
	}

	public void setValor(String codigo) {
		this.valor = codigo;
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descrip) {
		this.descripcion = descrip;
	}

	@Column(name="ESTADO")
	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechModi) {
		this.fechaModificacion = fechModi;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechCrea) {
		this.fechaCreacion = fechCrea;
	}

	@Column(name = "USUARIO_MODIFICACION", length = 14)
	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuModi) {
		this.usuarioModificacion = usuModi;
	}

	@Column(name = "USUARIO_CREACION", length = 14)
	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuCrea) {
		this.usuarioCreacion = usuCrea;
	}
}
