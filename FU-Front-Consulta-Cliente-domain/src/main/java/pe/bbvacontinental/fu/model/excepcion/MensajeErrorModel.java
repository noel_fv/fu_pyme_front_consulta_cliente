package pe.bbvacontinental.fu.model.excepcion;

public class MensajeErrorModel {
	private String codigoError;
	private String mensajeError;

	public MensajeErrorModel() {
		super();
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

}
