package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductoModel implements Serializable {
	private static final long serialVersionUID = 5983388725459235471L;

	private String idProducto;
	private String producto;
	private String idSubProducto; // valor webpymes
	private String subProducto;
	private Boolean derivado;
	private BigDecimal montoSolicitado = new BigDecimal("0");
	private String montoSolicitadoString;
	private Long idMoneda;
	private String moneda;
	private Double tasaAnual;
	private String fechaVencimiento;
	private Short plazoVencimientoDias;
	private Short plazoReembolsoCuotas;
	private String idCampanha;
	private String campanha;
	private String flagLinea;

	private BigDecimal importeSolicitadoMN = new BigDecimal("0");
	private BigDecimal importePropuestoMN = new BigDecimal("0");

	private String valorWp;

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getIdSubProducto() {
		return idSubProducto;
	}

	public void setIdSubProducto(String idSubProducto) {
		this.idSubProducto = idSubProducto;
	}

	public String getSubProducto() {
		return subProducto;
	}

	public void setSubProducto(String subProducto) {
		this.subProducto = subProducto;
	}

	public Boolean getDerivado() {
		return derivado;
	}

	public void setDerivado(Boolean derivado) {
		this.derivado = derivado;
	}


	public BigDecimal getMontoSolicitado() {
		return montoSolicitado;
	}

	public void setMontoSolicitado(BigDecimal montoSolicitado) {
		this.montoSolicitado = montoSolicitado;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Double getTasaAnual() {
		return tasaAnual;
	}

	public void setTasaAnual(Double tasaAnual) {
		this.tasaAnual = tasaAnual;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Short getPlazoVencimientoDias() {
		return plazoVencimientoDias;
	}

	public void setPlazoVencimientoDias(Short plazoVencimientoDias) {
		this.plazoVencimientoDias = plazoVencimientoDias;
	}

	public Short getPlazoReembolsoCuotas() {
		return plazoReembolsoCuotas;
	}

	public void setPlazoReembolsoCuotas(Short plazoReembolsoCuotas) {
		this.plazoReembolsoCuotas = plazoReembolsoCuotas;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(String idCampanha) {
		this.idCampanha = idCampanha;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getMontoSolicitadoString() {
		return montoSolicitadoString;
	}

	public void setMontoSolicitadoString(String montoSolicitadoString) {
		this.montoSolicitadoString = montoSolicitadoString;
	}

	public String getFlagLinea() {
		return flagLinea;
	}

	public void setFlagLinea(String flagLinea) {
		this.flagLinea = flagLinea;
	}

	public BigDecimal getImporteSolicitadoMN() {
		return importeSolicitadoMN;
	}

	public void setImporteSolicitadoMN(BigDecimal importeSolicitadoMN) {
		this.importeSolicitadoMN = importeSolicitadoMN;
	}

	public BigDecimal getImportePropuestoMN() {
		return importePropuestoMN;
	}

	public void setImportePropuestoMN(BigDecimal importePropuestoMN) {
		this.importePropuestoMN = importePropuestoMN;
	}

	public String getValorWp() {
		return valorWp;
	}

	public void setValorWp(String valorWp) {
		this.valorWp = valorWp;
	}
}
