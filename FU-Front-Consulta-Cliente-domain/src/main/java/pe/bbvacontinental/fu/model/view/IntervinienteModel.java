package pe.bbvacontinental.fu.model.view;

public class IntervinienteModel {
	private String tipoDocumento;
	private String numeroDocumento;
	
	private String tipoParticipante;
	private String descParticipante;
	private String nombre;
	private String codigoCentral;
	
	private String tipoPersona;
	private String codigoGestor;
	
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	public String getTipoParticipante() {
		return tipoParticipante;
	}

	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}

	public String getDescParticipante() {
		return descParticipante;
	}

	public void setDescParticipante(String descParticipante) {
		this.descParticipante = descParticipante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoCentral() {
		return codigoCentral;
	}

	public void setCodigoCentral(String codigoCentral) {
		this.codigoCentral = codigoCentral;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
	public String getCodigoGestor() {
		return codigoGestor;
	}

	public void setCodigoGestor(String codigoGestor) {
		this.codigoGestor = codigoGestor;
	}
}
