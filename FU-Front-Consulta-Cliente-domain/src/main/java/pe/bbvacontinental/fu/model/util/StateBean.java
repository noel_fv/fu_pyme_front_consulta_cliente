package pe.bbvacontinental.fu.model.util;

import java.io.Serializable;

public class StateBean implements Serializable {
	
	private static final long serialVersionUID = 2474081809067118710L;
	
	public enum StateType{
		OK, ERROR
	}
	
	private StateType typeState;
	private Object bean;
	private String codeOptional;

	public StateType getTypeState() {
		return typeState;
	}

	public void setTypeState(StateType typeState) {
		this.typeState = typeState;
	}

	public Object getBean() {
		return bean;
	}

	public void setBean(Object bean) {
		this.bean = bean;
	}

	public String getCodeOptional() {
		return codeOptional;
	}

	public void setCodeOptional(String codeOptional) {
		this.codeOptional = codeOptional;
	}
}