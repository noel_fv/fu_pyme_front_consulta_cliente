package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;
import java.util.Map;

/**
 * Clase que permite contiene los datos que se guardaran en local browser
 * en cada ingreso a la session de usuario.
 * @author huber.quinto
 *
 */
public class LocalCacheModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7268547511751325236L;

	private UsuarioPortal usuarioPortal;
	
	private ClienteModel clienteModel;
	
	private String codigoSolicitud;
	
	private Map<String,String> mapDatos;

	public UsuarioPortal getUsuarioPortal() {
		return usuarioPortal;
	}

	public void setUsuarioPortal(UsuarioPortal usuarioPortal) {
		this.usuarioPortal = usuarioPortal;
	}

	public ClienteModel getClienteModel() {
		return clienteModel;
	}

	public void setClienteModel(ClienteModel clienteModel) {
		this.clienteModel = clienteModel;
	}

	public String getCodigoSolicitud() {
		return codigoSolicitud;
	}

	public void setCodigoSolicitud(String codigoSolicitud) {
		this.codigoSolicitud = codigoSolicitud;
	}

	public Map<String, String> getMapDatos() {
		return mapDatos;
	}

	public void setMapDatos(Map<String, String> mapDatos) {
		this.mapDatos = mapDatos;
	}	

}
