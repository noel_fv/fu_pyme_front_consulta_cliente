package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class DatosErrorModel extends GenericModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numeroSolicitudFU;
	
	private String codigoError;
	
	private String tipoError;
	
	private String detalleError;

	public String getNumeroSolicitudFU() {
		return numeroSolicitudFU;
	}

	public void setNumeroSolicitudFU(String numeroSolicitudFU) {
		this.numeroSolicitudFU = numeroSolicitudFU;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codError) {
		this.codigoError = codError;
	}

	public String getTipoError() {
		return tipoError;
	}

	public void setTipoError(String tipError) {
		this.tipoError = tipError;
	}

	public String getDetalleError() {
		return detalleError;
	}

	public void setDetalleError(String detError) {
		this.detalleError = detError;
	}	
}
