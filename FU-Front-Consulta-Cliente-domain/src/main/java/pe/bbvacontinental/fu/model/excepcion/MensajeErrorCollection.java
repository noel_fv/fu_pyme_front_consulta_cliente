package pe.bbvacontinental.fu.model.excepcion;

import java.io.Serializable;
import java.util.List;

public class MensajeErrorCollection implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<MensajeErrorModel> errores;

	public MensajeErrorCollection() {
		super();
	}

	public List<MensajeErrorModel> getErrores() {
		return errores;
	}

	public void setErrores(List<MensajeErrorModel> errores) {
		this.errores = errores;
	}
}
