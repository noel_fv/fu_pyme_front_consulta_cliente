package pe.bbvacontinental.fu.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DET_PJ_FLUJO_PROC_BPM")
public class TbFlujoProcBPM implements java.io.Serializable {
	
	private long idFlujoProcBPM;
	private String codigoTipoFlujo;
	private String codigoTipoProcesoBPM;
	private Boolean estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;
	
	@Id
	@Column(name = "ID_DET_PJ_FLUJO_PROC_BPM", unique = true, nullable = false, precision = 12, scale = 0)
	public long getIdFlujoProcBPM() {
		return this.idFlujoProcBPM;
	}

	public void setIdFlujoProcBPM(long idFlujoProcBPM) {
		this.idFlujoProcBPM = idFlujoProcBPM;
	}

	@Column(name = "COD_TIPO_FLUJO", length = 2)
	public String getCodigoTipoFlujo() {
		return this.codigoTipoFlujo;
	}

	public void setCodigoTipoFlujo(String codigoTipoFlujo) {
		this.codigoTipoFlujo = codigoTipoFlujo;
	}

	@Column(name = "COD_TIPO_PROCESO_BPM", length = 2)
	public String getCodigoTipoProcesoBPM() {
		return this.codigoTipoProcesoBPM;
	}

	public void setCodigoTipoProcesoBPM(String codigoTipoProcesoBPM) {
		this.codigoTipoProcesoBPM = codigoTipoProcesoBPM;
	}

	@Column(name = "ESTADO", precision = 1, scale = 0)
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCrea) {
		this.fechaCreacion = fechaCrea;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModi) {
		this.fechaModificacion = fechaModi;
	}

	@Column(name = "USUARIO_CREACION", length = 20)
	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCrea) {
		this.usuarioCreacion = usuarioCrea;
	}

	@Column(name = "USUARIO_MODIFICACION", length = 14)
	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModi) {
		this.usuarioModificacion = usuarioModi;
	}

}
