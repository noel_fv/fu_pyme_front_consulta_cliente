package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class GenericSoapRequestModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigoEmpresa;
	
	private String codigoTerminalEmpresa;
	
	private String canal;
	
	private String codigoAplicacion;
	
	private String usuario;
	
	private String fechaHoraEnvio;
	
	private String idSesion;
	
	private String idPeticionEmpresa;
	
	private String idPeticionBanco;
	
	private String idOperacion;
	
	private String idServicio;
	
	private String idInterConexion;

	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public String getCodigoTerminalEmpresa() {
		return codigoTerminalEmpresa;
	}

	public void setCodigoTerminalEmpresa(String codigoTerminalEmpresa) {
		this.codigoTerminalEmpresa = codigoTerminalEmpresa;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getCodigoAplicacion() {
		return codigoAplicacion;
	}

	public void setCodigoAplicacion(String codAplicacion) {
		this.codigoAplicacion = codAplicacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usu) {
		this.usuario = usu;
	}

	public String getFechaHoraEnvio() {
		return fechaHoraEnvio;
	}

	public void setFechaHoraEnvio(String fechaHoraEnvio) {
		this.fechaHoraEnvio = fechaHoraEnvio;
	}

	public String getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(String idSess) {
		this.idSesion = idSess;
	}

	public String getIdPeticionEmpresa() {
		return idPeticionEmpresa;
	}

	public void setIdPeticionEmpresa(String idPeticEmpresa) {
		this.idPeticionEmpresa = idPeticEmpresa;
	}

	public String getIdPeticionBanco() {
		return idPeticionBanco;
	}

	public void setIdPeticionBanco(String idPeticBanco) {
		this.idPeticionBanco = idPeticBanco;
	}

	public String getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(String idOperc) {
		this.idOperacion = idOperc;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getIdInterConexion() {
		return idInterConexion;
	}

	public void setIdInterConexion(String idInterConexion) {
		this.idInterConexion = idInterConexion;
	}
}
