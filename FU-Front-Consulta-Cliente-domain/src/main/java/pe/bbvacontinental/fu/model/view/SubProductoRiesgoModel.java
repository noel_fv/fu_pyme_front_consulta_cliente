package pe.bbvacontinental.fu.model.view;

import java.math.BigDecimal;

public class SubProductoRiesgoModel {

	private Long idSolSubProducto;
	private Long idSubProducto;
	private String valorWPSubProducto;
	private String subProducto;
	private Long idMoneda;
	private String moneda;
	private String idEstado;
	private String estado;
	private String nroCuenta;
	
	private boolean mostrarFechaDesembolso;
	private String fechaDesembolso;
	
	private BigDecimal monto;
	private String montoString;

	private boolean mostrarNumeroContratoFormalizacion;
	private String numeroContratoFormalizacion;

	private boolean mostrarNumeroGarantiaVincular;
	private String numeroGarantiaVincular;

	private boolean mostrarCodigoGestor;

	private boolean mostrarNumeroGarantiaHipotecaria;
	private String numeroGarantiaHipotecaria;

	private boolean mostrarNumeroGarantiaPrendaria;
	private String numeroGarantiaPrendaria;

	private boolean mostrarFechaPago;
	private String fechaPago;
	private Short fechaPagoOtra;
	
	private boolean mostrarFechaCierre;
	private Short fechaCierre;
	
	private String aprobadoCliente;
	private String tieneRetenciones;
	private String aceptaDesembolso;
	private String envioEstadoCuenta;
	private String correoElectronico;
	private String poderesInscritos;
	private String dpsFormalizada;

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getFechaDesembolso() {
		return fechaDesembolso;
	}

	public void setFechaDesembolso(String fechaDesembolso) {
		this.fechaDesembolso = fechaDesembolso;
	}

	public Long getIdSubProducto() {
		return idSubProducto;
	}

	public void setIdSubProducto(Long idSubProducto) {
		this.idSubProducto = idSubProducto;
	}

	public String getSubProducto() {
		return subProducto;
	}

	public void setSubProducto(String subProducto) {
		this.subProducto = subProducto;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Long getIdSolSubProducto() {
		return idSolSubProducto;
	}

	public void setIdSolSubProducto(Long idSolSubProducto) {
		this.idSolSubProducto = idSolSubProducto;
	}

	public String getNumeroContratoFormalizacion() {
		return numeroContratoFormalizacion;
	}

	public void setNumeroContratoFormalizacion(
			String numeroContratoFormalizacion) {
		this.numeroContratoFormalizacion = numeroContratoFormalizacion;
	}

	public String getMontoString() {
		return montoString;
	}

	public void setMontoString(String montoString) {
		this.montoString = montoString;
	}

	public String getValorWPSubProducto() {
		return valorWPSubProducto;
	}

	public void setValorWPSubProducto(String valorWPSubProducto) {
		this.valorWPSubProducto = valorWPSubProducto;
	}

	public boolean isMostrarNumeroGarantiaVincular() {
		return mostrarNumeroGarantiaVincular;
	}

	public void setMostrarNumeroGarantiaVincular(
			boolean mostrarNumeroGarantiaVincular) {
		this.mostrarNumeroGarantiaVincular = mostrarNumeroGarantiaVincular;
	}

	public String getNumeroGarantiaVincular() {
		return numeroGarantiaVincular;
	}

	public void setNumeroGarantiaVincular(String numeroGarantiaVincular) {
		this.numeroGarantiaVincular = numeroGarantiaVincular;
	}

	public boolean isMostrarFechaDesembolso() {
		return mostrarFechaDesembolso;
	}

	public void setMostrarFechaDesembolso(boolean mostrarFechaDesembolso) {
		this.mostrarFechaDesembolso = mostrarFechaDesembolso;
	}

	public boolean isMostrarCodigoGestor() {
		return mostrarCodigoGestor;
	}

	public void setMostrarCodigoGestor(boolean mostrarCodigoGestor) {
		this.mostrarCodigoGestor = mostrarCodigoGestor;
	}

	public boolean isMostrarNumeroGarantiaPrendaria() {
		return mostrarNumeroGarantiaPrendaria;
	}

	public void setMostrarNumeroGarantiaPrendaria(
			boolean mostrarNumeroGarantiaPrendaria) {
		this.mostrarNumeroGarantiaPrendaria = mostrarNumeroGarantiaPrendaria;
	}

	public String getNumeroGarantiaPrendaria() {
		return numeroGarantiaPrendaria;
	}

	public void setNumeroGarantiaPrendaria(String numeroGarantiaPrendaria) {
		this.numeroGarantiaPrendaria = numeroGarantiaPrendaria;
	}

	public boolean isMostrarNumeroGarantiaHipotecaria() {
		return mostrarNumeroGarantiaHipotecaria;
	}

	public void setMostrarNumeroGarantiaHipotecaria(
			boolean mostrarNumeroGarantiaHipotecaria) {
		this.mostrarNumeroGarantiaHipotecaria = mostrarNumeroGarantiaHipotecaria;
	}

	public String getNumeroGarantiaHipotecaria() {
		return numeroGarantiaHipotecaria;
	}

	public void setNumeroGarantiaHipotecaria(String numeroGarantiaHipotecaria) {
		this.numeroGarantiaHipotecaria = numeroGarantiaHipotecaria;
	}

	public boolean isMostrarFechaPago() {
		return mostrarFechaPago;
	}

	public void setMostrarFechaPago(boolean mostrarFechaPago) {
		this.mostrarFechaPago = mostrarFechaPago;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}

	public boolean isMostrarFechaCierre() {
		return mostrarFechaCierre;
	}

	public void setMostrarFechaCierre(boolean mostrarFechaCierre) {
		this.mostrarFechaCierre = mostrarFechaCierre;
	}

	public Short getFechaCierre() {
		return fechaCierre;
	}

	public void setFechaCierre(Short fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public Short getFechaPagoOtra() {
		return fechaPagoOtra;
	}

	public void setFechaPagoOtra(Short fechaPagoOtra) {
		this.fechaPagoOtra = fechaPagoOtra;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getAprobadoCliente() {
		return aprobadoCliente;
	}

	public void setAprobadoCliente(String aprobadoCliente) {
		this.aprobadoCliente = aprobadoCliente;
	}

	public String getTieneRetenciones() {
		return tieneRetenciones;
	}

	public void setTieneRetenciones(String tieneRetenciones) {
		this.tieneRetenciones = tieneRetenciones;
	}

	public String getAceptaDesembolso() {
		return aceptaDesembolso;
	}

	public void setAceptaDesembolso(String aceptaDesembolso) {
		this.aceptaDesembolso = aceptaDesembolso;
	}

	public String getPoderesInscritos() {
		return poderesInscritos;
	}

	public void setPoderesInscritos(String poderesInscritos) {
		this.poderesInscritos = poderesInscritos;
	}

	public String getDpsFormalizada() {
		return dpsFormalizada;
	}

	public void setDpsFormalizada(String dpsFormalizada) {
		this.dpsFormalizada = dpsFormalizada;
	}

	public String getEnvioEstadoCuenta() {
		return envioEstadoCuenta;
	}

	public void setEnvioEstadoCuenta(String envioEstadoCuenta) {
		this.envioEstadoCuenta = envioEstadoCuenta;
	}

	public boolean isMostrarNumeroContratoFormalizacion() {
		return mostrarNumeroContratoFormalizacion;
	}

	public void setMostrarNumeroContratoFormalizacion(boolean mostrarNumeroContratoFormalizacion) {
		this.mostrarNumeroContratoFormalizacion = mostrarNumeroContratoFormalizacion;
	}
}
