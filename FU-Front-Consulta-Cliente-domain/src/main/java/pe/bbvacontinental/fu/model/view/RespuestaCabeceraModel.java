package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class RespuestaCabeceraModel implements Serializable {

	private static final long serialVersionUID = 1L;
	private String estado;
	private String codigo;
	private String mensaje;

	public RespuestaCabeceraModel() {
		super();
	}

	public RespuestaCabeceraModel(String estado, String codigo, String mensaje) {
		super();
		this.estado = estado;
		this.codigo = codigo;
		this.mensaje = mensaje;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
