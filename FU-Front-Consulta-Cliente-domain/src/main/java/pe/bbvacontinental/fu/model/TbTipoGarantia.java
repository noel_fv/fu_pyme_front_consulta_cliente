package pe.bbvacontinental.fu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TIPO_GARANTIA")
public class TbTipoGarantia implements java.io.Serializable{
	
	private int idTipoGarantia;
	private String valor;
	private String nombre;
	
	public TbTipoGarantia(){
		
	}
	
	public TbTipoGarantia(int idTipoGarantia){
		this.idTipoGarantia = idTipoGarantia;		
	}
	
	public TbTipoGarantia(int idTipoGarantia, String valor, String nombre){
		this.idTipoGarantia = idTipoGarantia;	
		this.valor = valor;
		this.nombre = nombre;
	}
	
	@Id

	@Column(name = "ID", unique = true, nullable = false, precision = 2, scale = 0)	
	public int getIdTipoGarantia() {
		return idTipoGarantia;
	}
	public void setIdTipoGarantia(int idTipoGarantia) {
		this.idTipoGarantia = idTipoGarantia;
	}
	
	@Column(name = "VALUE", length = 12)
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	@Column(name = "NAME", length = 100)
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
