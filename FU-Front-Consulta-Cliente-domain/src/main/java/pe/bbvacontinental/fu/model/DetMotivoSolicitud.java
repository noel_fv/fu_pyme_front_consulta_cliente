package pe.bbvacontinental.fu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DET_PJ_MOTIVO_SOLICITUD")
public class DetMotivoSolicitud implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idDetMotivoSolicitud;
	private TbMotivoSolicitud tbMotivoSolicitud;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;
	private TbTrazabilidad tbTrazabilidad;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_DET_PJ_MOTIVO_SOLICITUD")
	@SequenceGenerator(
		name="SQ_DET_PJ_MOTIVO_SOLICITUD",
		sequenceName="SQ_DET_PJ_MOTIVO_SOLICITUD",
		allocationSize=1
	)
	@Column(name = "ID_DET_PJ_MOTIVO_SOLICITUD", unique = true)
	public long getIdDetMotivoSolicitud() {
		return idDetMotivoSolicitud;
	}

	public void setIdDetMotivoSolicitud(long idDetMotivoSolicitud) {
		this.idDetMotivoSolicitud = idDetMotivoSolicitud;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_MAE_PJ_MOTIVO_SOLICITUD")
	public TbMotivoSolicitud getTbMotivoSolicitud() {
		return tbMotivoSolicitud;
	}

	public void setTbMotivoSolicitud(TbMotivoSolicitud tbMotivoSolicitud) {
		this.tbMotivoSolicitud = tbMotivoSolicitud;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "USUARIO_CREACION", length = 14)
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	@Column(name = "USUARIO_MODIFICACION", length = 14)
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_HIS_PJ_TRAZABILIDAD")
	public TbTrazabilidad getTbTrazabilidad() {
		return tbTrazabilidad;
	}

	public void setTbTrazabilidad(TbTrazabilidad tbTrazabilidad) {
		this.tbTrazabilidad = tbTrazabilidad;
	}
}
