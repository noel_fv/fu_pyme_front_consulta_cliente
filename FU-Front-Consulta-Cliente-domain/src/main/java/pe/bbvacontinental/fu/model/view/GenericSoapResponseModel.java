package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class GenericSoapResponseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idSesion;
	
	private String idPeticionEmpresa;
	
	private String idPeticionBanco;
	
	private String idOperacion;
	
	private String codigoError;
	
	private String mensajeError;

	public String getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(String idSession) {
		this.idSesion = idSession;
	}

	public String getIdPeticionEmpresa() {
		return idPeticionEmpresa;
	}

	public void setIdPeticionEmpresa(String idPetEmpresa) {
		this.idPeticionEmpresa = idPetEmpresa;
	}

	public String getIdPeticionBanco() {
		return idPeticionBanco;
	}

	public void setIdPeticionBanco(String idPetBanco) {
		this.idPeticionBanco = idPetBanco;
	}

	public String getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(String idOper) {
		this.idOperacion = idOper;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}	
}
