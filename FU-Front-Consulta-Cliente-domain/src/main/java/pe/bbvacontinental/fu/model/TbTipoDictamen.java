package pe.bbvacontinental.fu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "MAE_PJ_TIPO_DICTAMEN")
public class TbTipoDictamen implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4947938782962120443L;
	
	private Long idTipoDictamen;
	private String valor;
	private String descripcion;
	private Boolean estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;
	
	@Id	
	@Column(name = "ID_MAE_PJ_TIPO_DICTAMEN", unique = true, nullable = false, precision = 12, scale = 0)
	public Long getIdTipoDictamen() {
		return idTipoDictamen;
	}
	public void setIdTipoDictamen(Long idTipoDictamen) {
		this.idTipoDictamen = idTipoDictamen;
	}
	
	@Column(name = "VALOR")
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	@Column(name = "DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name = "ESTADO")
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "USUARIO_MODIFICACION", length = 14)
	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	@Column(name = "USUARIO_CREACION", length = 14)
	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

}
