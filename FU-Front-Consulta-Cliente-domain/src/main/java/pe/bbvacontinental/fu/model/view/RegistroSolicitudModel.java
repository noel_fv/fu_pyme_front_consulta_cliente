package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
 

public class RegistroSolicitudModel extends GenericModel implements Serializable {
	private static final long serialVersionUID = -6700945337338498481L;

	private Long idSolicitud;
	private String numeroSolicitud;
	private String tipoOperacion;
	private String nombreTipoOperacion;
	private Long tipoCampania;
	private Long tipoOperacionCDR;
	
	private String estadoSolicitud;
	private String nombreEstadoSolicitud;
	private Date fechaEstadoSolicitud;
	private String numeroInstanciaBPM;
	
	private DatosTitularModel datosTitularModel;
	private List<ProductoModel> listaCarritoProducto = new ArrayList<>();
	
	private String oficina;
	private String nombreOficina;
	private String territorio;
	private String nombreTerritorio;
	private String nombreCanal;
	private Date fechaRegistro;
	private String usuarioAsignado;
	private String modoEdicion;
	
	private String codigoUsuario;
    private String nombreUsuario; 
    
    private String comentario;
	
	@JsonIgnore
	public Long getIdSolicitud() {
		return idSolicitud;
	}

	@JsonIgnore
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	public Long getTipoCampania() {
		return tipoCampania;
	}

	public void setTipoCampania(Long tipoCampania) {
		this.tipoCampania = tipoCampania;
	}

	public Long getTipoOperacionCDR() {
		return tipoOperacionCDR;
	}

	public void setTipoOperacionCDR(Long tipoOperacionCDR) {
		this.tipoOperacionCDR = tipoOperacionCDR;
	}

	public DatosTitularModel getDatosTitularModel() {
		return datosTitularModel;
	}

	public void setDatosTitularModel(DatosTitularModel datosTitularModel) {
		this.datosTitularModel = datosTitularModel;
	}

	public List<ProductoModel> getListaCarritoProducto() {
		return listaCarritoProducto;
	}

	public void setListaCarritoProducto(List<ProductoModel> listaCarritoProducto) {
		this.listaCarritoProducto = listaCarritoProducto;
	}

	public String getEstadoSolicitud() {
		return estadoSolicitud;
	}

	public void setEstadoSolicitud(String estadoSolicitud) {
		this.estadoSolicitud = estadoSolicitud;
	}

	public String getNombreEstadoSolicitud() {
		return nombreEstadoSolicitud;
	}

	public void setNombreEstadoSolicitud(String nombreEstadoSolicitud) {
		this.nombreEstadoSolicitud = nombreEstadoSolicitud;
	}

	public String getNumeroInstanciaBPM() {
		return numeroInstanciaBPM;
	}

	public void setNumeroInstanciaBPM(String numeroInstanciaBPM) {
		this.numeroInstanciaBPM = numeroInstanciaBPM;
	}

	public Date getFechaEstadoSolicitud() {
		return fechaEstadoSolicitud;
	}

	public void setFechaEstadoSolicitud(Date fechaEstadoSolicitud) {
		this.fechaEstadoSolicitud = fechaEstadoSolicitud;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getNombreOficina() {
		return nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	public String getTerritorio() {
		return territorio;
	}

	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}

	public String getNombreTerritorio() {
		return nombreTerritorio;
	}

	public void setNombreTerritorio(String nombreTerritorio) {
		this.nombreTerritorio = nombreTerritorio;
	}

	public String getNombreCanal() {
		return nombreCanal;
	}

	public void setNombreCanal(String nombreCanal) {
		this.nombreCanal = nombreCanal;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	public String getNombreTipoOperacion() {
		return nombreTipoOperacion;
	}

	public void setNombreTipoOperacion(String nombreTipoOperacion) {
		this.nombreTipoOperacion = nombreTipoOperacion;
	}

	public String getModoEdicion() {
		return modoEdicion;
	}

	public void setModoEdicion(String modoEdicion) {
		this.modoEdicion = modoEdicion;
	}

	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getUsuarioAsignado() {
		return usuarioAsignado;
	}

	public void setUsuarioAsignado(String usuarioAsignado) {
		this.usuarioAsignado = usuarioAsignado;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	
	
	
}