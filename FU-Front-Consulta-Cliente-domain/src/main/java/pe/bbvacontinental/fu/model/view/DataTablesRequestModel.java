package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

public class DataTablesRequestModel  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String draw;
	
	private String length;
	
	private String start;

	public String getDraw() {
		return draw;
	}

	public void setDraw(String draw) {
		this.draw = draw;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}
}
