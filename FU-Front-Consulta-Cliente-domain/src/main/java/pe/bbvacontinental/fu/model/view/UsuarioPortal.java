package pe.bbvacontinental.fu.model.view;

import java.io.Serializable;

/**
 * Clase que guarda los datos del usuario que se logueo a
 * File Unico mediante la aplicacion Portal
 * @author huber.quinto
 *
 */
public class UsuarioPortal implements Serializable{

	private static final long serialVersionUID = -1615899279804067996L;
	
	private String codigoUsuario;
	private String nombreUsuario;
	private String apellidosUsuario;
	private String codigoOficina;
	private String nombreOficina;
	private String codigoTerritorio;
	private String nombreTerritorio;
	
	private String puestoCorporativo;
	private String puestoFuncionalLocal;
	private String descripcionPuesto;
	
	private String cookieWebseal;
	private String clave;
	
	private String ip;
	
	public String getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public String getApellidosUsuario() {
		return apellidosUsuario;
	}
	public void setApellidosUsuario(String apellidosUsuario) {
		this.apellidosUsuario = apellidosUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getCodigoOficina() {
		return codigoOficina;
	}
	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
	public String getCodigoTerritorio() {
		return codigoTerritorio;
	}
	public void setCodigoTerritorio(String codigoTerritorio) {
		this.codigoTerritorio = codigoTerritorio;
	}
	public String getNombreTerritorio() {
		return nombreTerritorio;
	}
	public void setNombreTerritorio(String nombreTerritorio) {
		this.nombreTerritorio = nombreTerritorio;
	}
	public String getPuestoCorporativo() {
		return puestoCorporativo;
	}
	public void setPuestoCorporativo(String puestoCorporativo) {
		this.puestoCorporativo = puestoCorporativo;
	}
	public String getPuestoFuncionalLocal() {
		return puestoFuncionalLocal;
	}
	public void setPuestoFuncionalLocal(String puestoFuncionalLocal) {
		this.puestoFuncionalLocal = puestoFuncionalLocal;
	}
	public String getDescripcionPuesto() {
		return descripcionPuesto;
	}
	public void setDescripcionPuesto(String descripcionPuesto) {
		this.descripcionPuesto = descripcionPuesto;
	}
	public String getCookieWebseal() {
		return cookieWebseal;
	}
	public void setCookieWebseal(String cookieWebseal) {
		this.cookieWebseal = cookieWebseal;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public void setIP(String remoteAddr) {
		this.ip = remoteAddr;
	}
	
	public String getIP(){
		return this.ip;
	}

}
