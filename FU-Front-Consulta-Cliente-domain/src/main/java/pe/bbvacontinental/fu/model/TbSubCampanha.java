package pe.bbvacontinental.fu.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MAE_PJ_SUB_CAMPANHA")
public class TbSubCampanha implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L; 
	
	private long idSubCampanha;
	private String valor;
	private String nombre;
	private Boolean estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;
	
	public TbSubCampanha() {
	}

	public TbSubCampanha(long idSubCampanha) {
		this.idSubCampanha = idSubCampanha;
	}
	


	
	@Id
	
	@Column(name = "ID_MAE_PJ_SUB_CAMPANHA", unique = true, nullable = false, precision = 12, scale = 0)
	public long getIdSubCampanha() {
		return this.idSubCampanha;
	}

	public void setIdSubCampanha(long idSubCampanha) {
		this.idSubCampanha = idSubCampanha;
	}
	
	@Column(name = "VALOR", length = 5)
	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Column(name = "NOMBRE", length = 50)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombreSubCampanha) {
		this.nombre = nombreSubCampanha;
	}

	@Column(name = "ESTADO", precision = 1, scale = 0)
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechModific) {
		this.fechaModificacion = fechModific;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechCreac) {
		this.fechaCreacion = fechCreac;
	}

	@Column(name = "USUARIO_MODIFICACION", length = 14)
	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuModific) {
		this.usuarioModificacion = usuModific;
	}

	@Column(name = "USUARIO_CREACION", length = 14)
	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuCreac) {
		this.usuarioCreacion = usuCreac;
	}

}
