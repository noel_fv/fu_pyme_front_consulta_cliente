package pe.bbvacontinental.fu.model.view;

public class RespuestaErrorGeneralModel {
	private RespuestaCabeceraModel head;
	private Object body;

	public RespuestaErrorGeneralModel() {
		super();
		this.head = new RespuestaCabeceraModel("ERR", "99", "Servicio fallido");
	}

	public RespuestaErrorGeneralModel(RespuestaCabeceraModel head, Object body) {
		super();
		this.head = head;
		this.body = body;
	}

	public RespuestaErrorGeneralModel(String mensajeError) {
		super();
		this.head = new RespuestaCabeceraModel("ERR", "99", "Servicio fallido");
		this.head.setMensaje(mensajeError);
	}
	


	public RespuestaCabeceraModel getHead() {
		return head;
	}

	public void setHead(RespuestaCabeceraModel head) {
		this.head = head;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}
}
