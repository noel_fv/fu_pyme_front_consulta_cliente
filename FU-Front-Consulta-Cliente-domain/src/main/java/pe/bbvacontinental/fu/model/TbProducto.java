package pe.bbvacontinental.fu.model;
// Generated 03/10/2016 03:18:57 PM by Hibernate Tools 5.2.0.Beta1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * TbProducto generated by hbm2java
 */
@Entity
@Table(name = "TB_PRODUCTOS")
public class TbProducto implements java.io.Serializable {

	private static final long serialVersionUID = 1L; 
	private long idProducto;
	private String valor;
	private String nombre;
	private Boolean estado;
	 
	
	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
	public long getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	@Column(name = "VALUE", length = 2)
	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Column(name = "NAME", length = 100)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombreProd) {
		this.nombre = nombreProd;
	}

	@Column(name = "VISIBILIDAD", precision = 1, scale = 0)
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}



}
