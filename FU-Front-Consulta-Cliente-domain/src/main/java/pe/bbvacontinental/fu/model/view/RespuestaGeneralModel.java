package pe.bbvacontinental.fu.model.view;

public class RespuestaGeneralModel {
	private RespuestaCabeceraModel head;
	private Object body;

	public RespuestaGeneralModel() {
		super();
		this.head = new RespuestaCabeceraModel("OK", "200", "Servicio ejecutado correctamente");
	}

	public RespuestaGeneralModel(RespuestaCabeceraModel head, Object body) {
		super();
		this.head = head;
		this.body = body;
	}

	public RespuestaGeneralModel(Object body) {
		super();
		this.head = new RespuestaCabeceraModel("OK", "200", "Servicio ejecutado correctamente");
		this.body = body;
	}

	public RespuestaCabeceraModel getHead() {
		return head;
	}

	public void setHead(RespuestaCabeceraModel head) {
		this.head = head;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}
}
