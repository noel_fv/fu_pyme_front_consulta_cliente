package pe.bbvacontinental.fu.service.impl;



import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import javax.xml.namespace.QName;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ma.glasnost.orika.MapperFacade;
import pe.bbvacontinental.fu.back.util.Util;
import pe.bbvacontinental.fu.common.enumeration.EstadoServiciosEnum;
import pe.bbvacontinental.fu.common.enumeration.MensajesGeneralesEnum;
import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.dto.DatosClienteDTO;
import pe.bbvacontinental.fu.dto.OficinaDTO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.UsuarioPortal;
import pe.bbvacontinental.fu.service.BuscadorClienteService;
import pe.com.bbva.centrosbbvawebservice.CentrosBBVAWebServiceService;
import pe.com.bbva.centrosbbvawebservice.ObtenerOficinaRequest;
import pe.com.bbva.centrosbbvawebservice.ObtenerOficinaResponse;
import pe.com.bbva.centrosbbvawebservice.Oficina;
import pe.com.bbva.hi.fileunico.cabecera.RequestHeader;
import pe.com.bbva.hi.fileunico.ccli.IntegrationRequest;
import pe.com.bbva.hi.fileunico.ccli.IntegrationResponse;
import pe.com.bbva.hi.fileunico.ccli.MSFILEUNICOSOAPHTTPService;

@Service
public class BuscadorClienteServiceImpl implements BuscadorClienteService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private MapperFacade mapper;
	
	@Autowired
	ParametroAplicativoDAO parametroAplicativoDAO;
	
    @Value("${servicio.dato.cliente.codigoEmpresa}")
    private String codigoEmpresa;
    @Value("${servicio.dato.cliente.codigoTerminalEmpresa}")
    private String  codigoTerminalEmpresa;
    @Value("${servicio.dato.cliente.canal}")
    private String canal;
    @Value("${servicio.dato.cliente.codigoAplicacion}")
    private String codigoAplicacion;

    @Value("${servicio.dato.cliente.idPeticionEmpresa}")
    private String idPeticionEmpresa;
	
    @Value("${servicio.dato.cliente.usuario}")
    private String dcUsuario;
    

    
    @Value("${servicio.dato.cliente.id.operacion}")
    private String dcIdOperacion;
    
    @Value("${servicio.dato.cliente.id.servicio}")
    private String dcIdServicio;
    
    @Value("${servicio.dato.cliente.id.interconexion}")
    private String dcIdInterconexion;
    
    
    @Value("${fielunico.cc.modo.dummy}")
    private String esModoDummy;
        
    @Value("${servicio.datos.oficina.cliente.sin.exito}")
    private String dcSinExito;
    
    @Autowired
	private Environment environment;
	
	@Override
	public ClienteModel buscarDatosCliente(ClienteModel buscarClienteModel,UsuarioPortal usuarioPortal) throws MalformedURLException {
		
		logger.info("INI {} ","BuscadorClienteServiceImpl.buscarDatosCliente");
		DatosClienteDTO datosClienteDTO = new DatosClienteDTO();
		ClienteModel clienteModel = new ClienteModel();
		

		try {
			TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_DATOS_CLIENTE.getCodigo());
			String ruta = "";
			String nameSpace = "";
			String localPart = "";
							
			if(tbParametrosAplicativo!=null){
				ruta = tbParametrosAplicativo.getValor1();
				nameSpace = tbParametrosAplicativo.getValor2();
				localPart = tbParametrosAplicativo.getValor3();
				logger.info(" buscarDatosCliente tbParametrosAplicativo.getValor1(): {} ",tbParametrosAplicativo.getValor1());
				logger.info(" buscarDatosCliente tbParametrosAplicativo.getValor2(): {} ",tbParametrosAplicativo.getValor2());
				logger.info(" buscarDatosCliente tbParametrosAplicativo.getValor3(): {} ",tbParametrosAplicativo.getValor3());
			}else {
				logger.info(" buscarDatosCliente tbParametrosAplicativo: {} ",StringUtils.EMPTY);
			}
			
			URL url = new URL(ruta); 
			
			QName qName = new QName(nameSpace, localPart);
			MSFILEUNICOSOAPHTTPService servicioDatosCliente = new MSFILEUNICOSOAPHTTPService(url, qName);
			
			logger.info(" buscarDatosCliente servicioDatosCliente: {} ",servicioDatosCliente);

			IntegrationRequest integrationRequest = new IntegrationRequest();
			
			RequestHeader requestHeader=new RequestHeader();
			
			requestHeader.setCodigoEmpresa(codigoEmpresa);
			requestHeader.setCodigoTerminalEmpresa(codigoTerminalEmpresa);
			requestHeader.setCanal(canal);
			requestHeader.setCodigoAplicacion(codigoAplicacion);
			requestHeader.setFechaHoraEnvio(Util.formatDate(new Date(), "yyyy-MM-dd-HH.mm.SS.ssssss"));
			requestHeader.setIdSesion(String.valueOf(Util.getDateNow().getTime()));
			requestHeader.setIdPeticionEmpresa(idPeticionEmpresa);
			
			String usuarioConsulta="";
			if("".equals(dcUsuario)){ //Para QA y Producccion debe estar vacio en el properties
				usuarioConsulta=usuarioPortal.getCodigoUsuario();
			}else{
				usuarioConsulta=dcUsuario;
			}
			requestHeader.setUsuario(usuarioConsulta);
			
			requestHeader.setIdPeticionBanco(Util.formatDate(new Date(), "yyyyMMddHHmmSSssssss")+codigoAplicacion+usuarioConsulta);
			requestHeader.setIdOperacion(dcIdOperacion);
			requestHeader.setIdServicio(dcIdServicio);
			requestHeader.setIdInterconexion(dcIdInterconexion);
			
			integrationRequest.setRequestHeader(requestHeader);
			
			integrationRequest.setCodigoCentral(buscarClienteModel.getCodigoCentral());
			integrationRequest.setTipoDocumento(buscarClienteModel.getTipoDocumentoIdentidad());
			integrationRequest.setNumeroDocumento(buscarClienteModel.getNumeroDocumento());
			
			logger.info("integrationRequest: ,{} ",integrationRequest);
			
			IntegrationResponse integrationResponse =servicioDatosCliente.getMSFILEUNICOSOAPHTTPPort().consultaCliente(integrationRequest);
			
			if(StringUtils.equals(integrationResponse.getResponseHeader().getCodigoError(), EstadoServiciosEnum.RESULTADO_EXITO_CONSULTAR_CLIENTE.getCodigo())){
				logger.info(" integrationResponse: {}", integrationResponse);		
				logger.info(" integrationResponse getTipoPersona: {}", integrationResponse.getTipoPersona());
				logger.info(" integrationResponse getCodigoSegmento: {}", integrationResponse.getCodigoSegmento());
				logger.info(" integrationResponse getCodigoCentral(): {}", integrationResponse.getCodigoCentral());

				datosClienteDTO.setCodigoCentral(integrationResponse.getCodigoCentral());
				datosClienteDTO.setNombreCompleto(integrationResponse.getNombres());
				datosClienteDTO.setNumeroDocumento(integrationResponse.getNumeroDocumento());
				datosClienteDTO.setCodigoOficina(integrationResponse.getOficinaGestora());
				datosClienteDTO.setTipoDocumentoIdentidad(integrationResponse.getTipoDocumento());
				datosClienteDTO.setTipoPersona(integrationResponse.getTipoPersona());
				datosClienteDTO.setCodigoSegmento(integrationResponse.getCodigoSegmento());	
				datosClienteDTO.setDescripcionSegmento(integrationResponse.getDescripcionSegmento());
				
				// Invocamos a servicio que retorna la oficina y su terrtirio
				logger.info(" integrationResponse.getOficinaGestora(): {}", integrationResponse.getOficinaGestora());
				OficinaDTO oficinaDTO=null;
				oficinaDTO = buscarOficinaClienteMOCK(integrationResponse.getOficinaGestora());  //comentar PARA QA
				if(esModoDummy.equals("0")){ //solo pruebas 
						logger.info(" buscarDatosCliente: esModoDummy {}",esModoDummy);
						oficinaDTO = buscarOficinaCliente(integrationResponse.getOficinaGestora());
				}

				if(StringUtils.equals(oficinaDTO.getTipoResultado(), EstadoServiciosEnum.RESULTADO_EXITO_CONSULTAR_CLIENTE.getDescripcion())){
					datosClienteDTO.setNombreTerritorio(oficinaDTO.getNombreTerritorio());
					datosClienteDTO.setCodigoTerritorio(oficinaDTO.getCodigoTerritorio());
					datosClienteDTO.setNombreOficina(oficinaDTO.getNombreOficina());
					datosClienteDTO.setCodigoOficina(oficinaDTO.getCodigoOficina());
					clienteModel = this.mapper.map(datosClienteDTO, ClienteModel.class);				
				}else{
					clienteModel = this.mapper.map(datosClienteDTO, ClienteModel.class);
					clienteModel.setMensajeServicio(environment.getProperty(MensajesGeneralesEnum.MENSAJE_109.getCodigo()));
					clienteModel.setCodigoServicio(oficinaDTO.getTipoResultado());
					clienteModel.setRespuesta1(environment.getProperty(MensajesGeneralesEnum.MENSAJE_109.getCodigo()));
					logger.error("FU Error BuscadorClienteServiceImpl.buscarDatosCliente {}",oficinaDTO.getMensaje(),oficinaDTO.getMensaje());
				}	
				
			}else {
				clienteModel.setMensajeServicio(environment.getProperty(MensajesGeneralesEnum.MENSAJE_103.getCodigo()));
				clienteModel.setCodigoServicio(integrationResponse.getResponseHeader().getCodigoError());
				logger.error("FU Error BuscadorClienteServiceImpl.buscarDatosCliente: {}",integrationResponse.getResponseHeader().getMensajeError());
			}
			
		} catch (MalformedURLException e) {
			logger.error("FU Error BuscadorClienteServiceImpl.buscarDatosCliente: "+ e,e);
			throw e;
		}
		
		logger.info("FIN {}","BuscadorClienteServiceImpl.buscarDatosCliente ");
		return clienteModel;
	}

	@Override
	public OficinaDTO buscarOficinaCliente(String codigoOficina) throws MalformedURLException {

		OficinaDTO oficinaDTO = new OficinaDTO();
		TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_TERRITORIO_OFICINA.getCodigo());
		
		String path = "";
		String nameSpace = "";
		String localPart = "";
						
		if(tbParametrosAplicativo!=null){
			path = tbParametrosAplicativo.getValor1();
			nameSpace = tbParametrosAplicativo.getValor2();
			localPart = tbParametrosAplicativo.getValor3();			
		}
		
		URL url = new URL(path);
		QName qName = new QName(nameSpace, localPart);
	
		CentrosBBVAWebServiceService servicioTerritorioOficina = new CentrosBBVAWebServiceService(url, qName);
		
		ObtenerOficinaRequest obtenerOficinaRequest = new ObtenerOficinaRequest();
		
		obtenerOficinaRequest.setCodOficina(codigoOficina);
		
		logger.info(" buscarOficinaCliente obtenerOficinaRequest.getCodOficina() {}", obtenerOficinaRequest.getCodOficina());
		
		ObtenerOficinaResponse obtenerOficinaResponse=servicioTerritorioOficina.getCentrosBBVAWebServiceSOAP().obtenerOficina(obtenerOficinaRequest);
		oficinaDTO.setTipoResultado(obtenerOficinaResponse.getTipoResultado());
		oficinaDTO.setMensaje(obtenerOficinaResponse.getMensaje());
		if(StringUtils.equals(obtenerOficinaResponse.getTipoResultado(), EstadoServiciosEnum.RESULTADO_EXITO_CONSULTAR_CLIENTE.getDescripcion()))
		{
			Oficina oficina=obtenerOficinaResponse.getOficina();
			
			logger.info("  obtenerOficinaResponse: {}", obtenerOficinaResponse);
			logger.info("  oficina: {}", oficina);
			
			if(oficina!=null){ //Generalmente cuando el cliente tiene ventas anuales igual cero no tiene oficina de registro.
				
				oficinaDTO.setCodigoTerritorio(oficina.getTerritorio().getId());
				oficinaDTO.setNombreTerritorio(oficina.getTerritorio().getNombreTerritorio());
				oficinaDTO.setCodigoOficina(oficina.getId());
				oficinaDTO.setNombreOficina(oficina.getNombreOficina());
				
				logger.info("  oficinaDTO: {}", oficinaDTO);
			}
		}
		return oficinaDTO;
	}
	
	
	
	//SOLO PRUEBASS
	public OficinaDTO buscarOficinaClienteMOCK(String codigoOficina) {
		logger.info(" INI {}","buscarOficinaClienteMOCK"); 

		OficinaDTO oficinaDTO = new OficinaDTO();
		oficinaDTO.setTipoResultado(EstadoServiciosEnum.RESULTADO_EXITO_CONSULTAR_CLIENTE.getDescripcion());
		oficinaDTO.setMensaje("");

				oficinaDTO.setCodigoTerritorio("3736");
				oficinaDTO.setNombreTerritorio("TERR.LIMA CENTRO");
				oficinaDTO.setCodigoOficina(codigoOficina);
				oficinaDTO.setNombreOficina("CAPON");
				
	logger.info("  buscarOficinaClienteMOCK oficinaDTO: {}" ,oficinaDTO);

	logger.info(" FIN {}","buscarOficinaClienteMOCK");
		return oficinaDTO;
		
	}
	
	public ClienteModel getDatosClienteMOCK(){
		
		
		ClienteModel clienteModel = null;
		
		DatosClienteDTO datosClienteDTO = new DatosClienteDTO();


			datosClienteDTO.setCodigoCentral("23088115");
			datosClienteDTO.setNombreCompleto("DISTRIBUIDORA OLIKAI");
			datosClienteDTO.setNumeroDocumento("20478073446");
			datosClienteDTO.setCodigoOficina("0103");
			datosClienteDTO.setTipoDocumentoIdentidad("R");
			datosClienteDTO.setTipoPersona("M");
			datosClienteDTO.setCodigoSegmento("VIP");	
			datosClienteDTO.setDescripcionSegmento("");
			
			OficinaDTO oficinaDTO = buscarOficinaClienteMOCK("0103");  //comentar PARA QA

				datosClienteDTO.setNombreTerritorio(oficinaDTO.getNombreTerritorio());
				datosClienteDTO.setCodigoTerritorio(oficinaDTO.getCodigoTerritorio());
				datosClienteDTO.setNombreOficina(oficinaDTO.getNombreOficina());
				datosClienteDTO.setCodigoOficina(oficinaDTO.getCodigoOficina());
				clienteModel = this.mapper.map(datosClienteDTO, ClienteModel.class);				

			return clienteModel;
	}

}
