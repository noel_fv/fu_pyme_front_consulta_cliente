package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSolicitud;

public interface SolicitudService extends IOperations<TbSolicitud> {
	
	public boolean esFlujoPjPnn(String tipoPersona,String ventasAnuales);
	
}