package pe.bbvacontinental.fu.service;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.view.GenericModel;



public interface ParametroAplicativoService extends  IOperations<TbParametrosAplicativo>{
	
	public TbParametrosAplicativo getParameterByCode(String codigo);
	
	public List<TbParametrosAplicativo> getListParameterByType(String tipo);
	
	public GenericModel getParametroAplicativo(String codigo);	
	
}
