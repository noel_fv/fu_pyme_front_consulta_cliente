package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoMoneda;

public interface TipoMonedaService extends IOperations<TbTipoMoneda> {

}
