package pe.bbvacontinental.fu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.dao.TipoMonedaDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoMoneda;
import pe.bbvacontinental.fu.service.TipoMonedaService;
import pe.bbvacontinental.fu.service.common.AbstractService;


@Service
public class TipoMonedaServiceImpl extends AbstractService<TbTipoMoneda> implements TipoMonedaService {

	@Autowired
	private TipoMonedaDAO tipoMonedaDAO;
	
	@Override
	protected IOperations<TbTipoMoneda> getDao() {
		return tipoMonedaDAO;
	}

}
