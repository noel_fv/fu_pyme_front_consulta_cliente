package pe.bbvacontinental.fu.service;


import java.net.MalformedURLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.model.view.UsuarioPortal;

public interface ConsultaIDMService {
	
	public UsuarioPortal obtenerUsuarioPortal(GenericModel datosRequest) throws MalformedURLException, JsonProcessingException;

}
