package pe.bbvacontinental.fu.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.dao.GiroNegocioDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbGiroNegocio;
import pe.bbvacontinental.fu.service.GiroNegocioService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
public class GiroNegocioServiceImpl extends AbstractService<TbGiroNegocio> implements GiroNegocioService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	GiroNegocioDAO giroNegocioDAO;

	@Override
	protected IOperations<TbGiroNegocio> getDao() {
		return giroNegocioDAO;
	}
	
	@Override
	public TbGiroNegocio buscarPorCodigo(String codigoGiroNegocio){
		return giroNegocioDAO.buscarPorCodigo(codigoGiroNegocio);
	}
}
