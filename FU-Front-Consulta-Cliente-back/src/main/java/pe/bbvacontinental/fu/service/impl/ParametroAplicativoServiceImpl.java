package pe.bbvacontinental.fu.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.service.ParametroAplicativoService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
public class ParametroAplicativoServiceImpl extends AbstractService<TbParametrosAplicativo> implements ParametroAplicativoService {

	@Autowired
	private ParametroAplicativoDAO parametroAplicativoDAO;
	
	@Autowired
	private Environment environment;
	
	@Override
	protected IOperations<TbParametrosAplicativo> getDao() {
		return parametroAplicativoDAO;
	}
	
	public TbParametrosAplicativo getParameterByCode(String codigo){
		return parametroAplicativoDAO.getParameterByCode(codigo);
	}
	/**
	 * Metodo que retorna la lista de registro de un tipo especifico.
	 * @param tipo, es el agrupador de registros en la tabla de tablas
	 * @return
	 */
	public List<TbParametrosAplicativo> getListParameterByType(String tipo) {
		return parametroAplicativoDAO.getListParameterByType(tipo);
	}
	
	@Override
	@Transactional
	public GenericModel getParametroAplicativo(String codigo){
		
		GenericModel respuesta= new GenericModel();
		
		TbParametrosAplicativo parametro=parametroAplicativoDAO.getParameterByCode(codigo);
		
		respuesta.setRespuesta1(parametro.getValor1());
		respuesta.setRespuesta2(parametro.getValor2());
		respuesta.setRespuesta3(parametro.getValor3());

		
		return respuesta;
	}
}
