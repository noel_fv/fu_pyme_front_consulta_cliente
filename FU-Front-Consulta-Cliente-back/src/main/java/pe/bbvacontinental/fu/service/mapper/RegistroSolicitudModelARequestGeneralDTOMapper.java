package pe.bbvacontinental.fu.service.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import pe.bbvacontinental.fu.model.view.ProductoModel;
import pe.bbvacontinental.fu.model.view.RegistroSolicitudModel;
import pe.bbvacontinental.fu.wp.dto.comun.IntegrationRequestDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ProductoDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestBodyDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestHeaderDTO;
import pe.bbvacontinental.fu.wp.dto.comun.SubProductoDTO;

@Component
public class RegistroSolicitudModelARequestGeneralDTOMapper extends CustomMapper<RegistroSolicitudModel, RequestGeneralDTO> {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void mapAtoB(RegistroSolicitudModel registroSolicitudModel,RequestGeneralDTO requestGeneralDTO, MappingContext context ){
		logger.info("mapAtoB: inicio - {}","RegistroSolicitudModelARequestGeneralDTOMapper");	
		
		IntegrationRequestDTO integrationRequest = new IntegrationRequestDTO();
		establecerRequestHeader(registroSolicitudModel, integrationRequest);
		establecerRequestBody(registroSolicitudModel, integrationRequest);
		requestGeneralDTO.setIntegrationRequest(integrationRequest);
		
		logger.info("mapAtoB: fin - {}","RegistroSolicitudModelARequestGeneralDTOMapper");
	}
	
	private void establecerRequestHeader(RegistroSolicitudModel registroSolicitudModel, IntegrationRequestDTO integrationRequest){
		
		RequestHeaderDTO requestHeader = new RequestHeaderDTO();
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getIdUsuario())){
			requestHeader.setUsuario(registroSolicitudModel.getIdUsuario());
		}
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getFechaHoraEnvio())){
			requestHeader.setFechaHoraEnvio(registroSolicitudModel.getFechaHoraEnvio());
		}
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getIdSesion())){
			requestHeader.setIdSesion(registroSolicitudModel.getIdSesion());
		}
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getIdOperacion())){
			requestHeader.setIdOperacion(registroSolicitudModel.getIdOperacion());
		}
		
		integrationRequest.setRequestHeader(requestHeader);
		
	}
	
	private void establecerRequestBody (RegistroSolicitudModel registroSolicitudModel, IntegrationRequestDTO integrationRequest){
		RequestBodyDTO requestBody = new RequestBodyDTO();		
		agregarDatosRequestBody(registroSolicitudModel, requestBody);
		
		if(registroSolicitudModel.getListaCarritoProducto() != null && !registroSolicitudModel.getListaCarritoProducto().isEmpty()){
			List<ProductoDTO> listaProductosDTO = new ArrayList<>();
			int j = 0;
			for (int i = 0; i < registroSolicitudModel.getListaCarritoProducto().size(); i++) {
				if(i==j){
					ProductoModel productoModel = registroSolicitudModel.getListaCarritoProducto().get(i);
					ProductoDTO productoDTO = new ProductoDTO();
					agregarDatosProducto(productoDTO,productoModel);			
					
					// SUBPRODUCTOS
					List<SubProductoDTO> listaSubproductosDTO = new ArrayList<>();
					do{
						ProductoModel subProductoModel = registroSolicitudModel.getListaCarritoProducto().get(j);
						SubProductoDTO subProductoDTO = new SubProductoDTO();
						agregarDatosSubProduto(subProductoDTO,subProductoModel);					
						listaSubproductosDTO.add(subProductoDTO);
						j++;
					}while (registroSolicitudModel.getListaCarritoProducto().size()!= j &&	
								productoModel.getIdProducto().equals(registroSolicitudModel.getListaCarritoProducto().get(j).getIdProducto()));
									
					productoDTO.setListaSubproductos(listaSubproductosDTO);						
					listaProductosDTO.add(productoDTO);				
				}
			}
			requestBody.setListaProductos(listaProductosDTO);
		}

		integrationRequest.setRequestBody(requestBody);
	}

	private void agregarDatosProducto(ProductoDTO productoDTO, ProductoModel productoModel) {
		if (productoModel.getIdProducto() != null) {
			productoDTO.setCodProducto(productoModel.getIdProducto());
		}
		if (StringUtils.isNotBlank(productoModel.getProducto())) {
			productoDTO.setDescProducto(productoModel.getProducto());
		}
	}

	private void agregarDatosRequestBody(RegistroSolicitudModel registroSolicitudModel, RequestBodyDTO requestBody) {
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getNumeroSolicitud())){
			requestBody.setCodigoSolicitudFU(registroSolicitudModel.getNumeroSolicitud());
		}
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getDatosTitularModel().getCodigoCentral())){
			requestBody.setCodigoCentral(registroSolicitudModel.getDatosTitularModel().getCodigoCentral());
		}			
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getTipoOperacion())){
			requestBody.setCodTipoFlujo(registroSolicitudModel.getTipoOperacion());
			
		}
		
		if(registroSolicitudModel.getTipoCampania() != null && StringUtils.isNotBlank(registroSolicitudModel.getTipoCampania().toString())){
			requestBody.setIdTipoCampania(registroSolicitudModel.getTipoCampania().toString());
			
		}

		if(registroSolicitudModel.getTipoOperacionCDR() != null && StringUtils.isNotBlank(registroSolicitudModel.getTipoOperacionCDR().toString())){
			requestBody.setIdTipoOperacion(registroSolicitudModel.getTipoOperacionCDR().toString());
		}		
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getDatosTitularModel().getClienteNuevo())){
			requestBody.setFlagClienteNuevo(registroSolicitudModel.getDatosTitularModel().getClienteNuevo());
		}
		
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getDatosTitularModel().getCodigoGiroNegocio())){
			String [] codigosGiroNegocio= registroSolicitudModel.getDatosTitularModel().getCodigoGiroNegocio().split("_"); 			
			requestBody.setIdGiroNegocio(codigosGiroNegocio[1]);
		}
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getDatosTitularModel().getCodigoRegimenTributario())){
			requestBody.setIdRegimenTributario(registroSolicitudModel.getDatosTitularModel().getCodigoRegimenTributario());
		}
		
		if(StringUtils.isNotBlank(registroSolicitudModel.getDatosTitularModel().getIdTipoPersona())){
			requestBody.setCodigoTipoPersona(registroSolicitudModel.getDatosTitularModel().getIdTipoPersona());
		}		
	}

	private void agregarDatosSubProduto(SubProductoDTO subProductoDTO, ProductoModel subProductoModel) {
		if(subProductoModel.getIdSubProducto() != null){
			String [] listaIdValorWPSubProd= subProductoModel.getIdSubProducto().split("_");						
			subProductoDTO.setCodSubProducto(listaIdValorWPSubProd[1]);
		}
		if(StringUtils.isNotBlank(subProductoModel.getSubProducto())){
			subProductoDTO.setDescSubProducto(subProductoModel.getSubProducto());
		}
		if(subProductoModel.getIdMoneda()!=null){
			subProductoDTO.setCodTipoMoneda(subProductoModel.getMoneda());
		}
		if(StringUtils.isNotBlank(subProductoModel.getIdCampanha())){
			subProductoDTO.setCodCampania(subProductoModel.getIdCampanha());
		}
		if(StringUtils.isNotBlank(subProductoModel.getCampanha())){
			subProductoDTO.setDescCampania(subProductoModel.getCampanha());
		}
		if(subProductoModel.getMontoSolicitado()!= null){
			subProductoDTO.setImporteSolicitado(subProductoModel.getMontoSolicitado().toString());
		}
		if(subProductoModel.getTasaAnual()!= null){
			subProductoDTO.setTasaAnual(String.valueOf(subProductoModel.getTasaAnual()));
		}
		if(subProductoModel.getFechaVencimiento()!= null){
			subProductoDTO.setFechaVencimientoLinea(subProductoModel.getFechaVencimiento());
		}
		if(subProductoModel.getPlazoReembolsoCuotas()!= null){
			subProductoDTO.setPlazoReembolso(subProductoModel.getPlazoReembolsoCuotas().toString());
		}

		if(subProductoModel.getPlazoVencimientoDias()!= null){
			subProductoDTO.setPlazoVencimiento(subProductoModel.getPlazoVencimientoDias().toString());
		}	
		
		if(subProductoModel.getFlagLinea()!= null){
			subProductoDTO.setFlagLinea(subProductoModel.getFlagLinea());
		}
	}

}
