package pe.bbvacontinental.fu.service.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.bbvacontinental.fu.back.util.Util;
import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.dto.OficinaDTO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.model.view.UsuarioPortal;
import pe.bbvacontinental.fu.service.BuscadorClienteService;
import pe.bbvacontinental.fu.service.ConsultaIDMService;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.ConsultaIDMWSPortType;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.ConsultaIDMWSService;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.ObtenerUsuarioRequest;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.ObtenerUsuarioResponse;
import pe.bbvacontinental.medios.gestiondemanda.consultaidmws.ws.types.base.AuditRequest;

@Service
public class ConsultaIDMServiceImpl implements ConsultaIDMService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	ParametroAplicativoDAO parametroAplicativoDAO;

	@Autowired
	BuscadorClienteService buscadorClienteService;

	@Autowired
	private ObjectMapper objectMapper;

	@Value("${fielunico.cc.modo.dummy}")
	private String esModoDummy;

	@Override
	public UsuarioPortal obtenerUsuarioPortal(GenericModel datosRequest) throws MalformedURLException,JsonProcessingException {

		logger.info(" Consulta WS-IDM-FU | {}","INICIO ");
		long tiempoINI = System.currentTimeMillis();
		UsuarioPortal usuarioPortal = null;
		try {

			logger.info(" Consulta WS-IDM-FU | esModoDummy {}",esModoDummy);

			// Solo para pruebas locales
			if (esModoDummy.equals("2")) {
				return servicioIDMMOCK(datosRequest);
			}

			TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_CONSULTAR_IDM.getCodigo());
			String ruta = "";
			String nameSpace = "";
			String localPart = "";

			if (tbParametrosAplicativo != null) {
				ruta = tbParametrosAplicativo.getValor1();
				nameSpace = tbParametrosAplicativo.getValor2();
				localPart = tbParametrosAplicativo.getValor3();
			}
			logger.info(" Consulta WS-IDM-FU | ruta: {} nameSpace: {} localPart: {} ",ruta , nameSpace , localPart);
			URL url = new URL(ruta);
			QName qName = new QName(nameSpace, localPart);

			ConsultaIDMWSService consultaIDMWSService = new ConsultaIDMWSService(url, qName);
			// Adicionar Cookie para SSO aqui
			ConsultaIDMWSPortType consultaIDMWSPortType = consultaIDMWSService.getEbsConsultaIDMSB11();
			//
			BindingProvider bindingProvider = (BindingProvider) consultaIDMWSPortType;
			Map<String, Object> requestContext = bindingProvider.getRequestContext();
			Map<String, List<String>> requestHeaders = new HashMap<>();

			List<String> cookies = new ArrayList<>();
			cookies.add("PD-S-SESSION-ID=" + datosRequest.getCookieSSO());
			requestHeaders.put("Cookie", cookies);
			requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
			// Fin

			AuditRequest auditRequest = new AuditRequest();
			auditRequest.setIdTransaccion(Util.getIdTransaccion());
			auditRequest.setIpAplicacion(datosRequest.getIpUsuario());
			auditRequest.setNombreAplicacion(datosRequest.getNombreAplicacion());
			auditRequest.setUsuarioAplicacion(datosRequest.getIdUsuario());

			ObtenerUsuarioRequest obtenerUsuarioRequest = new ObtenerUsuarioRequest();
			obtenerUsuarioRequest.setAuditRequest(auditRequest);
			// Codigo de usuario
			obtenerUsuarioRequest.setRegistro(datosRequest.getIdUsuario());

			String a=objectMapper.writeValueAsString(obtenerUsuarioRequest);
			logger.info(" Consulta WS-IDM-FU Request: {}",a);
			ObtenerUsuarioResponse obtenerUsuarioResponse = consultaIDMWSPortType.obtenerUsuario(obtenerUsuarioRequest);
			String aa=objectMapper.writeValueAsString(obtenerUsuarioResponse);
			logger.info(" Consulta WS-IDM-FU Response: {}", aa);

			if ("0".equals(obtenerUsuarioResponse.getAuditResponse().getCodigoRespuesta())) {
				usuarioPortal = new UsuarioPortal();
				usuarioPortal.setCodigoUsuario(obtenerUsuarioResponse.getRegistro());
				usuarioPortal.setNombreUsuario(obtenerUsuarioResponse.getNombres());
				usuarioPortal.setApellidosUsuario(obtenerUsuarioResponse.getApellidos());
				usuarioPortal.setCodigoOficina(obtenerUsuarioResponse.getOficina());
				usuarioPortal.setPuestoCorporativo(obtenerUsuarioResponse.getPuestoCorporativo());
				usuarioPortal.setPuestoFuncionalLocal(obtenerUsuarioResponse.getPuestoFuncionalLocal());
				usuarioPortal.setDescripcionPuesto(obtenerUsuarioResponse.getDescripcionPuesto());

				// Buscamos territorio de la Oficina
				OficinaDTO oficinaDTO = null;
				oficinaDTO = buscarOficinaClienteMOCK(usuarioPortal.getCodigoOficina());

				if (esModoDummy.equals("0")) { // quitar para test
					oficinaDTO = buscarTerritorioUsuario(usuarioPortal.getCodigoOficina());
				}
				
				usuarioPortal.setCodigoTerritorio(oficinaDTO.getCodigoTerritorio());
				usuarioPortal.setNombreTerritorio(oficinaDTO.getNombreTerritorio());
				usuarioPortal.setNombreOficina(oficinaDTO.getNombreOficina());

			} else {
				datosRequest.setMensajeServicio(obtenerUsuarioResponse.getAuditResponse().getMensajeRespuesta());
			}
		} catch (MalformedURLException | JsonProcessingException e) {
			usuarioPortal = null;
			logger.error(" Consulta WS-IDM-FU Error:", e);
			throw e;
		}finally {
			logger.info(" Consulta WS-IDM-FU | FIN {} ms",(System.currentTimeMillis() - tiempoINI));
		}
		return usuarioPortal;
	}

	private OficinaDTO buscarTerritorioUsuario(String codigoOficina) throws MalformedURLException {
		long tiempoINI = System.currentTimeMillis();
		logger.info("INI  Consulta WS-IDM-FU buscarTerritorioUsuario  codigoOficina: {}", codigoOficina);
		OficinaDTO oficinaDTO = buscadorClienteService.buscarOficinaCliente(codigoOficina);
		logger.info("FIN  Consulta WS-IDM-FU buscarTerritorioUsuario [ {} ms]",(System.currentTimeMillis() - tiempoINI));
		return oficinaDTO;
	}

	// SOLO PRUEBAS CON DUMMY
	private UsuarioPortal servicioIDMMOCK(GenericModel datosRequest) {

		logger.info("[DUMMY] ConsultaIDMServiceImpl --obtenerUsuarioPortal: {}",datosRequest);
		UsuarioPortal usuarioPortal = new UsuarioPortal();

		usuarioPortal.setCodigoUsuario(parametroAplicativoDAO.getParameterByCode("USUARIO_PERFIL").getValor1());
		usuarioPortal.setNombreUsuario(" CARLOS AMERICO Dummy");
		usuarioPortal.setApellidosUsuario("BARRIGA AREVALO Dummy");
		usuarioPortal.setCodigoOficina("0267");
		usuarioPortal.setPuestoCorporativo("B21");
		usuarioPortal.setPuestoFuncionalLocal("B21");
		usuarioPortal.setDescripcionPuesto("GERENTE DE OFICINA");
		usuarioPortal.setCodigoTerritorio("3736");
		usuarioPortal.setNombreTerritorio("3736");
		usuarioPortal.setNombreOficina("LIMA");

		return usuarioPortal;
	}

	// SOLO PRUEBASS CON DUMMY
	public OficinaDTO buscarOficinaClienteMOCK(String codigoOficina) {
		logger.info(" [DUMMY] {}","buscarOficinaClienteMOCK ");

		OficinaDTO oficinaDTO = new OficinaDTO();
		oficinaDTO.setTipoResultado("EXITO");
		oficinaDTO.setMensaje("");
		oficinaDTO.setCodigoTerritorio("3736");
		oficinaDTO.setNombreTerritorio("TERR.LIMA CENTRO");
		oficinaDTO.setCodigoOficina(codigoOficina);
		oficinaDTO.setNombreOficina("LIMA");

		logger.info(" FIN  {}","buscarOficinaClienteMOCK  ");
		return oficinaDTO;

	}

}
