package pe.bbvacontinental.fu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.bbvacontinental.fu.dao.SolicitudSubProductDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSolicitdSubProduct;
import pe.bbvacontinental.fu.service.SolicitudSubProductService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
@Transactional
public class SolicitudSubProductServiceImpl  extends AbstractService<TbSolicitdSubProduct> implements SolicitudSubProductService {

	@Autowired
	private SolicitudSubProductDAO solicitudSubProductDAO;
	
	@Override
	protected IOperations<TbSolicitdSubProduct> getDao() {
		return solicitudSubProductDAO;
	}

	@Override
	public List<TbSolicitdSubProduct> listaSubProductosPorIdSolicitud(
			Long idSolicitud) {
		return solicitudSubProductDAO.listaSubProductosPorIdSolicitud(idSolicitud);
	}

}
