package pe.bbvacontinental.fu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.dao.ProductoDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbProducto;
import pe.bbvacontinental.fu.service.ProductoService;
import pe.bbvacontinental.fu.service.common.AbstractService;


@Service
public class ProductoServiceImpl extends AbstractService<TbProducto> implements ProductoService {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ProductoDAO productoDAO;
	
	@Override
	protected IOperations<TbProducto> getDao() {
		return productoDAO;
	}
	
	public List<TbProducto> listProducto(){
		return productoDAO.findAll();
	}

	public TbProducto getProducto(String codigo){
		return  productoDAO.getProducto(codigo); 
	}

	

}
