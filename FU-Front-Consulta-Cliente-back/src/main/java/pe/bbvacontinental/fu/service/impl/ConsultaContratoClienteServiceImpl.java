package pe.bbvacontinental.fu.service.impl;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.service.ConsultaContratoClienteService;
import pe.com.bbva.hi.fileunico.cabecera.RequestHeader;
import pe.com.bbva.hi.fileunico.cconcli.IntegrationRequest;
import pe.com.bbva.hi.fileunico.cconcli.IntegrationResponse;
import pe.com.bbva.hi.fileunico.cconcli.MSFILEUNICOSOAPHTTPService;

@Service
public class ConsultaContratoClienteServiceImpl implements ConsultaContratoClienteService {
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	ParametroAplicativoDAO parametroAplicativoDAO;
	
	@Override
	public String consultarContratoCliente(String numeroContrato) throws MalformedURLException {
		
		TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_CONTRATO_CLIENTE.getCodigo());
		String ruta = "";
		String nameSpace = "";
		String localPart = "";
		
		if (tbParametrosAplicativo != null) {
			ruta = tbParametrosAplicativo.getValor1();
			nameSpace = tbParametrosAplicativo.getValor2();
			localPart = tbParametrosAplicativo.getValor3();
		}
		
		URL url = new URL(ruta); 
		QName qName = new QName(nameSpace, localPart);
		MSFILEUNICOSOAPHTTPService servicioConsultaContratoCliente = new MSFILEUNICOSOAPHTTPService(url, qName);
		logger.info("consultarContratoCliente servicioConsultaContratoCliente: {}", servicioConsultaContratoCliente);
		
		IntegrationRequest integrationRequest = new IntegrationRequest();
		RequestHeader requestHeader = new RequestHeader();
		requestHeader.setUsuario("UCQGSPPT");
		requestHeader.setIdPeticionBanco("P0018");
		requestHeader.setIdOperacion("CCONCLI");
		requestHeader.setIdServicio("SFILEUNICO");
		requestHeader.setIdInterconexion("FILEUNICO");
		integrationRequest.setRequestHeader(requestHeader);
		integrationRequest.setContrato(numeroContrato);
		
		IntegrationResponse integrationResponse = servicioConsultaContratoCliente.getMSFILEUNICOSOAPHTTPPort().consultaContratoCliente(integrationRequest);
		logger.info("consultarContratoCliente integrationResponse: {}", integrationResponse);
		if (integrationResponse.getResponseHeader().getCodigoError().equals("0000000"))
			return integrationResponse.getResponseHeader().getCodigoError();
		else
			return integrationResponse.getResponseHeader().getMensajeError();
	}
}
