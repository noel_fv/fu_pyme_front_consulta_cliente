package pe.bbvacontinental.fu.service.impl;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.dao.SolicitudDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.TbSolicitud;
import pe.bbvacontinental.fu.service.SolicitudService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
@Transactional
public class SolicitudServiceImpl extends AbstractService<TbSolicitud> implements SolicitudService {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private SolicitudDAO solicitudDAO;
	
	@Autowired
	private ParametroAplicativoDAO parametroAplicativoDAO;

	@Override
	protected IOperations<TbSolicitud> getDao() {
		return solicitudDAO;
	}

	@Override
	public boolean esFlujoPjPnn(String tipoPersona, String ventasAnualesCliente) {

		log.info("INI {}","SolicitudServiceImpl.esFlujoPJ_PNN ");
		boolean respuesta = false;

		try {
			log.info(" esFlujoPJ_PNN  tipoPersona: {}", tipoPersona);
			log.info(" esFlujoPJ_PNN  ventasAnualesCliente: {}", ventasAnualesCliente);

			if ("PNN".equals(tipoPersona) || "PJ".equals(tipoPersona)) {

				BigDecimal totalVentas = new BigDecimal("0");
				if (ventasAnualesCliente != null && !ventasAnualesCliente.trim().isEmpty()) {
					totalVentas = new BigDecimal(ventasAnualesCliente);
				}
				
				BigDecimal limiteVentas = new BigDecimal(0);
				
				TbParametrosAplicativo parametroAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.VAL_VENTAS_ANUALES.getCodigo());
				if(parametroAplicativo != null && !"".equals(parametroAplicativo.getValor1())){
					limiteVentas = new BigDecimal(parametroAplicativo.getValor1());
				}

				log.info(" limiteVentas: {}", limiteVentas);
				log.info(" totalVentas.longValue(): {}", totalVentas.longValue());
				log.info(" totalVentas.compareTo(limiteVentas): {}", totalVentas.compareTo(limiteVentas));

				if (BigDecimal.ZERO.compareTo(limiteVentas) < 0 && totalVentas.compareTo(limiteVentas) <= 0 && tipoPersona.equals("PJ")) { 
					respuesta = true;
				}

				if (tipoPersona.equals("PNN")) {
					respuesta = true;
				}
			}
		} catch (Exception e) {
			log.error("FU Error SolicitudServiceImpl.esFlujoPJ_PNN: ", e);
		}

		log.info("FIN {}","SolicitudServiceImpl.esFlujoPJ_PNN ");
		return respuesta;
	}

}