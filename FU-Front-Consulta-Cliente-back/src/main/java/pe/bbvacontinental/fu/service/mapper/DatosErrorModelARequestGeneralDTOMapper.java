package pe.bbvacontinental.fu.service.mapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import pe.bbvacontinental.fu.model.view.DatosErrorModel;
import pe.bbvacontinental.fu.wp.dto.comun.IntegrationRequestDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestBodyDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestHeaderDTO;

@Component
public class DatosErrorModelARequestGeneralDTOMapper extends CustomMapper<DatosErrorModel, RequestGeneralDTO> {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void mapAtoB(DatosErrorModel datosErrorModel,RequestGeneralDTO requestGeneralDTO, MappingContext context ){
		logger.info("mapAtoB: inicio - DatosErrorModelARequestGeneralDTOMapper");	
		
		IntegrationRequestDTO integrationRequest = new IntegrationRequestDTO();
		establecerRequestHeader(datosErrorModel, integrationRequest);
		establecerRequestBody(datosErrorModel, integrationRequest);
		requestGeneralDTO.setIntegrationRequest(integrationRequest);
		
		logger.info("mapAtoB: fin - DatosErrorModelARequestGeneralDTOMapper");
	}
	
	private void establecerRequestHeader(DatosErrorModel datosErrorModel, IntegrationRequestDTO integrationRequest){
		
		RequestHeaderDTO requestHeader = new RequestHeaderDTO();
		
		if(StringUtils.isNotBlank(datosErrorModel.getIdUsuario())){
			requestHeader.setUsuario(datosErrorModel.getIdUsuario());
		}
		
		if(StringUtils.isNotBlank(datosErrorModel.getFechaHoraEnvio())){
			requestHeader.setFechaHoraEnvio(datosErrorModel.getFechaHoraEnvio());
		}
		
		if(StringUtils.isNotBlank(datosErrorModel.getIdSesion())){
			requestHeader.setIdSesion(datosErrorModel.getIdSesion());
		}
		
		if(StringUtils.isNotBlank(datosErrorModel.getIdOperacion())){
			requestHeader.setIdOperacion(datosErrorModel.getIdOperacion());
		}
		
		integrationRequest.setRequestHeader(requestHeader);
		
	}
	
	private void establecerRequestBody (DatosErrorModel datosErrorModel, IntegrationRequestDTO integrationRequest){
		logger.info("establecerRequestBody -- datosErrorModel:{} integrationRequest:{}",datosErrorModel,integrationRequest);
		RequestBodyDTO requestBody = new RequestBodyDTO();
		
		if(StringUtils.isNotBlank(datosErrorModel.getNumeroSolicitudFU())){
			requestBody.setNumeroSolicitudFU(datosErrorModel.getNumeroSolicitudFU());
		}
		
		if(StringUtils.isNotBlank(datosErrorModel.getCodigoError())){
			requestBody.setCodigoError(datosErrorModel.getCodigoError());
		}
		
		if(StringUtils.isNotBlank(datosErrorModel.getTipoError())){
			requestBody.setTipoError(datosErrorModel.getTipoError());
		}
		
		if(StringUtils.isNotBlank(datosErrorModel.getDetalleError())){
			requestBody.setDetalleError(datosErrorModel.getDetalleError());
		}
	}
}
