package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.wp.model.ConsultaDatosNegocioModel;

public interface DatosNegocioService {
	
	public ConsultaDatosNegocioModel consultarDatosNegocio(ClienteModel buscarClienteModel);

}
