package pe.bbvacontinental.fu.service;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSolicitdSubProduct;

public interface SolicitudSubProductService  extends IOperations<TbSolicitdSubProduct> {

	public List<TbSolicitdSubProduct> listaSubProductosPorIdSolicitud(Long idSolicitud);
}
