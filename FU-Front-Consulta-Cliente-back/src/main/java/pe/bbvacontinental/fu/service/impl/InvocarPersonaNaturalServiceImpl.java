package pe.bbvacontinental.fu.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.common.enumeration.TipoTablaEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.service.InvocarPersonaNaturalService;
import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.restcliente.InvocarPersonaNaturalRestClient;
import pe.bbvacontinental.fu.wp.util.ParametrosRestClientEnum;

@Service
public class InvocarPersonaNaturalServiceImpl implements InvocarPersonaNaturalService{
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MapperFacade mapper;
	
	@Autowired
	ParametroAplicativoDAO parametroAplicativoDAO;
	
	@Autowired
	InvocarPersonaNaturalRestClient invocarPersonaNaturalRestClient;
	
	@Override
	public GenericModel invocarPantallaPersonaNatural(ClienteModel clienteModel, String cookie) {
		
		logger.info("INI - {}","InvocarPersonaNaturalServiceImpl.invocarPantallaPersonaNatural");
		GenericModel genericModel = new GenericModel();
		try {
			
			TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCodeAndType(ParametrosEnum.PANTALLA_PERSONA_NATURAL.getCodigo(),TipoTablaEnum.TABLA_URL_APPS.getCodigo());
			RequestGeneralDTO request = this.mapper.map(clienteModel, RequestGeneralDTO.class);
			
			logger.info(" cookie: {}",cookie);
			
			List<String> headerList = new ArrayList<>();
			headerList.add(clienteModel.getIdUsuario());
//			headerList.add("P012647");//prueba local
			headerList.add("prueba-creds");
			headerList.add(cookie);
			
			Map<Object, Object> parametros = new HashMap<>();
			parametros.put(ParametrosRestClientEnum.OBJETO_PEDIDO.getCodigo(), request);
			parametros.put(ParametrosRestClientEnum.URL.getCodigo(), tbParametrosAplicativo!=null?tbParametrosAplicativo.getValor1():"");
			parametros.put(ParametrosRestClientEnum.OBJETO_HEADER.getCodigo(), headerList);
			
			logger.info("SERV urlPersonaNatural TIME_INI: {}",(System.currentTimeMillis()/1000));
			
			ResponseGeneralDTO response = invocarPersonaNaturalRestClient.invocarPantallaPersonaNatural(parametros);
			
			logger.info("SERV urlPersonaNatural TIME_FIN: {}",(System.currentTimeMillis()/1000));
			
			genericModel = mapper.map(response, GenericModel.class);
			
		} catch (Exception e) {
			logger.error("FU Error invocarPantallaPersonaNatural",e);
			//throw e
		}
				
		logger.info("FIN - {}","InvocarPersonaNaturalServiceImpl.invocarPantallaPersonaNatural");
		return genericModel;
	}

}
