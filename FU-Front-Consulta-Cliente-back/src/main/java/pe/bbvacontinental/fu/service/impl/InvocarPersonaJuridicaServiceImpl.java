package pe.bbvacontinental.fu.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.glasnost.orika.MapperFacade;
import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.service.InvocarPersonaJuridicaService;
import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.restcliente.InvocarPersonaJuridicaRestClient;
import pe.bbvacontinental.fu.wp.util.ParametrosRestClientEnum;

@Service
public class InvocarPersonaJuridicaServiceImpl implements InvocarPersonaJuridicaService{

protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MapperFacade mapper;
	
	@Autowired
	ParametroAplicativoDAO parametroAplicativoDAO;
	
	@Autowired
	InvocarPersonaJuridicaRestClient invocarPersonaJuridicaRestClient;

	@Override
	public GenericModel invocarPantallaPersonaJuridica(ClienteModel clienteModel) {
		
		logger.info("INI - {}","InvocarPersonaNaturalServiceImpl.invocarPantallaPersonaJuridica");
		GenericModel genericModel = new GenericModel();
		try {
			TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_CONSULT_CLIENT_PJ.getCodigo());
			//falta los mappers adecuarlo
			RequestGeneralDTO request = this.mapper.map(clienteModel, RequestGeneralDTO.class);
		
			Map<Object, Object> parametros = new HashMap<>();
			parametros.put(ParametrosRestClientEnum.OBJETO_PEDIDO.getCodigo(), request);
			parametros.put(ParametrosRestClientEnum.URL.getCodigo(), tbParametrosAplicativo!=null?tbParametrosAplicativo.getValor1():"");
			
			ResponseGeneralDTO response = invocarPersonaJuridicaRestClient.invocarPantallaPersonaJuridica(parametros);			
			genericModel = mapper.map(response, GenericModel.class);			
			
		} catch (Exception e) {
			logger.error("FU Error InvocarPersonaNaturalServiceImpl.invocarPantallaPersonaJuridica: ",e);
			//throw e 
		}
		
		logger.info("FIN - {}","InvocarPersonaNaturalServiceImpl.invocarPantallaPersonaJuridica");
		return genericModel;
	}
}
