package pe.bbvacontinental.fu.service.mapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import pe.bbvacontinental.fu.common.enumeration.EstadoServiciosEnum;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseBodyDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseHeaderDTO;
import pe.bbvacontinental.fu.wp.model.ConsultaDatosNegocioModel;

@Component
public class ResponseGeneralDTOAConsultaDatosNegocioModelMapper extends CustomMapper<ResponseGeneralDTO, ConsultaDatosNegocioModel>{
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void mapAtoB(ResponseGeneralDTO responseGeneralDTO,ConsultaDatosNegocioModel consultaDatosNegocioModel, MappingContext context ){
		logger.info("mapAtoB: inicio - ResponseGeneralDTOAConsultaDatosNegocioModelMapper");
		
		estableciendoHeader(responseGeneralDTO.getIntegrationResponse().getResponseHeader(), consultaDatosNegocioModel);
		if(StringUtils.equals(responseGeneralDTO.getIntegrationResponse().getResponseHeader().getEstado().toUpperCase(), EstadoServiciosEnum.RESULTADO_OK.getDescripcion())){
			estableciendoBody(responseGeneralDTO.getIntegrationResponse().getResponseBody(), consultaDatosNegocioModel);
		}
		
		logger.info("mapAtoB: fin - ResponseGeneralDTOAConsultaDatosNegocioModelMapper");	
	}
	
	private void estableciendoHeader(ResponseHeaderDTO responseHeader, ConsultaDatosNegocioModel consultaDatosNegocioModel){
		
		consultaDatosNegocioModel.setIdSesion(responseHeader.getIdSesion());
		consultaDatosNegocioModel.setIdOperacion(responseHeader.getIdOperacion());
		consultaDatosNegocioModel.setCodigoServicio(responseHeader.getCodigoServicio());		
		consultaDatosNegocioModel.setEstadoServicio(responseHeader.getEstado());

		if(StringUtils.isNotBlank(responseHeader.getMensaje())){
			consultaDatosNegocioModel.setMensajeServicio(responseHeader.getMensaje());
		}
	}

	private void estableciendoBody(ResponseBodyDTO responseBody, ConsultaDatosNegocioModel consultaDatosNegocioModel){
		
		consultaDatosNegocioModel.setVentasAnualesMN(responseBody.getVentasAnualesMN());
		consultaDatosNegocioModel.setDeudaTotalMN(responseBody.getDeudaTotalMN());
		consultaDatosNegocioModel.setRating(responseBody.getRating());
		consultaDatosNegocioModel.setAntiguedadEmpresa(responseBody.getAntiguedadEmpresa());
		consultaDatosNegocioModel.setCodClasificacionBanco(responseBody.getCodClasificacionBanco());
		consultaDatosNegocioModel.setCodClasificacionSSFF(responseBody.getCodClasificacionSSFF());
		
	}
}
