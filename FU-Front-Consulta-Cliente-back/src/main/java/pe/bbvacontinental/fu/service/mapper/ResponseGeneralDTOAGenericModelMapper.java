package pe.bbvacontinental.fu.service.mapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import pe.bbvacontinental.fu.common.enumeration.EstadoServiciosEnum;
import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseBodyDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseHeaderDTO;


@Component
public class ResponseGeneralDTOAGenericModelMapper extends CustomMapper<ResponseGeneralDTO, GenericModel> {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void mapAtoB(ResponseGeneralDTO responseGeneralDTO,GenericModel genericModel, MappingContext context ){
		logger.info("mapAtoB: inicio - ResponseGeneralDTOAGenericModelMapper");
		
		estableciendoHeader(responseGeneralDTO.getIntegrationResponse().getResponseHeader(), genericModel);
		if(StringUtils.equals(responseGeneralDTO.getIntegrationResponse().getResponseHeader().getEstado().toUpperCase(), EstadoServiciosEnum.RESULTADO_OK.getDescripcion())){
			estableciendoBody(responseGeneralDTO.getIntegrationResponse().getResponseBody(), genericModel);
		}
		
		logger.info("mapAtoB: fin - ResponseGeneralDTOAGenericModelMapper");	
	}
	
	private void estableciendoHeader(ResponseHeaderDTO responseHeader, GenericModel genericModel){
		
		genericModel.setIdSesion(responseHeader.getIdSesion());
		genericModel.setIdOperacion(responseHeader.getIdOperacion());
		genericModel.setCodigoServicio(responseHeader.getCodigoServicio());		
		genericModel.setEstadoServicio(responseHeader.getEstado());

		if(StringUtils.isNotBlank(responseHeader.getMensaje())){
			genericModel.setMensajeServicio(responseHeader.getMensaje());
		}
	}

	private void estableciendoBody(ResponseBodyDTO responseBody, GenericModel genericModel){
				
		if(StringUtils.isNotBlank(responseBody.getCodigoSolicitudWP())){
			genericModel.setRespuesta1(responseBody.getCodigoSolicitudWP());
			
		} else if(StringUtils.isNotBlank(responseBody.getNumeroSolicitudWP())){
			genericModel.setRespuesta1(responseBody.getNumeroSolicitudWP());
		}
		
		if(StringUtils.isNotBlank(responseBody.getTieneDelegacion())){
			genericModel.setRespuesta2(responseBody.getTieneDelegacion());
		}
		
		if(StringUtils.isNotBlank(responseBody.getCodigoSolicitudFU())){
			genericModel.setRespuesta2(responseBody.getCodigoSolicitudFU());
		}	
		
		if(StringUtils.isNotBlank(responseBody.getUrlPersonaNatural())){
			genericModel.setRespuesta1(responseBody.getUrlPersonaNatural());
		}
		
		if(StringUtils.isNotBlank(responseBody.getUrlPersonaJuridca())){
			genericModel.setRespuesta1(responseBody.getUrlPersonaJuridca());
		}
	}
}
