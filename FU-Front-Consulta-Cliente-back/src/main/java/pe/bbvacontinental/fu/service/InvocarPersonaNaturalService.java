package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.GenericModel;

public interface InvocarPersonaNaturalService {
	
	public GenericModel invocarPantallaPersonaNatural(ClienteModel clienteModel, String cookie);

}
