package pe.bbvacontinental.fu.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.bbvacontinental.fu.back.util.Util;
import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.service.DatosNegocioService;
import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.model.ConsultaDatosNegocioModel;
import pe.bbvacontinental.fu.wp.restcliente.NegocioRestClient;
import pe.bbvacontinental.fu.wp.util.ParametrosRestClientEnum;

@Service
public class DatosNegocioServiceImpl implements DatosNegocioService{

	@Autowired
	private NegocioRestClient negocioRestClient;
	
	@Autowired
	ParametroAplicativoDAO parametroAplicativoDAO;
	
	@Autowired
	private MapperFacade mapper;
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	@Transactional
	public ConsultaDatosNegocioModel consultarDatosNegocio(ClienteModel buscarClienteModel) {
		
		logger.info("INI {}","DatosNegocioServiceImpl.consultarDatosNegocio ");
		ConsultaDatosNegocioModel consultaDatosNegocioModel = new ConsultaDatosNegocioModel();		
		try{
			TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_DATOS_NEGOCIO_WP.getCodigo());
			
			buscarClienteModel.setFechaHoraEnvio(Util.formatDate(new Date(), "yy-MM-dd'T'HH:mm:ss.SSS"));
			
			RequestGeneralDTO request = this.mapper.map(buscarClienteModel, RequestGeneralDTO.class);

			Map<Object, Object> parametros = new HashMap<>();
			parametros.put(ParametrosRestClientEnum.OBJETO_PEDIDO.getCodigo(), request);
			parametros.put(ParametrosRestClientEnum.URL.getCodigo(), tbParametrosAplicativo!=null?tbParametrosAplicativo.getValor1():"");
			
			parametros.put("CookieSSO", buscarClienteModel.getCookieSSO());
			
			logger.info(" consultarDatosNegocio: parametros: {}",parametros);

			
			ResponseGeneralDTO response = negocioRestClient.consultarDatosNegocio(parametros);

			consultaDatosNegocioModel = mapper.map(response, ConsultaDatosNegocioModel.class);
	
			logger.info(" response: {}",response);
			
		}catch(Exception e){
			logger.error("FU Error DatosNegocioServiceImpl.consultarDatosNegocio: ",e);
			//throw e
		}
		
		logger.info("FIN {}","DatosNegocioServiceImpl.consultarDatosNegocio ");
		return consultaDatosNegocioModel;
	}

}
