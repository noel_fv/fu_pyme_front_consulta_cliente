package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.GenericModel;

public interface InvocarPersonaJuridicaService {
	
	public GenericModel invocarPantallaPersonaJuridica(ClienteModel clienteModel);

}
