package pe.bbvacontinental.fu.service.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import pe.bbvacontinental.fu.dto.DatosClienteDTO;
import pe.bbvacontinental.fu.model.view.ClienteModel;

@Component
public class BuscarClienteMapper extends CustomMapper<ClienteModel, DatosClienteDTO> {
	private static final Logger log = LoggerFactory.getLogger(BuscarClienteMapper.class);
	
	@Override
	public void mapAtoB(ClienteModel buscarClienteModel, DatosClienteDTO datosClienteDTO, MappingContext context) {
		log.debug("inicio BuscarClienteModelADTO:mapAtoB");
		
		datosClienteDTO.setCodigoCentral(buscarClienteModel.getCodigoCentral());
		datosClienteDTO.setTipoDocumentoIdentidad(buscarClienteModel.getTipoDocumentoIdentidad());
		datosClienteDTO.setNumeroDocumento(buscarClienteModel.getNumeroDocumento());
		
		log.debug("fin BuscarClienteModelADTO:mapAtoB");
	}
	
	@Override
	public void mapBtoA(DatosClienteDTO datosClienteDTO, ClienteModel buscarClienteModel, MappingContext context){
		log.debug("inicio BuscarClienteModelADTO:mapBtoA");
		
		buscarClienteModel.setTipoPersona(datosClienteDTO.getTipoPersona());
		buscarClienteModel.setNombreCompleto(datosClienteDTO.getNombreCompleto());
		buscarClienteModel.setTerritorio(datosClienteDTO.getCodigoTerritorio());
		buscarClienteModel.setOficina(datosClienteDTO.getCodigoOficina());
		buscarClienteModel.setNombreTerritorio(datosClienteDTO.getNombreTerritorio());
		buscarClienteModel.setNombreOficina(datosClienteDTO.getNombreOficina());
		buscarClienteModel.setCodigoSegmento(datosClienteDTO.getCodigoSegmento());
		buscarClienteModel.setDescripcionSegmento(datosClienteDTO.getDescripcionSegmento());
		buscarClienteModel.setCodigoCentralSesion(datosClienteDTO.getCodigoCentral());
		buscarClienteModel.setTipoDocumentoIdentidadSesion(datosClienteDTO.getTipoDocumentoIdentidad());
		buscarClienteModel.setNumeroDocumentoSesion(datosClienteDTO.getNumeroDocumento());
		
		log.debug("fin BuscarClienteModelADTO:mapBtoA");
	}
}
