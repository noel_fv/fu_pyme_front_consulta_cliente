package pe.bbvacontinental.fu.service.mapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.wp.dto.comun.IntegrationRequestDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestBodyDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.RequestHeaderDTO;

@Component
public class BuscarClienteModelARequestGeneralDTOMapper extends CustomMapper<ClienteModel, RequestGeneralDTO> {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void mapAtoB(ClienteModel buscarClienteModel,RequestGeneralDTO requestGeneralDTO, MappingContext context ){
		logger.info("mapAtoB: inicio - {}","BuscarClienteMOdelAConsultaDatosNegocioDTOMapper");	
		
		IntegrationRequestDTO integrationRequest = new IntegrationRequestDTO();
		establecerRequestHeader(buscarClienteModel, integrationRequest);
		establecerRequestBody(buscarClienteModel, integrationRequest);
		requestGeneralDTO.setIntegrationRequest(integrationRequest);
		
		logger.info("mapAtoB: fin - {}","BuscarClienteMOdelAConsultaDatosNegocioDTOMapper");
	}
	
	private void establecerRequestHeader(ClienteModel buscarClienteModel, IntegrationRequestDTO integrationRequest){
		
		RequestHeaderDTO requestHeader = new RequestHeaderDTO();
		
		if(StringUtils.isNotBlank(buscarClienteModel.getIdUsuario())){
			requestHeader.setUsuario(buscarClienteModel.getIdUsuario());
		}
		
		if(StringUtils.isNotBlank(buscarClienteModel.getFechaHoraEnvio())){
			requestHeader.setFechaHoraEnvio(buscarClienteModel.getFechaHoraEnvio());
		}
		
		if(StringUtils.isNotBlank(buscarClienteModel.getIdSesion())){
			requestHeader.setIdSesion(buscarClienteModel.getIdSesion());
		}
		
		if(StringUtils.isNotBlank(buscarClienteModel.getIdOperacion())){
			requestHeader.setIdOperacion(buscarClienteModel.getIdOperacion());
		}
		
		integrationRequest.setRequestHeader(requestHeader);
		
	}
	
	private void establecerRequestBody (ClienteModel buscarClienteModel, IntegrationRequestDTO integrationRequest){
		RequestBodyDTO requestBody = new RequestBodyDTO();
		
		if(StringUtils.isNotBlank(buscarClienteModel.getCodigoCentral())){
			requestBody.setCodigoCentral(buscarClienteModel.getCodigoCentral());
		}
		
		if(StringUtils.isNotBlank(buscarClienteModel.getTipoDocumentoIdentidad()) && StringUtils.isNotBlank(buscarClienteModel.getNumeroDocumento())){
			requestBody.setTipoDocumentoIdentidad(buscarClienteModel.getTipoDocumentoIdentidad());
			requestBody.setNumeroDocumentoIdentidad(buscarClienteModel.getNumeroDocumento());
			requestBody.setNumeroDocumento(buscarClienteModel.getNumeroDocumento());
		}
		
		if(StringUtils.isNotBlank(buscarClienteModel.getTipoPersona())) {	
			requestBody.setTipoPersona(buscarClienteModel.getTipoPersonaFU());
		}

		integrationRequest.setRequestBody(requestBody);
	}

}
