package pe.bbvacontinental.fu.service;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoDoi;

public interface TipoDocumentoService extends IOperations<TbTipoDoi> {
	
	public List<TbTipoDoi> listarTipoDocumento();
	
	public TbTipoDoi buscarPorValor(String valor);

}
