package pe.bbvacontinental.fu.service.impl;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.service.CacheService;

@Service
public class CacheServiceImpl implements CacheService{
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ParametroAplicativoDAO parametroAplicativoDAO;

	@PostConstruct
	public void postConstruct() {
		try {
			parametroAplicativoDAO.actualizarCacheLocal();
		} catch (Exception e) {
			logger.error("ERROR AL REALIZAR CACHE CARGADO DESPUES DE SPRING");
		}
	}
	
}

