package pe.bbvacontinental.fu.service;

import java.net.MalformedURLException;
import java.util.List;

import pe.bbvacontinental.fu.dto.OficinaDTO;
import pe.bbvacontinental.fu.dto.TerritorioDTO;

public interface TerritorioService {
	
	public List<TerritorioDTO> listarTerritorio(String codigoArea)throws MalformedURLException;
	
	public List<OficinaDTO> listarOficinaPorTerritorio(String codTerritorio)throws MalformedURLException;

}
