package pe.bbvacontinental.fu.service;

import java.net.MalformedURLException;

public interface ConsultaContratoClienteService {
	public String consultarContratoCliente(String numeroContrato) throws MalformedURLException;
}
