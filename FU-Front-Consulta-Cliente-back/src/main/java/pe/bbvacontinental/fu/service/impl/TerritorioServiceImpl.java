package pe.bbvacontinental.fu.service.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.common.enumeration.ParametrosEnum;
import pe.bbvacontinental.fu.dao.ParametroAplicativoDAO;
import pe.bbvacontinental.fu.dto.OficinaDTO;
import pe.bbvacontinental.fu.dto.TerritorioDTO;
import pe.bbvacontinental.fu.model.TbParametrosAplicativo;
import pe.bbvacontinental.fu.service.TerritorioService;
import pe.com.bbva.centrosbbvawebservice.CentrosBBVAWebServiceService;
import pe.com.bbva.centrosbbvawebservice.ListarOficinaTerritorioRequest;
import pe.com.bbva.centrosbbvawebservice.ListarOficinaTerritorioResponse;
import pe.com.bbva.centrosbbvawebservice.ListarTerritorioRequest;
import pe.com.bbva.centrosbbvawebservice.ListarTerritorioResponse;
import pe.com.bbva.centrosbbvawebservice.Oficina;
import pe.com.bbva.centrosbbvawebservice.OficinaTerritorio;
import pe.com.bbva.centrosbbvawebservice.Territorio;


@Service
public class TerritorioServiceImpl implements TerritorioService{

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	ParametroAplicativoDAO parametroAplicativoDAO;
	
	@Override
	public List<TerritorioDTO> listarTerritorio(String codigoArea) throws MalformedURLException {

		List<TerritorioDTO> listaTerritorio = new ArrayList<>();
		
		logger.info("PROXY {}","SELECTTOR");
		
		TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_TERRITORIO_OFICINA.getCodigo());
		String rutaEbdPoint = "";
		String nameSpace = "";
		String localPart = "";
		
		if(tbParametrosAplicativo!=null){
			rutaEbdPoint = tbParametrosAplicativo.getValor1();
			nameSpace = tbParametrosAplicativo.getValor2();
			localPart = tbParametrosAplicativo.getValor3();			
		}

		URL url = new URL(rutaEbdPoint);
		QName qName = new QName(nameSpace, localPart);
	
		CentrosBBVAWebServiceService servicioTerritorioOficina = new CentrosBBVAWebServiceService(url, qName);
		

		ListarTerritorioRequest listarTerritorioRequest = new ListarTerritorioRequest();

		listarTerritorioRequest.setCodArea(codigoArea);
		
		logger.info(" listarTerritorioRequest: {}",listarTerritorioRequest);
		
		ListarTerritorioResponse listarTerritorioResponse = servicioTerritorioOficina.getCentrosBBVAWebServiceSOAP().listarTerritorio(listarTerritorioRequest);
		
		logger.info(" listarTerritorioResponse: {}",listarTerritorioResponse);

		
		for(Territorio territorio:listarTerritorioResponse.getListaTerritorio()){
			
			TerritorioDTO territorioDTO = new TerritorioDTO();
			
			territorioDTO.setId(territorio.getId());
			territorioDTO.setNombreTerritorio(territorio.getNombreTerritorio());

			
			List<OficinaDTO> listaOficinas= new ArrayList<>();
			
			for(OficinaTerritorio oficinaTerritorio:territorio.getOficinas()){
				
				logger.info(" getIdTerritorio: {}",oficinaTerritorio.getIdTerritorio());
				logger.info(" getId: {}",oficinaTerritorio.getId());
				logger.info(" getNombreOficina: {}",oficinaTerritorio.getNombreOficina());
				
				OficinaDTO oficinaDTO = new OficinaDTO();
				oficinaDTO.setCodigoOficina(oficinaTerritorio.getId());
				oficinaDTO.setNombreOficina(oficinaTerritorio.getNombreOficina());
				
				listaOficinas.add(oficinaDTO);
			}
			
			territorioDTO.setListaOficinas(listaOficinas);
			
			listaTerritorio.add(territorioDTO);
		}
        
		
		return listaTerritorio;
		
	}

	@Override
	public List<OficinaDTO> listarOficinaPorTerritorio(String codTerritorio)
			throws MalformedURLException {

		 
		List<OficinaDTO> listaOficinas= new ArrayList<>();
		
		TbParametrosAplicativo tbParametrosAplicativo = parametroAplicativoDAO.getParameterByCode(ParametrosEnum.SERVICIO_TERRITORIO_OFICINA.getCodigo());
		String ruta = "";
		String nameSpace = "";
		String localPart = "";
		
		if(tbParametrosAplicativo!=null){
			ruta = tbParametrosAplicativo.getValor1();
			nameSpace = tbParametrosAplicativo.getValor2();
			localPart = tbParametrosAplicativo.getValor3();			
		}
		
		URL url = new URL(ruta);
		QName qName = new QName(nameSpace, localPart);
	
		CentrosBBVAWebServiceService servicioTerritorioOficina = new CentrosBBVAWebServiceService(url, qName);
		
		ListarOficinaTerritorioRequest request = new ListarOficinaTerritorioRequest();
		request.setCodTerritorio(codTerritorio);
		
		ListarOficinaTerritorioResponse response = servicioTerritorioOficina.getCentrosBBVAWebServiceSOAP().listarOficinaTerritorio(request);

		for(Oficina oficina:response.getListaOficina()){
			
			OficinaDTO oficinaDTO = new OficinaDTO();
			oficinaDTO.setCodigoOficina(oficina.getId());
			oficinaDTO.setNombreOficina(oficina.getNombreOficina());
			
			listaOficinas.add(oficinaDTO);
		}
		
		return listaOficinas;
	}
}
