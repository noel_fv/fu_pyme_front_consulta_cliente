package pe.bbvacontinental.fu.service;

import java.net.MalformedURLException;

import pe.bbvacontinental.fu.dto.OficinaDTO;
import pe.bbvacontinental.fu.model.view.ClienteModel;
import pe.bbvacontinental.fu.model.view.UsuarioPortal;

public interface BuscadorClienteService {

	public ClienteModel buscarDatosCliente(ClienteModel buscarClienteModel,UsuarioPortal usuarioPortal) throws MalformedURLException;
	public OficinaDTO buscarOficinaCliente(String codigoOficina) throws MalformedURLException;

}
