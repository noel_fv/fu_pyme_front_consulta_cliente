package pe.bbvacontinental.fu.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import pe.bbvacontinental.fu.dao.TipoDoiDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoDoi;
import pe.bbvacontinental.fu.service.TipoDocumentoService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
public class TipoDocumentoServiceImpl extends AbstractService<TbTipoDoi> implements TipoDocumentoService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	TipoDoiDAO tipoDocumentoDAO;

	@Override
	protected IOperations<TbTipoDoi> getDao() {
		return tipoDocumentoDAO;
	}

	@Override
	public List<TbTipoDoi> listarTipoDocumento() {
		return tipoDocumentoDAO.listaTipoDocumentos();
	}

	@Override
	public TbTipoDoi buscarPorValor(String valor) {
		return tipoDocumentoDAO.buscarPorValor(valor);
	}
}
