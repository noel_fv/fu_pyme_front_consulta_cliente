package pe.bbvacontinental.fu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.dao.SubProductoDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSubProducto;
import pe.bbvacontinental.fu.service.SubProductoService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
public class SubProductoServiceImpl extends AbstractService<TbSubProducto>  implements SubProductoService {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	
	@Autowired
	SubProductoDAO subProductoDAO;
	
	@Override
	protected IOperations<TbSubProducto> getDao() {
		return subProductoDAO;
	}
	
	public List<TbSubProducto> listaSubProducto(Long idProducto){
		
		return subProductoDAO.findSubProductoById(idProducto);
		
	}


	public TbSubProducto buscarPorValor(String valor){
		
		return subProductoDAO.buscarPorValor(valor);
		
	}

	@Override
	public List<TbSubProducto> findByCodigoProductoWPFlujoRegular(String codProductoWP) {

		return subProductoDAO.findByCodigoProductoWPFlujoRegular(codProductoWP);
	}

	@Override
	public List<TbSubProducto> findByCodigoProductoWPEstado(String codProductoWP) {
		
		return subProductoDAO.findByCodigoProductoWPEstado(codProductoWP);
	}

	@Override
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampCalc(
			String codProductoWP) {
		return subProductoDAO.findByCodigoProductoWPFlujoCampCalc(codProductoWP);
	}

	@Override
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampAprob(
			String codProductoWP) {
		return subProductoDAO.findByCodigoProductoWPFlujoCampAprob(codProductoWP);
	}

	@Override
	public TbSubProducto findByValorWP(String valorWP) {
		return subProductoDAO.findByValorWP(valorWP);
	}
	

}
