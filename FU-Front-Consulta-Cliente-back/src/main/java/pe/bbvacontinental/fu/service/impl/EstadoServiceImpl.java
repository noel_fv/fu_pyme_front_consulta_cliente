package pe.bbvacontinental.fu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bbvacontinental.fu.dao.EstadoDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbEstado;
import pe.bbvacontinental.fu.service.EstadoService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
public class EstadoServiceImpl extends AbstractService<TbEstado> implements EstadoService {

	@Autowired
	private EstadoDAO estadoDAO;
	
	@Override
	protected IOperations<TbEstado> getDao() {
		return estadoDAO;
	}
	
	public TbEstado buscarPorValor(String valor) {
		return estadoDAO.buscarPorValor(valor);
	}
}
