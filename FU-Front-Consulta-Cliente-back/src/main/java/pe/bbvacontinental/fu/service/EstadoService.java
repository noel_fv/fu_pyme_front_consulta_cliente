package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbEstado;

public interface EstadoService extends IOperations<TbEstado> {
	public TbEstado buscarPorValor(String valor);
}
