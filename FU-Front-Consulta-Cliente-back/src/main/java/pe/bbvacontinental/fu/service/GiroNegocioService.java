package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbGiroNegocio;

public interface GiroNegocioService extends IOperations<TbGiroNegocio> {

	public TbGiroNegocio buscarPorCodigo(String codigoGiroNegocio);

}
