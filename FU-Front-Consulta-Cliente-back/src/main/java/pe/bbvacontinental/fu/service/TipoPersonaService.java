package pe.bbvacontinental.fu.service;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoPersona;

public interface TipoPersonaService extends IOperations<TbTipoPersona> {

	public List<TbTipoPersona> listaTipoPersona();
	
	public TbTipoPersona findByCodigo(String codigo);

}
