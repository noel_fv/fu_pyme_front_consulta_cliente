package pe.bbvacontinental.fu.service;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbSubProducto;

public interface SubProductoService extends IOperations<TbSubProducto> {
	
	public List<TbSubProducto> listaSubProducto(Long idProducto);
	public TbSubProducto buscarPorValor(String valor);
	public List<TbSubProducto> findByCodigoProductoWPFlujoRegular(String codProductoWP);
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampCalc(String codProductoWP);
	public List<TbSubProducto> findByCodigoProductoWPFlujoCampAprob(String codProductoWP);
	public List<TbSubProducto> findByCodigoProductoWPEstado(String codProductoWP);	
	
	public TbSubProducto findByValorWP(String valorWP);
	

}
