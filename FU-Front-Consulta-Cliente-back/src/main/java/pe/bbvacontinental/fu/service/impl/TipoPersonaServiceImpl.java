package pe.bbvacontinental.fu.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import pe.bbvacontinental.fu.dao.TipoPersonaDAO;
import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbTipoPersona;
import pe.bbvacontinental.fu.service.TipoPersonaService;
import pe.bbvacontinental.fu.service.common.AbstractService;

@Service
public class TipoPersonaServiceImpl extends AbstractService<TbTipoPersona> implements TipoPersonaService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	TipoPersonaDAO tipoPersonaDAO;

	@Override
	public List<TbTipoPersona> listaTipoPersona() {
		return tipoPersonaDAO.findAll();
	}

	@Override
	protected IOperations<TbTipoPersona> getDao() {
		return tipoPersonaDAO;
	}

	@Override
	public TbTipoPersona findByCodigo(String codigo) {
		return tipoPersonaDAO.findByCodigo(codigo);
	}
}
