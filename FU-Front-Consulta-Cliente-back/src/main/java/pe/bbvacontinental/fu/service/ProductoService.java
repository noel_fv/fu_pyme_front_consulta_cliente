package pe.bbvacontinental.fu.service;

import java.util.List;

import pe.bbvacontinental.fu.dao.common.IOperations;
import pe.bbvacontinental.fu.model.TbProducto;

public interface ProductoService extends IOperations<TbProducto>{
	
	public List<TbProducto> listProducto();
	
	public TbProducto getProducto(String codigo);
	
}
