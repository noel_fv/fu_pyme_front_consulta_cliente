package pe.bbvacontinental.fu.service;

import pe.bbvacontinental.fu.model.view.GenericModel;
import pe.bbvacontinental.fu.model.view.RegistroSolicitudModel;

public interface RegistroSolicitudService {
	
	public GenericModel registrarSolicitudWP(RegistroSolicitudModel registroSolicitud);
	
	public void convertirMontosSubProductosASoles(RegistroSolicitudModel registroSolicitud);
	
	public RegistroSolicitudModel obtenerSolicitud(String codigoSolicitud);

}
