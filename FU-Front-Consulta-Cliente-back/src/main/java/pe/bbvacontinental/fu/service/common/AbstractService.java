package pe.bbvacontinental.fu.service.common;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import pe.bbvacontinental.fu.dao.common.IOperations;

@Transactional(value="transactionManager")
public abstract class AbstractService<T extends Serializable> implements IOperations<T> {

	
    @Override
    public T findOne(final long id) {
        return getDao().findOne(id);
    }

    @Override
    public List<T> findAll() {
        return getDao().findAll();
    }
    
    @Override
    public List<T> findAllActive() {
    	
        return getDao().findAllActive();
    }
    
    @Override
    public List<T> findAllActivePNatural() {
    	
        return getDao().findAllActivePNatural();
    }

    @Override
    public void create(final T entity) {
        getDao().create(entity);
    }

    @Override
    public T update(final T entity) {
        return getDao().update(entity);
    }

    @Override
    public void delete(final T entity) {
        getDao().delete(entity);
    }

    @Override
    public void deleteById(final long entityId) {
        getDao().deleteById(entityId);
    }
    
    @Override
    public void insertAll(List<T> list) {
        getDao().insertAll(list);
    }

    protected abstract IOperations<T> getDao();

}
