package pe.bbvacontinental.fu.back.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

public class Util {
	
	private Util(){
		super();
	}
	public static String formatoFecha(String formato, Date fecha) {
		return new SimpleDateFormat(formato).format(fecha);
	}
	
	public static String formatStringDate(String fecha,String formatoInicial,String formatoFinal) {

		String fechaFormateada="";
		try {
			
			 Date fechaInicial=null;
			 //Formato inicial por defectos
			 SimpleDateFormat formatoFechaInicial =new SimpleDateFormat("dd/MM/yyyy");
			 if(formatoInicial!=null){
				 formatoFechaInicial = new SimpleDateFormat(formatoInicial);
			 }

			 fechaInicial = formatoFechaInicial.parse(fecha);
			 
			 //Formato final por fecto
			 SimpleDateFormat formatoSalida=new SimpleDateFormat("yyyy-MM-dd'T'00:00:00");
			 if(formatoFinal!=null){
				  formatoSalida = new SimpleDateFormat(formatoFinal);
			 }
			 
			 fechaFormateada = formatoSalida.format(fechaInicial);
		     
		} catch (ParseException e) {
			fechaFormateada="";
		}

		return fechaFormateada;
	}
	
	public static String formatDate(Date fechaInicial,String formatoFinal) {

		String fechaFormateada="";
		//Formato por defecto
		SimpleDateFormat formatoSalida = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00");
		if(formatoFinal!=null){
				 formatoSalida = new SimpleDateFormat(formatoFinal);
		}
		fechaFormateada = formatoSalida.format(fechaInicial);

		return fechaFormateada;
	}
	
	public static boolean isEmpty(String dato){
		
		return StringUtils.isEmpty(dato);
	}
	
	/* int numberMonth */
	public static Date addMonth(){
		Calendar cal = Calendar.getInstance();
	    cal.setTime(new Date());
	    cal.add(Calendar.MONTH, 1);
	    return cal.getTime();
	}
	
	public static Date getDateNow(){
		
		return new Date();
	}
	
	public static String getDateUTCFormat(Date fecha,String formatoSalida){
		
		//Formato inicial por defectos
		TimeZone utc = TimeZone.getTimeZone("UTC");
		SimpleDateFormat formatoFechaInicial =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		if(formatoSalida!=null){
			formatoFechaInicial = new SimpleDateFormat(formatoSalida);
		}
		formatoFechaInicial.setTimeZone(utc);

		return formatoFechaInicial.format(fecha);
	}
	public static String getDateUTCFormat(String fecha,String formatoInicial,String formatoSalida){
		
		String fechaResultante="";
		try {
			
			if(fecha!=null && !fecha.isEmpty()){

				Date fechaInicial=null;
				//Formato inicial por defectos
				SimpleDateFormat formatoFechaInicial =new SimpleDateFormat("dd/MM/yyyy");
				if(formatoInicial!=null){
					formatoFechaInicial = new SimpleDateFormat(formatoInicial);
				}
				fechaInicial=formatoFechaInicial.parse(fecha);
				fechaResultante =getDateUTCFormat(fechaInicial,formatoSalida);

			}
			
		} catch (ParseException e) {
			fechaResultante="";
		}
		return fechaResultante;
	}
	
	
	/**
	 * Metodo que retorna el codigo de transaccion que se utilizara en las invocaciones a servicios web SOAP o REST
	 * Su contenido es el numero de milisegundos desde 1970 hasta el ahora.
	 * @return
	 */
	public static String getIdTransaccion(){
		
		Date fecha= new Date();
		
		return String.valueOf(fecha.getTime());
	}
}
