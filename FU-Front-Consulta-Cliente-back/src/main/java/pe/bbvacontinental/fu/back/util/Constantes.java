package pe.bbvacontinental.fu.back.util;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

public class Constantes {
	
	private Constantes(){
		super();
	}
	
	public static final String YYYY_MM_DD = "yyyyMMdd";
	
	public static final Map<String, String> TIPO_INTERVINIENTE = ImmutableMap.<String, String>builder()
		.put("TIT","Titular")
		.put("CNY","Cónyuge")
		.put("AVA","Avalista")
		.put("PAR","Partícipe")
		.put("CAV","Cónyuge del Aval")
		.put("REP","Representante Legal")
		.put("DIR","Director")
		.put("ACC","Accionista")
		.put("EGR","Empresa del grupo")
		.put("EAV","Empresa Aval")
		.put("RLA","Representante Legal Aval")
		.put("OTR","Otros")
    .build();
	
	public static final Map<String, String> TIPO_DOCUMENTO_CHECKLIST = ImmutableMap.<String, String>builder()
			.put("INC", "Incompleto")
			.put("VEN", "Vencido")
			.put("FAL", "Falta Firma")
			.put("ILE", "Ilegible")
			.put("NCO", "No corresponde")
			.put("OTR", "Otros")
	    .build();
	
	public enum CalculoVigencia{
		FBVS("FBVS"),
		FBVM("FBVM"),
		FFVS("FFVS"),
		FFVM("FFVM");
		
		private String valor;

		CalculoVigencia(String valor) {
			this.valor=valor;
		}

		public String getValor() {
			return valor;
		}

	
	}
	
	public static final String VALOR_FILTRO_GENERALES = "COMUN_MULTIPRODUCTO";
	public static final String AGRUPACION = "AGRUPACION";
	public static final String VALOR_FILTRO_INTERVINIENTES = AGRUPACION;
	public static final String VALOR_FILTRO_OTROS = AGRUPACION;
	public static final String VALOR_FILTRO_SUBPRODUCTOS = AGRUPACION;
	public static final String VALOR_FILTRO_GARANTIAS = AGRUPACION;
	
	protected static final String[] FILTRO_DOC_GENERALES = { "NO" };
	protected static final String[] FILTRO_DOC_INTERVINIENTES = { "SCL", "SGI" };
	protected static final String[] FILTRO_DOC_OTROS = { "OTR" };
	protected static final String[] FILTRO_DOC_SUBPRODUCTOS = { "CON", "NOR", "SOP", "SBS", "AFI" };
	protected static final String[] FILTRO_DOC_GARANTIAS = { "SGA" };
}
