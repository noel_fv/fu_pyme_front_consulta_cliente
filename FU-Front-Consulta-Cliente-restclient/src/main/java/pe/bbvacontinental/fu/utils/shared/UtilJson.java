package pe.bbvacontinental.fu.utils.shared;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.bbvacontinental.fu.common.excepcion.ExcepcionBBVA;

public class UtilJson {
	
	private UtilJson(){}
	
	public static Object convertirCadenaADTO(String json, Class<?> claseDTO) {
		ObjectMapper jackson = new ObjectMapper();
		Object objetoDTO = null;
		try {
			objetoDTO = jackson.readValue(json, claseDTO);
		}
		catch (Exception e) {
			throw new ExcepcionBBVA("mensaje.error.conversion");
		}
		return objetoDTO;
	}

	public static String convertirObjectoACadena(Object objeto) {
		ObjectMapper jackson = new ObjectMapper();
		String json = null;
		try {
			json = jackson.writeValueAsString(objeto);
		}
		catch (Exception e) {
			throw new ExcepcionBBVA("mensaje.error.conversion");
		}
		return json;
	}

}
