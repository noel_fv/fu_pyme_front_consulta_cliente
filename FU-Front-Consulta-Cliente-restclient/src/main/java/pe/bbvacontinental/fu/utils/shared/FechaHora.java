package pe.bbvacontinental.fu.utils.shared;


import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FechaHora{
	private FechaHora(){
		super();
	}
	public static String fechahoraactual(){
    try{
      Calendar c = Calendar.getInstance();
      Date fechora = c.getTime();
      c.setTime(fechora);

      int anoActual = c.get(1);
      int mesActual = c.get(2);
      int diaActual = c.get(5);
      int horaActual = c.get(11);
      int minutosActual = c.get(12);
      int segundosActual = c.get(13);
      int milisegundosActual = c.get(14);
      GregorianCalendar cal = new GregorianCalendar(anoActual, mesActual, diaActual, horaActual, minutosActual, segundosActual);
      Timestamp timeActual = new Timestamp(cal.getTimeInMillis());
      
      return timeActual.toString() + String.format("%08d",milisegundosActual);
    } catch (Exception e) {
    	/* */
    }
    return null;
  }

  public static Date fechahoraCurrent(){
    try{
      Calendar c = Calendar.getInstance();
      Date fechora = c.getTime();
      c.setTime(fechora);
      return c.getTime();
    } catch (Exception e) {
    	/* */
    }
    return null;
  }
}