package pe.bbvacontinental.fu.utils.shared;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UtilCalendario {

	private UtilCalendario(){
		super();
	}
	
	private static String[] meses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre",
	        "Diciembre" };

	public static Date obtenerFechaAnterior(Date fecha) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		calendario.set(calendario.get(Calendar.YEAR), calendario.get(Calendar.MONTH) - 1, calendario.get(Calendar.DATE));
		return calendario.getTime();
	}
	
	public static Date obtenerFechaHaceNDias(Date fecha, int dias) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		calendario.set(calendario.get(Calendar.YEAR), calendario.get(Calendar.MONTH), calendario.get(Calendar.DATE) - dias);
		return calendario.getTime();
	}


	public static Date obtenerFecha(String fechaCadena, String patronFecha) {
		SimpleDateFormat formatter = new SimpleDateFormat(patronFecha);
		Date fecha = null;
		if (fechaCadena != null) {
			try {
				fecha = formatter.parse(fechaCadena);
			}
			catch (ParseException e) {
				/* */
			}
		}
		return fecha;
	}
	
	/**
	 * Obtiene el primer dia del intervalo elegido
	 * @param fecha
	 * @param N, establece la cantidad de tiempo hacia atras. Ejm: Hace 2 semanas
	 * @param intervaloTiempo, puede ser Semana o Mes
	 * @return
	 */
	public static Date obtenerPrimerDiaHaceNIntervaloTiempo(Date fecha, int nn, int intervaloTiempo) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		if (intervaloTiempo == 0) {
			calendario.add(Calendar.WEEK_OF_YEAR, -nn);
			calendario.set(Calendar.DAY_OF_WEEK, calendario.getFirstDayOfWeek());
		}else{
			calendario.add(Calendar.MONTH, -nn);
			calendario.set(Calendar.DAY_OF_MONTH, calendario.getActualMinimum(Calendar.DAY_OF_MONTH));
		}
		return calendario.getTime();
	}
	
	/**
	 * Obtiene el ultimo dia del intervalo elegido
	 * @param fecha
	 * @param N, establece la cantidad de tiempo hacia atras. Ejm: Hace 2 semanas
	 * @param intervaloTiempo, puede ser Semana o Mes
	 * @return
	 */
	public static Date obtenerUltimoDiaHaceNIntervaloTiempo(Date fecha, int nn, int intervaloTiempo) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		if (intervaloTiempo == 0) {
			calendario.set(Calendar.DAY_OF_WEEK, calendario.getFirstDayOfWeek());
			calendario.add(Calendar.DAY_OF_MONTH, -nn);
		}else{
			calendario.add(Calendar.MONTH, -nn);
			calendario.set(Calendar.DAY_OF_MONTH, calendario.getActualMaximum(Calendar.DAY_OF_MONTH));
		}
		return calendario.getTime();
	}
	

	public static String obtenerNombreMes(int numeroMes) {
		return meses[numeroMes];
	}
}
