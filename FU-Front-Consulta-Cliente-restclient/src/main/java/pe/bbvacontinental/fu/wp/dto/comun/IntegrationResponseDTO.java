package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IntegrationResponseDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("responseHeader")
	private ResponseHeaderDTO responseHeader;

	@JsonProperty("responseBody")
	private ResponseBodyDTO responseBody;

	public ResponseHeaderDTO getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(ResponseHeaderDTO responseHeader) {
		this.responseHeader = responseHeader;
	}

	public ResponseBodyDTO getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(ResponseBodyDTO responseBody) {
		this.responseBody = responseBody;
	}
}
