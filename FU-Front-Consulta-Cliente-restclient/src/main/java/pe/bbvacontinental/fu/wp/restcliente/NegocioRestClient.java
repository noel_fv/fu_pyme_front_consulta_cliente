package pe.bbvacontinental.fu.wp.restcliente;

import java.util.Map;

import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;

public interface NegocioRestClient {
	public ResponseGeneralDTO consultarDatosNegocio(Map<Object, Object> parametros);
}
