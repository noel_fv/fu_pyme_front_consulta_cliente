package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseGeneralDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("integrationResponse")
	private IntegrationResponseDTO integrationResponse;

	public IntegrationResponseDTO getIntegrationResponse() {
		return integrationResponse;
	}

	public void setIntegrationResponse(IntegrationResponseDTO integrationResponse) {
		this.integrationResponse = integrationResponse;
	}
}
