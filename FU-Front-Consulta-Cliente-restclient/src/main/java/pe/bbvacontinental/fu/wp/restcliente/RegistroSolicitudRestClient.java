package pe.bbvacontinental.fu.wp.restcliente;

import java.util.Map;

import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;

public interface RegistroSolicitudRestClient {

	public ResponseGeneralDTO registrarSolicitudWP(Map<Object, Object> parametros);
}
