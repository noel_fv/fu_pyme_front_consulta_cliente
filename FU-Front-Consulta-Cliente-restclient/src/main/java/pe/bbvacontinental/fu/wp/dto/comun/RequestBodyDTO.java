package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestBodyDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("codigoSolicitudFU")
	private String codigoSolicitudFU;
	
	@JsonProperty("codigoTipoFlujo")
	private String codTipoFlujo;
	
	@JsonProperty("codigoTipoCampania")
	private String idTipoCampania;
	
	@JsonProperty("codigoTipoOperacion")
	private String idTipoOperacion;
	
	@JsonProperty("codigoCentral")
	private String codigoCentral;
	
	@JsonProperty("codigoTipoRegimen")
	private String idRegimenTributario;
	
	@JsonProperty("codigoGiroNegocio")
	private String idGiroNegocio;
	
	@JsonProperty("flagClienteNuevo")
	private String flagClienteNuevo;
		
	@JsonProperty("tipoDocumentoIdentidad")
	private String tipoDocumentoIdentidad;
	
	@JsonProperty("numeroDocumentoIdentidad")
	private String numeroDocumentoIdentidad;
	
	@JsonProperty("numeroDocumento")
	private String numeroDocumento;

	@JsonProperty("productos")
	private List<ProductoDTO> listaProductos;	
	
	@JsonProperty("numeroSolicitudFU")
	private String numeroSolicitudFU;	
	
	@JsonProperty("codigoUsuario")
	private String codigoUsuario;	
	
	@JsonProperty("importeSolicitado")
	private String importeSolicitado;	
	
	@JsonProperty("codigoOficina")
	private String codigoOficina;	
	
	@JsonProperty("ventasAnualesMN")
	private String ventasAnualesMN;	
	
	@JsonProperty("monedaSolicitud")
	private String monedaSolicitud;		
	
	@JsonProperty("codigoError")
	private String codigoError;		
	
	@JsonProperty("tipoError")
	private String tipoError;		
	
	@JsonProperty("detalleError")
	private String detalleError;		
	
	@JsonProperty("codigoTipoPersona")
	private String codigoTipoPersona;
	
	@JsonProperty("tipoPersona")
	private String tipoPersona;	
	
	@JsonProperty("rol")
	private String rol;
	
	@JsonProperty("estado")
	private String estado;
	
	@JsonProperty("comentario")
	private String comentario;
	
	@JsonProperty("codigoAplicativo")
	private String codigoAplicativo;
	
	public String getCodigoCentral() {
		return codigoCentral;
	}

	public void setCodigoCentral(String codigoCentral) {
		this.codigoCentral = codigoCentral;
	}

	public String getTipoDocumentoIdentidad() {
		return tipoDocumentoIdentidad;
	}

	public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
		this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
	}

	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}

	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}

	public String getCodTipoFlujo() {
		return codTipoFlujo;
	}

	public void setCodTipoFlujo(String codTipoFlujo) {
		this.codTipoFlujo = codTipoFlujo;
	}

	public String getFlagClienteNuevo() {
		return flagClienteNuevo;
	}

	public void setFlagClienteNuevo(String flagClienteNuevo) {
		this.flagClienteNuevo = flagClienteNuevo;
	}

	public String getIdGiroNegocio() {
		return idGiroNegocio;
	}

	public void setIdGiroNegocio(String idGiroNegocio) {
		this.idGiroNegocio = idGiroNegocio;
	}

	public String getIdRegimenTributario() {
		return idRegimenTributario;
	}

	public void setIdRegimenTributario(String idRegimenTributario) {
		this.idRegimenTributario = idRegimenTributario;
	}

	public List<ProductoDTO> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProductoDTO> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public String getNumeroSolicitudFU() {
		return numeroSolicitudFU;
	}

	public void setNumeroSolicitudFU(String numeroSolicitudFU) {
		this.numeroSolicitudFU = numeroSolicitudFU;
	}

	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public String getImporteSolicitado() {
		return importeSolicitado;
	}

	public void setImporteSolicitado(String importeSolicitado) {
		this.importeSolicitado = importeSolicitado;
	}

	public String getCodigoOficina() {
		return codigoOficina;
	}

	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}

	public String getVentasAnualesMN() {
		return ventasAnualesMN;
	}

	public void setVentasAnualesMN(String ventasAnualesMN) {
		this.ventasAnualesMN = ventasAnualesMN;
	}

	public String getMonedaSolicitud() {
		return monedaSolicitud;
	}

	public void setMonedaSolicitud(String monedaSolicitud) {
		this.monedaSolicitud = monedaSolicitud;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getTipoError() {
		return tipoError;
	}

	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	public String getDetalleError() {
		return detalleError;
	}

	public void setDetalleError(String detalleError) {
		this.detalleError = detalleError;
	}	
	
	public String getCodigoSolicitudFU() {
		return codigoSolicitudFU;
	}

	public void setCodigoSolicitudFU(String codigoSolicitudFU) {
		this.codigoSolicitudFU = codigoSolicitudFU;
	}

	public String getIdTipoCampania() {
		return idTipoCampania;
	}

	public void setIdTipoCampania(String idTipoCampania) {
		this.idTipoCampania = idTipoCampania;
	}

	public String getIdTipoOperacion() {
		return idTipoOperacion;
	}

	public void setIdTipoOperacion(String idTipoOperacion) {
		this.idTipoOperacion = idTipoOperacion;
	}

	public String getCodigoTipoPersona() {
		return codigoTipoPersona;
	}

	public void setCodigoTipoPersona(String codigoTipoPersona) {
		this.codigoTipoPersona = codigoTipoPersona;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getCodigoAplicativo() {
		return codigoAplicativo;
	}

	public void setCodigoAplicativo(String codigoAplicativo) {
		this.codigoAplicativo = codigoAplicativo;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
}
