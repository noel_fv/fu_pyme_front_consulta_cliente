package pe.bbvacontinental.fu.wp.restcliente.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.restcliente.InvocarPersonaJuridicaRestClient;
import pe.bbvacontinental.fu.wp.util.ParametrosRestClientEnum;

@Service
public class InvocarPersonaJuridicaRestClientImpl implements InvocarPersonaJuridicaRestClient{

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	protected RestTemplate restTemplateLigero;

	@Override
	public ResponseGeneralDTO invocarPantallaPersonaJuridica(
			Map<Object, Object> parametros) {
		
		logger.info("INICIO --- {}","InvocarPersonaJuridicaRestClientImpl.invocarPantallaPersonaJuridica");
		
		RequestGeneralDTO objetoSolicitud = (RequestGeneralDTO) parametros.get(ParametrosRestClientEnum.OBJETO_PEDIDO.getCodigo());
		String urlRegistroSolicitud = (String) parametros.get(ParametrosRestClientEnum.URL.getCodigo());
		
		HttpEntity<RequestGeneralDTO> entidadPedido = new HttpEntity<>(objetoSolicitud);

		ObjectMapper objMapper = new ObjectMapper();
		try {
			String a=objMapper.writeValueAsString(objetoSolicitud);
			logger.info("REQUEST: ---- {}", a);
		} catch (JsonProcessingException e) {
			logger.error("----invocarPantallaPersonaJuridica----",e);
		}
		
		logger.info("SERV urlPersonaJuridica TIME_INI: {}",(System.currentTimeMillis()/1000));
		
		ResponseEntity<ResponseGeneralDTO> entidadRespuesta= restTemplateLigero.exchange(urlRegistroSolicitud, HttpMethod.POST, entidadPedido, ResponseGeneralDTO.class);
		
		logger.info("SERV urlPersonaJuridica TIME_FIN: {}",(System.currentTimeMillis()/1000));
		
		ResponseGeneralDTO responseGeneralDTO = entidadRespuesta.getBody();
		try {
			String aa=objMapper.writeValueAsString(responseGeneralDTO);
			logger.info("RESPONSE:  --- {}", aa);
		} catch (JsonProcessingException e) {
			logger.error("invocarPantallaPersonaJuridica",e);
		}
		logger.info("FIN - {}","InvocarPersonaJuridicaRestClientImpl.invocarPantallaPersonaJuridica");
		return responseGeneralDTO;
	}	
	
	
}
