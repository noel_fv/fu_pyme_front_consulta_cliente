package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseHeaderDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("estadoServicio")
	private String estadoServicio;

	@JsonProperty("codigoServicio")
	private String codigoServicio;

	@JsonProperty("mensajeServicio")
	private String mensajeServicio;

	@JsonProperty("idSesion")
	private String idSesion;

	@JsonProperty("idOperacion")
	private String idOperacion;

	public String getEstado() {
		return estadoServicio;
	}

	public void setEstado(String estado) {
		this.estadoServicio = estado;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public String getMensaje() {
		return mensajeServicio;
	}

	public void setMensaje(String mensaje) {
		this.mensajeServicio = mensaje;
	}

	public String getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}

	public String getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
}