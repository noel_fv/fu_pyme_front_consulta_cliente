package pe.bbvacontinental.fu.wp.restcliente.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.restcliente.NegocioRestClient;
import pe.bbvacontinental.fu.wp.util.ParametrosRestClientEnum;

@Service
public class NegocioRestClientImpl implements NegocioRestClient {
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	protected RestTemplate restTemplateLigero;

	public ResponseGeneralDTO consultarDatosNegocio(Map<Object, Object> parametros)  {
		logger.info("consultarDatosNegocio: {}","inicio");
		ResponseGeneralDTO responseGeneralDTO=null;
		
		try{
			RequestGeneralDTO objetoSolicitud = (RequestGeneralDTO) parametros.get(ParametrosRestClientEnum.OBJETO_PEDIDO.getCodigo());
			String urlConsultaDatosNegocio = (String) parametros.get(ParametrosRestClientEnum.URL.getCodigo());
			ObjectMapper mapper = new ObjectMapper();
			mapperRequest(mapper, objetoSolicitud);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			logger.info("CookieSSO: {}",(String)parametros.get("CookieSSO"));
			
			headers.set("Cookie", "PD-S-SESSION-ID="+ (String)parametros.get("CookieSSO"));
			
			HttpEntity<RequestGeneralDTO> entidadPedido = new HttpEntity<>(objetoSolicitud,headers);
			
			logger.info("SERV consultarDatosNegocioWP TIME_INI: {}",(System.currentTimeMillis()/1000));
			
			ResponseEntity<ResponseGeneralDTO> entidadRespuesta= restTemplateLigero.exchange(urlConsultaDatosNegocio, HttpMethod.POST, entidadPedido, ResponseGeneralDTO.class);
			
			logger.info("SERV consultarDatosNegocioWP TIME_FIN: {} ",(System.currentTimeMillis()/1000));
			
		    responseGeneralDTO = entidadRespuesta.getBody();
		    mapperResponse(mapper, responseGeneralDTO);
		
		}catch(Exception e){
			logger.error(" consultarDatosNegocioWP:",e);
			throw e;
		}
	
		logger.info("consultarDatosNegocio: {}","fin");		
		return responseGeneralDTO;

	}
	
	private void mapperRequest(ObjectMapper mapper, RequestGeneralDTO objetoSolicitud) {
		try {
			String a= mapper.writeValueAsString(objetoSolicitud);
			logger.info("REQUEST: {}",a);
		} catch (JsonProcessingException e) {
			logger.error("FU Error consultarDatosNegocio : ",e);
		}
	}
	
	private void mapperResponse(ObjectMapper mapper, ResponseGeneralDTO responseGeneralDTO) {
		try {
			String aa=mapper.writeValueAsString(responseGeneralDTO);
	    	logger.info("RESPUESTA: {}",aa);
		} catch (JsonProcessingException e) {
			logger.error("FU Error.consultarDatosNegocio : ",e);
		}
	}
	
	@SuppressWarnings("unused")
	private ResponseGeneralDTO obtenerDatosNegocioMOCK(){
		
		
		return new ResponseGeneralDTO();
	}
}

