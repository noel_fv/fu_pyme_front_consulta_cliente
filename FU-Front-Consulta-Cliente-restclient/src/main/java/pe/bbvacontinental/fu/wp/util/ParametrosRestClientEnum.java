package pe.bbvacontinental.fu.wp.util;

public enum ParametrosRestClientEnum {
	OBJETO_PEDIDO("OBJETO_PEDIDO"),
	URL("URL"),
	OBJETO_HEADER("OBJETO_HEADER");

	private final String codigo;

	private ParametrosRestClientEnum(final String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
}
