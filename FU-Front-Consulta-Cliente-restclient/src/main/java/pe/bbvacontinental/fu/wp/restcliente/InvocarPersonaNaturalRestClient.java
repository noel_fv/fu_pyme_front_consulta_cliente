package pe.bbvacontinental.fu.wp.restcliente;

import java.util.Map;

import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;

public interface InvocarPersonaNaturalRestClient {
	
	public ResponseGeneralDTO invocarPantallaPersonaNatural(Map<Object, Object> parametros);

}
