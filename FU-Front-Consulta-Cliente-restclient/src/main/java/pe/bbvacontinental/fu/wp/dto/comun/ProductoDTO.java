package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	@JsonProperty("codigoProducto")
	private String codProducto;
	
	@JsonProperty("nombreProducto")
	private String descProducto;
	
	@JsonProperty("subproductos")
	private List<SubProductoDTO> listaSubproductos;

	public String getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

	public String getDescProducto() {
		return descProducto;
	}

	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}

	public List<SubProductoDTO> getListaSubproductos() {
		return listaSubproductos;
	}

	public void setListaSubproductos(List<SubProductoDTO> listaSubproductos) {
		this.listaSubproductos = listaSubproductos;
	}
}
