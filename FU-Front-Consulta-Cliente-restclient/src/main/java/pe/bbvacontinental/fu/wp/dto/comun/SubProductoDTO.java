package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubProductoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("codigoSubProducto")
	private String codSubProducto;
	
	@JsonProperty("nombreSubProducto")
	private String descSubProducto;
	
	@JsonProperty("flagLinea")
	private String flagLinea;
	
	@JsonProperty("importeSolicitado")
	private String importeSolicitado;
	
	@JsonProperty("codigoTipoMoneda")
	private String codTipoMoneda;
	
	@JsonProperty("codigoCampania")
	private String codCampania;
	
	@JsonProperty("nombreCampania")
	private String descCampania;
	
	@JsonProperty("tasaAnual")
	private String tasaAnual;
	
	@JsonProperty("plazoReembolso")
	private String plazoReembolso;
	
	@JsonProperty("plazoVencimiento")
	private String plazoVencimiento;
	
	@JsonProperty("fechaVencimientoLinea")
	private String fechaVencimientoLinea;
	

	public String getCodSubProducto() {
		return codSubProducto;
	}

	public void setCodSubProducto(String codSubProducto) {
		this.codSubProducto = codSubProducto;
	}

	public String getDescSubProducto() {
		return descSubProducto;
	}

	public void setDescSubProducto(String descSubProducto) {
		this.descSubProducto = descSubProducto;
	}

	public String getCodTipoMoneda() {
		return codTipoMoneda;
	}

	public void setCodTipoMoneda(String codTipoMoneda) {
		this.codTipoMoneda = codTipoMoneda;
	}

	public String getCodCampania() {
		return codCampania;
	}

	public void setCodCampania(String codCampania) {
		this.codCampania = codCampania;
	}

	public String getDescCampania() {
		return descCampania;
	}

	public void setDescCampania(String descCampania) {
		this.descCampania = descCampania;
	}

	public String getImporteSolicitado() {
		return importeSolicitado;
	}

	public void setImporteSolicitado(String importeSolicitado) {
		this.importeSolicitado = importeSolicitado;
	}

	public String getTasaAnual() {
		return tasaAnual;
	}

	public void setTasaAnual(String tasaAnual) {
		this.tasaAnual = tasaAnual;
	}

	public String getFlagLinea() {
		return flagLinea;
	}

	public void setFlagLinea(String flagLinea) {
		this.flagLinea = flagLinea;
	}

	public String getPlazoReembolso() {
		return plazoReembolso;
	}

	public void setPlazoReembolso(String plazoReembolso) {
		this.plazoReembolso = plazoReembolso;
	}

	public String getPlazoVencimiento() {
		return plazoVencimiento;
	}

	public void setPlazoVencimiento(String plazoVencimiento) {
		this.plazoVencimiento = plazoVencimiento;
	}

	public String getFechaVencimientoLinea() {
		return fechaVencimientoLinea;
	}

	public void setFechaVencimientoLinea(String fechaVencimientoLinea) {
		this.fechaVencimientoLinea = fechaVencimientoLinea;
	}
}
