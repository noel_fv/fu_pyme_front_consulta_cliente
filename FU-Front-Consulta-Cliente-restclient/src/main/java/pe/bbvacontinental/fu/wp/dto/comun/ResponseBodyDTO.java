package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseBodyDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("ventasAnualesMN")
	private String ventasAnualesMN;

	@JsonProperty("deudaTotalMN")
	private String deudaTotalMN;

	@JsonProperty("rating")
	private String rating;

	@JsonProperty("antiguedadEmpresa")
	private String antiguedadEmpresa;

	@JsonProperty("codClasificacionBanco")
	private String codClasificacionBanco;

	@JsonProperty("codClasificacionSSFF")
	private String codClasificacionSSFF;
	
	@JsonProperty("codigoSolicitudWP")
	private String codigoSolicitudWP;

	@JsonProperty("numeroSolicitudWP")
	private String numeroSolicitudWP;
	
	@JsonProperty("codigoSolicitudFU")
	private String codigoSolicitudFU;

	@JsonProperty("tieneDelegacion")
	private String tieneDelegacion;	

	@JsonProperty("urlPersonaNatural")
	private String urlPersonaNatural;		
	
	@JsonProperty("urlPersonaJuridca")
	private String urlPersonaJuridca;	
	
	public String getVentasAnualesMN() {
		return ventasAnualesMN;
	}

	public void setVentasAnualesMN(String ventsAnualesMN) {
		this.ventasAnualesMN = ventsAnualesMN;
	}

	public String getDeudaTotalMN() {
		return deudaTotalMN;
	}

	public void setDeudaTotalMN(String deudTotalMN) {
		this.deudaTotalMN = deudTotalMN;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getAntiguedadEmpresa() {
		return antiguedadEmpresa;
	}

	public void setAntiguedadEmpresa(String antiguedadEmpresa) {
		this.antiguedadEmpresa = antiguedadEmpresa;
	}

	public String getCodClasificacionBanco() {
		return codClasificacionBanco;
	}

	public void setCodClasificacionBanco(String codClasificacionBanco) {
		this.codClasificacionBanco = codClasificacionBanco;
	}

	public String getCodClasificacionSSFF() {
		return codClasificacionSSFF;
	}

	public void setCodClasificacionSSFF(String codClasificacionSSFF) {
		this.codClasificacionSSFF = codClasificacionSSFF;
	}

	public String getCodigoSolicitudWP() {
		return codigoSolicitudWP;
	}

	public void setCodigoSolicitudWP(String codigoSolicitudWP) {
		this.codigoSolicitudWP = codigoSolicitudWP;
	}

	public String getCodigoSolicitudFU() {
		return codigoSolicitudFU;
	}

	public void setCodigoSolicitudFU(String codigoSolicitudFU) {
		this.codigoSolicitudFU = codigoSolicitudFU;
	}

	public String getNumeroSolicitudWP() {
		return numeroSolicitudWP;
	}

	public void setNumeroSolicitudWP(String numeroSolicitudWP) {
		this.numeroSolicitudWP = numeroSolicitudWP;
	}

	public String getTieneDelegacion() {
		return tieneDelegacion;
	}

	public void setTieneDelegacion(String tieneDelegacion) {
		this.tieneDelegacion = tieneDelegacion;
	}

	public String getUrlPersonaNatural() {
		return urlPersonaNatural;
	}

	public void setUrlPersonaNatural(String urlPersonaNatural) {
		this.urlPersonaNatural = urlPersonaNatural;
	}

	public String getUrlPersonaJuridca() {
		return urlPersonaJuridca;
	}

	public void setUrlPersonaJuridca(String urlPersonaJuridca) {
		this.urlPersonaJuridca = urlPersonaJuridca;
	}
}
