package pe.bbvacontinental.fu.wp.restcliente.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.bbvacontinental.fu.wp.dto.comun.RequestGeneralDTO;
import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;
import pe.bbvacontinental.fu.wp.restcliente.InvocarPersonaNaturalRestClient;
import pe.bbvacontinental.fu.wp.util.ParametrosRestClientEnum;

@Service
public class InvocarPersonaNaturalRestClientImpl implements InvocarPersonaNaturalRestClient {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
		
	@Autowired
	protected RestTemplate restTemplateLigero;	
	
	@SuppressWarnings("unchecked")
	@Override
	public ResponseGeneralDTO invocarPantallaPersonaNatural(
			Map<Object, Object> parametros)  {
		
		logger.info("INI - {}","InvocarPersonaNaturalRestClientImpl.invocarPantallaPersonaNatural");
		
		RequestGeneralDTO objetoSolicitud = (RequestGeneralDTO) parametros.get(ParametrosRestClientEnum.OBJETO_PEDIDO.getCodigo());
		String urlRegistroSolicitud = (String) parametros.get(ParametrosRestClientEnum.URL.getCodigo());
		List<String> header =  (List<String>) parametros.get(ParametrosRestClientEnum.OBJETO_HEADER.getCodigo());
		
		logger.info(" invocarPantallaPersonaNatural header.toString(): {}",header);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		if(headers.size() > 3){
			headers.set("iv-user", header.get(0));
			headers.set("iv-creds", header.get(1));
		}
		
		if(header.get(2) != null && !header.get(2).isEmpty()){
			logger.info("Agregando cabecera Cookie con valor: {}",header.get(2));
			headers.set("Cookie", header.get(2));
		}
		
		HttpEntity<RequestGeneralDTO> entidadPedido = new HttpEntity<>(objetoSolicitud, headers);
		

		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String a=mapper.writeValueAsString(objetoSolicitud);
			logger.info("REQUEST: {}", a);
		} catch (JsonProcessingException e) {
			logger.error("invocarPantallaPersonaNatural",e);
		}
		
		ResponseGeneralDTO responseGeneralDTO = restTemplateLigero.postForObject(urlRegistroSolicitud, entidadPedido, ResponseGeneralDTO.class);
		//ResponseEntity<ResponseGeneralDTO> entidadRespuesta= restTemplateLigero.exchange(urlRegistroSolicitud, HttpMethod.POST, entidadPedido, ResponseGeneralDTO.class)
		//ResponseGeneralDTO responseGeneralDTO = entidadRespuesta.getBody()
		try {
			String aa= mapper.writeValueAsString(responseGeneralDTO);
			logger.info("RESPONSE: {}",aa);
		} catch (JsonProcessingException e) {
			logger.error("invocarPantallaPersonaNatural",e);
		}
		logger.info("FIN - {}","InvocarPersonaNaturalRestClientImpl.invocarPantallaPersonaNatural");
		return responseGeneralDTO;
	}

}
