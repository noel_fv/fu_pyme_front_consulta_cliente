package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonProperty;

public class IntegrationRequestDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("requestHeader")
	private RequestHeaderDTO requestHeader;

	@JsonProperty("requestBody")
	private RequestBodyDTO requestBody;

	public RequestHeaderDTO getRequestHeader() {
		return requestHeader;
	}

	public void setRequestHeader(RequestHeaderDTO requestHeader) {
		this.requestHeader = requestHeader;
	}

	public RequestBodyDTO getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(RequestBodyDTO requestBody) {
		this.requestBody = requestBody;
	}
}
