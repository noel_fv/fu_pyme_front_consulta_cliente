package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestGeneralDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("integrationRequest")
	private IntegrationRequestDTO integrationRequest;

	public IntegrationRequestDTO getIntegrationRequest() {
		return integrationRequest;
	}

	public void setIntegrationRequest(IntegrationRequestDTO integrationRequest) {
		this.integrationRequest = integrationRequest;
	}
}
