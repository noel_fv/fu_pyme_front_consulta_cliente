package pe.bbvacontinental.fu.wp.restcliente;

import java.util.Map;

import pe.bbvacontinental.fu.wp.dto.comun.ResponseGeneralDTO;

public interface InvocarPersonaJuridicaRestClient {

	public ResponseGeneralDTO invocarPantallaPersonaJuridica(Map<Object, Object> parametros);
}
