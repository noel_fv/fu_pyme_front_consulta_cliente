package pe.bbvacontinental.fu.wp.dto.comun;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestHeaderDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("usuario")
	private String usuario;

	@JsonProperty("fechaHoraEnvio")
	private String fechaHoraEnvio;

	@JsonProperty("idSesion")
	private String idSesion;

	@JsonProperty("idOperacion")
	private String idOperacion;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaHoraEnvio() {
		return fechaHoraEnvio;
	}

	public void setFechaHoraEnvio(String fechaHoraEnvio) {
		this.fechaHoraEnvio = fechaHoraEnvio;
	}

	public String getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}

	public String getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
}
