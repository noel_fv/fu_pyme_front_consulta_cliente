package pe.bbvacontinental.fu.wp.model;

import java.io.Serializable;

import pe.bbvacontinental.fu.model.view.GenericModel;

public class ConsultaDatosNegocioModel extends GenericModel implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String ventasAnualesMN;
	
	private String deudaTotalMN;
	
	private String rating;
	
	private String antiguedadEmpresa;
	
	private String codClasificacionBanco;
	
	private String codClasificacionSSFF;

	public String getVentasAnualesMN() {
		return ventasAnualesMN;
	}

	public void setVentasAnualesMN(String ventasAnualesMN) {
		this.ventasAnualesMN = ventasAnualesMN;
	}

	public String getDeudaTotalMN() {
		return deudaTotalMN;
	}

	public void setDeudaTotalMN(String deudaTotalMN) {
		this.deudaTotalMN = deudaTotalMN;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getAntiguedadEmpresa() {
		return antiguedadEmpresa;
	}

	public void setAntiguedadEmpresa(String antiguedadEmpre) {
		this.antiguedadEmpresa = antiguedadEmpre;
	}

	public String getCodClasificacionBanco() {
		return codClasificacionBanco;
	}

	public void setCodClasificacionBanco(String codClasificacionBanc) {
		this.codClasificacionBanco = codClasificacionBanc;
	}

	public String getCodClasificacionSSFF() {
		return codClasificacionSSFF;
	}

	public void setCodClasificacionSSFF(String codClasificacionSSFF) {
		this.codClasificacionSSFF = codClasificacionSSFF;
	}
}
