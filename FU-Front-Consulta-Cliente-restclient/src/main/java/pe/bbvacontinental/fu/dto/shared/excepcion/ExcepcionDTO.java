package pe.bbvacontinental.fu.dto.shared.excepcion;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExcepcionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("version")
	private String version;
	
	@JsonProperty("severity")
	private String severidad;
	
	@JsonProperty("http-status")
	private String estatusHTTP;

	@JsonProperty("error-code")
	private String codigoError;

	@JsonProperty("error-message")
	private String mensajeError;

	@JsonProperty("system-error-code")
	private String codigoDelErrorDeSistema;

	@JsonProperty("system-error-description")
	private String descripcionDelErrorDeSistema;

	@JsonProperty("system-error-cause")
	private String causaDelErrorDeSistema;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSeveridad() {
		return severidad;
	}

	public void setSeveridad(String severidad) {
		this.severidad = severidad;
	}

	public String getEstatusHTTP() {
		return estatusHTTP;
	}

	public void setEstatusHTTP(String estatusHTTP) {
		this.estatusHTTP = estatusHTTP;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getCodigoDelErrorDeSistema() {
		return codigoDelErrorDeSistema;
	}

	public void setCodigoDelErrorDeSistema(String codigoDelErrorDeSistema) {
		this.codigoDelErrorDeSistema = codigoDelErrorDeSistema;
	}

	public String getDescripcionDelErrorDeSistema() {
		return descripcionDelErrorDeSistema;
	}

	public void setDescripcionDelErrorDeSistema(String descripcionDelErrorDeSistema) {
		this.descripcionDelErrorDeSistema = descripcionDelErrorDeSistema;
	}

	public String getCausaDelErrorDeSistema() {
		return causaDelErrorDeSistema;
	}

	public void setCausaDelErrorDeSistema(String causaDelErrorDeSistema) {
		this.causaDelErrorDeSistema = causaDelErrorDeSistema;
	}

}
